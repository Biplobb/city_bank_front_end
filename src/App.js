import React, { Component } from 'react';
import './Static/css/App.css';
import {  Route, Switch, HashRouter, BrowserRouter } from "react-router-dom";

import ProtectedRoute from "./ProtectedRoute";
import Login from "./Components/Login";

class App extends Component {
    render() {
        return (


            <div className="App">

                <HashRouter>
                    <Switch>

                        <Route path="/login" component={Login} />

                        <Route path="/" component={ProtectedRoute} />

                    </Switch>



                </HashRouter>


            </div>


        );
    }
}

export default App;
