import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import {Link} from 'react-router-dom';


const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        mode: "inline",
        marginTop: 40,
        //only menu content color


    },

    nested: {
        paddingLeft: theme.spacing.unit * 5,

        //sub menu back ground color
        color: "#FF6347",
        '&:hover': {
            color: "#FFFFFF",
            background: "#FF4500"

        }
    },
    menuLink: {
        color: "#FF6347",

        '&:hover': {
            color: "#FFFFFF",
            background: "#FF4500"
        }
    }
});

class DashboardMenu extends React.Component {
    state = {
        open: false,
        open1: false,
        openHome: false,
        openAdmin: false,
        openCustomer: false,
        openMaster: false,
        openLiability: false,
        openAsset: false,

    };


    handleClickAdmin = () => {

        this.setState(state => ({

            openAdmin: !state.openAdmin,
        }));
    };
    handleClickMaster = () => {

        this.setState(state => ({

            openMaster: !state.openMaster,
        }));
    };
    handleClick360 = () => {

        this.setState(state => ({

            openCustomer: !state.openCustomer,
        }));
    };
    handleClickLiability = () => {

        this.setState(state => ({

            openLiability: !state.openLiability,
        }));
    };
    handleClickMaintenance = () => {

        this.setState(state => ({

            openMaintenance: !state.openMaintenance,
        }));
    };
    handleClickAsset = () => {

        this.setState(state => ({

            openAsset: !state.openAsset,
        }));
    };

    showMenu = (menuList, props, includeMenu, handleClick, openMenu, menuName) => {
        const {classes} = props;
        if (menuList.includes(includeMenu)) {
            return (
                <ListItem className={classes.menuLink} button onClick={handleClick}>

                    {openMenu ? <ExpandLess/> : <ExpandMore/>}<ListItemText style={{marginLeft: "-45px"}} inset
                                                                            primary={menuName}/>


                </ListItem>
            );
        }
    };
    /* showSubMenu = (menuList, props, includeMenu, handleClick, openMenu, menuName) => {
         const {classes} = props;
         if (menuList.includes(includeMenu)) {
             return (
                 <ListItem className={classes.menuLink} button onClick={handleClick}>

                     {openMenu ? <ExpandLess/> : <ExpandMore/>}<ListItemText style={{marginLeft: "-45px"}} inset
                                                                             primary={menuName}/>


                 </ListItem>
             );
         }
     };*/
    showCollapse = (menuList, props, includeMenu, openMenu, url, menuName) => {

        const {classes} = props;
        if (menuList.includes(includeMenu)) {
            return (

                <Collapse in={openMenu} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem component={Link} to={url} button className={classes.nested}>

                            {openMenu ? <ExpandLess/> : <ExpandMore/>}<ListItemText style={{marginLeft: "-45px"}} inset
                                                                                    primary={menuName}/>
                        </ListItem>
                    </List>
                </Collapse>
            );
        }
    };
    showCollapseClaim = (menuList, props, includeMenu, openMenu, url, menuName) => {

        const {classes} = props;
        if (menuList.includes(includeMenu)) {
            if (localStorage.getItem("roles").indexOf('SD_ADMIN') !== -1) {

                return (

                    <Collapse in={openMenu} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem component={Link} to="/AdminReassignCase" button className={classes.nested}>

                                {openMenu ? <ExpandLess/> : <ExpandMore/>}<ListItemText style={{marginLeft: "-45px"}}
                                                                                        inset
                                                                                        primary={menuName}/>
                            </ListItem>
                        </List>
                    </Collapse>
                );
            }
            else{
                return (

                    <Collapse in={openMenu} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem component={Link} to="/claimcase" button className={classes.nested}>

                                {openMenu ? <ExpandLess/> : <ExpandMore/>}<ListItemText style={{marginLeft: "-45px"}}
                                                                                        inset
                                                                                        primary={menuName}/>
                            </ListItem>
                        </List>
                    </Collapse>
                );
            }
        }

    };

    showCollapseMenu = (menuList, props) => {

        const {classes} = props;
        if (menuList.includes("Menu")) {
            return (

                <Collapse in={this.state.openAdmin} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem component={Link} to="/MenuListDashboard" button className={classes.nested}>

                            {this.state.openAdmin ? <ExpandLess/> : <ExpandMore/>} <ListItemText
                            style={{marginLeft: "-45px"}} inset primary="Menu"/>
                        </ListItem>
                    </List>
                </Collapse>
            );
        }
    };



    render() {
        const {classes} = this.props;
        const menuList = JSON.parse(localStorage.getItem("menus"));
        return (

            <List
                component="nav"
                subheader={<ListSubheader component="div"></ListSubheader>}
                className={classes.root}
            >

                {
                    this.showMenu(menuList, this.props, "Admin", this.handleClickAdmin, this.state.openAdmin, "Admin")
                }
                {
                    this.showCollapse(menuList, this.props, "User", this.state.openAdmin, "/user", "User")
                }
                {
                    this.showCollapse(menuList, this.props, "Role", this.state.openAdmin, "/RoleListDashboard", "Role")
                }
                {
                    this.showCollapse(menuList, this.props, "Menu", this.state.openAdmin, "/MenuListDashboard", "Menu")
                }

                {
                    this.showMenu(menuList, this.props, "Master", this.handleClickMaster, this.state.openMaster, "Master")
                }
                {
                    this.showCollapse(menuList, this.props, "City", this.state.openMaster, "/city", "City")
                }
                {
                    this.showCollapse(menuList, this.props, "Branch", this.state.openMaster, "/branch", "Branch")
                }
                {
                    this.showCollapse(menuList, this.props, "Branch workflow", this.state.openMaster, "/SdAndBranchFlowConfigureAllData", "Branch workflow")
                }
                {
                    this.showCollapse(menuList, this.props, "Product", this.state.openMaster, "/product", "Product")
                }
                {
                    this.showCollapse(menuList, this.props, "State", this.state.openMaster, "/state", "State")
                }
                {
                    this.showCollapse(menuList, this.props, "Country", this.state.openMaster, "/country", "Country")
                }
                {
                    this.showCollapse(menuList, this.props, "Salutation", this.state.openMaster, "/salutation", "Salutation")
                }

                {
                    this.showMenu(menuList, this.props, "Customer", this.handleClick360, this.state.openCustomer, "Liability")
                }
                {
                    this.showCollapse(menuList, this.props, "Dedup", this.state.openCustomer, "/CustomerDedupSearch", "Dedup")
                }
                {
                    this.showCollapse(menuList, this.props, "Agent", this.state.openCustomer, "/AgentBanking", "Agent")
                }
                {
                    this.showCollapse(menuList, this.props, "Inbox", this.state.openCustomer, "/inbox", "Inbox")
                }
                {
                    this.showCollapseClaim(menuList, this.props, "Dashboard", this.state.openCustomer, "/claimcase", "New Request")
                }
                {
                    this.showCollapse(menuList, this.props, "Offer", this.state.openCustomer, "/fileRead", "Offer")
                }
                {
                    this.showCollapse(menuList, this.props, "Grouping", this.state.openCustomer, "/GroupAndMemberAdd", "Grouping")
                }
                {
                    this.showCollapse(menuList, this.props, "Statistics", this.state.openCustomer, "/Statistics", "Statistics")
                }
                {
                    this.showCollapse(menuList, this.props, "Customer View", this.state.openCustomer, "/Statistics", "Customer View")
                }

                {
                    this.showMenu(menuList, this.props, "Liability", this.handleClickLiability, this.state.openLiability, "Liability")
                }
                {
                    this.showMenu(menuList, this.props, "Account Maintanence", this.handleClickMaintenance, this.state.openLiability, "Account Maintanence")
                }
                {
                    this.showCollapse(menuList, this.props, "With Input Field", this.state.openMaintenance, "/MaintenanceWIthPrintFile", "With Input Field")
                }
                {
                    this.showCollapse(menuList, this.props, "Hard Copy Receive", this.state.openLiability, "/MaintenanceWIthFile", "Hard Copy Receive")
                }
                {
                    this.showCollapse(menuList, this.props, "Others", this.state.openLiability, "/Statistics", "Others")
                }
                {
                    this.showCollapse(menuList, this.props, "Open an account", this.state.openLiability, "/newaccount", "Open an account")
                }
                {
                    this.showMenu(menuList, this.props, "Asset", this.handleClickAsset, this.state.openAsset, "Asset")
                }
                {
                    this.showCollapse(menuList, this.props, "Certificate", this.state.openAsset, "/newaccount", "Certificate")
                }
                {
                    this.showCollapse(menuList, this.props, "AccountMaintenanceCS", this.state.openAsset, "/Statistics", "AccountMaintenanceCS")
                }


            </List>
        );

    }
}

DashboardMenu.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashboardMenu);