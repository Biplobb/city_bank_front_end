import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import DashboardMenu from "./DashboardMenu";
import { backEndServerURL} from '../Common/Constant';
import logo from '../../src/Static/citybanklogo.png';
import Link from "react-router-dom/es/Link";
import cookie from "react-cookies";
import axios from "axios";
import profile from "../Static/profile.jpg";
import Functions from "../Common/Functions";
import {Redirect} from "react-router-dom";


const drawerWidth = 240;


const styles = theme => ({
    root: {
        display: 'flex',

    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
        //header line background Color
        backgroundColor: "#ED0000"
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        //tool ber back ground color
        backgroundColor: "#ffffff"
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
            backgroundColor: "#d82530"

        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,

        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
        backgroundColor: "#ED0000"
    },
    menuButtonHidden: {
        display: 'none',
        backgroundColor: "#ED0000"
    },
    title: {
        flexGrow: 5,
        display: 'block',
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        //sidebar back ground color
        backgroundColor: "#ffffff",
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
            backgroundColor: "#ffffff"
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
            backgroundColor: "#ff3443"
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,

        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
        //content color
        backgroundColor: "#f0f0fe"

    },
    tableContainer: {
        height: 320,


    },
    h5: {
        marginBottom: theme.spacing.unit * 2,

    },
    logout: {
        backgroundColor: "#f0f0fe",
        height: 50,


    },
});

class DashBoardHeader extends React.Component {
    state = {
        open: true,
        redirectLogin:false,

    };

    handleDrawerOpen = () => {
        this.setState({open: true});
    };

    handleDrawerClose = () => {
        this.setState({open: false});
    };
    userLogout = () => {

        let url = backEndServerURL + "/logout";

        axios.get(url, {
            withCredentials: true,
        });

        // cookie.remove("spring_session");
        // localStorage.removeItem("username");
        // localStorage.removeItem("roles");
        // localStorage.removeItem("menus");

        Functions.removeCookie();

        this.setState({
            redirectLogin:true
        })
    };


    render() {
        const {classes} = this.props;
        if(!(cookie.load("spring_session"))){
            return (<Redirect to={{
                pathname: '/login',
            }}/>);
        }
        else {
            return (
                <div>
                    {
                        //this.renderRedirect()
                        //redirectToLogin(this.state)
                        Functions.redirectToLogin(this.state)

                    }

                </div>,
                    <div className={classes.root}>
                        <CssBaseline/>

                        <AppBar
                            position="absolute"
                            className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
                        >
                            <Toolbar disableGutters={!this.state.open} className={classes.toolbar}>
                                <IconButton
                                    color="inherit"
                                    aria-label="Open drawer"
                                    onClick={this.handleDrawerOpen}
                                    className={classNames(
                                        classes.menuButton,
                                        this.state.open && classes.menuButtonHidden,
                                    )}
                                >
                                    <MenuIcon/>
                                </IconButton>
                                <IconButton color="inherit">
                                    <Badge color="inherit">


                                        <img height={50} src={logo}/>

                                    </Badge>
                                </IconButton>
                                <Typography
                                    component="h1"
                                    variant="h6"
                                    color="inherit"
                                    noWrap
                                    className={classes.title}
                                >

                                    360 Dashboard

                                </Typography>
                                <img style={{"border-radius": '50%'}} height={40} src={profile}/>&nbsp;&nbsp;
                                <h4><Link style={{backgroundColor: "#ED0000", color: "#FFFFFF"}}
                                          onClick={this.userLogout} to='/login'>Logout</Link></h4>

                            </Toolbar>

                        </AppBar>

                        <Drawer
                            variant="permanent"
                            classes={{
                                paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
                            }}
                            open={this.state.open}
                        >
                            <div className={classes.toolbarIcon}>
                                <IconButton onClick={this.handleDrawerClose}>
                                    <ChevronLeftIcon/>
                                </IconButton>
                            </div>
                            <Divider/>
                            <List>
                                <DashboardMenu permission={this.props.permission}/>
                            </List>
                            <Divider/>

                        </Drawer>


                    </div>

            );
        }
    }
}

DashBoardHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashBoardHeader);
