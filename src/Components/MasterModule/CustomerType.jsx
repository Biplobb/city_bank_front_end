import Table from "../Table/Table.jsx";
import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import CustomerTypeAddEditDelete from "./CustomerTypeAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "typeName",
            "type": "text",
            "label": "Type Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class CustomerType extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerType: false,
            customerType: '',
            editTypeMaster:false,
            tableData: [[" ", " "]],
            open: true,
            typeData:[[" "," "]],
            redirectLogin:false,
        }
    }
    createTableData = (id,typeName,createdBy,modifiedBy,createdDate,modifiedDate,status) => {

        return ([typeName,createdBy,modifiedBy,createdDate,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editTypeMaster(id)}>
                Edit</button>
        ]);

    }
    editTypeMaster = (id) => {
        this.setState({
            editTypeMaster: true,
            typeMasterId: id
        })
    }

    componentDidMount() {
        let url = backEndServerURL + "/typeMaster/getAll";

        const getAllType = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((type) => {

                    getAllType.push(type);
                });
                this.setState({
                    tableData: getAllType
                })


            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    addCustomerType() {
        this.setState({
            addCustomerType: true,
        });
    }
    cancelCustomerTypeModal = () => {
        this.setState({ addCustomerType: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCustomerType: false,
            editTypeMaster: false,

        });
    }
    onAdd=(object)=>{
        this.setState({
            addCustomerType: false
        });
        notification.open({
            message: 'Successfully Added'
        });


    }
    onUpdate=(object)=>{

        this.setState({
            editTypeMaster: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((typeMaster) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== typeMaster[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(typeMaster.id,typeMaster.typeName,typeMaster.createdBy,typeMaster.modifiedBy,typeMaster.createdDate,typeMaster.modifiedDate,typeMaster.status));


        })
        this.setState({
            typeData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerType}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        <CustomerTypeAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal} secondButtonName="close" name="Add New Type"/>
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editTypeMaster}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        <CustomerTypeAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal} secondButtonName="close" name="Edit Type" id={this.state.typeMasterId}/>
                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Type List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerType: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Type
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Type Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.typeData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerType);
