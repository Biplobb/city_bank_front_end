import Table from "../Table/Table.jsx";
import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import StateAddEditDelete from "./StateAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';

import FormSample from "../JsonForm/FormSample";

import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [
        {
            "varName": "stateName",
            "type": "text",
            "label": "State Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class State extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addState: false,
            stateName: '',
            tableData: [[" ", " "]],
            editState:false,
            open: true,
            stateData:[[" "," "]],
            redirectLogin:false,
        }
    }

    componentDidMount() {
        let url = backEndServerURL + "/stateMaster/getAll";

        const getAllState = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {
                response.data.map((state) => {

                    getAllState.push(state);
                });
                this.setState({
                    tableData: getAllState
                })


            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }

            });

    }
    createTableData = (id,countryName,stateName,createdBy,createdDate,modifiedBy,modifiedDate,status) => {

        return ([countryName,stateName,createdBy,createdDate,modifiedBy,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editState(id)}>
                Edit</button>
        ]);

    }
    editState = (id) => {
        this.setState({
            editState: true,
            stateId: id
        })
    }
    addState() {
        this.setState({
            addState: true,
        });
    }
    cancelStateModal = () => {
        this.setState({ addState: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addState: false,
            editState: false,

        });
    }
    onAdd=(object)=>{
        /* console.log(object);
     this.state.tableData.push([object.subSectorCode,object.subSectorName,"","","","",object.status,

         <button
             className="btn btn-outline-danger"
             style={{
                 verticalAlign: 'middle',
             }}
             onClick={() => this.editSubSector()}>
             Edit</button>
     ]);*/
        this.setState({
            addState: false


        });
        notification.open({
            message: 'Successfully Added'
        });

        //window.location.reload();

    }
    onUpdate=(object)=>{

        this.setState({
            editState: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    stateAddModal=()=>{
        return(
            <StateAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal} secondButtonName="close" close={this.closeModal} name="Add New State"/>
        )
    }
    getSubmitedForm=(object)=>{

        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((state) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== state[variable])
                    showable = false;
            }
            if (showable)

                tableArray.push(this.createTableData(state.id,state.countryMaster.countryName,state.stateName,state.createdBy,state.createdDate,state.modifiedBy,state.modifiedDate,state.status));


        })
        this.setState({
            stateData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addState}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.stateAddModal()}

                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="sm"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editState}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        <StateAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal} secondButtonName="close" name="Edit State" id={this.state.stateId}/>

                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>State List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addState: true
                                            })
                                        }
                                        }
                                    >
                                        Add New State
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Country Name","State Name", "Created by", "Created Date", "Modified by", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.stateData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(State);
