import Table from "../Table/Table.jsx";
import React, {Component} from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import {Dialog} from "@material-ui/core";
import {Grow} from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import ProductAddEditDelete from "./ProductAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "productName",
            "type": "text",
            "label": "Product Name",


        },
        {
            "varName": "productCode",
            "type": "text",
            "label": "Product Code",


        },

        {
            "varName": "description",
            "type": "text",
            "label": "Description",


        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }


    ],

};

function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addProduct: false,
            editProduct: false,
            tableData: [[" ", " "]],
            productId: "",
            poductData: [[" ", " "]],
            open: true,
            redirectLogin:false,
        }
    }

    createTableData = (id,productName, productCode , description, productType, effectiveFrom, validTo, createdBy, modifiedBy, createdDate, modifiedDate, status) => {

        return ([productName, productCode , description, productType, effectiveFrom, validTo, createdBy, modifiedBy, status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editProduct(id)}>
                Edit</button>
        ]);

    };

    editProduct = (id) => {

        this.setState({
            editProduct: true,
            productId: id
        })
    }

    componentDidMount() {
        let url = backEndServerURL + "/productMaster/getAll";


        let getAllProduct = [];
        axios.get(url, {withCredentials: true})
            .then((response) => {
                response.data.map((product) => {

                    getAllProduct.push(product);
                });
                this.setState({
                    tableData: getAllProduct
                })


            })
            .catch((error) => {
                console.log(error);
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }

    addProduct() {
        this.setState({
            addProduct: true,
        });
    }

    editProduct() {
        this.setState({
            editProduct: true,
        });
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {

        this.setState({
            addProduct: false,
            editProduct: false,

        });

    }
    productAddModal = () => {

        return (
            <ProductAddEditDelete secondButtonFunction={this.closeModal} close={this.closeModal} secondButtonName="close"
                                  name="Add New product"/>
        )
    }
    productEditModal = () => {
        return (
            <ProductAddEditDelete secondButtonFunction={this.closeModal}  close={this.closeModal}
                                  secondButtonName="close" name="Edit product" id={this.state.productId}/>
        )
    }
    getSubmitedForm = (object) => {
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((product) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== product[variable])
                    showable = false;
            }
            if (showable)
            tableArray.push(this.createTableData(product.id, product.productName, product.productCode, product.description, product.productType, product.effectiveFrom, product.validTo, product.createdBy, product.modifiedBy, product.createdDate, product.modifiedDate, product.status));


        })
        this.setState({
            poductData: tableArray
        })
        console.log(this.state.poductData)
    }
    renderFilterForm = () => {
        return (
            <FormSample   close={this.closeModal} grid="4" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm}/>
        )
    }

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="xl"
                    className={classes.modal}
                    classes={{paper: classes.dialogPaper}}
                    open={this.state.addProduct}

                    TransitionComponent={Transition}

                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.productAddModal()}

                    </DialogContent>


                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{paper: classes.dialogPaper}}
                    open={this.state.editProduct}

                    TransitionComponent={Transition}

                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.productEditModal()}

                    </DialogContent>


                </Dialog>

                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Product List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addProduct: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Product
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Product Name", "Product Code", "Description", "Product Type", "Effective Date", "Valid To", "Created by", "Modified by", "Status", "Action"]}
                                        tableData={this.state.poductData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(Product);
