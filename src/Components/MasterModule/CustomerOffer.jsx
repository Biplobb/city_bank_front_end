import Table from "../Table/Table.jsx";
import React, {Component} from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import {Dialog} from "@material-ui/core";
import {Grow} from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import CustomerOfferAddEditDelete from "./CustomerOfferAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import {notification} from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "customerOffer",
            "type": "text",
            "label": "Customer Offer Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }


    ],

};

function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}

class CustomerOffer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerOffer: false,
            editCustomerOffer: false,
            tableData: [[" ", " "]],
            customerOfferId: "",
            open: true,
            offerData: [[" ", " "]],
            redirectLogin:false,
        }
    }

    createTableData = (id, customerId, productCode) => {

        return ([customerId, productCode,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCustomerOffer(id)}>
                Edit</button>
        ]);

    }
    editCustomerOffer = (id) => {

        this.setState({
            editCustomerOffer: true,
            customerOfferId: id
        })
    }


    // componentDidMount() {
    //     let url = backEndServerURL + "/customerOfferMaster/getAll";
    //
    //     const tableData = [];
    //
    //     axios.get(url, { withCredentials: true })
    //         .then((response) => {
    //
    //             this.setState({ roles: response.data });
    //             response.data.map((customerOffer) => {
    //                 //console.log();
    //                 tableData.push(this.createTableData(customerOffer.id,customerOffer.customerOfferCode,customerOffer.customerOfferName,customerOffer.effectiveFrom,customerOffer.validTo,customerOffer.createdBy,customerOffer.modifiedBy,customerOffer.createdDate,customerOffer.modifiedDate,customerOffer.status));
    //
    //             });
    //
    //             this.setState({
    //                 tableData: tableData
    //             });
    //
    //
    //         })
    //         .catch((error) => { console.log(error) });
    //
    // }

    componentDidMount() {
        let url = backEndServerURL + "/customerOffer/getAll";

        const getAllCustomerOffer = [];

        axios.get(url, {withCredentials: true})
            .then((response) => {


                response.data.map((customerOffer) => {

                    getAllCustomerOffer.push(customerOffer);
                });
                this.setState({
                    tableData: getAllCustomerOffer
                })


            })
            .catch((error) => {
                console.log(error);
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }

            });

    }


    addCustomerOffer() {
        this.setState({
            addCustomerOffer: true,
        });
    }

    editCustomerOffer() {
        this.setState({
            editCustomerOffer: true,
        });
    }

    cancelCustomerOfferModal = () => {
        this.setState({addCustomerOffer: false});
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {

        this.setState({
            addCustomerOffer: false,
            editCustomerOffer: false,

        });

    }
    customerOfferAddModal = () => {

        return (
            <CustomerOfferAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal}
                                        secondButtonName="close" name="Add New Customer Offer"/>
        )
    }
    customerOfferEditModal = () => {
        return (
            <CustomerOfferAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal}
                                        secondButtonName="close" name="Edit Customer Offer"
                                        id={this.state.customerOfferId}/>
        )
    }
    onAdd = (object) => {

        this.setState({
            addCustomerOffer: false

        });
        notification.open({
            message: 'Successfully Added'
        });


    }
    onUpdate = (object) => {

        this.setState({
            editCustomerOffer: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm = (object) => {
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for (let variable in object) {
            let trimData = object[variable].trim();
            if (trimData !== '')
                objectTable[variable] = trimData;
        }

        this.state.tableData.map((customerOffer) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== customerOffer[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(customerOffer.id, customerOffer.customerId, customerOffer.productCode));


        })
        this.setState({
            offerData: tableArray
        })

    }
    renderFilterForm = () => {
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm}/>
        )
    }

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerOffer}

                    TransitionComponent={Transition}
                    style={{width: 700}}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerOfferAddModal()}
                    </DialogContent>


                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCustomerOffer}

                    TransitionComponent={Transition}
                    style={{width: 700}}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerOfferEditModal()}
                    </DialogContent>


                </Dialog>

                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Customer Offer List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerOffer: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Customer Offer
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"

                                        tableHead={["Customer Id", "Product Code", "Action"]}
                                        tableData={this.state.offerData}
                                        tableAllign={['left', 'left', 'left']}

                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerOffer);
