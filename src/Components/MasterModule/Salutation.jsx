import Table from "../Table/Table.jsx";
import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import SalutationAddEditDelete from "./SalutationAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "salutationName",
            "type": "text",
            "label": "Salutation Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class Salutation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addSalutationName: false,
            salutationName: '',
            editSalutation:false,
            salutationId:"",
            tableData: [[" ", " "]],
            open: true,
            salutationData:[[" "," "]],
            redirectLogin:false,
        }
    }

    createTableData = (id,salutationName,createdBy,modifiedBy,createdDate,modifiedDate,status) => {

        return ([salutationName,createdBy,modifiedBy,createdDate,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editSalutation(id)}>
                Edit</button>
        ]);

    }
    editSalutation = (id) => {
        this.setState({
            editSalutation: true,
            salutationId: id
        })
    }

    componentDidMount() {
        let url = backEndServerURL + "/salutationMaster/getAll";

        const getAllSalutation = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((salutation) => {

                    getAllSalutation.push(salutation);
                });
                this.setState({
                    tableData: getAllSalutation
                })

            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    addSalutation() {
        this.setState({
            addSalutation: true,
        });
    }
    cancelSalutationModal = () => {
        this.setState({ addSalutation: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addSalutation: false,
            editSalutation: false,

        });
    }
    onAdd=(object)=>{
        this.setState({
            addSalutation: false
        });
        notification.open({
            message: 'Successfully Added'
        });

    }
    onUpdate=(object)=>{

        this.setState({
            editSalutation: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((salutation) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== salutation[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(salutation.id,salutation.salutationName,salutation.createdBy,salutation.modifiedBy,salutation.createdDate,salutation.modifiedDate,salutation.status));


        })
        this.setState({
            salutationData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addSalutation}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        <SalutationAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal} secondButtonName="close" name="Add New salutation"/>

                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editSalutation}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        <SalutationAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal} secondButtonName="close" name="Edit salutation" id={this.state.salutationId}/>

                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Salutation List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addSalutation: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Salutation
                                </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Salutation Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.salutationData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(Salutation);
