import Table from "../Table/Table.jsx";
import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import NotificationAddEditDelete from "./NotificationAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "type",
            "type": "text",
            "label": "Notification Type",

        },
        {
            "varName": "message",
            "type": "text",
            "label": "Notification Message",


        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",


        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",


        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class Notification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addNotification: false,
            notification: '',
            NotificationId:"",
            editNotification:false,
            tableData: [[" ", " "]],
            open: true,
            sourceData:[[" "," "]],
            redirectLogin:false,

        }
    }

    createTableData = (id,type, message,createdBy,modifiedBy,createdDate,modifiedDate,status) => {

        return ([type, message,createdBy,modifiedBy,createdDate,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editNotification(id)}>
                Edit</button>
        ]);

    }
    editNotification = (id) => {
        this.setState({
            editNotification: true,
            NotificationId: id
        })
    }

    componentDidMount() {
        let url = backEndServerURL + "/notificationMaster/getAll";

        const getAllSource = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((notification) => {

                    getAllSource.push(notification);
                });
                this.setState({
                    tableData: getAllSource
                })


            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }

            });

    }

    addNotification() {
        this.setState({
            addNotification: true,
        });
    }
    cancelNotificationModal = () => {
        this.setState({ addNotification: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addNotification: false,
            editNotification: false,

        });
    }
    onAdd=(object)=>{
        this.setState({
            addNotification: false
        });
        notification.open({
            message: 'Successfully Added'
        });


    }
    onUpdate=(object)=>{

        this.setState({
            editNotification: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((notification) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== notification[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(notification.id,notification.type,notification.message,notification.createdBy,notification.modifiedBy,notification.createdDate,notification.modifiedDate,notification.status));


        })
        this.setState({
            sourceData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addNotification}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        <NotificationAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal} secondButtonName="close" name="Add New Source-Master"/>

                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editNotification}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        <NotificationAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal} secondButtonName="close" name="Edit Source Master" id={this.state.NotificationId}/>

                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Notification Master List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addNotification: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Notification
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Notification Type", "Notification Message", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.sourceData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(Notification);
