import Table from "../Table/Table.jsx";
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import CategoryAddEditDelete from "./CategoryAddEditDelete";
import { backEndServerURL } from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "categoryName",
            "type": "text",
            "label": "Customer Category Name"
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class CustomerCategory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerCategory: false,
            editCustomerCategory: false,
            customerCategoryType: '',
            open: true,
            tableData: [[" ", " "]],
            categoryId: "",
            categoryData:[[" "," "]],
            redirectLogin:false,
        }
    }
    addCustomerCategory() {
        this.setState({
            addCustomerCategory: true,
        });
    }
    editCustomerCategory=(id) =>{
        this.setState({
            editCustomerCategory: true,
            categoryId:id
        });
    }
    cancelCustomerCategoryModal = () => {
        this.setState({ addCustomerCategory: false });
    }
    cancelEditCustomerCategoryModal = () => {
        this.setState({ editCustomerCategory: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCustomerCategory: false,
            editCustomerCategory: false
        });
    }
    createTableData = (id, categoryName,  createdBy, modifiedBy, createdDate, modifiedDate, status) => {

        return ([categoryName, createdBy, modifiedBy, createdDate, modifiedDate, status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCustomerCategory(id)}>
                Edit</button>
        ]);

    }
    

    componentDidMount() {
        let url = backEndServerURL + "/categoryMaster/getAll";

        const getAllCategory = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((category) => {

                    getAllCategory.push(category);
                });
                this.setState({
                    tableData: getAllCategory
                })


            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    customerCategoryAddModal=()=>{

        return(
            <CategoryAddEditDelete onAdd={this.onAdd} close={this.closeModal} data={this.state.tableData} name="Add New Category"/>
            )
    }
    customerCategoryEditModal=()=>{
        return(
        <CategoryAddEditDelete onUpdate={this.onUpdate} close={this.closeModal} data={this.state.tableData} name="Edit Category" id={this.state.categoryId}/>
        )
    }
    onAdd=(object)=>{
       
        this.setState({
            addCustomerCategory: false

        });
        notification.open({
            message: 'Successfully Added'
        });

       

    }
    onUpdate=(object)=>{

        this.setState({
            editCustomerCategory: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{

        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((category) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== category[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(category.id, category.categoryName,  category.createdBy, category.modifiedBy, category.createdDate, category.modifiedDate, category.status));


        })
        this.setState({
            categoryData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerCategory}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerCategoryAddModal()}
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCustomerCategory}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerCategoryEditModal()}
                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Category List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerCategory: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Category
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Category Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.categoryData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerCategory);
