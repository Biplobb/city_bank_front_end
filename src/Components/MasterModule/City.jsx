import Table from "../Table/Table.jsx";
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import CityAddEditDelete from './CityAddEditDelete';

import { backEndServerURL } from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto',

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "cityName",
            "type": "text",
            "label": "City Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}

class City extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCity: false,
            editCity: false,
            cityName: '',
            open: true,
            tableData: [[" ", " "]],
            cityId: '',
            name: "Add City",
            editName: "Edit City",
            cityData: [[" ", " "]],
            redirectLogin:false,
        }

    }
    addCity() {
        this.setState({
            addCity: true,
        });
    }
    editCity = (id) => {
        this.setState({
            editCity: true,
            cityId: id
        });
    }
    cancelCityModal = () => {
        this.setState({ addCity: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCity: false,
            editCity: false,
        });
    }
    createTableData = (id, cityName, createdBy, modifiedBy, createdDate, modifiedDate, status) => {

        return ([cityName, createdBy, modifiedBy, createdDate, modifiedDate, status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCity(id)}>
                Edit</button>
        ]);

    }
    componentDidMount() {
        let url = backEndServerURL + "/cityMaster/getAll";

        const getAllCity = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((city) => {

                    getAllCity.push(city);
                });
                this.setState({
                    tableData: getAllCity
                })


            })
            .catch((error) => { console.log(error)

                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    cityAddModal = () => {

        return (

            <CityAddEditDelete   close={this.closeModal} data={this.state.tableData} name="Add New City" />
        )
    }
    cityEditModal = () => {
        return (
            <CityAddEditDelete   close={this.closeModal} data={this.state.tableData} name="Edit City" id={this.state.cityId} />
        )
    }

    getSubmitedForm=(object)=>{
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((city) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== city[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(city.id, city.cityName, city.createdBy, city.modifiedBy, city.createdDate, city.modifiedDate, city.status));


        })
        this.setState({
            cityData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        // console.log(this.state.tableData);
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="sm"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCity}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.cityAddModal()}
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCity}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.cityEditModal()}
                    </DialogContent>

                </Dialog>

                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>City List</h4>
                            </CardHeader>

                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCity: true
                                            })
                                        }
                                        }
                                    >
                                        Add New City
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["City Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.cityData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(City);
