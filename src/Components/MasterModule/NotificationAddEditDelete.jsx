import React from "react";
import FormSample from '../JsonForm/FormSample';
import {backEndServerURL} from '../../Common/Constant';
import axios from 'axios';
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader";
import CardBody from "../Card/CardBody";
import GridItem from "../Grid/GridItem";
import GridContainer from "../Grid/GridContainer";
import withStyles from "@material-ui/core/styles/withStyles";
import Functions from '../../Common/Functions';


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};
const jsonForm = {
    "variables": [
        {
            "varName": "type",
            "type": "text",
            "label": "Notification Type",
            "required": true,

        },
        {
            "varName": "message",
            "type": "text",
            "label": "Notification Message",
            "required": true,


        },

        {
            "varName": "status",
            "type": "select",
            "label": "Status",
            "required": true,
            "enum": [
                "ACTIVE",
                "INACTIVE"
            ]
        },

    ],

};



class NotificationAddEditDelete extends React.Component {
    constructor(props) {
        super(props)
        console.log(props.id)
        this.state = {

            showValue: true,
            varValue: [],
            getNotificationData: false,
            redirectLogin:false,
        }
    }

    componentDidMount() {
        if(this.props.id!== undefined){
            let url = backEndServerURL + "/notificationMaster/get/" + this.props.id;
            axios.get(url, {withCredentials: true})
                .then((response) => {

                    let varValue = [];
                    varValue["type"] = response.data.type;
                    varValue["message"] = response.data.message
                    varValue["status"] = response.data.status;
                    this.setState({
                        varValue: varValue,
                        getNotificationData: true

                    })
                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }

                })
        }
    }

    getSubmitedForm = (object) => {
        if (this.props.id !== undefined) {
            let url = backEndServerURL + "/notificationMaster/update/" + this.props.id;
            axios.post(url, {
                "type": object.type,
                "message": object.message,
                "status": object.status,
            }, {withCredentials: true})
                .then((response) => {

                        this.props.onUpdate()

                    console.log("Successfully Edited");

                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }

                })
        } else {
            let url = backEndServerURL + "/notificationMaster/add";

            axios.post(url,
                {

                    type: object.type,
                    message: object.message,
                    status: object.status,

                }, {withCredentials: true})
                .then((response) => {

                    this.props.onAdd(object);
                    console.log(response.data+ "Added Successfully");

                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }

                });

        }
    }
    renderEditForm = (() => {
        if (this.state.getNotificationData) {
            return (
                <FormSample secondButtonFunction={this.props.secondButtonFunction}  secondButtonName={this.props.secondButtonName}  showValue={true} varValue={this.state.varValue} grid="12"
                            buttonName="Submit"
                            onSubmit={this.getSubmitedForm} jsonForm={jsonForm}/>
            )
        }
        else  if (this.props.id===undefined) {
            return (
                <FormSample secondButtonFunction={this.props.secondButtonFunction}  secondButtonName={this.props.secondButtonName}  grid="12"
                            buttonName="Submit"
                            onSubmit={this.getSubmitedForm} jsonForm={jsonForm}/>
            )
        }
        else{

        }
    })
    render() {
        const{classes}=this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <GridContainer>

                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4 className={classes.cardTitleWhite}>Add New Notification</h4>
                        </CardHeader>
                        <CardBody>
                            <div>

                                {this.renderEditForm()}
                            </div>

                        </CardBody>
                    </Card>
                </GridItem>


            </GridContainer>

        )
    }

}

export default withStyles(styles)(NotificationAddEditDelete);