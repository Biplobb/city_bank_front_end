import Table from "../Table/Table.jsx";
import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import SubSectorAddEditDelete from "./SubSectorAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [
        {
            "varName": "subSectorCode",
            "type": "text",
            "label": "Sub Sector Code",

        },
        {
            "varName": "subSectorName",
            "type": "text",
            "label": "Sub Sector Name",

        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class SubSector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addSubSector: false,
            editSubSector: false,
            tableData: [[" ", " "]],
            subSectorId:"",
            subSectorData:[[" "," "]],
            open: true,
            redirectLogin:false,
        }
    }
    createTableData = (id,subSectorCode,subSectorName,createdBy,modifiedBy,createdDate,modifiedDate,status) => {

        return ([subSectorCode,subSectorName,createdBy,modifiedBy,createdDate,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editSubSector(id)}>
                Edit</button>
        ]);

    }
    editSubSector = (id) => {

        this.setState({
            editSubSector: true,
            subSectorId: id
        })
    }

    componentDidMount() {
        let url = backEndServerURL + "/subSectorMaster/getAll";

        const getAllSubSector = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {
                console.log(response)
                response.data.map((subSector) => {

                    getAllSubSector.push(subSector);
                });
                this.setState({
                    tableData: getAllSubSector
                })

            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }

    addSubSector() {
        this.setState({
            addSubSector: true,
        });
    }
    editSubSector() {
        this.setState({
            editSubSector: true,
        });
    }
    cancelSubSectorModal = () => {
        this.setState({ addSubSector: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {

        this.setState({
            addSubSector: false,
            editSubSector: false,

        });

    }
    subSectorAddModal=()=>{

        return(
            <SubSectorAddEditDelete onAdd={this.onAdd} secondButtonFunction={this.closeModal} secondButtonName="close"  name="Add New Sub-Sector"/>
            )
    }
    subSectorEditModal=()=>{
        return(
        <SubSectorAddEditDelete onUpdate={this.onUpdate} secondButtonFunction={this.closeModal}  secondButtonName="close" name="Edit Sub-Sector" id={this.state.subSectorId}/>
        )
    }
    onAdd=(object)=>{
       /* console.log(object);
    this.state.tableData.push([object.subSectorCode,object.subSectorName,"","","","",object.status,

        <button
            className="btn btn-outline-danger"
            style={{
                verticalAlign: 'middle',
            }}
            onClick={() => this.editSubSector()}>
            Edit</button>
    ]);*/
        this.setState({
            addSubSector: false

        });
        notification.open({
            message: 'Successfully Added'
        });

        //window.location.reload();

    }
    onUpdate=(object)=>{

        this.setState({
            editSubSector: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((subSector) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== subSector[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(subSector.id,subSector.subSectorCode,subSector.subSectorName,subSector.createdBy,subSector.modifiedBy,subSector.createdDate,subSector.modifiedDate,subSector.status));


        })
        this.setState({
            subSectorData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="sm"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addSubSector}

                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.subSectorAddModal()}

                    </DialogContent>


                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editSubSector}

                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.subSectorEditModal()}

                    </DialogContent>


                </Dialog>

                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Sub-Sector List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addSubSector: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Sub-Sector
                                 </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Sub-Sector Code", "Sub-Sector Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.subSectorData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(SubSector);
