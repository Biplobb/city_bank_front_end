import React from "react";
import FormSample from '../JsonForm/FormSample';
import CardHeader from "../Card/CardHeader";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import GridItem from "../Grid/GridItem";
import GridContainer from "../Grid/GridContainer";
import withStyles from "@material-ui/core/styles/withStyles";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
};


const jsonForm = {
    "variables": [

        {
            "varName": "typeName",
            "type": "text",
            "label": "Type Name",
            "required": true,

        },


        {
            "varName": "status",
            "type": "select",
            "label": "status",
            "required": false,
            "select": true,
            "enum": [
                "ACTIVE",
                "INACTIVE"
            ]


        },

    ],

};

class CustomerTypeAddEditDelete extends React.Component {


    constructor(props) {
        super(props)
        console.log(props.id)
        this.state = {

            showValue: true,
            varValue: [],
            getSubSectorData: false,
            redirectLogin:false,
        }
    }

    componentDidMount() {
        let url = backEndServerURL + "/typeMaster/get/" + this.props.id;
        axios.get(url, {withCredentials: true})
            .then((response) => {
                let varValue = [];
                varValue["typeName"] = response.data.typeName;
                varValue["status"] = response.data.status;
                this.setState({
                    varValue: varValue,
                    getSubSectorData: true

                })
            })
            .catch((error) => {
                console.log(error);
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            })
    }

    firstLetterUpperCase = (string) => {
        var str = string.trim();
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }
    getSubmitedForm = (object) => {
        if (this.props.id !== undefined) {
            let url = backEndServerURL + "/typeMaster/update/" + this.props.id;
            axios.post(url, {
                "typeName": this.firstLetterUpperCase(object.typeName),
                "status": object.status,
            }, {withCredentials: true})
                .then((response) => {
                    this.props.onUpdate()
                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }
                })
        } else {

            let url = backEndServerURL + "/typeMaster/add";
            axios.post(url, {
                "typeName": this.firstLetterUpperCase(object.typeName),
                "status": object.status,
            }, {withCredentials: true})
                .then((response) => {

                        this.props.onAdd()

                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }
                })
        }
    }
    renderEditForm = (() => {
        if (this.state.getSubSectorData) {
            return (
                <FormSample secondButtonFunction={this.props.secondButtonFunction}
                            secondButtonName={this.props.secondButtonName} showValue={true}
                            varValue={this.state.varValue} grid="12"
                            buttonName="Submit"
                            onSubmit={this.getSubmitedForm} jsonForm={jsonForm}/>
            )
        } else if (this.props.id === undefined) {
            return (
                <FormSample secondButtonFunction={this.props.secondButtonFunction}
                            secondButtonName={this.props.secondButtonName} grid="12"
                            buttonName="Submit"
                            onSubmit={this.getSubmitedForm} jsonForm={jsonForm}/>
            )
        } else {

        }
    })


    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <GridContainer>

                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4 className={classes.cardTitleWhite}>Add New Customer</h4>
                        </CardHeader>
                        <CardBody>
                            <div>

                                {this.renderEditForm()}
                            </div>

                        </CardBody>
                    </Card>
                </GridItem>


            </GridContainer>

        )
    }
}

export default withStyles(styles)(CustomerTypeAddEditDelete);
