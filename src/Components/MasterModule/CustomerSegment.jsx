import Table from "../Table/Table.jsx";
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import { backEndServerURL } from "../../Common/Constant";
import SegmentAddEditDelete from './SegmentAddEditDelete';
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "segmentName",
            "type": "text",
            "label": "Segment Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class CustomerSegment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerSegment: false,
            editCustomerSegment: false,
            customerSegmentName: '',
            open: true,
            segmentId: '',
            tableData: [[" ", " "]],
           segmentData:[[" "," "]],
            redirectLogin:false,
           
        }
    }
    addCustomerSegment() {
        this.setState({
            addCustomerSegment: true,
        });
    }
    editCustomerSegment = (id) => {
        this.setState({
            editCustomerSegment: true,
            segmentId: id
        })
        
    }

    cancelCustomerSegmentModal = () => {
        this.setState({ addCustomerSegment: false });
    }
    cancelEditCustomerSegmentModal = () => {
        this.setState({ editCustomerSegment: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCustomerSegment: false,
            editCustomerSegment: false
        });
    }
    createTableData = (id, segmentName, createdBy, modifiedBy, createdDate, modifiedDate, status) => {

        return ([segmentName, createdBy, modifiedBy, createdDate, modifiedDate, status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCustomerSegment(id)}>
                Edit</button>
        ]);

    }


    componentDidMount() {
        let url = backEndServerURL + "/segmentMaster/getAll";

        const getAllSegment = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((segment) => {

                    getAllSegment.push(segment);
                });
                this.setState({
                    tableData: getAllSegment
                })


            })
            .catch((error) => { console.log(error)

                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    customerSegmentAddModal=()=>{

        return(
            <SegmentAddEditDelete onAdd={this.onAdd} close={this.closeModal} data={this.state.tableData} name="Add New Segment"/>
            )
    }
    customerSegmentEditModal=()=>{
        return(
        <SegmentAddEditDelete onUpdate={this.onUpdate} close={this.closeModal} data={this.state.tableData} name="Edit Segment" id={this.state.segmentId}/>
        )
    }
    onAdd=(object)=>{
       
        this.setState({
            addCustomerSegment: false

        });
        notification.open({
            message: 'Successfully Added'
        });

       

    }
    onUpdate=(object)=>{

        this.setState({
            editCustomerSegment: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((segment) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== segment[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(segment.id, segment.segmentName, segment.createdBy, segment.modifiedBy, segment.createdDate, segment.modifiedDate, segment.status));


        })
        this.setState({
            segmentData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerSegment}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerSegmentAddModal()}
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCustomerSegment}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerSegmentEditModal()}
                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Segment List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerSegment: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Segment
                                     </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Segment Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.segmentData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerSegment);
