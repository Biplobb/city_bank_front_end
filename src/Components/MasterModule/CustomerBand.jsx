import Table from "../Table/Table.jsx";
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import { backEndServerURL } from "../../Common/Constant";
import axios from "axios";
import CustomerBandAddEditDelete from './CustomerBandAddEditDelete';
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "bandNo",
            "type": "text",
            "label": "Band No",
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class CustomerBand extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerBand: false,
            editCustomerBand: false,
            bandNo: '',
            open: true,
            tableData: [[" ", " "]],
            bandId: '',
            bandData:[[" "," "]],
            redirectLogin:false,
        }
    }
    addCustomerBand() {
        this.setState({
            addCustomerBand: true,
        });
    }
    editCustomerBand = (id) => {
        this.setState({
            editCustomerBand: true,
            bandId: id
        });
    }
    cancelCustomerBandModal = () => {
        this.setState({ addCustomerBand: false });
    }
    cancelEditCustomerBandModal = () => {
        this.setState({ editCustomerBand: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCustomerBand: false,
            editCustomerBand: false
        });
    }
    createTableData = (id, bandNo, createdBy, modifiedBy, createdDate, modifiedDate, status) => {

        return ([bandNo, createdBy, modifiedBy, createdDate, modifiedDate, status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCustomerBand(id)}>
                Edit</button>
        ]);

    }


    componentDidMount() {
        let url = backEndServerURL + "/bandMaster/getAll";

        const getAllBand = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((band) => {

                    getAllBand.push(band);
                });
                this.setState({
                    tableData: getAllBand
                })


            })
            .catch((error) => { console.log(error)

                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }

            });

    }
    customerBandAddModal=()=>{

        return(
            <CustomerBandAddEditDelete onAdd={this.onAdd} close={this.closeModal} data={this.state.tableData} name="Add New Band"/>
            )
    }
    customerBandEditModal=()=>{
        return(
        <CustomerBandAddEditDelete onUpdate={this.onUpdate} close={this.closeModal} data={this.state.tableData} name="Edit Band" id={this.state.bandId}/>
        )
    }
    onAdd=(object)=>{
       
        this.setState({
            addCustomerBand: false

        });
        notification.open({
            message: 'Successfully Added'
        });

       

    }
    onUpdate=(object)=>{

        this.setState({
            editCustomerBand: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((band) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== band[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(band.id, band.bandNo, band.createdBy, band.modifiedBy, band.createdDate, band.modifiedDate, band.status));


        })
        this.setState({
            bandData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;

        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerBand}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerBandAddModal()}
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCustomerBand}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.customerBandEditModal()}
                    </DialogContent>

                </Dialog>

                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Band List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerBand: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Band
                                     </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Band No", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.bandData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerBand);
