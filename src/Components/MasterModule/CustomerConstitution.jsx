import Table from "../Table/Table.jsx";
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import { Dialog } from "@material-ui/core";
import { Grow } from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import ConstitutionAddEditDelete from "./ConstitutionAddEditDelete";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import 'antd/dist/antd.css';
import { notification } from 'antd';
import FormSample from "../JsonForm/FormSample";
import DialogContent from "@material-ui/core/DialogContent";
import Functions from '../../Common/Functions';
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        overflow: "visible"
    }
};
const jsonForm = {
    "variables": [

        {
            "varName": "constitutionName",
            "type": "text",
            "label": "Constitution Name",
            "number": false,
        },
        {
            "varName": "createdBy",
            "type": "text",
            "label": "Created By",
        },
        {
            "varName": "modifiedBy",
            "type": "text",
            "label": "Modified By",
        }



    ],

};
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class CustomerConstitution extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addCustomerConstitution: false,
            editCustomerConstitution:false,
            customerConstitutionName: '',
            open: true,
            tableData: [[" ", " "]],
            constitutionId:"",
            constitutionData:[[" "," "]],
            redirectLogin:false,
        }
    }
    addCustomerConstitution() {
        this.setState({
            addCustomerConstitution: true,
        });
    }
    editCustomerConstitution=(id)=> {
        this.setState({
            editCustomerConstitution: true,
            constitutionId:id
        });
    }
    cancelCustomerConstitutionModal = () => {
        this.setState({ addCustomerConstitution: false });
    }
    cancelEditCustomerConstitutionModal = () => {
        this.setState({ addCustomerConstitution: false });
    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    closeModal = () => {
        this.setState({
            addCustomerConstitution: false,
            editCustomerConstitution:false,
        });
    }
    createTableData = (id,constitutionName,createdBy,modifiedBy,createdDate,modifiedDate,status) => {

        return ([constitutionName,createdBy,modifiedBy,createdDate,modifiedDate,status,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editCustomerConstitution(id)}>
                Edit</button>
        ]);

    }
   

    componentDidMount() {
        let url = backEndServerURL + "/constitutionMaster/getAll";

        const getAllConstitution = [];

        axios.get(url, { withCredentials: true })
            .then((response) => {

                response.data.map((constitution) => {

                    getAllConstitution.push(constitution);
                });
                this.setState({
                    tableData: getAllConstitution
                })

            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });

    }
    constitutionAddModal=()=>{

        return(
            <ConstitutionAddEditDelete onAdd={this.onAdd} close={this.closeModal} data={this.state.tableData} name="Add New Constitution"/>
            )
    }
    constitutionEditModal=()=>{
        return(
        <ConstitutionAddEditDelete onUpdate={this.onUpdate} close={this.closeModal} data={this.state.tableData} name="Edit Constitution" id={this.state.constitutionId}/>
        )
    }
    onAdd=(object)=>{
       
        this.setState({
            addCustomerConstitution: false

        });
        notification.open({
            message: 'Successfully Added'
        });

       

    }
    onUpdate=(object)=>{

        this.setState({
            editCustomerConstitution: false

        });
        notification.open({
            message: 'Successfully Edited'
        });


    }
    getSubmitedForm=(object)=>{
        console.log(object.data);
        let objectTable = {};
        let tableArray = [];
        for(let variable in object ){
            let trimData=object[variable].trim();
            if(trimData!=='')
                objectTable[variable] =trimData;
        }

        this.state.tableData.map((constitution) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== constitution[variable])
                    showable = false;
            }
            if (showable)
                tableArray.push(this.createTableData(constitution.id,constitution.constitutionName,constitution.createdBy,constitution.modifiedBy,constitution.createdDate,constitution.modifiedDate,constitution.status));


        })
        this.setState({
            constitutionData: tableArray
        })
    }
    renderFilterForm=()=>{
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filtering" onSubmit={this.getSubmitedForm}
                        jsonForm={jsonForm} />
        )
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.addCustomerConstitution}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.constitutionAddModal()}
                    </DialogContent>

                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="md"
                    className={classes.modal}
                    classes={{ paper: classes.dialogPaper }}
                    open={this.state.editCustomerConstitution}
                    onClose={this.closeModal}
                    TransitionComponent={Transition}
                    style={{ width: 700 }}
                >
                    <DialogContent className={classes.dialogPaper}>

                        {this.constitutionEditModal()}
                    </DialogContent>

                </Dialog>


                <GridContainer>

                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Constitution List</h4>
                            </CardHeader>
                            <CardBody>
                                <div>
                                    {this.renderFilterForm()}
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'middle',
                                        }}

                                        onClick={() => {
                                            this.setState({
                                                addCustomerConstitution: true
                                            })
                                        }
                                        }
                                    >
                                        Add New Constitution
                                    </button>

                                    <Table
                                        tableHeaderColor="primary"
                                        tableHead={["Constitution Name", "Created by", "Modified by", "Created Date", "Modified Date", "Status", "Action"]}
                                        tableData={this.state.constitutionData}
                                        tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                                    />
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>


                </GridContainer>
            </section>
        );
    }
}

export default withStyles(styles)(CustomerConstitution);
