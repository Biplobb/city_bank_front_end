import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {Redirect} from "react-router-dom";
import axios from 'axios';
import '../Static/css/login.css';
import cookie from 'react-cookies';
import {backEndServerURL} from '../Common/Constant';
import logo from "../Static/citybanklogo.png";


const styles = theme => ({

    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {

        marginTop: theme.spacing.unit * 1,

    },
    Buttoncolorchange: {
        backgroundColor: '#ff3443',
        '&:hover': {
            backgroundColor: "#ff3443"
        }
    },
    Errorr: {
        color: '#ff3443',


    }

});

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            redirect: false,
            errorMessage: "",
            id: 100,
            tableData: [[" ", " "]],
            roleId: '',


        }

    };

    handleusername = (event) => {
        this.setState({
            username: event.target.value
        })
    };

    handlepassword = event => {
        this.setState({
            password: event.target.value
        })
    };
    createTableData = (roles, menus) => {
        return ([roles, menus
        ]);


    }
    userPermission = () => {

        let url = backEndServerURL + "/get/roleAndMenu/" + this.state.username;



        axios.get(url, {withCredentials: true})
            .then((response) => {
                console.log("permission");
                console.log(response);
                cookie.save("spring_session", response.headers.spring_session);
                localStorage.setItem("username", this.state.username);
                localStorage.setItem("roles", response.data.roles);
                localStorage.setItem("workplace", response.data.workplace);
                localStorage.setItem("menus", JSON.stringify(response.data.menus));
                this.setState({
                    redirect: true
                })

            })
            .catch((error) => {
                console.log(error);
            });


    }
    handlesubmit = event => {
        event.preventDefault()


        axios.get(backEndServerURL + "/logMeIn", {
            withCredentials: true,
            auth: {
                username: this.state.username,
                password: this.state.password
            }
        })
            .then((response) => {
                console.log(response);
              /*  cookie.save("accessToken", response.headers.access_token);*/
                this.userPermission()
            })
            .catch(error => {

                this.setState({errorMessage: "Username or password invalid"});
            })
    }

    renderRedirect = () => {
        if (this.state.redirect) {


            return (<Redirect to={{
                /*pathname: '/CustomerDedupSearch',*/
                pathname: '/inbox',
                state: {roles: this.state.tableData}

            }}/>);
        }

    };


    render() {


        const {classes} = this.props;

        return (
            <div>

                {
                    this.renderRedirect()

                }
                <center>


                    <main className={classes.main}>
                        <CssBaseline/>
                        <Paper className={classes.paper}>

                            <img height={120} src={logo}/>

                            <Typography component="h1" variant="h5">
                                Sign in
                            </Typography>

                            {
                                this.state.errorMessage
                            }
                            <form onSubmit={this.handlesubmit} className={classes.form}>
                                <FormControl margin="normal" required fullWidth>
                                    <InputLabel htmlFor="email">User Name</InputLabel>
                                    <Input onChange={this.handleusername} id="email" name="email" autoComplete="email"
                                           autoFocus/>
                                </FormControl>

                                <FormControl margin="normal" required fullWidth>
                                    <InputLabel htmlFor="password">Password</InputLabel>
                                    <Input onChange={this.handlepassword} name="password" type="password" id="password"
                                           autoComplete="current-password"/>
                                </FormControl>
                                <br/>
                                <br/>
                                <br/>

                                <center>
                                    <Button
                                        type="primary" htmlType="submit" className={classes.Buttoncolorchange}


                                        variant="contained"
                                        color="secondary"

                                    >

                                        Sign in
                                    </Button>
                                </center>

                            </form>
                        </Paper>
                    </main>
                </center>
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);