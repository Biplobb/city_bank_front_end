import React from "react";
import {backEndServerURL} from "../Common/Constant";
import axios from "axios";

import Notification from "./NotificationMessage/Notification";
import Functions from "../Common/Functions";
import SelectComponent from "./JsonForm/SelectComponent";
import CommonJsonFormComponent from "./JsonForm/CommonJsonFormComponent";
import Table from "./Table/Table";
import Grid from "@material-ui/core/Grid";
import CardHeader from "./Card/CardHeader";
import GridList from "@material-ui/core/GridList";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import loader from "../Static/loader.gif";
import MakerCumInput from "./workflow/CASA/MakerCumInput";
import {ThemeProvider} from "@material-ui/styles";
import theme from "./JsonForm/CustomeTheme";
import GridContainer from "./Grid/GridContainer";
import GridItem from "./Grid/GridItem";
import Card from "./Card/Card";
import withStyles from "@material-ui/core/styles/withStyles";
import Fab from '@material-ui/core/Fab';
import Pageview from '@material-ui/icons/Pageview';
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Customer from "./360View/Customer";
import MakerView from "./360View/MakerView";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    gridList: {
        width: 500,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },

};


class DedupResultFunction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            appData: {},
            getData: false,
            varValue: [],
            showValue: false,
            redirectLogin: false,
            title: "",
            notificationMessage: "",
            alert: false,
            inputData: {},
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},
            values: [],
            customerName: [],
            deferalType: [],
            expireDate: [],
            other: [],
            getCheckerList: [],
            getAllDefferal: [],
            getDeferalList: [],
            loading: false,
            jointAccountCustomerNumber: 0,
            objectForJoinAccount: [],
            getgenerateForm: false,
            renderCumModalopen: false,
            generateAccountNo: '',
            getDedupData: {},
            jointDedupData: {},
            jointSearchTableData: [],
            propritorshipData: [],
            dedupData: [],
            relatedData: {},
            CustomerModal: false,
            IDENTIFICATION_NO: '',
            searchTableData: false,
            searchTableRelatedData: false,
            getRadioButtonData: {},
            tagingModalCbnumber: '',
            getCustomerNumber: '',
            err: false,
            errorArray: {},
            errorMessages: {},


        };


    }

    closeCustomerModal = () => {
        this.setState({
            CustomerModal: false
        })
    }
    CustomerModal = (uniqueId) => {
        this.setState({
            CustomerModal: true,
            IDENTIFICATION_NO: uniqueId
        })
    }

    radioButtonChange = (event) => {

        let variable = {};
        console.log(event.target.name)
        console.log(event.target.value)
        variable[event.target.name] = event.target.value
        this.setState({
            getRadioButtonData: variable,
            getCustomerNumber:event.target.name.slice(-1)
        })

    }


    tagingModal = (index) => {
        this.setState({
            renderCumModalopen: true,
            tagingModalCbnumber: index
        })
    }

    taging = (index,data) => {

        let value = index + 1;
       if(data!==undefined){
           this.props.getTaggingData("tagging" + value,data,value);
       }
       else{
           this.props.getTaggingData("tagging" + value, this.state.getRadioButtonData["tagging" + value],value);
       }


    }
    createTableData = (randomNumber, id, uniqueId, customerId, accountSource, customerName, nid, passport, tin, phone, dob, email, registration, matchType) => {
        //this.renderCustomerNumber(sl);

        if (this.props.subServiceType === 'Joint Account') {
            if (accountSource === "TRANZWARE") {
                return ([customerId, accountSource, customerName,
                    <button
                        className="btn"
                        style={{
                            verticalAlign: 'middle',
                        }}

                        onClick={() => this.CustomerModal(uniqueId)}
                    >
                        <Fab size="small" color="secondary" aria-label="pageview">

                            <Pageview/>
                        </Fab>

                    </button>])
            } else {
                return (

                    [<input type="radio" onChange={(event) => this.radioButtonChange(event)} name={randomNumber}
                            value={customerId}/>, customerId, accountSource, customerName,
                        <button
                            className="btn"
                            style={{
                                verticalAlign: 'middle',
                            }}

                            onClick={() => this.CustomerModal(uniqueId)}
                        >
                            <Fab size="small" color="secondary" aria-label="pageview">

                                <Pageview/>
                            </Fab>

                        </button>


                    ])
            }
        } else {
            if (accountSource === "TRANZWARE") {
                return ([customerId, accountSource, customerName,
                    <button
                        className="btn"
                        style={{
                            verticalAlign: 'middle',
                        }}

                        onClick={() => this.CustomerModal(uniqueId)}
                    >
                        <Fab size="small" color="secondary" aria-label="pageview">

                            <Pageview/>
                        </Fab>

                    </button>])
            } else if (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C') {
                return ([<input type="radio" onChange={(event) => this.radioButtonChange(event)} name={randomNumber}
                                value={customerId}/>, customerId, accountSource, customerName,


                    <button
                        className="btn"
                        style={{
                            verticalAlign: 'middle',
                        }}

                        onClick={() => this.CustomerModal(uniqueId)}
                    >
                        <Fab size="small" color="secondary" aria-label="pageview">

                            <Pageview/>
                        </Fab>

                    </button>


                ])
            } else {
                return ([<input type="radio" onChange={(event) => this.radioButtonChange(event)} name={randomNumber}
                                value={customerId}/>, customerId, accountSource, customerName,


                    <button
                        className="btn"
                        style={{
                            verticalAlign: 'middle',
                        }}

                        onClick={() => this.CustomerModal(uniqueId)}
                    >
                        <Fab size="small" color="secondary" aria-label="pageview">

                            <Pageview/>
                        </Fab>

                    </button>


                ])
            }
        }

    };
    createTableDataWithoutProprietorship = (id, uniqueId, customerId, accountSource, customerName, nid, passport, tin, phone, dob, email, registration, matchType) => {
        //this.renderCustomerNumber(sl);
        if (this.props.subServiceType === 'Company Account') {
            if (accountSource === "TRANZWARE") {
                return ([customerId, accountSource, customerName,
                    <button
                        className="btn"
                        style={{
                            verticalAlign: 'middle',
                        }}

                        onClick={() => this.CustomerModal(uniqueId)}
                    >
                        <Fab size="small" color="secondary" aria-label="pageview">

                            <Pageview/>
                        </Fab>

                    </button>])
            } else {
                return (

                    [customerId, accountSource, customerName,
                        <button
                            className="btn"
                            style={{
                                verticalAlign: 'middle',
                            }}

                            onClick={() => this.CustomerModal(uniqueId)}
                        >
                            <Fab size="small" color="secondary" aria-label="pageview">

                                <Pageview/>
                            </Fab>

                        </button>


                    ])
            }
        }


    };
    createTableproCompany = (id, uniqueId, customerId, accountSource, customerName, nid, passport, tin, phone, dob, email, registration, matchType) => {


        if (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C') {
            return ([customerId, accountSource, customerName,
                <button
                    className="btn"
                    style={{
                        verticalAlign: 'middle',
                    }}

                    onClick={() => this.CustomerModal(uniqueId)}
                >
                    <Fab size="small" color="secondary" aria-label="pageview">

                        <Pageview/>
                    </Fab>

                </button>


            ])

        } else {
            return ([customerId, accountSource, customerName,
                <button
                    className="btn"
                    style={{
                        verticalAlign: 'middle',
                    }}

                    onClick={() => this.CustomerModal(uniqueId)}
                >
                    <Fab size="small" color="secondary" aria-label="pageview">

                        <Pageview/>
                    </Fab>

                </button>


            ])
        }

    };

    componentDidMount() {

        /*  this.setState({
              loading: true
          })*/


        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log("llll")
                    console.log(response.data)
                    let objectForJoinAccount = [];
                    var sl;


                    this.setState({
                        getDedupData: response.data,
                        objectForJoinAccount: objectForJoinAccount,
                        getgenerateForm: true

                    })
                    console.log(response.data)
                    let checkerListUrl = backEndServerURL + "/checkers";
                    if (this.props.subServiceType === "Joint Account" || this.props.subServiceType === "Company Account") {

                        let tableArray = [];
                        let relatedTableArray = [];
                        let dedupSaveId = "";


                        let dedupDataViewUrl = backEndServerURL + "/dedup/get/" + this.state.getDedupData.jointDedupData;
                        axios.get(dedupDataViewUrl, {withCredentials: true})
                            .then((response) => {
                                console.log(this.state.getDedupData)
                                console.log("high")
                                console.log(response.data.jointDedupData.highMatchCustomers)
                                let jointDedupSearchTable = [];
                                if (response.data.jointDedupData.highMatchCustomers !== null && Array.isArray(response.data.jointDedupData.highMatchCustomers)) {
                                    var randomNumber = 0;
                                    response.data.jointDedupData.highMatchCustomers.map((dedup, i) => {
                                        let customer = [];
                                        randomNumber = randomNumber + 1;
                                        dedup.map((dedup) => {

                                            customer.push(this.createTableData("tagging" + randomNumber, dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Higher-Match"));

                                        })

                                        jointDedupSearchTable.push(customer);

                                    });


                                }

                                if (response.data.jointDedupData.relatedCustomers !== null && Array.isArray(response.data.jointDedupData.relatedCustomers)) {
                                    response.data.jointDedupData.relatedCustomers.map((dedup) => {

                                        relatedTableArray.push(this.createTableData(dedup.id, dedup.mainCB, dedup.relatedCB, dedup.relatedCustomerName, dedup.relationship));

                                    });
                                }

                                this.setState({

                                    jointSearchTableData: jointDedupSearchTable,
                                    relatedData: relatedTableArray,
                                    searchTableData: true,
                                    searchTableRelatedData: true,
                                    loading: false,

                                })
                            })
                            .catch((error) => {
                                console.log(error);
                                this.setState({
                                    jointSearchTableData: [],
                                    relatedData: [],
                                    searchTableData: true,
                                    searchTableRelatedData: true,
                                    loading: false,
                                })
                            })


                    } else {
                        let tableArray = [];
                        let relatedTableArray = [];

                        /*   if (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C') {
                               let tableArray = [];

                               let dedupDataViewUrl = backEndServerURL + "/dedup/get/" + this.state.getDedupData.companyDedupData;
                               axios.get(dedupDataViewUrl, {withCredentials: true})
                                   .then((response) => {

                                       console.log("lko")
                                       console.log(response.data)

                                       if (response.data.companyDedupData.highMatchCustomers !== null && Array.isArray(response.data.companyDedupData.highMatchCustomers)) {

                                           response.data.companyDedupData.highMatchCustomers.map((dedup) => {
                                               tableArray.push(this.createTableproCompany(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Higher-Match"));

                                           });

                                       }

                                       if (response.data.companyDedupData.mediumMatchCustomers !== null && Array.isArray(response.data.companyDedupData.mediumMatchCustomers)) {

                                           response.data.companyDedupData.mediumMatchCustomers.map((dedup) => {

                                               tableArray.push(this.createTableproCompany(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Medium-Match"));

                                           });
                                       }


                                       this.setState({

                                           propritorshipData: tableArray,
                                           //relatedData: relatedTableArray,
                                           searchTableData: true,
                                           searchTableRelatedData: true,
                                           //getsearchValue: object,
                                           loading: false,

                                       })


                                   })
                                   .catch((error) => {

                                       this.setState({
                                           propritorshipData: [],
                                           relatedData: [],
                                           searchTableData: true,
                                           searchTableRelatedData: true,
                                           loading: false,
                                       })

                                   });

                           }*/
                        if (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C') {

                            let dedupDataViewUrl = backEndServerURL + "/dedup/get/" + this.state.getDedupData.individualDedupData;
                            axios.get(dedupDataViewUrl, {withCredentials: true})
                                .then((response) => {

                                    console.log("lko")
                                    console.log(this.state.getDedupData.individualDedupData)
                                    console.log(response.data)

                                    if (response.data.individualDedupData.highMatchCustomers !== null && Array.isArray(response.data.individualDedupData.highMatchCustomers)) {

                                        response.data.individualDedupData.highMatchCustomers.map((dedup) => {
                                            tableArray.push(this.createTableData(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Higher-Match"));

                                        });

                                    }

                                    if (response.data.individualDedupData.mediumMatchCustomers !== null && Array.isArray(response.data.individualDedupData.mediumMatchCustomers)) {

                                        response.data.individualDedupData.mediumMatchCustomers.map((dedup) => {

                                            tableArray.push(this.createTableData(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Medium-Match"));

                                        });
                                    }


                                    this.setState({

                                        dedupData: tableArray,
                                        //relatedData: relatedTableArray,
                                        searchTableData: true,
                                        searchTableRelatedData: true,

                                        loading: false,

                                    })


                                })
                                .catch((error) => {

                                    this.setState({
                                        propritorshipData: [],
                                        //relatedData: [],
                                        searchTableData: true,
                                        searchTableRelatedData: true,
                                        loading: false,
                                    })

                                });

                        }
                        /*else if (this.props.subServiceType === 'Company Account') {
                            let tableArray = [];

                            let dedupDataViewUrl = backEndServerURL + "/dedup/get/" + this.state.getDedupData.companyDedupData;
                            axios.get(dedupDataViewUrl, {withCredentials: true})
                                .then((response) => {

                                    console.log("lko")
                                    console.log(response.data)

                                    if (response.data.companyDedupData.highMatchCustomers !== null && Array.isArray(response.data.companyDedupData.highMatchCustomers)) {

                                        response.data.companyDedupData.highMatchCustomers.map((dedup) => {
                                            tableArray.push(this.createTableproCompany(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Higher-Match"));

                                        });

                                    }

                                    if (response.data.companyDedupData.mediumMatchCustomers !== null && Array.isArray(response.data.companyDedupData.mediumMatchCustomers)) {

                                        response.data.companyDedupData.mediumMatchCustomers.map((dedup) => {

                                            tableArray.push(this.createTableproCompany(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Medium-Match"));

                                        });
                                    }

                                    /!* if (response.data.relatedCustomers !== null && Array.isArray(response.data.relatedCustomers)) {
                                         response.data.relatedCustomers.map((dedup) => {

                                             relatedTableArray.push(this.createRelatedTableData(dedup.id, dedup.mainCB, dedup.relatedCB, dedup.relatedCustomerName));

                                         });
                                     }*!/
                                    this.setState({

                                        propritorshipData: tableArray,
                                        //relatedData: relatedTableArray,
                                        searchTableData: true,
                                        searchTableRelatedData: true,
                                        //getsearchValue: object,
                                        loading: false,

                                    })


                                })
                                .catch((error) => {

                                    this.setState({
                                        propritorshipData: [],
                                        relatedData: [],
                                        searchTableData: true,
                                        searchTableRelatedData: true,
                                        loading: false,
                                    })

                                });

                        }*/
                        else {

                            let tableArray = [];
                            let relatedTableArray = [];

                            //set 02/02/1983, 00:00:00 to 1983-02-02 format

                            let dedupDataViewUrl = backEndServerURL + "/dedup/get/" + this.state.getDedupData.individualDedupData;
                            axios.get(dedupDataViewUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data.individualDedupData)
                                    if (response.data.individualDedupData.highMatchCustomers !== null && Array.isArray(response.data.individualDedupData.highMatchCustomers)) {

                                        response.data.individualDedupData.highMatchCustomers.map((dedup) => {
                                            tableArray.push(this.createTableData(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Higher-Match"));

                                        });

                                    }

                                    if (response.data.individualDedupData.mediumMatchCustomers !== null && Array.isArray(response.data.individualDedupData.mediumMatchCustomers)) {

                                        response.data.individualDedupData.mediumMatchCustomers.map((dedup) => {

                                            tableArray.push(this.createTableData(dedup.id, dedup.identification_no, dedup.szcustomerid, dedup.sz_source, dedup.sz_full_name, dedup.sz_national_id, dedup.sz_passport, dedup.sz_tin_number, dedup.cust_comu_phone_num, dedup.dt_birth, dedup.email_id, dedup.sz_registration_no, "Medium-Match"));

                                        });
                                    }


                                    this.setState({

                                        dedupData: tableArray,
                                        //relatedData: relatedTableArray,
                                        searchTableData: true,
                                        searchTableRelatedData: true,

                                        loading: false,

                                    })


                                })
                                .catch((error) => {

                                    this.setState({
                                        dedupData: [],
                                        relatedData: [],
                                        searchTableData: true,
                                        searchTableRelatedData: true,
                                        loading: false,
                                    })

                                });

                        }

                    }

                    console.log(response.data)
                    let varValue = response.data;
                    this.setState({
                        jointAccountCustomerNumber: response.data.jointAccountCustomerNumber,
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        showValue: true,
                        loading: false
                    });
                })
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        loading: false
                    })

                });
        }


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    close = () => {
        this.props.closeModal();
    }


    renderCumModalopen = () => {
        this.setState({
            renderCumModalopen: true
        })
    }


    closeModal = (account) => {

        this.setState({
            renderCumModalopen: false,
            generateAccountNo: "",

        })
    }

    renderNewAccountOpeingForm = (index) => {

        this.setState({

            renderCumModalopen: true,
            generateAccountNo: "NEW",
            tagingModalCbnumber: index,

        })


    }
    /*renderJointSearchtabledata = () => {
        return (
            this.state.jointSearchTableData.map((tableData, index) => {
                return (

                    <div>
                        <center>
                            <br/>
                            <Grid item xs={12}>
                                <h3>Customer {index + 1}</h3>
                            </Grid>
                        </center>
                        <Table
                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["", "CB No", "Source", "Customer Name", "View"]}

                            tableData={tableData}
                            tableAllign={['left', 'left', 'left', 'left', 'left', 'right',]}
                        />
                    </div>

                )
            })
        )


    };*/
    renderWithoutProprietorshipSearchtabledata = () => {
        return (
            this.state.jointSearchTableData.map((tableData, index) => {
                return (

                    <div>
                        <br/>


                        <center>
                            <Grid item xs={6}>
                                <h3>Customer {index + 1}</h3>
                            </Grid>
                        </center>
                        <br/>
                        <center>
                            <Grid item xs={6}>
                                <Grid container spacing={1}>
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{

                                            verticalAlign: 'right',
                                            position: "absolute",
                                            right: 80,

                                        }}
                                        onClick={(event) => this.tagingModal(index)}>
                                        Tag
                                    </button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{

                                            verticalAlign: 'right',
                                            position: "absolute",
                                            right: 15,
                                        }}
                                        onClick={(event) => this.renderNewAccountOpeingForm(index)}>
                                        New
                                    </button>
                                </Grid>

                            </Grid>
                        </center>


                        <Table
                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["CB No", "Source", "Customer Name", "View"]}

                            tableData={tableData}
                            tableAllign={['left', 'left', 'left', 'left', 'left', 'right']}
                        />

                    </div>

                )
            })
        )


    };

    searchTableData = (style) => {

        if (this.state.searchTableData && (this.props.subServiceType === "Company Account" || this.props.subServiceType === 'Joint Account') && this.state.getDedupData.jointDedupData !== undefined) {

            return (

                <div style={{marginBottom: 40}}>
                    <paper>
                        <CardHeader color="rose">
                            <h4>Proprietor Result</h4>

                        </CardHeader>
                    </paper>

                    {this.renderWithoutProprietorshipSearchtabledata()}
                    <br/>
                </div>


            )
        }
        /* else if (this.state.searchTableData && (this.props.subServiceType === 'Company Account')) {

             return (

                 <div style={{marginBottom: 40}}>
                     <paper>
                         <CardHeader color="rose">
                             <h4>Proprietor Result</h4>

                         </CardHeader>
                     </paper>

                     {this.renderWithoutProprietorshipSearchtabledata()}
                     <br/>
                 </div>

             )
         }*/
        /*else if (this.state.searchTableData && this.props.subServiceType === 'INDIVIDUAL') {
    
            return (
    
                <div style={{marginBottom: 40}}>
                    <paper>
                        <CardHeader color="rose">
                            <h4>Search Result</h4>
    
                        </CardHeader>
                    </paper>
                    <br/>
                    <div>
    
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',
                                position: "absolute",
                                right: 10,
    
                            }}
                            onClick={() => this.renderNewAccountOpeingForm(event)}>
                            New Account Creation
                        </button>
    
                    </div>
                    <br/>
                    <br/>
                    <Table
    
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={[ "CB No", "Source", "Customer Name","Action", "", "View"]}
    
                        tableData={this.state.dedupData}
                        tableAllign={['left', 'left', 'left', 'left', 'left', 'right']}
                    />
    
                    <br/>
    
    
                </div>
    
            )
    
    
        } */
        else if (this.state.searchTableData && (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C')) {
            return (
                <div style={{marginBottom: 40}}>
                    <paper>
                        <CardHeader color="rose">
                            <h4>Proprietor Result</h4>

                        </CardHeader>
                    </paper>

                    <div>
                        <br/>
                        <Grid container spacing={3}>
                            <Grid item xs={6}></Grid>
                            <Grid item xs={6}>
                                <Grid container spacing={3}>
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            float: 'right',
                                            verticalAlign: 'middle',

                                        }}
                                        onClick={(event) => this.tagingModal(1)}>
                                        Tag
                                    </button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            float: 'right',
                                            verticalAlign: 'middle',

                                        }}
                                        onClick={(event) => this.renderNewAccountOpeingForm(event)}>
                                        New
                                    </button>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Table

                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["", "CB No", "Source", "Customer Name", "View", "", ""]}
                            tableData={this.state.dedupData}
                            tableAllign={['left', 'left', 'left', 'left', 'left', 'right', 'right', 'right']}
                        />
                    </div>
                    <br/>
                </div>
                /* <div style={{marginBottom: 40}}>
                     <paper>
                         <CardHeader color="rose">
                             <h4>Proprietor Result</h4>

                         </CardHeader>
                     </paper>



                        <center>

                                <Grid container spacing={3}>
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            float: 'right',
                                            verticalAlign: 'middle',

                                        }}
                                        onClick={(event) => this.taging("","")}>
                                        Tag
                                    </button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            float: 'right',
                                            verticalAlign: 'middle',

                                        }}
                                        onClick={(event) => this.renderNewAccountOpeingForm(event)}>
                                        New
                                    </button>

                            </Grid>

                        </center>

                     <Table

                         tableHovor="yes"
                         tableHeaderColor="primary"
                         tableHead={["","CB No", "Source", "Customer Name", "View","",""]}
                         tableData={this.state.dedupData}
                         tableAllign={['left', 'left', 'left', 'left', 'left', 'right','right','right']}
                     />

                     <br/>


                 </div>
 */
            )


        } else {

        }

    }

    /* searchTableRelatedData = (style) => {

         if (this.state.searchTableRelatedData && this.props.subServiceType === 'Joint Account') {

             return (

                 <div style={{marginBottom: 40}}>
                     <paper>
                         <CardHeader color="rose">
                             <h4>Related CB</h4>

                         </CardHeader>
                     </paper>
                     <br/>
                     <div>

                         <Table

                             tableHovor="yes"
                             tableHeaderColor="primary"
                             tableHead={["Main CB", "Related CB", "Customer Name", "RelationShip", "Action"]}

                             tableData={this.state.relatedData}
                             tableAllign={['left', 'left', 'left', 'left']}
                         />

                         <br/>


                     </div>

                 </div>

             )
         }
         /!*   else if (this.state.searchTableRelatedData && this.props.subServiceType === 'INDIVIDUAL') {

                return (

                    <div style={{marginBottom: 40}}>
                        <paper>
                            <CardHeader color="rose">
                                <h4>Related CB</h4>

                            </CardHeader>
                        </paper>
                        <br/>
                        <div>

                            <Table

                                tableHovor="yes"
                                tableHeaderColor="primary"
                                tableHead={["Main CB", "Related CB", "Customer Name", "RelationShip"]}

                                tableData={this.state.relatedData}
                                tableAllign={['left', 'left', 'left', 'left']}
                            />

                            <br/>


                        </div>

                    </div>

                )
            }*!/
      /!*   else if (this.state.searchTableRelatedData && (this.props.subServiceType === 'NONINDIVIDUAL' || this.props.subServiceType === 'Proprietorship A/C' || this.props.subServiceType === 'Partnership A/C' || this.props.subServiceType === 'Limited Company A/C' || this.props.subServiceType === 'Company Account')) {

             return (

                 <div style={{marginBottom: 40}}>
                     <paper>
                         <CardHeader color="rose">
                             <h4>Company/Business Result</h4>

                         </CardHeader>
                     </paper>
                     <br/>

                     <br/>
                     <br/>
                     <Table

                         tableHovor="yes"
                         tableHeaderColor="primary"
                         tableHead={["CB No", "Source", "Customer Name", "View"]}

                         tableData={this.state.propritorshipData}
                         tableAllign={['left', 'left', 'left', 'left', 'right']}
                     />

                     <br/>


                 </div>

             )


         }*!/
      else {

         }

     };*/

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }


        return (

            <div>
                <Dialog
                    fullWidth="true"
                    maxWidth="sm"
                    className={classes.modal}
                    classes={{paper: classes.dialogPaper}}
                    open={this.state.loading}>
                    <DialogContent className={classes.dialogPaper}>

                        <center>
                            <img src={loader} alt=""/>
                        </center>
                    </DialogContent>
                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="xl"
                    fullScreen={true}

                    open={this.state.renderCumModalopen}>
                    <DialogContent>
                        <MakerCumInput customerNumber={this.state.getCustomerNumber} appId={this.props.appId}
                                       generateAccountNo={this.state.generateAccountNo}
                                       tagingModalCbnumber={this.state.tagingModalCbnumber} taging={this.taging} cbNumber={this.state.getRadioButtonData}
                                       closeModal={this.closeModal}/>
                    </DialogContent>
                </Dialog>
                <Dialog
                    fullWidth="true"
                    maxWidth="xl"
                    className={classes.modal}
                    classes={{paper: classes.dialogPaper}}
                    open={this.state.CustomerModal}

                >
                    <DialogContent className={classes.dialogPaper}>
                        <MakerView closeModal={this.closeCustomerModal}
                                   IDENTIFICATION_NO={this.state.IDENTIFICATION_NO}/>
                    </DialogContent>
                </Dialog>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <div color="rose" className={classes.root}>

                                {this.searchTableData()}
                            </div>
                        </Card>
                    </GridItem>

                </GridContainer>
                {/*<GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <div color="rose" className={classes.root}>
                                {this.addCustomerIdForm()}
                            </div>
                        </Card>
                    </GridItem>

                </GridContainer>*/}


            </div>

        )

    }


}

export default withStyles(styles)(DedupResultFunction);
