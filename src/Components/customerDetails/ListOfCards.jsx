import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components

import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";



import {Dialog} from "@material-ui/core";
import {Grow} from "@material-ui/core";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import Table from "../Table/Table.jsx";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import Fab from '@material-ui/core/Fab';
import Pageview from '@material-ui/icons/Pageview';
import CloseIcon from '@material-ui/icons/Close';
import CustomerCreditCard from "./CustomerCreditCard";
import CircularProgress from '@material-ui/core/CircularProgress';
import Functions from '../../Common/Functions';
const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

});
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class ListOfCards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            SingleCards: false,
            cardType:'',
            cardNo:'',
            tableData: [[" ", " "]],
            IDENTIFICATION_NO:'',
            ACCOUNTNO:"",
            renderCardDetailsData:false,
            redirectLogin:false,
        }
    }

    SingleCards(cardNo) {
        this.setState({
            SingleCards: true,
            cardNo:cardNo
        });
    }
    closeSingleCardsModal = () => {
        this.setState({
            SingleCards: false,

        });
    }
    createTableData = (id, cardNo, cardHolderName, cardType,status,view) => {

        return ([cardNo, cardHolderName, cardType,status,
            <button
                className="btn"
                style={{
                    verticalAlign: 'middle',
                }}

                onClick={
                    event => this.SingleCards(cardNo)
                }
            >
                <Fab size="small" color="secondary" aria-label="pageview">

                    <Pageview/>
                </Fab>

            </button>
        ]);

    }


    componentDidMount() {
        if (this.props.IDENTIFICATION_NO !== undefined) {
            let url = backEndServerURL + "/demographic/cards/" + this.props.IDENTIFICATION_NO;

            const ListOfCards = [];
            axios.get(url, {withCredentials: true})
                .then((response) => {
                        console.log(response);
                        this.setState({
                            ListOfCards: response.data,

                        });
                        response.data.map((card) => {

                            ListOfCards.push(this.createTableData(card.id, card.cardNo, card.cardHolderName, card.cardType,card.status));
                        });
                        this.setState({
                            tableData: ListOfCards,
                            renderCardDetailsData:true

                        })
                })
                .catch((error) => {
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }
                    this.setState({
                        renderCardDetailsData:true
                    })
                })

        }else{

        }

    }

    renderCardDetailsData=()=>{
        if (this.state.renderCardDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"

                    tableHead={["Card No", "Card Holder Name", "Card Type", "Status", "View"
                    ]}
                    tableData={
                        this.state.tableData

                    }
                    tableAllign={['left', 'left', 'left', 'left', 'left']}

                />
            )
        } else if (this.state.renderCardDetailsData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

}
    close=()=>{
        this.props.closeModal()
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <Dialog

                    className={classes.modal}
                    open={this.state.SingleCards}
                    fullWidth="true"
                    maxWidth="md"
                    TransitionComponent={Transition}
                    onClose={this.closeSingleCardsModal}
                >

                    <CustomerCreditCard closeModal={this.closeSingleCardsModal} cardNo={this.state.cardNo} cardType={this.state.cardType}/>
                </Dialog>

                <center>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>
                            <Card>
                                <CardHeader color="rose">


                                    <h4>Cards Details<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                                </CardHeader>
                                <CardBody>

                                    <div className={classes.root}>
                                        <Grid container spacing={1}>
                                            <Grid  container item xs={12} spacing={5}>
                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderCardDetailsData()}
                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </center>
            </section>

        );
    }
}

export default withStyles(styles)(ListOfCards);
