import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Table from "../Table/Table";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import CircularProgress from '@material-ui/core/CircularProgress';
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";

import Functions from '../../Common/Functions';

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

});

class CustomerCreditCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            getData: false,
            renderCustomerCreditCardSummeryInfo: false,
            redirectLogin:false,
        }
    }
    componentDidMount() {

        if (this.props.cardNo !== undefined) {
            let url = backEndServerURL + "/demographic/card/" + this.props.cardNo ;

            let varValue = [];


            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(true);
                    console.log(response.data);
                    if (response.data[0] !== null) {


                        varValue["LASTSTROUTSTANDINGBDT"] = response.data[0].LASTSTROUTSTANDINGBDT;
                        varValue["CARDHOLDERNAME"] = response.data[0].CARDHOLDERNAME;

                        varValue["CONTRACTSTATUS"] = response.data[0].CONTRACTSTATUS;
                        varValue["LASTSTATEMENTDATE"] = response.data[0].LASTSTATEMENTDATE;

                        varValue["LASTSTROUTSTANDINGUSD"] = response.data[0].LASTSTROUTSTANDINGUSD;
                        varValue["CURRENTOUTSTANDINGBDT"] = response.data[0].CURRENTOUTSTANDINGBDT;

                        varValue["LASTPAYMENTUSD"] = response.data[0].LASTPAYMENTUSD;
                        varValue["CONTRACTTYPE"] = response.data[0].CONTRACTTYPE;

                        varValue["CONTRACTNO"] = response.data[0].CONTRACTNO;
                        varValue["CURRENTOUTSTANDINGUSD"] = response.data[0].CURRENTOUTSTANDINGUSD;

                        varValue["CLIENTID"] = response.data[0].CLIENTID;
                        varValue["LASTPAYMENTBDT"] = response.data[0].LASTPAYMENTBDT;







                    } else {

                    }

                    this.setState({
                        varValue: varValue,
                        renderCustomerCreditCardSummeryInfo:true

                    })

                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }
                    this.setState({
                        renderCustomerCreditCardSummeryInfo:true

                    })
                });
        } else {

        }
    }
    renderCustomerCreditCardSummeryInfo=()=>{
        if (this.state.renderCustomerCreditCardSummeryInfo === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Card Holder Name",this.state.varValue["CARDHOLDERNAME"]],
                        ["Card NO",this.state.varValue["CONTRACTNO"]],
                        ["Outstanding Balance BDT", this.state.varValue["CURRENTOUTSTANDINGBDT"] ],
                        ["Outstanding Balance USD", this.state.varValue["CURRENTOUTSTANDINGUSD"] ],
                        ["Minimum Payment Due", this.state.varValue[""] ],
                        ["Non Payment Of Minimum Payment", this.state.varValue[""] ],
                        ["Last Payment BDT", this.state.varValue["LASTPAYMENTBDT"] ],
                        ["Last Payment USD", this.state.varValue["LASTPAYMENTUSD"] ],
                        ["Interest Accrued", this.state.varValue[""] ],



                    ]}
                    tableAllign={['left', 'left']}
                />
            )
        } else if (this.state.renderCustomerCreditCardSummeryInfo === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    close=()=>{
        this.props.closeModal()
    }
    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <center>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>
                            <Card>
                                <CardHeader color="rose">
                                    <h4>Loan Summery<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>



                                </CardHeader>
                                <CardBody>

                                    <div className={classes.root}>
                                        <Grid container spacing={1}>
                                            <Grid  container item xs={12} spacing={5}>
                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderCustomerCreditCardSummeryInfo()}

                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </center>
            </section>

        );
    }
}

export default withStyles(styles)(CustomerCreditCard);
