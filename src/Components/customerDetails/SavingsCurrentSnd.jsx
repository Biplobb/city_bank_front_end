import React, {Component} from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Table from "../Table/Table";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import CircularProgress from '@material-ui/core/CircularProgress';
import CloseIcon from '@material-ui/icons/Close';

import Functions from '../../Common/Functions';

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

});

class SavingsCurrentSnd extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],

            accountDataFlag: null,
            redirectLogin:false,



        }
    }


    componentDidMount() {
        this.setState({accountDataFlag: false})
        if (this.props.ACCOUNTNO !== undefined && this.props.type !== undefined) {
            let url = backEndServerURL + "/demographic/account/" + this.props.type + "/" + this.props.ACCOUNTNO;
            console.log(url);
            let varValue = [];


            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(true);
                    console.log(response.data);
                    if (response.data[0] !== null) {


                        varValue["ACCOUNTNO"] = response.data[0].ACCOUNTNO;
                        varValue["ACCT_CRNCY_CODE"] = response.data[0].ACCT_CRNCY_CODE;
                        varValue["RATE"] = response.data[0].RATE;

                        varValue["LASTMONTHAVGBAL"] = response.data[0].LASTMONTHAVGBAL;
                        varValue["AVAILABLEBALANCE"] = response.data[0].AVAILABLEBALANCE;

                    } else {

                    }

                    this.setState({
                        varValue: varValue,
                        accountDataFlag: true

                    })

                })
                .catch((error) => {
                    console.log(error);

                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }

                    this.setState({accountDataFlag: true})
                });
        } else {

        }
    }

    renderaccountData = () => {
        if (this.state.accountDataFlag) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Account No", this.state.varValue["ACCOUNTNO"]],
                        ["Available Balance", this.state.varValue["AVAILABLEBALANCE"]],
                        ["Ledger Balance ", this.state.varValue[""]],
                        ["Currency", this.state.varValue["ACCT_CRNCY_CODE"]],
                        ["Interest Accured", this.state.varValue[""]],
                        ["Interest Rate", this.state.varValue["RATE"]],
                        ["Monthly Average Balance", this.state.varValue["LASTMONTHAVGBAL"]],


                    ]}
                    tableAllign={['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']}
                />
            )
        } else {
            return (<CircularProgress style={{marginLeft: '50%'}}/>)
        }
    }

    close=()=>{
        this.props.closeModal()
    }
    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>
                <center>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>
                            <Card>
                                <CardHeader color="rose">


                                    <h4>Account Summery<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>

                                </CardHeader>
                                <CardBody>

                                    <div className={classes.root}>
                                        <Grid container spacing={1}>
                                            <Grid container item xs={12} spacing={5}>
                                                <Grid item xs={12}>
                                                    <Paper className={classes.paper}>
                                                        {this.renderaccountData()}

                                                    </Paper>

                                                </Grid>


                                            </Grid>
                                        </Grid>
                                    </div>


                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </center>
            </section>

        );
    }
}

export default withStyles(styles)(SavingsCurrentSnd);
