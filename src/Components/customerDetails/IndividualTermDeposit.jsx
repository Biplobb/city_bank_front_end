import React, {Component} from "react";

import withStyles from "@material-ui/core/styles/withStyles";

import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Table from "../Table/Table";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {backEndServerURL} from "../../Common/Constant";
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from "axios";
import CloseIcon from '@material-ui/icons/Close';

import Functions from '../../Common/Functions';

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

});

class IndividualTermDeposit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            getData: false,
            termDepositData: null,

            redirectLogin:false,

        }
    }

    componentDidMount() {
        this.setState({
            termDepositData: false
        })
        if (this.props.ACCOUNTNO !== undefined && this.props.type !== undefined) {
            let url = backEndServerURL + "/demographic/account/" + this.props.type + "/" + this.props.ACCOUNTNO;

            let varValue = [];


            axios.get(url, {withCredentials: true})
                .then((response) => {

                    console.log(response.data);
                    if (response.data[0] !== null) {


                        varValue["ACCOUNT"] = response.data[0].FORACID;
                        varValue["CURRENCY"] = response.data[0].CURRENCY;
                        varValue["TENOR"] = response.data[0].TENOR;

                        varValue["BRANCHNAME"] = response.data[0].BRANCHNAME;
                        varValue["BALANCE"] = response.data[0].BALANCE;
                        varValue["INTERESTRATE"] = response.data[0].INTERESTRATE;
                        if (response.data[0].INITIALOPENDATE !== null) {
                            const INITIALOPENDATE = response.data[0].INITIALOPENDATE.split('T');
                            varValue["INITIALOPENDATE"] = INITIALOPENDATE[0];
                        } else {
                            varValue["INITIALOPENDATE"] = "";
                        }

                        varValue["INTPRFTPAID"] = response.data[0].INTPRFTPAID;

                        varValue["TAXPRFPAID"] = response.data[0].TAXPRFPAID;
                        varValue["TINTPRFTPAID"] = response.data[0].TINTPRFTPAID;
                        varValue["TTINTPRFTPAID"] = response.data[0].TTINTPRFTPAID;

                        if (response.data[0].INTMATURUTYDATE !== null) {
                            const INTMATURUTYDATE = response.data[0].INTMATURUTYDATE.split('T');
                            varValue["INTMATURUTYDATE"] = INTMATURUTYDATE[0];
                        } else {
                            varValue["INTMATURUTYDATE"] = "";
                        }


                        varValue["MATURITYINSTRUCTION"] = response.data[0].MATURITYINSTRUCTION;
                    } else {

                    }

                    this.setState({
                        varValue: varValue,
                        termDepositData: true

                    })

                })
                .catch((error) => {
                    console.log(error);

                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }

                    this.setState({termDepositData: true})

                });
        } else {

        }
    }

    renderTermDepositData = () => {
        if (this.state.termDepositData) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Account No", this.state.varValue["ACCOUNT"]],
                        ["Tenor", this.state.varValue["TENOR"]],
                        ["Branch Name", this.state.varValue["BRANCHNAME"]],
                        ["Currency", this.state.varValue["CURRENCY"]],
                        ["Balance", this.state.varValue["BALANCE"]],
                        ["Interest Rate", this.state.varValue["INTERESTRATE"]],
                        ["Initial Open Date", this.state.varValue["INITIALOPENDATE"]],
                        ["Current Cycle Interest/Profit Paid", this.state.varValue[""]],


                    ]}
                    tableAllign={['left', 'left']}
                />
            )
        } else {
            return (<CircularProgress style={{marginLeft: '50%'}}/>)
        }
    }
    renderTermDepositData2 = () => {
        if (this.state.termDepositData) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Last Cycle Interest/Profit Paid", this.state.varValue["INTPRFTPAID"]],
                        ["Last Cycle Tax Deducted on Interest/Profit Paid", this.state.varValue["TAXPRFPAID"]],
                        ["Total Interest/Profit Paid", this.state.varValue["TINTPRFTPAID"]],
                        ["Total Tax Deducted on Interest/Profit Paid", this.state.varValue["TTINTPRFTPAID"]],
                        ["Initial Maturity Date", this.state.varValue["INTMATURUTYDATE"]],
                        ["Maturity Instruction", this.state.varValue["MATURITYINSTRUCTION"]],


                    ]}
                    tableAllign={['left', 'left']}
                />
            )
        } else {
            return (<CircularProgress style={{marginLeft: '50%'}}/>)
        }
    }
    close=()=>{
        this.props.closeModal()
    }
    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <section>
                <center>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>
                            <Card>
                                <CardHeader color="rose">


                                    <h4>Term Deposit Summery<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                                </CardHeader>
                                <CardBody>

                                    <div className={classes.root}>
                                        <Grid container spacing={1}>
                                            <Grid container item xs={12} spacing={5}>


                                                <Grid item xs={6}>


                                                    <Paper className={classes.paper}>

                                                        {this.renderTermDepositData()}
                                                    </Paper>

                                                </Grid>
                                                <Grid item xs={6}>


                                                    <Paper className={classes.paper}>
                                                        {this.renderTermDepositData2()}

                                                    </Paper>

                                                </Grid>

                                            </Grid>
                                        </Grid>
                                    </div>


                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </center>
            </section>

        );
    }
}

export default withStyles(styles)(IndividualTermDeposit);
