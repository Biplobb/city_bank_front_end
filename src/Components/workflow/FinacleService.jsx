import React,{Component} from 'react';

import OpeningCS from "./fdr/OpeningCS";
import {Menu, MenuItem} from "@material-ui/core";
import GridItem from "../Grid/GridItem";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../Card/CardBody";
import GridContainer from "../Grid/GridContainer";
import AccountOpeningForm from "./AccountOpeningForm";
import AccountList from "./fdr/AccountList";
import {CSjsonFormIndividualAccountOpeningSearch} from "./WorkflowJsonForm2";
import {
    CSJsonFormForCasaIndividualTagFdr,
    CSJsonFormForCasaIndividualTagFdr2,
} from "./WorkflowJsonFormArin";
class FinacleService extends Component{
    constructor(props) {
        super(props);
        this.state =  {
            selectedMenu : "NOTSELECTED"
        }
    }

    handleMenuSelect(menu){
        this.setState({selectedMenu : menu})
    }

    renderServiceMenu() {
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4>Customer Services<a><CloseIcon onClick={this.props.closeModal} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                <MenuItem onClick={(event) => {
                                    this.handleMenuSelect("accountOpening")
                                }}>Account Opening</MenuItem>
                                <MenuItem onClick={(event) => {
                                    this.handleMenuSelect("fdrOpening")
                                }}>FDR Opening</MenuItem>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

    render() {
        if (this.state.selectedMenu === "accountOpening"){
            return (
                <div>
                    <AccountOpeningForm
                        closeModal={this.props.closeModal}
                        getAccountType={this.props.getAccountType}
                        accountType={this.props.accountType}
                        serviceType={this.props.serviceType}
                        workplace={this.props.workplace}
                        jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                        individualDedupData={this.props.individualDedupData}
                        jointDedupData={this.props.jointDedupData}
                        companyDedupData={this.props.companyDedupData}
                        commonJsonForm={this.props.commonJsonForm}
                        subServiceType={this.props.subServiceType}
                        searchValue={this.props.searchValue}  />
                </div>
            );

        }
        else if (this.state.selectedMenu === "fdrOpening"){
            return (
                <div>
                    <AccountList
                                closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                customerId = {this.props.getCustomerId}
                                serviceType="FdrOpening"
                                commonJsonForm={CSJsonFormForCasaIndividualTagFdr}
                                serviceJsonForm={CSJsonFormForCasaIndividualTagFdr2}
                                subServiceType="IndividualFdrOpening"
                                searchValue={this.props.searchValue}/>
                     </div>
            );

        }
        else{
            return this.renderServiceMenu();
        }
    }
}

export default FinacleService;