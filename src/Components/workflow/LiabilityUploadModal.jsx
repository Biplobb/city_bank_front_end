import {
    BMjsonFormIndividualAccountOpeningSearch,
} from "./WorkflowJsonForm3";
import React, {Component} from "react";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import GridContainer from "../Grid/GridContainer";
import GridItem from "../Grid/GridItem";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';

import {ThemeProvider} from "@material-ui/styles";
import theme from "../JsonForm/CustomeTheme";
import FileTypeComponent from "../JsonForm/FileTypeComponent";
import SelectComponent from "../JsonForm/SelectComponent";
import CircularProgress from "@material-ui/core/CircularProgress";
import {DropdownContext} from "reactstrap/es/DropdownContext";
import DropdownComponent from "../JsonForm/DropdownComponent";


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});

var fileUpload = {
    "varName": "scanningFile",
    "type": "file",
    "label": "Scan and Upload File",
    "grid": 12
};
var selectFileName = {
    "varName": "fileName",
    "type": "dropdown",
    "required":true,
    "label": "This photo name",

    "grid": 6
}

class LiabilityUploadModal extends Component {
    state = {
        fileUploadData: {},
        getSplitFile: [],
        multipleScanningphotoShow: null,
        inputData: {},
        dropdownSearchData: {}

    }

    constructor(props) {
        super(props);


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    close = () => {

        this.props.closeModal();
    }
    handleSubmit = (event) => {

        event.preventDefault();
        this.setState({
            multipleScanningphotoShow: false
        })
        if (!this.state.multipleScanningphotoShow) {
            const formData = new FormData();
            formData.append("appId", this.props.appId);
            formData.append("file", this.state.fileUploadData[fileUpload.varName]);
            axios({
                method: 'post',
                url: backEndServerURL + '/case/split',
                data: formData,
                withCredentials: true,
                headers: {'content-type': 'multipart/form-data'}
            })
                .then((response) => {
                    this.setState({
                        getSplitFile: response.data,
                        multipleScanningphotoShow: true
                    })
                    console.log(response.data);


                })
                .catch((error) => {
                    console.log(error)
                })
        } else {


            let url = backEndServerURL + "/case/mapFiles";
            axios.post(url, {appId: this.props.appId, fileNames: this.state.inputData}, {withCredentials: true})
                .then((response) => {
                    this.props.closeModal(response.data);
                    let image=response.data;
                    this.setState({
                        multipleScanningphotoShow:null
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }

    }

    renderDropdownData = (varName) => {

        this.state.dropdownSearchData[varName] = [

            {
                "label": "PASSPORT",
                "value": "PASSPORT"
            },
            {
                "label": "NID",
                "value": "NID"
            },
            {
                "label": "AOF1",
                "value": "AOF1"
            },
            {
                "label": "AOF2",
                "value": "AOF2"
            },
            {
                "label": "AOF3",
                "value": "AOF3"
            },
            {
                "label": "AOF4",
                "value": "AOF4"
            },
            {
                "label": "AOF5",
                "value": "AOF5"
            },
            {
                "label": "AOF6",
                "value": "AOF6"
            },
            {
                "label": "AOF7",
                "value": "AOF7"
            },
            {
                "label": "AOF8",
                "value": "AOF8"
            },
            {
                "label": "TP",
                "value": "TP"
            },
            {
                "label": "KYC1",
                "value": "KYC1"
            }, {
                "label": "KYC2",
                "value": "KYC2"
            },
            {
                "label": "IIF1",
                "value": "IIF1"
            },
            {
                "label": "IIF2",
                "value": "IIF2"
            },
            {
                "label": "AM01",
                "value": "AM01"
            },
            {
                "label": "AM02",
                "value": "AM02"
            }

        ];


    }
    renderSelectOption = (data) => {
        return (
            {
                "varName": data,
                "type": "dropdown",
                "label": "This photo name",
                "grid": 6,

            }
        )
    }

    renderMultipleScanningphotoShow = () => {
        if (this.state.multipleScanningphotoShow) {
            return (
                <Grid container spacing={1}>
                    {
                        this.state.getSplitFile.map((data) => {
                            return (

                                <Grid item xs={4}>
                                    <div>
                                        <br/>
                                        {DropdownComponent.dropdown(this.state, this.updateComponent, this.renderSelectOption(data))}
                                        <br/>
                                        {this.renderDropdownData(data)}
                                        <br/>

                                    </div>
                                    <img width='90%' src={backEndServerURL + "/file/" + data} alt=""/>

                                    <br/>
                                </Grid>

                            )
                        })

                    }
                </Grid>
            )
        } else if (this.state.multipleScanningphotoShow === false) {
            return (

                <CircularProgress style={{marginLeft: '33%'}}/>


            )
        }
        //this.updateComponent();
    }

    render() {
        const {classes} = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">

                            <h4>Scanning File<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                        </CardHeader>
                        <CardBody>
                            <br/>

                            <ThemeProvider theme={theme}>
                                <div>
                                    <Grid container spacing={3}>
                                        {FileTypeComponent.file(this.state, this.updateComponent, fileUpload)}
                                        {this.renderMultipleScanningphotoShow()}
                                    </Grid>
                                </div>


                                <div>

                                    <br/>
                                    <br/>
                                    <center>
                                        <button
                                            className="btn btn-outline-danger"
                                            style={{
                                                verticalAlign: 'right',

                                            }}

                                            type='button' value='add more'
                                            onClick={this.handleSubmit}
                                        >Submit
                                        </button>
                                    </center>

                                </div>
                            </ThemeProvider>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(LiabilityUploadModal);