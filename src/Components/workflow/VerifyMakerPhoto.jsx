import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {backEndServerURL} from "../../Common/Constant";
import Functions from '../../Common/Functions';
import GridList from "@material-ui/core/GridList";
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    gridList: {
        width: 500,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },

};


class VerifyMakerPhoto extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            appData: {},
            getData: false,
            varValue: [],
            showValue: false,
            redirectLogin: false,
            title: "",
            notificationMessage: "",
            alert: false,
            inputData: {},
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},
            values: [],
            customerName: [],
            deferalType: [],
            expireDate: [],
            other: [],
            getCheckerList: [],
            getAllDefferal: [],
            getDeferalList: [],
            loading: false,
            jointAccountCustomerNumber: 0,
            objectForJoinAccount: [],
            getgenerateForm: false,
        };


    }


    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };





   renderImage=()=>{
       let aof1, aof2, aof3, aof4, tp, iif1, iif2, kyc1, kyc2,am1,am2;

           this.props.documentList.map((document) => {
               if (document.indexOf("AOF1") > -1)
                   aof1 = document;
               else if (document.indexOf("AOF2") > -1)
                   aof2 = document;
               else if (document.indexOf("AOF3") > -1)
                   aof3 = document;
               else if (document.indexOf("AOF4") > -1)
                   aof4 = document;
               else if (document.indexOf("TP") > -1)
                   tp = document;
               else if (document.indexOf("KYC1") > -1)
                   kyc1 = document;
               else if (document.indexOf("KYC2") > -1)
                   kyc2 = document;
               else if (document.indexOf("IIF1") > -1)
                   iif1 = document;
               else if (document.indexOf("IIF2") > -1)
                   iif2 = document;
               else if (document.indexOf("AM01") > -1)
                   am1 = document;
               else if (document.indexOf("AM02") > -1)
                   am2 = document;

           })
           return (

               <React.Fragment>

                   <img width='100%' src={backEndServerURL + "/file/" + aof1} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + aof2} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + aof3} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + aof4} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + tp} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + kyc1} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + kyc2} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + iif1} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + iif2} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + am1} alt=""/>
                   <img width='100%' src={backEndServerURL + "/file/" + am2} alt=""/>


               </React.Fragment>

           )

       }





    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

            return (
                <GridList  cellHeight={800} cols={1} >
                    <div>
                        {this.renderImage()}
                    </div>
                </GridList>

            )

    }


}

export default withStyles(styles)(VerifyMakerPhoto);
