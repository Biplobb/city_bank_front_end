import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import {ThemeProvider} from "@material-ui/styles";
import CloseIcon from '@material-ui/icons/Close';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import SingleImageShow from "./../SingleImageShow";
import AccountNoGenerate from "./../AccountNoGenerate";
import LiabilityUploadModal from "./../LiabilityUploadModal";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};
var csDeferral = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferral",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther = {
    "varName": "deferalOther",
    "type": "text",
    "label": "Please Specify",
    "grid": 6
};

var deferal = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

let JsonFormCasaIndividualDeferral = {

    "type": {
        "varName": "type",
        "label": "Deferral Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}

var csBearer = {
    "varName": "csBearer",
    "type": "text",
    "label": "Need Bearer",

    "grid": 6
};
var remarks = [
    {
        "varName": "remarksCall",
        "type": "text",
        "label": "Remarks",
        "grid": 12,


    },
];
var bearerApproval = {
    "varName": "bearerApproval",
    "type": "text",
    "label": "Bearer Approval",
    "grid": 12,

};
const bearerCallApproval = {
    "varName": "bearerApproval",
    "type": "text",
    "label": "Approval",
    "grid": 12,
    "readOnly": true
};

const maintenanceList = [
    {
        " varName": "12DigitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing PAN GIR No TIN Update",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New PAN GIR No TIN Update",
        "grid": 5,
        "conditionalVarName": "12DigitTinChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "dOBNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "postal CodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "postal CodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1 ",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1 ",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Guardian Existing Country",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing Postal Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing City Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Guardian's Existing State Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Guardian Existing Address",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": " Photo Id Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1 ",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": "true",
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": "true",
    },

    {
        " varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Despatch ModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Despatch ModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": "true",
    },


    {
        " varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE ",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditionalVarName": "dobUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditionalVarName": "fatherNameUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditionalVarName": "motherNameUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        // "conditionalVarName":"fatherNameUpdateChange",
        // "conditionalVarValue":"true",
    },

    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        " varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Account StatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Account StatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "grid": 5,
        "readOnly": true,

    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "grid": 5,
        "readOnly": true,

    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "grid": 5,
        "readOnly": true,

    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        " varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Cash Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Cash Limit (DR/CR)New",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": "true",
    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "readOnly": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": "true",
    },

    {
        " varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Clearing Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Clearing Limit (DR/CR)New",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Transfer Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Transfer Limit (DR/CR)New",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": "true",
    },

    {
        " varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Sector CodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Sector CodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Sub Sector CodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Sub Sector CodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": "true",
    },


    {
        " varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "Free Code 3 Old",
        "type": "text",
        "label": "Existing Free Code 3 ",
        "grid": 5,
        "readOnly": true,
    },
    {
        "varName": "Free Code 3 New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": "true",
    },

]

class VerifyCallCenter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            csBearer:'',
            varValue: [],
            getDedupData: false,
            dedupData: [[" ", " "]],
            relatedData: [[" ", " "]],
            tableData: [],
            sourceMappingData: [],
            getsearchValue: [],
            getCustomerId: '',
            getAccountType: '',
            accountOpeningFromModal: false,
            SelectedData: '',
            tabMenuSelect: 'INDIVIDUAL',
            existingAcoountOpeningModal: false,
            newAcoountOpeningModal: false,
            searchTableData: null,
            searchTableRelatedData: null,
            oldAccountData: [],

            dataNotFound: false,
            CustomerModal: false,
            uniqueId: '',
            IDENTIFICATION_NO: '',
            id: '',
            alert: false,
            value: "INDIVIDUAL",
            NonIndividualabel: "",
            individualLabel: "Individual A/C",
            content: "INDIVIDUAL",
            anchorEl: null,
            anchorE2: null,
            individualDropdownOpen: null,
            objectForJoinAccount: {},
            numberOfJointMember: "",
            notificationMessage: "CB number / NID / Passport / Birth Certificate or Driving License is Required!!",

            accountNumber: "",
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},

            csDeferralPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            caseId: "",
            title: "",
            app_uid: "-1",
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferral: "",
            },
            fileUploadData: {},
            AddDeferral: false,
            debitCard: "",
            showValue: false,
            getDeferralList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            maintenanceType: '',
            getDocument: false,
            // customerName: 'Jamal',
            // cbNumber: 12345,
            digitTIN: false,
            titleChange: false,
            nomineeUpdate: false,
            updateChangePhotoId: false,
            contactNumberChange: false,
            emailAddressChange: false,
            estatementEnrollment: false,
            addressChange: false,
            otherInformationChange: false,
            signatureCard: false,
            dormantAccountActivation: false,
            dormantAccountDataUpdate: false,
            schemeMaintenanceLinkChange: false,
            schemeMaintenance: false,
            mandateUpdateChange: false,
            cityLive: false,
            projectRelatedDataUpdateADUP: false,
            accountSchemeClose: false,
            lockerSIOpen: false,
            lockerSIClose: false,
            others: false,
            bearerApproval: '',


        }
    }
    renderRemarks = () => {
        if (this.state.getData) {
            return (


                CommonJsonFormComponent.renderJsonForm(this.state, remarks, this.updateComponent)


            )
        }
    }
    handleChange = (event, value) => {

        this.state.inputData["csDeferral"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }

    addDeferralForm() {
        if (this.state.inputData["csDeferral"] === "YES") {
            return this.state.values.map((el, i) =>
                <React.Fragment>

                    <Grid item xs="6">
                        {
                            this.dynamicDeferral(el)
                        }
                    </Grid>
                    <Grid item xs="6">
                        {this.dynamicDeferralOther(el)}
                    </Grid>
                    <Grid item xs="6">
                        {
                            this.dynamicDate(el)
                        }
                    </Grid>


                    <Grid item xs="3">
                        <button
                            style={{float: 'right',}}
                            className="btn btn-outline-danger"
                            type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                        >
                            Remove
                        </button>
                    </Grid>

                </React.Fragment>
            )
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferralList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferralList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {
        if (this.state.inputData["csDeferral"] === "YES") {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'left',

                    }}

                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}


                >Add Deferral</button>
            )
        } else {
            return;
        }
    }
    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    renderSelectMenu = () => {

        if (this.props.appId !== undefined) {
            return (

                <Grid item xs='12'>


                    <TextField

                        value="YES"
                        label="Need Deferral"

                        InputProps={{
                            readOnly: true
                        }}
                    />


                </Grid>

            )

        } else {
            // console.log(this.state.inputData["csDeferral"]);
            return (


                <Grid item xs='12'>
                    <FormLabel component="legend">Need Deferral</FormLabel>
                    <RadioGroup aria-label="csDeferral" name="csDeferral" value={this.state.inputData["csDeferral"]}
                                onChange={this.handleChange}>
                        <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                        <FormControlLabel value="NO" control={<Radio/>} label="NO"/>

                    </RadioGroup>

                </Grid>

            )
        }


    }
    renderButton = () => {
        if (this.state.getData) {
            return (

                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmit}
                    >Submit
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmitReturn}
                    >Return
                    </button>
                </div>

            )
        }
    }
    handleSubmitReturn = (event) => {
        event.preventDefault();

        if (this.state.inputData.cs_bearer === "CALL_CENTER")
            this.state.inputData.call_center_approval = "RETURN";

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }

    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }
    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }


    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false
        })
    }
    closeUploadModal = (data) => {
        this.setState({
            uploadModal: false,
            imgeListLinkSHow: true,
            getImageLink: data
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    bearerApproval = () => {
        if (this.state.inputData["csBearer"] === "YES") {

            return (

                <Grid item xs={12}>
                    {TextFieldComponent.text(this.state, this.updateComponent, bearerApproval)}
                </Grid>

            )

        }
        return;
    }

    componentDidMount() {
        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log("call Variable")
                    console.log(response.data)

                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {
                            let imageUrl = backEndServerURL + "/case/files/" + this.props.appId;
                            axios.get(imageUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    this.setState({
                                        getImageLink: response.data,
                                        getImageBoolean: true
                                    })
                                })
                                .catch((error) => {
                                    console.log(error);
                                })
                            console.log(response.data);
                            let tableArray = [];
                            response.data.map((deferal) => {
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, deferal.status));

                            });
                            this.setState({
                                getDeferralList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        showValue: true,
                        inputData: response.data,
                        varValue: response.data,
                        appData: response.data


                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }
    renderBearerApproval = () => {
        if (this.state.getData) {

            return (

                <Grid item xs={12}>
                    {TextFieldComponent.text(this.state, this.updateComponent, csBearer)}
                </Grid>

            )

        }
        return;
    }

    renderSearchData = () => {

        if (this.state.showValue) {

            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>


                        <Grid container spacing={1}>
                            <ThemeProvider theme={theme}>
                                <br/><br/>
                                <Grid item xs={6}>
                                    <label for="customerName"><font size="3"><b> Customer Name :</b></font></label>

                                    {this.state.customerName}</Grid>
                                <Grid item xs={6}>
                                    <label for="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                    {this.state.cbNumber}
                                </Grid>
                                {
                                    CommonJsonFormComponent.renderJsonForm(this.state, this.props.jsonForm, this.updateComponent)
                                }
                            </ThemeProvider>
                        </Grid>

                        <br/>
                        {this.renderBearerApproval()}
                        <br/>
                        <Grid container spacing={3}>
                            <ThemeProvider theme={theme}>

                                <Grid item xs='12'>
                                    {this.bearerApproval()}
                                </Grid>


                                <br/><br/>


                                {this.renderRemarks()}
                            </ThemeProvider>
                        </Grid>

                        <ThemeProvider theme={theme}>
                            <Grid container spacing={1}>
                                {this.renderImageLink()}
                            </Grid>
                        </ThemeProvider>
                        <br/><br/><br/>
                        {/*{this.renderUploadButton()}*/}
                        <Dialog
                            fullWidth="true"
                            maxWidth="md"
                            open={this.state.accountDetailsModal}>
                            <DialogContent>

                                <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.uploadModal}>
                            <DialogContent>

                                <LiabilityUploadModal appId={this.state.appId}
                                                      closeModal={this.closeUploadModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.imageModalBoolean}>
                            <DialogContent>

                                <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                            </DialogContent>
                        </Dialog>
                        <br/>
                        <br/>

                        {this.renderButton()}


                    </GridItem>
                </GridContainer>

            )
        }
    }


    handleSubmit = (event) => {
        if (this.state.getData)
            event.preventDefault();

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        let data = this.state.inputData;
        this.state.inputData.cs_bearer = "APPROVED";
        this.state.inputData.call_center_approval = "APPROVED";
        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log("call variable");
                console.log(response.data);
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {

                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true,

                        })
                        this.props.closeModal()


                    })
                    .catch((error) => {
                        console.log(error);
                        /* if(error.response.status===452){
                             Functions.removeCookie();

                             this.setState({
                                 redirectLogin:true
                             })

                         }*/
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }

    render() {
        // console.log(this.state.inputData)
        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }


        return (

            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>Account
                                Maintenance</h4>
                        </CardHeader>
                        <CardBody>
                            <div>

                                <br/>
                                <br/>
                                {this.renderSearchData()}
                                <br/>
                                <br/>


                            </div>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );


    }

}

export default withStyles(styles)(VerifyCallCenter);
