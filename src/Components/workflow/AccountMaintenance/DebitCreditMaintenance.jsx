import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import FormControl from '@material-ui/core/FormControl';
import Notification from "../../NotificationMessage/Notification";
import Grow from "@material-ui/core/Grow";
import {ThemeProvider} from "@material-ui/styles";
import CloseIcon from '@material-ui/icons/Close';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import SingleImageShow from "../SingleImageShow";
import AccountNoGenerate from ".././AccountNoGenerate";
import {CSExistJsonFormIndividualAccountOpening} from "../WorkflowJsonForm3";
import LiabilityUploadModal from "../LiabilityUploadModal";
import FormSample from "../../JsonForm/FormSample";
import {CSjsonFormIndividualAccountOpeningSearch} from "../WorkflowJsonForm";
import Fab from "@material-ui/core/Fab";
import Pageview from "@material-ui/core/SvgIcon/SvgIcon";
import Checkbox from "@material-ui/core/Checkbox";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};
var csDeferal = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferral",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther = {
    "varName": "deferalOther",
    "type": "text",
    "label": "Please Specify",
    "grid": 6
};

var Deferral = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

let JsonFormCasaIndividualDeferal = {

    "type": {
        "varName": "type",
        "label": "Deferral Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}


let accountMaintenanceSearch = [
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account No",

    }
]
var bearerApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Bearer Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
var bearerBomApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
const debitCreditMaintenanceList = [
    {" varName":"amexGoldUpgradation",
        "type":"checkbox",
        "label":"Amex Gold Up-gradation",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftPriority",
        "type":"checkbox",
        "label":"Amex Platinum Gift - 6 Priority Pass Free Visits",
        "grid":12,

    },

    {" varName":"amexPlatinumGiftSquare",
        "type":"checkbox",
        "label":"Amex Platinum Gift  Square Hospital Voucher",
        "grid":12,

    },


    {"varName":"amexPlatinumGiftAirlines",
        "type":"checkbox",
        "label":"Amex Platinum gift- Airlines voucher",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftMRpoint",
        "type":"checkbox",
        "label":"Amex platinum gift-25000 MR point",
        "grid":12
    },

    {"varName":"amexPlatinumGiftAviation",
        "type":"checkbox",
        "label":"Amex Platinum Gift-Partex Aviation",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftResortvoucher",
        "type":"checkbox",
        "label":"Amex Platinum gift-Resort voucher",
        "grid":12,

    },


    {" varName":"amexPlatinumUpgradation",
        "type":"checkbox",
        "label":"Amex Platinum Up-gradation",
        "grid":12,

    },



    {" varName":"applicationstatusquery",
        "type":"checkbox",
        "label":"Application status query",
        "grid":12,

    },


    {"varName":"aTMdisputeadjustment",
        "type":"checkbox",
        "label":"ATM dispute adjustment",
        "grid":12,

    },

    {
        "varName": "autoDebitEnrollment",
        "type": "checkbox",
        "label": "Auto Debit Enrollment",
        "grid": 12,
    },

    {" varName":"autodebitstandinginstructioncancellation",
        "type":"checkbox",
        "label":"Auto debit/standing instruction cancellation",
        "grid":12,

    },

    {
        "varName": "balanceTransferAmex",
        "type": "checkbox",
        "label": "Balance Transfer (Amex)",
        "grid": 12,

    },

    {
        "varName": "balanceTransferVisa",
        "type": "checkbox",
        "label": "Balance Transfer (Visa)",
        "grid": 12,

    },

    {
        "varName": "captureCreditCardReplacement",
        "type": "checkbox",
        "label": "Capture Credit Card Replacement",
        "grid": 12,

    },

    {
        "varName": "cardchequereissue",
        "type": "checkbox",
        "label": "Card cheque reissue",
        "grid": 12,

    },

    {
        "varName": "cardChequereturnnotreceived",
        "type": "checkbox",
        "label": "Card Cheque return/not received",
        "grid": 12,

    },

    {" varName":"cardnotreceivedAmex",
        "type":"checkbox",
        "label":"Card not received (Amex)",
        "grid":12,

    },

    {
        "varName": "cardnotreceivedVisa",
        "type": "checkbox",
        "label": "Card not received (Visa)",
        "grid": 12,

    },

    {
        "varName": "cardprintingcorrection",
        "type": "checkbox",
        "label": "Card printing correction",
        "grid": 12,

    },
    //////////
    {" varName":"certificatefeesrealization",
        "type":"checkbox",
        "label":"Certificate fees realization",
        "grid":12,

    },

    {"varName":"cityTouchbillpaymentDispute",
        "type":"checkbox",
        "label":"City Touch bill payment Dispute",
        "grid":12,

    },

    {" varName":"cityTouchCreditCardpaymentdispute",
        "type":"checkbox",
        "label":"City Touch Credit Card payment dispute",
        "grid":12,

    },


    {"varName":"cityTouchonlineshoppingdispute",
        "type":"checkbox",
        "label":"City Touch online shopping dispute",
        "grid":12,

    },

    {"varName":"citytouchDisputeAccounts",
        "type":"checkbox",
        "label":"Citytouch Dispute-Accounts",
        "grid":12
    },

    {"varName":"annualFeereversalunderCorporateAgreement",
        "type":"checkbox",
        "label":"Annual Fee reversal under Corporate Agreement",
        "grid":12,

    },

    {"varName":"creditbalancetransferAmex",
        "type":"checkbox",
        "label":"Credit balance transfer (Amex)",
        "grid":12,

    },


    {" varName":"creditbalancetransferVisa",
        "type":"checkbox",
        "label":"Credit balance transfer (Visa)",
        "grid":12,

    },



    {" varName":"creditCardRenewalRequest",
        "type":"checkbox",
        "label":"Credit Card Renewal Request",
        "grid":12,

    },


    {"varName":"creditCardReplaceRequest",
        "type":"checkbox",
        "label":"Credit Card Replace Request",
        "grid":12,

    },

    {
        "varName": "creditCardUpgradationRequest",
        "type": "checkbox",
        "label": "Credit Card Upgradation Request",
        "grid": 12,
    },

    {" varName":"carddatacorrection",
        "type":"checkbox",
        "label":"Card data correction",
        "grid":12,

    },

    {
        "varName": "delinquentCreditCardReissueRenewal",
        "type": "checkbox",
        "label": "Delinquent Credit Card Reissue/Renewal",
        "grid": 12,

    },

    {
        "varName": "eCommerceOTPIssue",
        "type": "checkbox",
        "label": "E-Commerce OTP Issue",
        "grid": 12,

    },

    {
        "varName": "estatementnotreceived",
        "type": "checkbox",
        "label": "E-statement not received",
        "grid": 12,

    },

    {
        "varName": "earlysettlementforFlexibuyEMIFlexiloan",
        "type": "checkbox",
        "label": "Early settlement for Flexi buy /EMI/ Flexi loan",
        "grid": 12,

    },

    {
        "varName": "eFTNCancelationRequestRegular",
        "type": "checkbox",
        "label": "EFTN Cancelation Request (Regular)",
        "grid": 12,

    },

    {" varName":"eFTNRequestUSDBDT",
        "type":"checkbox",
        "label":"EFTN Request USD & BDT",
        "grid":12,

    },

    {
        "varName": "eFTNRequestUSDBDTDone",
        "type": "checkbox",
        "label": "EFTN Request USD (BDT Done)",
        "grid": 12,

    },

    {
        "varName": "eMIrequest",
        "type": "checkbox",
        "label": "EMI request",
        "grid": 12,

    },
    {" varName":"fDRencashment",
        "type":"checkbox",
        "label":"FDR encashment",
        "grid":12,

    },

    {"varName":"fDRencashmentClosurerequest",
        "type":"checkbox",
        "label":"FDR encashment & Closure request",
        "grid":12,

    },

    {" varName":"feereversalforCreditCardAmex",
        "type":"checkbox",
        "label":"Fee reversal for Credit Card (Amex)",
        "grid":12,

    },


    {"varName":"feereversalforCreditCardVisa",
        "type":"checkbox",
        "label":"Fee reversal for Credit Card (Visa)",
        "grid":12,

    },

    {"varName":"fundtransferVisa",
        "type":"checkbox",
        "label":"Fund transfer (Visa)",
        "grid":12
    },

    {"varName":"fundtransferAmex",
        "type":"checkbox",
        "label":"Fund transfer (Amex)",
        "grid":12,

    },

    {"varName":"giftCardReplacementRequest",
        "type":"checkbox",
        "label":"Gift Card Replacement Request",
        "grid":12,

    },


    {" varName":"hardCopySoftCopyEnableRequest",
        "type":"checkbox",
        "label":"Hard Copy / Soft Copy Enable Request",
        "grid":12,

    },



    {" varName":"hardcopystatementrelatedissue",
        "type":"checkbox",
        "label":"Hard copy statement related issue",
        "grid":12,

    },


    {"varName":"insuranceSchemeDeEnrollmentrequest",
        "type":"checkbox",
        "label":"Insurance Scheme De-Enrollment request",
        "grid":12,

    },

    {
        "varName": "insuranceSchemeenrollmentRequest",
        "type": "checkbox",
        "label": "Insurance Scheme enrollment Request",
        "grid": 12,
    },

    {" varName":"lienMarking",
        "type":"checkbox",
        "label":"Lien Marking",
        "grid":12,

    },

    {
        "varName": "lienwithdraw",
        "type": "checkbox",
        "label": "Lien withdraw",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawcarddueadjustment",
        "type": "checkbox",
        "label": "Lien withdraw & card dues adjustment",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawforinteresttransfer",
        "type": "checkbox",
        "label": "Lien withdraw for interest transfer",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawadjustmentRelien",
        "type": "checkbox",
        "label": "Lien withdraw-adjustment- Re-lien",
        "grid": 12,

    },

    {
        "varName": "limitRearrange",
        "type": "checkbox",
        "label": "Limit Re-arrange",
        "grid": 12,

    },

    {" varName":"limitRectificationRequest",
        "type":"checkbox",
        "label":"Limit Rectification Request",
        "grid":12,

    },

    {
        "varName": "mRCardFeeRedemption",
        "type": "checkbox",
        "label": "MR Card Fee Redemption",
        "grid": 12,

    },

    {
        "varName": "mRPointsReversalIssues",
        "type": "checkbox",
        "label": "MR Points Reversal Issues",
        "grid": 12,

    },
    //////////
    {" varName":"mRRedemptionforCapturedcardFees",
        "type":"checkbox",
        "label":"MR Redemption for Captured card Fees",
        "grid":12,

    },

    {"varName":"mRRedemptionforCardReplacementFees",
        "type":"checkbox",
        "label":"MR Redemption for Card Replacement Fees",
        "grid":12,

    },

    {" varName":"mRRedemptionforCertificateFees",
        "type":"checkbox",
        "label":"MR Redemption for Certificate Fees",
        "grid":12,

    },


    {"varName":"mRRedemptionforChequeReturnFees",
        "type":"checkbox",
        "label":"MR Redemption for Cheque Return Fees",
        "grid":12,

    },

    {"varName":"mRRedemptionforCIBFees",
        "type":"checkbox",
        "label":"MR Redemption for CIB Fees",
        "grid":12
    },

    {"varName":"mRredemptionforGiftCard",
        "type":"checkbox",
        "label":"MR redemption for Gift Card",
        "grid":12,

    },

    {"varName":"mRRedemptionforLatePaymentFees",
        "type":"checkbox",
        "label":"MR Redemption for Late Payment Fees",
        "grid":12,

    },


    {" varName":"mRRedemptionforOutstandingBDT",
        "type":"checkbox",
        "label":"MR Redemption for Outstanding (BDT)",
        "grid":12,

    },



    {" varName":"mRRedemptionforOutstandingUSD",
        "type":"checkbox",
        "label":"MR Redemption for Outstanding (USD)",
        "grid":12,

    },


    {"varName":"mRRedemptionforOverLimitFees",
        "type":"checkbox",
        "label":"MR Redemption for Over Limit Fees",
        "grid":12,

    },

    {
        "varName": "mRRedemptionPINReplacementFees",
        "type": "checkbox",
        "label": "MR Redemption for PIN Replacement Fees",
        "grid": 12,
    },

    {" varName":"mRRedemptionSMSFees",
        "type":"checkbox",
        "label":"MR Redemption for SMS Fees",
        "grid":12,

    },

    {
        "varName": "mRRedemptionStatementRetrievalFees",
        "type": "checkbox",
        "label": "MR Redemption for Statement Retrieval Fees",
        "grid": 12,

    },

    {
        "varName": "mRVoucherReissue",
        "type": "checkbox",
        "label": "MR Voucher Reissue",
        "grid": 12,

    },

    {
        "varName": "passportInformationupdaterequest",
        "type": "checkbox",
        "label": "Passport Information update request",
        "grid": 12,

    },

    {
        "varName": "paymentUpdatewrongPart",
        "type": "checkbox",
        "label": "Payment Update wrong Part",
        "grid": 12,

    },

    {
        "varName": "personalcallverification",
        "type": "checkbox",
        "label": "Personal detail update request without call verification",
        "grid": 12,

    },

    {" varName":"photoSignatureupload",
        "type":"checkbox",
        "label":"Photo & Signature upload for Card Cheque Activation",
        "grid":12,

    },

    {
        "varName": "pODrequestonCard",
        "type": "checkbox",
        "label": "POD request on Card",
        "grid": 12,

    },

    {
        "varName": "pODrequestonVoucher",
        "type": "checkbox",
        "label": "POD request on Voucher",
        "grid": 12,

    },
    ///////////////////////////////////////////////
    {" varName":"prepaidcardcontactinformationupdate",
        "type":"checkbox",
        "label":"Prepaid card contact information update",
        "grid":12,

    },

    {"varName":"priorityPassCardnotreceived",
        "type":"checkbox",
        "label":"Priority Pass Card not received",
        "grid":12,

    },

    {" varName":"prioritypassissuingrequestforGoldPrimary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Gold Primary",
        "grid":12,

    },


    {"varName":"prioritypassissuingrequestPlatinumPrimary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Platinum Primary",
        "grid":12,

    },

    {"varName":"prioritypassissuingrequestPlatinumSupplementary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Platinum Supplementary",
        "grid":12
    },

    {"varName":"prioritypassReplacementPlatinumPrimary",
        "type":"checkbox",
        "label":"Priority pass Replacement for Platinum Primary",
        "grid":12,

    },

    {"varName":"prioritypassReplacementPlatinumSupplementary",
        "type":"checkbox",
        "label":"Priority pass Replacement for Platinum Supplementary",
        "grid":12,

    },


    {" varName":"prioritypassReplacementGoldPrimary",
        "type":"checkbox",
        "label":"Priority pass Replacement Gold Primary",
        "grid":12,

    },



    {" varName":"queryCardPindeliveryAmex",
        "type":"checkbox",
        "label":"Query on Card/Pin delivery (Amex)",
        "grid":12,

    },


    {"varName":"queryCardPindeliveryVisa",
        "type":"checkbox",
        "label":"Query on Card/Pin delivery (Visa)",
        "grid":12,

    },

    {
        "varName": "queryDebitCardPINdelivery",
        "type": "checkbox",
        "label": "Query on Debit Card/PIN delivery",
        "grid": 12,
    },

    {" varName":"queryUDC",
        "type":"checkbox",
        "label":"Query on UDC",
        "grid":12,

    },

    {
        "varName": "returnCardchequeresendrequest",
        "type": "checkbox",
        "label": "Return Card cheque resend request",
        "grid": 12,

    },

    {
        "varName": "returnCardresendrequestAmex",
        "type": "checkbox",
        "label": "Return Card resend request (Amex)",
        "grid": 12,

    },

    {
        "varName": "returnCardresendrequestVisa",
        "type": "checkbox",
        "label": "Return Card resend request (Visa)",
        "grid": 12,

    },

    {
        "varName": "returnPINresendrequestAmex",
        "type": "checkbox",
        "label": "Return PIN resend request (Amex)",
        "grid": 12,

    },

    {
        "varName": "returnVoucherresendrequestBirthdayVoucher",
        "type": "checkbox",
        "label": "Return Voucher resend request (Birthday Voucher)",
        "grid": 12,

    },

    {" varName":"returnVoucherresendrequestMRVoucher",
        "type":"checkbox",
        "label":"Return Voucher resend request (MR Voucher)",
        "grid":12,

    },

    {
        "varName": "returnVoucherresendrequestSpendingVoucher",
        "type": "checkbox",
        "label": "Return Voucher resend request (Spending Voucher)",
        "grid": 12,

    },

    {
        "varName": "returnedCardResend AMEXUpdatedAddress",
        "type": "checkbox",
        "label": "Returned Card Resend  AMEX (Updated Address)",
        "grid": 12,

    },
    //////////
    {" varName":"returnedCardResendVISAUpdatedAddress",
        "type":"checkbox",
        "label":"Returned Card Resend  VISA (Updated Address)",
        "grid":12,

    },

    {"varName":"returnedPriorityPassCardresend",
        "type":"checkbox",
        "label":"Returned Priority Pass Card resend",
        "grid":12,

    },

    {" varName":"securedCardClosureSecuredUnsecured",
        "type":"checkbox",
        "label":"Secured Card Closure_Secured to Unsecured",
        "grid":12,

    },


    {"varName":"securedCardClosureSecurityClosureRelease",
        "type":"checkbox",
        "label":"Secured Card Closure_Security Closure/Release",
        "grid":12,

    },

    {"varName":"signaturePhotoUpdate",
        "type":"checkbox",
        "label":"Signature / Photo Update",
        "grid":12
    },

    {"varName":"statementCycleChange",
        "type":"checkbox",
        "label":"Statement Cycle Change",
        "grid":12,

    },

    {"varName":"statementFeeAmex",
        "type":"checkbox",
        "label":"Statement Fee for Amex",
        "grid":12,

    },


    {" varName":"statementFeeVisa",
        "type":"checkbox",
        "label":"Statement Fee for Visa",
        "grid":12,

    },



    {" varName":"supplementarycardlimitsetrequest",
        "type":"checkbox",
        "label":"Supplementary card limit set request",
        "grid":12,

    },


    {"varName":"uDCreturnrequest",
        "type":"checkbox",
        "label":"UDC return request",
        "grid":12,

    },

    {
        "varName": "citytouchDisputeAccountsCreditCardBillPayment",
        "type": "checkbox",
        "label": "Citytouch Dispute-Accounts » Credit Card Bill Payment",
        "grid": 12,
    },

    {" varName":"citytouchDisputeAccountsVisaInstantPayment",
        "type":"checkbox",
        "label":"Citytouch Dispute-Accounts » Visa Instant Payment",
        "grid":12,

    },

    {
        "varName": "accountTaggingDebitCard",
        "type": "checkbox",
        "label": "Account Tagging with Debit Card",
        "grid": 12,

    },

    {
        "varName": "captureDebitCardReplacement",
        "type": "checkbox",
        "label": "Capture Debit Card Replacement",
        "grid": 12,

    },

    {
        "varName": "debitCardCashwithdrawallimitmodification",
        "type": "checkbox",
        "label": "Debit Card Cash withdrawal limit modification",
        "grid": 12,

    },

    {
        "varName": "debitCardFeesRealization",
        "type": "checkbox",
        "label": "Debit Card Fees Realization",
        "grid": 12,

    },

    {
        "varName": "debitCardPINReplace",
        "type": "checkbox",
        "label": "Debit Card PIN Replace",
        "grid": 12,

    },

    {" varName":"debitCardPODcopy",
        "type":"checkbox",
        "label":"Debit Card POD copy",
        "grid":12,

    },

    {
        "varName": "debitCardRenewalRequest",
        "type": "checkbox",
        "label": "Debit Card Renewal Request",
        "grid": 12,

    },

    {
        "varName": "debitCardReplaceRequest",
        "type": "checkbox",
        "label": "Debit Card Replace Request",
        "grid": 12,

    },
    {" varName":"debitCardPINnotreceived",
        "type":"checkbox",
        "label":"Debit Card/PIN not received",
        "grid":12,

    },

    {"varName":"complainagainstSalespersonCreditCard",
        "type":"checkbox",
        "label":"Complain against Sales person (Credit Card)",
        "grid":12,

    },

    {" varName":"officeQuotaEndorsementCorporateCards",
        "type":"checkbox",
        "label":"Office Quota Endorsement  Corporate Cards",
        "grid":12,

    },


    {"varName":"advanceendorsementupdaterequest",
        "type":"checkbox",
        "label":"Advance endorsement update request",
        "grid":12,

    },

    {"varName":"amexCreditCardAcceptanceissueInternational",
        "type":"checkbox",
        "label":"Amex Credit Card Acceptance issue (International)",
        "grid":12
    },

    {"varName":"autoLimitEnhancement",
        "type":"checkbox",
        "label":"Auto Limit Enhancement",
        "grid":12,

    },

    {"varName":"cardLimitComplainbyRFC",
        "type":"checkbox",
        "label":"Card Limit Complain (by RFC)",
        "grid":12,

    },


    {" varName":"cardSurrenderAmex",
        "type":"checkbox",
        "label":"Card Surrender (Amex)",
        "grid":12,

    },



    {" varName":"cardSurrenderVisa",
        "type":"checkbox",
        "label":"Card Surrender (Visa)",
        "grid":12,

    },


    {"varName":"cardSurrenderDowngradeAmexPlatinum",
        "type":"checkbox",
        "label":"Card Surrender/Down grade (Amex Platinum)",
        "grid":12,

    },

    {
        "varName": "cardmembersAppreciation",
        "type": "checkbox",
        "label": "Cardmembers Appreciation",
        "grid": 12,
    },

    {" varName":"certificaterequestRegularCard",
        "type":"checkbox",
        "label":"Certificate request for Regular Card",
        "grid":12,

    },

    {
        "varName": "endorsementCancellationrequest",
        "type": "checkbox",
        "label": "Endorsement Cancellation request",
        "grid": 12,

    },

    {
        "varName": "endorsementUpdateRequest",
        "type": "checkbox",
        "label": "Endorsement Update Request",
        "grid": 12,

    },

    {
        "varName": "limitDecreaseRequest",
        "type": "checkbox",
        "label": "Limit Decrease Request",
        "grid": 12,

    },

    {
        "varName": "limitEnhancement",
        "type": "checkbox",
        "label": "Limit Enhancement",
        "grid": 12,

    },

    {
        "varName": "lPCInterestEOLwaiverrequest",
        "type": "checkbox",
        "label": "LPC/Interest/EOL waiver request",
        "grid": 12,

    },

    {" varName":"priorityPassComplain",
        "type":"checkbox",
        "label":"Priority Pass Complain",
        "grid":12,

    },

    {
        "varName": "securedCardClosure",
        "type": "checkbox",
        "label": "Secured Card Closure",
        "grid": 12,

    },

    {
        "varName": "securedCardClosureRetentionCall",
        "type": "checkbox",
        "label": "Secured Card Closure_Retention Call",
        "grid": 12,

    },
    //////////
    {" varName":"unBilledStatement",
        "type":"checkbox",
        "label":"Un-Billed Statement",
        "grid":12,

    },

    {"varName":"debitCardSurrender",
        "type":"checkbox",
        "label":"Debit Card Surrender",
        "grid":12,

    },

    {" varName":"eCommerceMerchantonboardingrequest",
        "type":"checkbox",
        "label":"E:Commerce Merchant on-boarding request",
        "grid":12,

    },


    {"varName":"aTMdisputeoffus",
        "type":"checkbox",
        "label":"ATM dispute (off us)",
        "grid":12,

    },

    {"varName":"aTMDisputeoverseas",
        "type":"checkbox",
        "label":"ATM Dispute (overseas)",
        "grid":12
    },

    {"varName":"disputedtransactionrequest",
        "type":"checkbox",
        "label":"Disputed transaction request",
        "grid":12,

    },

    {"varName":"fraudCounterfeittransaction",
        "type":"checkbox",
        "label":"Fraud & Counterfeit transaction",
        "grid":12,

    },


    {" varName":"pOSdiputeOffUS",
        "type":"checkbox",
        "label":"POS dipute (Off US)",
        "grid":12,

    },



    {" varName":"amexCreditCardAcceptanceissueLocal",
        "type":"checkbox",
        "label":"Amex Credit Card Acceptance issue (Local)",
        "grid":12,

    },


    {"varName":"citytouchDisputeMerchant",
        "type":"checkbox",
        "label":"Citytouch Dispute-Merchant",
        "grid":12,

    },

    {
        "varName": "flexibuyrequest",
        "type": "checkbox",
        "label": "Flexi buy request",
        "grid": 12,
    },

    {" varName":"merchantServicesComplaint",
        "type":"checkbox",
        "label":"Merchant Services Complaint",
        "grid":12,

    },

    {
        "varName": "mRcomplainMerchant",
        "type": "checkbox",
        "label": "MR complain (Merchant)",
        "grid": 12,

    },

    {
        "varName": "creditCardActivationRegularCard",
        "type": "checkbox",
        "label": "Credit Card Activation for Regular Card",
        "grid": 12,

    },

    {
        "varName": "delinquentCreditCardActivationOutbound",
        "type": "checkbox",
        "label": "Delinquent Credit Card Activation by Outbound",
        "grid": 12,

    },

    {
        "varName": "personaldetailupdaterequestwithcallverification",
        "type": "checkbox",
        "label": "Personal detail update request with call verification",
        "grid": 12,

    },

    {
        "varName": "easypayDeEnrollment",
        "type": "checkbox",
        "label": "Easypay De-Enrollment",
        "grid": 12,

    },

    {" varName":"easypayEnrollment",
        "type":"checkbox",
        "label":"Easypay Enrollment",
        "grid":12,

    },

    {
        "varName": "mRDisputeComplaint",
        "type": "checkbox",
        "label": "MR Dispute & Complaint",
        "grid": 12,

    },

    {
        "varName": "delinquentAMEXCardClosureConfirmation",
        "type": "checkbox",
        "label": "Delinquent AMEX Card Closure Confirmation (above 6 aging card)",
        "grid": 12,

    },
    {" varName":"delinquentCardOSConfirmation",
        "type":"checkbox",
        "label":"Delinquent Card O/S Confirmation (above 6 aging card)",
        "grid":12,

    },


    {"varName":"delinquentCreditCardActive",
        "type":"checkbox",
        "label":"Delinquent Credit Card Active / Reissue Feedback from Collection",
        "grid":12,

    },

    {
        "varName": "delinquentVISACardClosureConfirmation",
        "type": "checkbox",
        "label": "Delinquent VISA Card Closure Confirmation (above 6 aging card)",
        "grid": 12,
    },

    {" varName":"eFTNCancelationRequest",
        "type":"checkbox",
        "label":"EFTN Cancelation Request (Delinquent)",
        "grid":12,

    },

    {
        "varName": "nOCNODrequestirregularCard",
        "type": "checkbox",
        "label": "NOC/NOD request for irregular Card (state 4 to 14, 92,98,99)",
        "grid": 12,

    },

    {
        "varName": "nOCNODrequestwrittenoffCard",
        "type": "checkbox",
        "label": "NOC/NOD request for written off Card (state -91)",
        "grid": 12,

    },

    {
        "varName": "aLEbyBackOffice",
        "type": "checkbox",
        "label": "ALE by Back Office",
        "grid": 12,

    },

    {
        "varName": "contractclosureBackOffice",
        "type": "checkbox",
        "label": "Contract closure of Back Office",
        "grid": 12,

    },

    {
        "varName": "pGCBackOffice",
        "type": "checkbox",
        "label": "PGC by Back Office",
        "grid": 12,

    },

    {" varName":"pGCBackofficefromAmexlounge",
        "type":"checkbox",
        "label":"PGC by Back office from Amex lounge",
        "grid":12,

    },

    {
        "varName": "amexIntlLoungeFee",
        "type": "checkbox",
        "label": "Amex Int l Lounge Fee",
        "grid": 12,

    },
]


class DebitCreditMaintenance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            getDedupData: false,
            dedupData: [[" ", " "]],
            relatedData: [[" ", " "]],
            tableData: [],
            sourceMappingData: [],
            getsearchValue: [],
            getCustomerId: '',
            getAccountType: '',
            accountOpeningFromModal: false,
            SelectedData: '',
            tabMenuSelect: 'INDIVIDUAL',
            existingAcoountOpeningModal: false,
            newAcoountOpeningModal: false,
            searchTableData: null,
            searchTableRelatedData: null,
            oldAccountData: [],

            dataNotFound: false,
            CustomerModal: false,
            uniqueId: '',
            IDENTIFICATION_NO: '',
            id: '',
            alert: false,
            value: "INDIVIDUAL",
            NonIndividualabel: "",
            individualLabel: "Individual A/C",
            content: "INDIVIDUAL",
            anchorEl: null,
            anchorE2: null,
            individualDropdownOpen: null,
            objectForJoinAccount: {},
            numberOfJointMember: "",
            notificationMessage: "CB number / NID / Passport / Birth Certificate or Driving License is Required!!",

            accountNumber: "",
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},

            csDeferalPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            caseId: "",
            title: "",
            app_uid: "-1",
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "",
            },
            fileUploadData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            maintenanceType: '',
            getDocument: false,
            customerName: 'Jamal',
            cbNumber: 12345,
            digitTIN: false,
            titleChange: false,
            nomineeUpdate: false,
            updateChangePhotoId: false,
            contactNumberChange: false,
            emailAddressChange: false,
            estatementEnrollment: false,
            addressChange: false,
            otherInformationChange: false,
            signatureCard: false,
            dormantAccountActivation: false,
            dormantAccountDataUpdate: false,
            schemeMaintenanceLinkChange: false,
            schemeMaintenance: false,
            mandateUpdateChange: false,
            cityLive: false,
            projectRelatedDataUpdateADUP: false,
            accountSchemeClose: false,
            lockerSIOpen: false,
            lockerSIClose: false,
            others: false,
            bearerApproval: '',

        }
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }

    addDeferalForm() {
        if (this.state.inputData["csDeferal"] === "YES") {
            return this.state.values.map((el, i) =>
                <React.Fragment>

                    <Grid item xs="6">
                        {
                            this.dynamicDeferral(el)
                        }
                    </Grid>
                    <Grid item xs="6">
                        {this.dynamicDeferralOther(el)}
                    </Grid>
                    <Grid item xs="6">
                        {
                            this.dynamicDate(el)
                        }
                    </Grid>


                    <Grid item xs="3">
                        <button
                            style={{float: 'right',}}
                            className="btn btn-outline-danger"
                            type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                        >
                            Remove
                        </button>
                    </Grid>

                </React.Fragment>
            )
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {
        if (this.state.inputData["csDeferal"] === "YES") {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'left',

                    }}

                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}


                >Add Deferral</button>
            )
        } else {
            return;
        }
    }
    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(Deferral));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    renderSelectMenu = () => {

        if (this.props.appId !== undefined) {
            return (

                <Grid item xs='12'>


                    <TextField

                        value="YES"
                        label="Need Deferral?"

                        InputProps={{
                            readOnly: true
                        }}
                    />


                </Grid>

            )

        } else {
            console.log(this.state.inputData["csDeferal"]);
            return (


                <Grid item xs='12'>
                    <FormLabel component="legend">Need Deferral?</FormLabel>
                    <RadioGroup aria-label="csDeferal" name="csDeferal" value={this.state.inputData["csDeferal"]}
                                onChange={this.handleChange}>
                        <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                        <FormControlLabel value="NO" control={<Radio/>} label="NO"/>

                    </RadioGroup>

                </Grid>

            )
        }


    }


    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }
    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }


    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false
        })
    }
    closeUploadModal = (data) => {
        this.setState({
            uploadModal: false,
            imgeListLinkSHow: true,
            getImageLink: data
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    bearerApproval = () => {
        if (this.state.getData) {

            return (

                <Grid item xs={12}>
                    {SelectComponent.select(this.state, this.updateComponent, bearerApproval)}
                </Grid>

            )

        }
        return;
    }

    componentDidMount() {
        this.updateComponent()
        // this.state.inputData["csDeferal"] ="NO";

        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;
            console.log(this.props.appId);
            axios.get(url,
                {withCredentials: true})
                .then((response) => {

                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,

                        showValue: true,
                        appId: this.props.appId
                    });
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            response.data.map((Deferral) => {
                                tableArray.push(this.createTableData(Deferral.id, Deferral.type, Deferral.dueDate, Deferral.appliedBy, Deferral.applicationDate, Deferral.status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })


                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            console.log(url)
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response.data)

                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        showValue: true,
                        getData: true,
                        getDocument: true,

                    });


                })
                .catch((error) => {
                    console.log(error);
                })
        }


    }


    renderSearchForm = () => {
        // !this.state.deferalNeeded &&&& this.state.getData
        if (this.state.showValue)

            return (
                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>

                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, accountMaintenanceSearch, this.updateComponent)
                            }
                        </ThemeProvider>
                    </Grid>
                    <br/><br/><br/>
                    <button
                        onClick={this.accountModal}
                        className="btn btn-danger">
                        Search
                    </button>
                </Grid>

            )

    }
    accountModal = () => {
        this.setState({
            searchTableData: true,
            accountNumber: this.state.inputData.accountNumber,

        })
    };
    renderSearchData = () => {

        if (this.state.searchTableData && this.state.showValue) {

            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>


                        <Grid container spacing={1}>
                            <ThemeProvider theme={theme}>
                                <br/><br/>
                                <Grid item xs={6}>
                                    <label for="customerName"><font size="3"><b> Customer Name :</b></font></label>

                                    {this.state.customerName}</Grid>
                                <Grid item xs={6}>
                                    <label for="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                    {this.state.cbNumber}
                                </Grid>
                                {
                                    CommonJsonFormComponent.renderJsonForm(this.state, debitCreditMaintenanceList, this.updateComponent)
                                }
                            </ThemeProvider>
                        </Grid>

                        <br/><br/>
                        <Grid container spacing={3}>
                            <ThemeProvider theme={theme}>

                                <Grid item xs='12'>
                                    {this.bearerApproval()}
                                </Grid>
                                {this.renderSelectMenu()}


                                <Grid item xs='12'>
                                    {
                                        this.renderAddButtonShow()
                                    }
                                </Grid>
                                <br/>


                                {
                                    this.addDeferalForm()
                                }

                            </ThemeProvider>
                        </Grid>

                        <ThemeProvider theme={theme}>
                            <Grid container spacing={1}>
                                {this.renderImageLink()}
                            </Grid>
                        </ThemeProvider>
                        <br/><br/><br/>
                        {this.renderUploadButton()}
                        <Dialog
                            fullWidth="true"
                            maxWidth="md"
                            open={this.state.accountDetailsModal}>
                            <DialogContent>

                                <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.uploadModal}>
                            <DialogContent>

                                <LiabilityUploadModal appId={this.state.appId}
                                                      closeModal={this.closeUploadModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.imageModalBoolean}>
                            <DialogContent>

                                <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                            </DialogContent>
                        </Dialog>
                        <br/>
                        <br/>

                        <center>
                            <button
                                className="btn btn-outline-danger"
                                style={{
                                    verticalAlign: 'middle',
                                }}
                                onClick={this.handleSubmit}

                            >
                                Submit
                            </button>
                        </center>


                    </GridItem>
                </GridContainer>

            )
        }
    }


    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.deferalNeeded) {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

            }
            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.customerName = this.state.customerName;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "DebitCreditMaintenance";

        if (this.state.inputData.bearerApproval === "BM Approval")
            data.cs_bearer = "BM";
        else if(this.state.inputData.bearerApproval === "BM Approval & Call Center Approval")
            data.cs_bearer = "BOTH";
        else if( this.state.inputData.bearerApproval === "Call Center Approval")
            data.cs_bearer = "CALLCENTER";


        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log("Cs Variable")
                console.log(response.data);
                this.setState({
                    appData: response.data
                })


                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",


                        })


                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 652) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }

    render() {
        console.log(this.state.inputData)
        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }


        return (

            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>Debit & Credit
                                Maintenance</h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                {this.renderSearchForm()}
                                <br/>
                                <br/>
                                {this.renderSearchData()}
                                <br/>
                                <br/>


                            </div>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );


    }

}

export default withStyles(styles)(DebitCreditMaintenance);
