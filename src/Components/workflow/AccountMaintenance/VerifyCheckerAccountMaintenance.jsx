import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import FormSample from '../../JsonForm/FormSample';
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Notification from "../../NotificationMessage/Notification";
import CloseIcon from '@material-ui/icons/Close';
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {ThemeProvider} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import theme from "../../JsonForm/CustomeTheme";
import SelectComponent from "../../JsonForm/SelectComponent";

import Table from "../../Table/Table";
import FileTypeComponent from "../../JsonForm/FileTypeComponent";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import AccountNoGenerate from "../AccountNoGenerate";
import LiabilityUploadModal from "../LiabilityUploadModal";
import SingleImageShow from "../SingleImageShow";
const bearerCallApproval = {
    "varName": "bearerApproval",
    "type": "text",
    "label": "Approval",
    "grid": 12,
    "readOnly": true
};
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};
const maintenanceList = [

    {
        "varName": "digitTIN",
        "type": "checkbox",
        "label": "12-Digit TIN",
        "grid": 12,

        "conditional" : true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {
        "varName": "digitTINOld",
        "type": "text",
        "label": "Existing 12-Digit TIN",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,

    },
    {
        "varName": "digitTINNew",
        "type": "text",
        "label": "New 12-Digit TIN",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {
        "varName": "titleChange",
        "type": "checkbox",
        "label": "Title Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "titleChangeOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "titleChangeNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,

    },

    {
        "varName": "nomineeUpdate",
        "type": "checkbox",
        "label": "Nominee Update",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },

    {
        "varName": "nomineeUpdateOld",
        "type": "text",
        "label": "Existing Nominee",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "nomineeUpdateNew",
        "type": "text",
        "label": "New Nominee",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoId",
        "type": "checkbox",
        "label": "Update/Change Photo Id",
        "grid": 12,
        "conditional":true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoIdOld",
        "type": "text",
        "label": "Existing Photo Id",
        "grid": 5,
        "readOnly": true,
        "conditional":true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoIdNew",
        "type": "text",
        "label": "New Photo Id",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },

    {
        "varName": "contactNumberChange",
        "type": "checkbox",
        "label": "Contact Number Change",
        "grid": 12,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "residenceNumberChangeOld",
        "type": "text",
        "label": "Existing  Residence Contact Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "residenceNumberChangeNew",
        "type": "text",
        "label": "New Residence Contact Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "officeNumberChangeOld",
        "type": "text",
        "label": "Existing  Office Contact Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },

    {
        "varName": "officeNumberChangeNew",
        "type": "text",
        "label": "New Office Contact Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mobileNumberChangeOld",
        "type": "text",
        "label": "Existing  Mobile Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mobileNumberChangeNew",
        "type": "text",
        "label": "New Mobile Number",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "emailAddressChange",
        "type": "checkbox",
        "label": "Email Address Change",
        "grid": 12,
        "conditional":true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "emailAddressChangeExisting",
        "type": "text",
        "label": "Existing Email Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "emailAddressChangeNew",
        "type": "text",
        "label": "New Email Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollment",
        "type": "checkbox",
        "label": "Estatement Enrollment",
        "grid": 12,
        "conditional":true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollmentExisting",
        "type": "text",
        "label": "Existing Estatement Enrollment",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollmentNew",
        "type": "text",
        "label": "New Estatement Enrollment",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressChange",
        "type": "checkbox",
        "label": "Address Change",
        "grid": 12,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPermanentChangeExisting",
        "type": "text",
        "label": "Existing  Permanent Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPermanentChangeNew",
        "type": "text",
        "label": "New Permanent Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPresentChangeExisting",
        "type": "text",
        "label": "Existing  Present Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPresentChangeNew",
        "type": "text",
        "label": "New Present Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressOfficeChangeExisting",
        "type": "text",
        "label": "Existing  Office Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressOfficeChangeNew",
        "type": "text",
        "label": "New Office Address",
        "grid": 5,
        "conditional":true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "otherInformationChange",
        "type": "checkbox",
        "label": "Other Information Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "fatherMotherNameRectifyExisting",
        "type": "text",
        "label": "Existing Father/Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "fatherMotherNameRectifyNew",
        "type": "text",
        "label": "New Father/Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "spouseNameExisting",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "spouseNameNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "dateOfBirthExisting",
        "type": "text",
        "label": "Existing Date Of Birth",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "dateOfBirthNew",
        "type": "text",
        "label": "New Date Of Birth",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "employerNameExisting",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "employerNameNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "professionExisting",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivation",
        "type": "checkbox",
        "label": "Dormant Account Activation",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivationExisting",
        "type": "text",
        "label": "Existing Dormant Account Activation",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivationNew",
        "type": "text",
        "label": "New Dormant Account Activation",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "linkChangeExisting",
        "type": "text",
        "label": "Existing Link A/C",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "linkChangeNew",
        "type": "text",
        "label": "New Link A/C",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "installmentRegularizationExisting",
        "type": "text",
        "label": "Existing Installment Regularization",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "installmentRegularizationNew",
        "type": "text",
        "label": "New Installment Regularization",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChange",
        "type": "checkbox",
        "label": "Mandate Update/Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChangeExisting",
        "type": "text",
        "label": "Existing Mandate",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChangeNew",
        "type": "text",
        "label": "New Mandate",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CTR, CIB etc",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cTRExisting",
        "type": "text",
        "label": "Existing CTR Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "cTRNew",
        "type": "text",
        "label": "New CTR Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "others",
        "type": "checkbox",
        "label": "Others",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
        "conditional" : true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "othersExisting",
        "type": "text",
        "label": "Existing Others",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "othersNew",
        "type": "text",
        "label": "New Others",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "title",
        "type":"title",
        "label":"AC ACM",
        "grid":12,

    },
    {
        "varName":"nomineeName",
        "type":"text",
        "label":"Nominee Name",
        "grid":6,
        required:true,
    },

    {
        "varName":"relationship",
        "type":"text",
        "label":"Relationship",
        "grid":6,
        required:true,
    },

    {
        "varName":"address1",
        "type":"text",
        "label":"Address 1",
        "grid":6,
        required:true,
    },

    {
        "varName":"address2",
        "type":"text",
        "label":"Address 2",
        "grid":6,

    },

    {
        "varName":"cityCode1",
        "type":"text",
        "label":"City Code",
        "grid":6,
        required:true,
    },

    {
        "varName":"stateCode1",
        "type":"text",
        "label":"State Code",
        "grid":6,
        required:true,
    },

    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code",
        "grid":6,
        required:true,
    },

    {
        "varName":"country1",
        "type":"text",
        "label":"Country",
        "grid":6,
        required:true,
    },

    {
        "varName":"regNo",
        "type":"text",
        "label":"Reg No",
        "grid":6,
        required:true,
    },

    {
        "varName":"nomineeMinor",
        "type":"text",
        "label":"Nominee Minor",
        "grid":6,
        required:true,
    },

    {
        "varName":"dob",
        "type":"text",
        "label":"Dob",
        "grid":6,

    },

    {
        "varName":"guardian'sName",
        "type":"text",
        "label":"Guardian's Name",
        "grid":6,

    },

    {
        "varName":"guardianCode",
        "type":"text",
        "label":"Guardian Code",
        "grid":6,

    },

    {
        "varName":"address",
        "type":"text",
        "label":"Address",
        "grid":6,

    },

    {
        "varName":"cityCode2",
        "type":"text",
        "label":"City Code",
        "grid":6,

    },

    {
        "varName":"stateCode2",
        "type":"text",
        "label":"State Code",
        "grid":6,

    },

    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code",
        "grid":6,

    },

    {
        "varName":"country2",
        "type":"text",
        "label":"Country",
        "grid":6,

    },

    {
        "varName":"modeOfOperation",
        "type":"text",
        "label":"Mode Of Operation",
        "grid":6,
        required:true,
    },

    {
        "varName":"accountManager",
        "type":"text",
        "label":"Account Manager",
        "grid":6,
        required:true,
    },

    {
        "varName":"cashLimit",
        "type":"text",
        "label":"Cash Limit",
        "grid":6,

    },

    {
        "varName":"clearingLimit",
        "type":"text",
        "label":"Clearing Limit",
        "grid":6,

    },

    {
        "varName":"transferLimit",
        "type":"text",
        "label":"Transfer Limit",
        "grid":6,

    },

    {
        "varName":"remarks",
        "type":"text",
        "label":"Remarks",
        "grid":6,

    },

    {
        "varName":"statement",
        "type":"text",
        "label":"Statement",
        "grid":6,
        required:true,
    },

    {
        "varName":"statementFrequency",
        "type":"text",
        "label":"Statement Frequency",
        "grid":6,
        required:true,
    },

    {
        "varName":"despatchMode",
        "type":"text",
        "label":"Despatch Mode",
        "grid":6,
        required:true,
    },

    {
        "varName":"contactPhoneNumber",
        "type":"text",
        "label":"Contact Phone Number",
        "grid":6,
        required:true,
    },

    {
        "varName":"sectorCode",
        "type":"text",
        "label":"Sector Code",
        "grid":6,
        required:true,
    },

    {
        "varName":"subSectorCode",
        "type":"text",
        "label":"Sub Sector Code",
        "grid":6,
        required:true,
    },

    {
        "varName":"occupationOode",
        "type":"text",
        "label":"Occupation Oode",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeCode1",
        "type":"text",
        "label":"Free Code 1",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeCode3",
        "type":"text",
        "label":"Free Code 3",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeCode7",
        "type":"text",
        "label":"Free Code 7",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeCode9",
        "type":"text",
        "label":"Free Code 9",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeCode10",
        "type":"text",
        "label":"Free Code 10",
        "grid":6,
        required:true,
    },

    {
        "varName":"freeText11",
        "type":"text",
        "label":"Free Text 11",
        "grid":6,
        required:true,
    },

    {

        "type":"title",
        "label":"AC CUMM",
        "grid":12,
    },
    {
        "varName":"customerId",
        "type":"text",
        "label":"Customer Id",
        "grid":6,
        "length":9,


    },

    {
        "varName":"title",
        "type":"text",
        "label":"Title",
        "grid":6,


        required:true,
    },

    {
        "varName":"customerName",
        "type":"text",
        "label":"Customer Name",
        "grid":6,
        "length":80,


    },

    {
        "varName":"shortName",
        "type":"text",
        "label":"Short Name",
        "grid":6,
        "length":10,


    },

    {
        "varName":"status",
        "type":"text",
        "label":"Status",
        "grid":6,


        required:true,
    },

    {
        "varName":"statusAsOnDate",
        "type":"date",
        "label":"Status as on Date",
        "grid":6,



    },

    {
        "varName":"acManager",
        "type":"text",
        "label":"AC Manager",
        "grid":6,



    },

    {
        "varName":"occupationCode",
        "type":"text",
        "label":"Occuoation Code",
        "grid":6,


        required:true,
    },

    {
        "varName":"constitution",
        "type":"text",
        "label":"Constitution",
        "grid":6,


        required:true,
    },

    {
        "varName":"gender",
        "type":"text",
        "label":"Gender",
        "grid":6,


        required:true,
    },

    {
        "varName":"staffFlag",
        "type":"text",
        "label":"Staff Flag",
        "grid":6,


        required:true,
    },

    {
        "varName":"staffNumber",
        "type":"text",
        "label":"Staff Number",
        "grid":6,


        required:true,
    },

    {
        "varName":"minor",
        "type":"text",
        "label":"Minor",
        "grid":6,



    },

    {
        "varName":"nonResident",
        "type":"text",
        "label":"Non Resident",
        "grid":6,


        required:true,
    },

    {
        "varName":"trade",
        "type":"text",
        "label":"Trade",
        "grid":6,


        required:true,
    },

    {
        "varName":"nationalIdCard",
        "type":"text",
        "label":"National ID Card",
        "grid":6,


        required:true,
    },

    {
        "varName":"dateOfBirth",
        "type":"date",
        "label":"Date of Birth",
        "grid":6,



    },

    {
        "varName":"introducerCustomerId",
        "type":"text",
        "label":"Introducer Customer Id",
        "grid":6,
        "length":9,

        required:true,
    },

    {
        "varName":"introducerName",
        "type":"text",
        "label":"Introducer Name",
        "grid":6,



    },

    {
        "varName":"introducerStaff",
        "type":"text",
        "label":"Introducer Staff",
        "grid":6,



    },

    {
        "varName":"maritialStatus",
        "type":"text",
        "label":"Maritial Status",
        "grid":6,


        required:true,
    },

    {
        "varName":"father",
        "type":"text",
        "label":"Father",
        "grid":6,


        required:true,
    },

    {
        "varName":"mother",
        "type":"text",
        "label":"Mother",
        "grid":6,


        required:true,
    },

    {
        "varName":"spouse",
        "type":"text",
        "label":"Spouse",
        "grid":6,


        required:true,
    },

    {
        "varName":"communicationAddress1",
        "type":"text",
        "label":"Communication Address1",
        "grid":6,



    },

    {
        "varName":"communicationAddress2",
        "type":"text",
        "label":"Communication Address2",
        "grid":6,



    },

    {
        "varName":"city1",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state1",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country1",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo11",
        "type":"text",
        "label":"Phone No1",
        "grid":6,
        "length":11,

        required:true,
    },

    {
        "varName":"phoneNo21",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo1",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email1",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"permanentAddress1",
        "type":"text",
        "label":"Permanent Address1",
        "grid":6,



    },

    {
        "varName":"permanentAddress2",
        "type":"text",
        "label":"Permanent Address2",
        "grid":6,



    },

    {
        "varName":"city2",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state2",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country2",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo12",
        "type":"text",
        "label":"Phone No1",
        "grid":6,



    },

    {
        "varName":"phoneNo222",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo2",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email2",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"employerAddress1",
        "type":"text",
        "label":"Employer Address1",
        "grid":6,



    },

    {
        "varName":"employerAddress2",
        "type":"text",
        "label":"Employer Address2",
        "grid":6,



    },

    {
        "varName":"city3",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state3",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode3",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo13",
        "type":"text",
        "label":"Phone No1",
        "grid":6,



    },

    {
        "varName":"phoneNo23",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email3",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"faxNo",
        "type":"text",
        "label":"Fax No",
        "grid":6,



    },

    {
        "varName":"combineStatement",
        "type":"text",
        "label":"Combine Statement",
        "grid":6,



    },

    {
        "varName":"tds",
        "type":"text",
        "label":"TDS",
        "grid":6,



    },

    {
        "varName":"pangirNo",
        "type":"text",
        "label":"PANGIR No",
        "grid":6,



    },

    {
        "varName":"passportNo",
        "type":"text",
        "label":"Passport No",
        "grid":6,



    },

    {
        "varName":"issueDate",
        "type":"date",
        "label":"Issue Date",
        "grid":6,



    },

    {
        "varName":"passportDetails",
        "type":"text",
        "label":"Passport Details",
        "grid":6,


        required:true,
    },

    {
        "varName":"expiryDate",
        "type":"date",
        "label":"Expiry Date",
        "grid":6,



    },

    {
        "varName":"purgedAllowed",
        "type":"text",
        "label":"Purged Allowed",
        "grid":6,



    },

    {
        "varName":"freeText2",
        "type":"text",
        "label":"Free Text2",
        "grid":6,



    },

    {
        "varName":"freeText5",
        "type":"text",
        "label":"Free Text 5",
        "grid":6,
        "length":10,


    },

    {
        "varName":"freeText8",
        "type":"text",
        "label":"Free Text 8",
        "grid":6,



    },

    {
        "varName":"freeText9",
        "type":"text",
        "label":"Free Text 9",
        "grid":6,



    },

    {
        "varName":"freeText13",
        "type":"text",
        "label":"Free Text13",
        "grid":6,



    },

    {
        "varName":"freeText14",
        "type":"text",
        "label":"Free Text14",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeText15",
        "type":"text",
        "label":"Free Text15",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode1",
        "type":"text",
        "label":"Free Code1",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode3",
        "type":"text",
        "label":"Free Code3",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode7",
        "type":"text",
        "label":"Free Code 7",
        "grid":6,


        required:true,
    },

    {

        "type":"title",
        "label":"AC Kyc",
        "grid":12,

    },
    {
        "varName":"date",
        "type":"date",
        "label":"Date",
        "grid":6,

    },

    {
        "varName":"uniqueCustomerID",
        "type":"text",
        "label":"Unique Customer ID",
        "grid":6,

    },

    {
        "varName":"aCNo",
        "type":"text",
        "label":"AC No",
        "grid":6,
        "length":13,
    },

    {
        "varName":"aCTitle",
        "type":"text",
        "label":"AC Title",
        "grid":6,

    },

    {
        "varName":"typeOfAC",
        "type":"text",
        "label":"Type of AC",
        "grid":6,

    },

    {
        "varName":"customerOccupation",
        "type":"text",
        "label":"Customer occupation",
        "grid":6,

    },

    {
        "varName":"customerProbableMonthlyIncome",
        "type":"text",
        "label":"Customer probable monthly income",
        "grid":6,

    },

    {
        "varName":"sourceOfFund",
        "type":"text",
        "label":"Source of Fund",
        "grid":6,

    },

    {
        "varName":"dOCCollectToEnsureSourceOfFund",
        "type":"text",
        "label":"DOC collect to Ensure Source of Fund",
        "grid":6,

    },

    {
        "varName":"collectedDOCHaveBeenVerified",
        "type":"text",
        "label":"Collected DOC have been Verified",
        "grid":6,

    },

    {
        "varName":"howTheAddressIsVerified",
        "type":"text",
        "label":"How the Address is verified",
        "grid":6,

    },

    {
        "varName":"hasTheBeneficialOwnerOfTheAcBeenIdentified",
        "type":"text",
        "label":"Has the Beneficial owner of the ac been identified",
        "grid":6,

    },

    {
        "varName":"passportNo",
        "type":"text",
        "label":"Passport no",
        "grid":6,

    },

    {
        "varName":"nIDNo",
        "type":"text",
        "label":"NID no",
        "grid":6,

    },

    {
        "varName":"birthCertificateNo",
        "type":"text",
        "label":"Birth certificate no",
        "grid":6,

    },

    {
        "varName":"eTIN",
        "type":"text",
        "label":"E-TIN",
        "grid":6,

    },

    {
        "varName":"drivingLicenseNo",
        "type":"text",
        "label":"Driving License no",
        "grid":6,

    },

    {
        "varName":"otherDocumentation",
        "type":"text",
        "label":"Other Documentation",
        "grid":6,

    },

    {
        "varName":"reasonForACOpeningOfForeignCompany",
        "type":"text",
        "label":"Reason for AC opening of Foreign Company",
        "grid":6,

    },

    {
        "varName":"typeOfVISA",
        "type":"text",
        "label":"Type of VISA",
        "grid":6,

    },

    {
        "varName":"expDate",
        "type":"date",
        "label":"Exp date",
        "grid":6,

    },

    {
        "varName":"workPermitAndPermission",
        "type":"text",
        "label":"Work permit and permission",
        "grid":6,

    },

    {
        "varName":"pepip",
        "type":"text",
        "label":"PEPIP",
        "grid":6,

    },

    {
        "varName":"ifyesThen",
        "type":"text",
        "label":"IF YES then",
        "grid":6,

    },

    {
        "varName":"anyMatchOfTerroristActivity",
        "type":"text",
        "label":"Any match of Terrorist activity",
        "grid":6,

    },

    {
        "varName":"whatDoesTheCustomerDoinWhatTypeOfBusinessIsTheCustomerEngaged",
        "type":"text",
        "label":"What does the customer doin what type of business is the customer engaged",
        "grid":6,

    },

    {
        "varName":"customersMonthlyIncome",
        "type":"text",
        "label":"Customers monthly income",
        "grid":6,

    },

    {
        "varName":"expectedAmountOfMonthlyTotalTransaction",
        "type":"text",
        "label":"Expected Amount of Monthly Total Transaction",
        "grid":6,

    },

    {
        "varName":"expectedAmountOfMonthlyCashTransaction",
        "type":"text",
        "label":"Expected Amount of Monthly Cash Transaction",
        "grid":6,

    },

    {
        "varName":"totalRiskScore",
        "type":"text",
        "label":"Total Risk Score",
        "grid":6,

    },

    {
        "varName":"comments",
        "type":"text",
        "label":"Comments",
        "grid":6,

    },
    {

        "type":"title",
        "label":"AC Minor",
        "grid":12,

    },
    {
        "varName":"guardian",
        "type":"text",
        "label":"Guardian",
        "grid":6,
        required:true,
    },

    {
        "varName":"guardianName",
        "type":"text",
        "label":"Guardian Name",
        "grid":6,
        required:true,
    },

    {
        "varName":"address",
        "type":"text",
        "label":"Address",
        "grid":6,
        required:true,
    },

    {
        "varName":"city",
        "type":"text",
        "label":"City",
        "grid":6,
        required:true,
    },

    {
        "varName":"state",
        "type":"text",
        "label":"State",
        "grid":6,
        required:true,
    },

    {
        "varName":"postal",
        "type":"text",
        "label":"Postal",
        "grid":6,
        required:true,
    },

    {
        "varName":"country",
        "type":"text",
        "label":"Country",
        "grid":6,
        required:true,
    },

    {

        "type":"title",
        "label":"AC TP",
        "grid":12,
    },
    {
        "varName":"date",
        "type":"date",
        "label":"Date",
        "grid":6,
    },

    {
        "varName":"uniqueCustomerID",
        "type":"text",
        "label":"Unique Customer ID",
        "grid":6,
    },

    {
        "varName":"aCNo",
        "type":"text",
        "label":"AC No",
        "grid":6,
    },

    {
        "varName":"aCTitle",
        "type":"text",
        "label":"AC Title",
        "grid":6,
    },

    {
        "varName":"monthlyProbableIncome",
        "type":"text",
        "label":"Monthly Probable Income",
        "grid":6,
    },

    {
        "varName":"monthlyProbableTournover",
        "type":"text",
        "label":"Monthly Probable Tournover",
        "grid":6,
    },

    {
        "varName":"sourceOfFund",
        "type":"text",
        "label":"Source of Fund",
        "grid":6,
    },

    {
        "varName":"cashDeposit",
        "type":"text",
        "label":"Cash Deposit",
        "grid":6,
    },

    {
        "varName":"depositByTransfer",
        "type":"text",
        "label":"Deposit by Transfer",
        "grid":6,
    },

    {
        "varName":"foreignInwardRemittance",
        "type":"text",
        "label":"Foreign Inward Remittance",
        "grid":6,
    },

    {
        "varName":"depositIncomeFromExport",
        "type":"text",
        "label":"Deposit Income from Export",
        "grid":6,
    },

    {
        "varName":"deposittransferFromBOAC",
        "type":"text",
        "label":"DepositTransfer from BO AC",
        "grid":6,
    },

    {
        "varName":"others1",
        "type":"text",
        "label":"Others",
        "grid":6,
    },

    {
        "varName":"totalProbableDeposit",
        "type":"text",
        "label":"Total Probable Deposit",
        "grid":6,
    },

    {
        "varName":"cashWithdrawal",
        "type":"text",
        "label":"Cash Withdrawal",
        "grid":6,
    },

    {
        "varName":"withdrawalThroughTransferinstrument",
        "type":"text",
        "label":"Withdrawal through TransferInstrument",
        "grid":6,
    },

    {
        "varName":"foreignOutwardRemittance",
        "type":"text",
        "label":"Foreign outward Remittance",
        "grid":6,
    },

    {
        "varName":"paymentAgainstImport",
        "type":"text",
        "label":"Payment against import",
        "grid":6,
    },

    {
        "varName":"deposittransferToBOAC",
        "type":"text",
        "label":"DepositTransfer to BO AC",
        "grid":6,
    },

    {
        "varName":"others2",
        "type":"text",
        "label":"Others",
        "grid":6,
    },

    {
        "varName":"totalProbableWithdrawal",
        "type":"text",
        "label":"Total Probable Withdrawal",
        "grid":6,
    },
]

var fileUpload = {
    "varName": "scanningFile",
    "type": "file",
    "label": "Upload File",
    "grid": 12
};

var checkerApproval = {
    "varName": "checkerApproval",
    "type": "select",
    "label": "Approval ?",
    "grid":12,
    "enum": [
        "APPROVED",
        "NOTAPPROVED",

    ]
};
var remarks = [
    {
        "varName": "remarksChecker",
        "type": "text",
        "label": "Remarks",
        "grid": 12,


    },
];

var csBearer = [
    {
        "varName": "csBearer",
        "type": "text",
        "label": "Bearer",
        "grid": 6,
        "readOnly": true,


    }, {
        "varName": "bearerApproval",
        "type": "text",
        "label": "Bearer Approval",
        "grid": 6,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "csBearer",
        "conditionalVarValue": "YES"
    },
];

class VerifyCheckerAccountMaintenance extends React.Component {
    state = {
        message: "",
        appData: {},
        getData: false,
        varValue: [],
        redirectLogin: false,
        title: "",
        notificationMessage: "",
        alert: false,
        inputData: {},
        selectedDate: {},
        SelectedDropdownSearchData: null,
        dropdownSearchData: {},
        values: [],
        showValue: false,
        // customerName: [],
        deferalType: [],
        expireDate: [],
        other: [],
        getCheckerList: [],
        getDeferralList: [],
        fileUploadData:{},
        loaderNeeded:null,
        objectForJoinAccount: [],
        getgenerateForm: false,
        bearerApproval: '',
        customerName:" FAISAL AHMED",
        cbNumber: " CB1401933",

    }

    handleChange = (event) => {

        event.preventDefault();

        this.state.inputData[event.target.name] = event.target.value;


    }
    handleSubmit = (event) => {
        event.preventDefault();

        this.state.inputData.checker_approval = this.state.inputData.checkerApproval==="APPROVED";
        if(this.state.fileUploadData.scanningFile!==undefined){
            let fileUploadPath = backEndServerURL + "/case/upload";
            let types = 'Attachments';
            let files = this.state.fileUploadData.scanningFile;
            console.log("dfghjk")
            console.log(files)
            let formData=new FormData();
            formData.append("appId",this.props.app_uid)
            formData.append("file",files)
            formData.append("type",types)
            axios({
                method: 'post',
                url: fileUploadPath,
                data: formData,
                withCredentials: true,
                headers: {'content-type': 'multipart/form-data'}
            })
                .then((response) => {

                    console.log(response);


                })
                .catch((error) => {
                    console.log(error)
                })
        }


        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }
    handleSubmitReturn = (event) => {
        event.preventDefault();

        this.state.inputData.next_user = this.state.inputData.checker_send_to;
        this.state.inputData.checker_update_all_info_approval = "NOTAPPORVED";

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                console.log("checker");
                console.log(response.data);
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }
    renderButton = () => {
        if (this.state.getData) {
            return (

                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmit}
                    >Approve
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmitReturn}
                    >Return
                    </button>
                </div>

            )
        }
    }
    createTableData = (id, type, dueDate,appliedBy,applicationDate,status) => {

        return([
            type,dueDate,appliedBy,applicationDate,status
        ])

    };
    componentDidMount() {

        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    let checkerListUrl = backEndServerURL + "/checkers";
                    axios.get(checkerListUrl, {withCredentials: true})
                        .then((response) => {
                            let deferalListUrl=backEndServerURL + "/case/deferral/"+this.props.app_uid;
                            axios.get(deferalListUrl,{withCredentials:true})
                                .then((response)=>{
                                    console.log(response.data);
                                    let tableArray=[];
                                    response.data.map((deferal) => {
                                        tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate,deferal.appliedBy,deferal.applicationDate,deferal.status));

                                    });
                                    this.setState({
                                        getDeferralList:tableArray
                                    })

                                })
                                .catch((error)=>{
                                    console.log(error);
                                })
                            this.setState({
                                getCheckerList: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    console.log(response.data)
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        showValue: true,
                        inputData: response.data,
                        varValue: response.data,
                        appData: response.data,
                        loaderNeeded:true
                    });

                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }
    renderBearerApproval = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, csBearer,
                    this.updateComponent)
            )

        }
        return;
    }
    renderRemarks = () => {
        if (this.state.getData) {
            return (


                CommonJsonFormComponent.renderJsonForm(this.state, remarks, this.updateComponent)


            )
        }
    }

    updateComponent = () => {
        this.forceUpdate();
    };


    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }
    // handleSubmit = (event) => {
    //     event.preventDefault();
    //
    //     this.state.inputData.checker_approval = this.state.inputData.checkerApproval;
    //     if(this.state.fileUploadData.scanningFile!==undefined){
    //         let fileUploadPath = backEndServerURL + "/case/upload";
    //         let types = 'Attachments';
    //         let files = this.state.fileUploadData.scanningFile;
    //         console.log("dfghjk")
    //         console.log(files)
    //         let formData=new FormData();
    //         formData.append("appId",this.props.appId)
    //         formData.append("file",files)
    //         formData.append("type",types)
    //         axios({
    //             method: 'post',
    //             url: fileUploadPath,
    //             data: formData,
    //             withCredentials: true,
    //             headers: {'content-type': 'multipart/form-data'}
    //         })
    //             .then((response) => {
    //
    //                 console.log(response);
    //
    //
    //             })
    //             .catch((error) => {
    //                 console.log(error)
    //             })
    //     }
    //
    //
    //     var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
    //     axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
    //         .then((response) => {
    //             var url = backEndServerURL + "/case/route/" + this.props.appId;
    //
    //             axios.get(url, {withCredentials: true})
    //                 .then((response) => {
    //
    //
    //                     console.log(response.data);
    //                     this.setState({
    //                         title: "Successfull!",
    //                         notificationMessage: "Successfully Routed!",
    //                         alert: true
    //                     })
    //                     this.props.closeModal()
    //
    //                 })
    //                 .catch((error) => {
    //                     console.log(error);
    //                     if (error.response.status === 452) {
    //                         Functions.removeCookie();
    //
    //                         this.setState({
    //                             redirectLogin: true
    //                         })
    //
    //                     }
    //                 });
    //         })
    //         .catch((error) => {
    //             console.log(error)
    //         });
    //
    //
    // }




    renderFileUpload = () => {
        if (this.state.getData) {
            return (

                <Grid item xs={12}>
                    {FileTypeComponent.file(this.state, this.updateComponent, fileUpload)}
                </Grid>

            )
        }
        return;
    }

    renderDefferalData = () => {


        if (this.state.getDeferralList.length>0) {

            return(
                <div  >
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date","Created By","Application Date","Status"]}
                        tableData={this.state.getDeferralList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }

    renderJsonForm = () => {



            if (this.state.showValue) {

                return (
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>


                            <Grid container spacing={1}>
                                <ThemeProvider theme={theme}>
                                    <br/><br/>
                                    <Grid item xs={6}>
                                        <label for="customerName"><font size="3"><b> Customer Name :</b></font></label>

                                        {this.state.customerName}</Grid>
                                    <Grid item xs={6}>
                                        <label for="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                        {this.state.cbNumber}
                                    </Grid>
                                    {
                                        CommonJsonFormComponent.renderJsonForm(this.state, this.props.jsonForm, this.updateComponent)
                                    }
                                </ThemeProvider>
                            </Grid>

                            <br/>  <br/>
                            {this.renderBearerApproval()}

                            {this.renderRemarks()}

                            <ThemeProvider theme={theme}>
                                <Grid container spacing={1}>
                                    {/*{this.renderImageLink()}*/}
                                </Grid>
                            </ThemeProvider>
                            <br/><br/><br/>
                            {/*{this.renderUploadButton()}*/}
                            <Dialog
                                fullWidth="true"
                                maxWidth="md"
                                open={this.state.accountDetailsModal}>
                                <DialogContent>

                                    <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                                </DialogContent>
                            </Dialog>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.uploadModal}>
                                <DialogContent>

                                    <LiabilityUploadModal appId={this.state.appId}
                                                          closeModal={this.closeUploadModal}/>
                                </DialogContent>
                            </Dialog>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.imageModalBoolean}>
                                <DialogContent>

                                    <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                                </DialogContent>
                            </Dialog>
                            <br/>
                            <br/>

                            {this.renderButton()}


                        </GridItem>
                    </GridContainer>
                )}
    }

    // renderImageLink = () => {
    //
    //     if (this.state.getImageBoolean) {
    //         return (
    //             this.state.getImageLink.map((data) => {
    //                 return (
    //                     <Grid item={6}>
    //                         <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
    //                     </Grid>
    //                 )
    //             })
    //
    //         )
    //
    //
    //     }
    //
    // }

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }


            return (
                <div>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>
                            {this.renderJsonForm()}



                        </ThemeProvider>
                    </Grid>


                </div>


            )

    }





}

export default withStyles(styles)(VerifyCheckerAccountMaintenance);

