import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import FormControl from '@material-ui/core/FormControl';
import Notification from "../../NotificationMessage/Notification";
import Grow from "@material-ui/core/Grow";
import {ThemeProvider} from "@material-ui/styles";
import CloseIcon from '@material-ui/icons/Close';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import SingleImageShow from "../SingleImageShow";
import AccountNoGenerate from ".././AccountNoGenerate";
import {CSExistJsonFormIndividualAccountOpening} from "../WorkflowJsonForm3";
import LiabilityUploadModal from "../LiabilityUploadModal";
import FormSample from "../../JsonForm/FormSample";
import {CSjsonFormIndividualAccountOpeningSearch} from "../WorkflowJsonForm";
import Fab from "@material-ui/core/Fab";
import Pageview from "@material-ui/core/SvgIcon/SvgIcon";
import Checkbox from "@material-ui/core/Checkbox";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};
var csDeferal = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferral",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther = {
    "varName": "deferalOther",
    "type": "text",
    "label": "Please Specify",
    "grid": 6
};

var Deferral = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

let JsonFormCasaIndividualDeferal = {

    "type": {
        "varName": "type",
        "label": "Deferral Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}


let accountMaintenanceSearch = [
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account No",

    }
]
var bearerApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Bearer Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
var bearerBomApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
const fDRMaintenanceList = [
    {
        "varName": "fDRMTDREncashment",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment",
        "grid": 6,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },
    {
        "varName": "fDDPSFaceValueRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },


    {
        "varName": "fDRInterestPayment",
        "type": "checkbox",
        "label": "FDR Interest Payment",
        "grid": 6,


    },
    {
        "varName": "fDRInterestPaymentRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "linkACChange",
        "type": "checkbox",
        "label": "Link A/C Change",
        "grid": 6,

    },
    {
        "varName": "linkACChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "fDRMTDREncashmentCertificate",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment Certificate",
        "grid": 6,
    },
    {
        "varName": "fDRMTDREncashmentCertificateRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "tenorChange",
        "type": "checkbox",
        "label": "Tenor Change",
        "grid": 6,
    },
    {
        "varName": "tenorChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "schemeChange",
        "type": "checkbox",
        "label": "Scheme Change",
        "grid": 6,

    },
    {
        "varName": "schemeChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

    {
        "varName": "duplicateAdviceChange",
        "type": "checkbox",
        "label": "Duplicate Advice Change",
        "grid": 6,
    },
    {
        "varName": "duplicateAdviceChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

    {
        "varName": "sourceTaxReversal",
        "type": "checkbox",
        "label": "Source Tax Reversal",
        "grid": 6,

    },
    {
        "varName": "sourceTaxReversalRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
//////
    {
        "varName": "fDNomineeUpdateChange",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment",
        "grid": 6,
        "conditionalVarName": "fDNomineeUpdateChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "fDNomineeUpdateChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,
        "conditionalVarName": "fDNomineeUpdateChange",
        "conditionalVarValue": true,

    },


    {
        "varName": "fDTenorSchemeChange",
        "type": "checkbox",
        "label": "FDR Interest Payment",
        "grid": 6,


    },
    {
        "varName": "fDTenorSchemeChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "sourceTaxReversalChange",
        "type": "checkbox",
        "label": "Link A/C Change",
        "grid": 6,

    },
    {
        "varName": "sourceTaxReversalChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "fDSchemeChangeWCCChange",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment Certificate",
        "grid": 6,
    },
    {
        "varName": "fDSchemeChangeWCCChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "fDNomineesGuardianUpdateChange",
        "type": "checkbox",
        "label": "Tenor Change",
        "grid": 6,
    },
    {
        "varName": "fDNomineesGuardianUpdateChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

]


class FDRMaintenance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            getDedupData: false,
            dedupData: [[" ", " "]],
            relatedData: [[" ", " "]],
            tableData: [],
            sourceMappingData: [],
            getsearchValue: [],
            getCustomerId: '',
            getAccountType: '',
            accountOpeningFromModal: false,
            SelectedData: '',
            tabMenuSelect: 'INDIVIDUAL',
            existingAcoountOpeningModal: false,
            newAcoountOpeningModal: false,
            searchTableData: null,
            searchTableRelatedData: null,
            oldAccountData: [],

            dataNotFound: false,
            CustomerModal: false,
            uniqueId: '',
            IDENTIFICATION_NO: '',
            id: '',
            alert: false,
            value: "INDIVIDUAL",
            NonIndividualabel: "",
            individualLabel: "Individual A/C",
            content: "INDIVIDUAL",
            anchorEl: null,
            anchorE2: null,
            individualDropdownOpen: null,
            objectForJoinAccount: {},
            numberOfJointMember: "",
            notificationMessage: "CB number / NID / Passport / Birth Certificate or Driving License is Required!!",

            accountNumber: "",
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},

            csDeferalPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            caseId: "",
            title: "",
            app_uid: "-1",
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "",
            },
            fileUploadData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            maintenanceType: '',
            getDocument: false,
            customerName: 'Jamal',
            cbNumber: 12345,
            digitTIN: false,
            titleChange: false,
            nomineeUpdate: false,
            updateChangePhotoId: false,
            contactNumberChange: false,
            emailAddressChange: false,
            estatementEnrollment: false,
            addressChange: false,
            otherInformationChange: false,
            signatureCard: false,
            dormantAccountActivation: false,
            dormantAccountDataUpdate: false,
            schemeMaintenanceLinkChange: false,
            schemeMaintenance: false,
            mandateUpdateChange: false,
            cityLive: false,
            projectRelatedDataUpdateADUP: false,
            accountSchemeClose: false,
            lockerSIOpen: false,
            lockerSIClose: false,
            others: false,
            bearerApproval: '',
            fDACOpeningDate:"",
            tenor:"",
            amount:"",
            debitACNumber:"",
            fDCategory:"",
            eTinNumber:"",
            fDRMTDREncashment:false,
            fDRInterestPayment:false,
            linkACChange:false,
            fDRMTDREncashmentCertificate:false,
            tenorChange:false,
            schemeChange:false,
            duplicateAdviceChange:false,
            sourceTaxReversal:false,
        }
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }

    addDeferalForm() {
        if (this.state.inputData["csDeferal"] === "YES") {
            return this.state.values.map((el, i) =>
                <React.Fragment>

                    <Grid item xs="6">
                        {
                            this.dynamicDeferral(el)
                        }
                    </Grid>
                    <Grid item xs="6">
                        {this.dynamicDeferralOther(el)}
                    </Grid>
                    <Grid item xs="6">
                        {
                            this.dynamicDate(el)
                        }
                    </Grid>


                    <Grid item xs="3">
                        <button
                            style={{float: 'right',}}
                            className="btn btn-outline-danger"
                            type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                        >
                            Remove
                        </button>
                    </Grid>

                </React.Fragment>
            )
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {
        if (this.state.inputData["csDeferal"] === "YES") {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'left',

                    }}

                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}


                >Add Deferral</button>
            )
        } else {
            return;
        }
    }
    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(Deferral));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    renderSelectMenu = () => {

        if (this.props.appId !== undefined) {
            return (

                <Grid item xs='12'>


                    <TextField

                        value="YES"
                        label="Need Deferral?"

                        InputProps={{
                            readOnly: true
                        }}
                    />


                </Grid>

            )

        } else {
            console.log(this.state.inputData["csDeferal"]);
            return (


                <Grid item xs='12'>
                    <FormLabel component="legend">Need Deferral?</FormLabel>
                    <RadioGroup aria-label="csDeferal" name="csDeferal" value={this.state.inputData["csDeferal"]}
                                onChange={this.handleChange}>
                        <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                        <FormControlLabel value="NO" control={<Radio/>} label="NO"/>

                    </RadioGroup>

                </Grid>

            )
        }


    }


    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }
    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }


    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false
        })
    }
    closeUploadModal = (data) => {
        this.setState({
            uploadModal: false,
            imgeListLinkSHow: true,
            getImageLink: data
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    bearerApproval = () => {
        if (this.state.getData) {

            return (

                <Grid item xs={12}>
                    {SelectComponent.select(this.state, this.updateComponent, bearerApproval)}
                </Grid>

            )

        }
        return;
    }

    componentDidMount() {
        this.updateComponent()
        // this.state.inputData["csDeferal"] ="NO";

        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;
            console.log(this.props.appId);
            axios.get(url,
                {withCredentials: true})
                .then((response) => {

                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,

                        showValue: true,
                        appId: this.props.appId
                    });
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            response.data.map((Deferral) => {
                                tableArray.push(this.createTableData(Deferral.id, Deferral.type, Deferral.dueDate, Deferral.appliedBy, Deferral.applicationDate, Deferral.status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })


                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            console.log(url)
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response.data)

                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        showValue: true,
                        getData: true,
                        getDocument: true,

                    });


                })
                .catch((error) => {
                    console.log(error);
                })
        }


    }


    renderSearchForm = () => {

        if (this.state.showValue)

            return (
                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>

                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, accountMaintenanceSearch, this.updateComponent)
                            }
                        </ThemeProvider>
                    </Grid>
                    <br/><br/><br/>
                    <button
                        onClick={this.accountModal}
                        className="btn btn-danger">
                        Search
                    </button>
                </Grid>

            )

    }
    accountModal = () => {
        this.setState({
            searchTableData: true,
            accountNumber: this.state.inputData.accountNumber,

        })
    };
    renderSearchData = () => {

        if (this.state.searchTableData && this.state.showValue) {

            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>


                        <Grid container spacing={1}>
                            <ThemeProvider theme={theme}>
                                <br/><br/>
                                <Grid item xs={6}>
                                    <label for="customerName"><font size="3"><b> Customer Name :</b></font></label>

                                    {this.state.customerName}</Grid>
                                <Grid item xs={6}>
                                    <label for="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                    {this.state.cbNumber}
                                </Grid>
                                <Grid item xs={6}>
                                    <label for="fDACOpeningDate"><font size="3"><b> FD A/C Opening Date:</b></font></label>

                                    {this.state.fDACOpeningDate}</Grid>
                                <Grid item xs={6}>
                                    <label for="tenor"><b><font size="3">Tenor:</font></b> </label>

                                    {this.state.tenor}
                                </Grid>
                                <Grid item xs={6}>
                                    <label for="amount"><b><font size="3">Amount:</font></b> </label>

                                    {this.state.amount}
                                </Grid>
                                <Grid item xs={6}>
                                    <label for="debitACNumber"><font size="3"><b> Debit A/C Number:</b></font></label>

                                    {this.state.debitACNumber}</Grid>
                                <Grid item xs={6}>
                                    <label for="fDCategory"><b><font size="3">FD Category:</font></b> </label>

                                    {this.state.fDCategory}
                                </Grid>

                                <Grid item xs={6}>
                                    <label for="eTinNumber"><b><font size="3">E-Tin Number:</font></b> </label>

                                    {this.state.eTinNumber}
                                </Grid>
                                {
                                    CommonJsonFormComponent.renderJsonForm(this.state, fDRMaintenanceList, this.updateComponent)
                                }
                            </ThemeProvider>
                        </Grid>

                        <br/><br/>
                        <Grid container spacing={3}>
                            <ThemeProvider theme={theme}>

                                <Grid item xs='12'>
                                    {this.bearerApproval()}
                                </Grid>
                                {this.renderSelectMenu()}


                                <Grid item xs='12'>
                                    {
                                        this.renderAddButtonShow()
                                    }
                                </Grid>
                                <br/>


                                {
                                    this.addDeferalForm()
                                }

                            </ThemeProvider>
                        </Grid>

                        <ThemeProvider theme={theme}>
                            <Grid container spacing={1}>
                                {this.renderImageLink()}
                            </Grid>
                        </ThemeProvider>
                        <br/><br/><br/>
                        {this.renderUploadButton()}
                        <Dialog
                            fullWidth="true"
                            maxWidth="md"
                            open={this.state.accountDetailsModal}>
                            <DialogContent>

                                <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.uploadModal}>
                            <DialogContent>

                                <LiabilityUploadModal appId={this.state.appId}
                                                      closeModal={this.closeUploadModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.imageModalBoolean}>
                            <DialogContent>

                                <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                            </DialogContent>
                        </Dialog>
                        <br/>
                        <br/>

                        <center>
                            <button
                                className="btn btn-outline-danger"
                                style={{
                                    verticalAlign: 'middle',
                                }}
                                onClick={this.handleSubmit}

                            >
                                Submit
                            </button>
                        </center>


                    </GridItem>
                </GridContainer>

            )
        }
    }


    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.deferalNeeded) {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

            }
            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    // console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.customerName = this.state.customerName;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "FDRMaintenance";

        if (this.state.inputData.bearerApproval === "BM Approval")
            data.cs_bearer = "BM";
        else if(this.state.inputData.bearerApproval === "BM Approval & Call Center Approval")
            data.cs_bearer = "BOTH";
        else if( this.state.inputData.bearerApproval === "Call Center Approval")
            data.cs_bearer = "CALL_CENTER";


        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log("Cs Variable")
                console.log(response.data);
                // this.setState({
                //     appData: response.data
                // })


                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",


                        })


                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 652) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }

    render() {
        console.log(this.state.appData)
        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }


        return (

            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>FDR
                                Maintenance</h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                {this.renderSearchForm()}
                                <br/>
                                <br/>
                                {this.renderSearchData()}
                                <br/>
                                <br/>


                            </div>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );


    }

}

export default withStyles(styles)(FDRMaintenance);
