import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import FormControl from '@material-ui/core/FormControl';
import Notification from "../../NotificationMessage/Notification";
import Grow from "@material-ui/core/Grow";
import {ThemeProvider} from "@material-ui/styles";
import CloseIcon from '@material-ui/icons/Close';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import SingleImageShow from "../SingleImageShow";
import AccountNoGenerate from ".././AccountNoGenerate";
import {CSExistJsonFormIndividualAccountOpening} from "../WorkflowJsonForm3";
import LiabilityUploadModal from "../LiabilityUploadModal";
import FormSample from "../../JsonForm/FormSample";
import {CSjsonFormIndividualAccountOpeningSearch} from "../WorkflowJsonForm";
import Fab from "@material-ui/core/Fab";
import Pageview from "@material-ui/core/SvgIcon/SvgIcon";
import Checkbox from "@material-ui/core/Checkbox";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};
var csDeferal = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferral",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther = {
    "varName": "deferalOther",
    "type": "text",
    "label": "Please Specify",
    "grid": 6
};

var Deferral = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

let JsonFormCasaIndividualDeferal = {

    "type": {
        "varName": "type",
        "label": "Deferral Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}


let accountMaintenanceSearch = [
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account No",

    }
]
var bearerApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Bearer Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
var bearerBomApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};
const callCenterMaintenanceList = [
    {" varName":"balanceInquiry",
        "type":"checkbox",
        "label":"Balance inquiry",
        "grid":12,

    },

    {"varName":"outstandingQuery",
        "type":"checkbox",
        "label":"Outstanding Query",
        "grid":12,

    },

    {" varName":"endorsementQuery",
        "type":"checkbox",
        "label":"Endorsement Query",
        "grid":12,

    },


    {"varName":"limitEnhancementQuery",
        "type":"checkbox",
        "label":"Limit Enhancement Query",
        "grid":12,

    },

    {"varName":"feesAndChargesQuery",
        "type":"checkbox",
        "label":"Fees and Charges Query",
        "grid":12
    },

    {"varName":" transactionDetails",
        "type":"checkbox",
        "label":"Transaction details",
        "grid":12,

    },

    {"varName":"cardAndChequeActivation ",
        "type":"checkbox",
        "label":"Card and Cheque Activation",
        "grid":12,

    },


    {" varName":"fCYExchangeRates",
        "type":"checkbox",
        "label":"FCY exchange rates",
        "grid":12,

    },



    {" varName":"currentMarketingPrograms",
        "type":"checkbox",
        "label":"Current marketing programs",
        "grid":12,

    },


    {"varName":"productInquiryAndServices",
        "type":"checkbox",
        "label":"Product inquiry and services",
        "grid":12,

    },
    {
        "varName": "transactionDeclineQuery",
        "type": "checkbox",
        "label": "Transaction Decline Query",
        "grid": 12,
    },

    {" varName":"cardActivationUnblock",
        "type":"checkbox",
        "label":"Card Activation/Unblock",
        "grid":12,

    },

    {
        "varName": "foreignPartEnableDisableRequest",
        "type": "checkbox",
        "label": "Foreign Part Enable/Disable Request",
        "grid": 12,

    },

    {
        "varName": "eCommerceEnableDisableRequest",
        "type": "checkbox",
        "label": "E-Commerce Enable/Disable Request",
        "grid": 12,

    },

    {
        "varName": "cardChequeProcessingRequest",
        "type": "checkbox",
        "label": "Card Cheque Processing Request",
        "grid": 12,

    },

    {
        "varName": "cardChequeActivation",
        "type": "checkbox",
        "label": "Card Cheque Activation",
        "grid": 12,

    },

    {
        "varName": "estatementResend",
        "type": "checkbox",
        "label": "E-statementResend",
        "grid": 12,

    },

    {" varName":"servicesFromOverseas",
        "type":"checkbox",
        "label":"Services from overseas /abroad number without system number",
        "grid":12,

    },

    {
        "varName": "generalInquiry",
        "type": "checkbox",
        "label": "General Inquiry",
        "grid": 12,

    },

    {
        "varName": "merchantRequestInstruction",
        "type": "checkbox",
        "label": "Merchant Request/Instruction",
        "grid": 12,

    },
/////
    {" varName":"accountBalanceQuery",
        "type":"checkbox",
        "label":"Account Balance Query",
        "grid":12,

    },

    {"varName":"paymentQueryTransactionDetails",
        "type":"checkbox",
        "label":"Payment Query/Transaction details",
        "grid":12,

    },

    {" varName":"pINGeneration",
        "type":"checkbox",
        "label":"PIN generation",
        "grid":12,

    },


    {"varName":"debitCardDeActivation",
        "type":"checkbox",
        "label":"Debit Card De/Activation",
        "grid":12,

    },

    {"varName":"debitCardReplaceRequest",
        "type":"checkbox",
        "label":"Debit Card Replace request",
        "grid":12
    },

    {"varName":"chequeLeafStop",
        "type":"checkbox",
        "label":"Cheque Leaf Stop",
        "grid":12,

    },

    {"varName":"chequeBookRequisition ",
        "type":"checkbox",
        "label":"Cheque Book Requisition",
        "grid":12,

    },


    {" varName":"onDemandStatement",
        "type":"checkbox",
        "label":"On Demand Statement",
        "grid":12,

    },



    {" varName":"onDemandCertificates",
        "type":"checkbox",
        "label":"On Demand Certificates",
        "grid":12,

    },


    {"varName":"accountBalanceConfirmation",
        "type":"checkbox",
        "label":"Account Balance Confirmation",
        "grid":12,

    },
    {
        "varName": "positivePayAdvanceConfirmation",
        "type": "checkbox",
        "label": "Positive Pay Advance Confirmation",
        "grid": 12,
    },

    {" varName":"debitCadForeignPartDeactive",
        "type":"checkbox",
        "label":"Debit Cad Foreign Part De/active",
        "grid":12,

    },

    {
        "varName": "callCenterTPINRegistration",
        "type": "checkbox",
        "label": "Call Center (TPIN) Registration",
        "grid": 12,

    },

    {
        "varName": "eCommerceDeactive",
        "type": "checkbox",
        "label": "E-Commerce De/active",
        "grid": 12,

    },

    {
        "varName": "iBankPasswordReset",
        "type": "checkbox",
        "label": "I'Bank Password Reset",
        "grid": 12,

    },

    {
        "varName": "citytouchUserID",
        "type": "checkbox",
        "label": "Citytouch User ID, Password resent request execution",
        "grid": 12,

    },

    {
        "varName": "citytouchDigitalRegistration",
        "type": "checkbox",
        "label": "Citytouch Digital Registration",
        "grid": 12,

    },

    {" varName":"citytouchGeneralQuery",
        "type":"checkbox",
        "label":"Citytouch General Query",
        "grid":12,

    },

    {
        "varName": "complainResolutionMail",
        "type": "checkbox",
        "label": "Complain Resolution (Mail)",
        "grid": 12,

    },

    {
        "varName": "abandonedCustomersCallBack ",
        "type": "checkbox",
        "label": "Abandoned Customers Call Back ",
        "grid": 12,

    },
/////
    {" varName":"feedbackCall",
        "type":"checkbox",
        "label":"Feedback Call (Helpdesk/E-mail)",
        "grid":12,

    },

    {"varName":"capturedCard",
        "type":"checkbox",
        "label":"Captured Card",
        "grid":12,

    },

    {" varName":"positivePayHelpdesk",
        "type":"checkbox",
        "label":"Positive Pay (Help desk)",
        "grid":12,

    },


    {"varName":"promotionalSMSEmail",
        "type":"checkbox",
        "label":"Promotional SMS/Email",
        "grid":12,

    },

    {"varName":"nPSTNPSCallBack",
        "type":"checkbox",
        "label":"NPS/TNPS Call Back",
        "grid":12
    },

    {"varName":"cityMaxxActivationThroughOB",
        "type":"checkbox",
        "label":"City Maxx Activation through OB",
        "grid":12,

    },

    {"varName":"debitCardCollectionCalls ",
        "type":"checkbox",
        "label":"Debit Card Collection Calls",
        "grid":12,

    },


    {" varName":"generalProductInformation",
        "type":"checkbox",
        "label":"General Product information",
        "grid":12,

    },



    {" varName":"webCaht",
        "type":"checkbox",
        "label":"Web Caht",
        "grid":12,

    },


    {"varName":"exchangeRate",
        "type":"checkbox",
        "label":"Exchange Rate",
        "grid":12,

    },
    {
        "varName": "appointmentsForAsset",
        "type": "checkbox",
        "label": "Appointments for Asset & Liability Products (Retail/SME/Manarah)",
        "grid": 12,
    },

    {" varName":"branchATMAgentBankingLocation",
        "type":"checkbox",
        "label":"Branch/ATM/Agent Banking Location",
        "grid":12,

    },

    {
        "varName": "islamiBanking",
        "type": "checkbox",
        "label": "Islami Banking",
        "grid": 12,

    },

    {
        "varName": "emailAddressandMobileNumber",
        "type": "checkbox",
        "label": "Email Address and Mobile Number Change Request",
        "grid": 12,

    },

    {
        "varName": "cardChequeBookLocation",
        "type": "checkbox",
        "label": "Card/Cheque Book Location Confirmation",
        "grid": 12,

    },

    {
        "varName": "loanApplicationStatus",
        "type": "checkbox",
        "label": "Loan Application Status",
        "grid": 12,

    },

    {
        "varName": "accountBalanceInfo",
        "type": "checkbox",
        "label": "Account Balance Info",
        "grid": 12,

    },

    {" varName":"last5Transaction",
        "type":"checkbox",
        "label":"Last 5 Transaction",
        "grid":12,

    },

    {
        "varName": "creditCardBillsPayment",
        "type": "checkbox",
        "label": "Credit Card Bills Payment",
        "grid": 12,

    },

    {
        "varName": "mailcallescalationcall",
        "type": "checkbox",
        "label": "Mail call/escalation call",
        "grid": 12,

    },
    //////////////////
    {" varName":"helpDeskcall",
        "type":"checkbox",
        "label":"Help Desk call",
        "grid":12,

    },

    {"varName":"supplyConfirmationcall",
        "type":"checkbox",
        "label":"Supply Confirmation call",
        "grid":12,

    },

    {" varName":"hardCopyVerificationcall",
        "type":"checkbox",
        "label":"Hard Copy Verification call",
        "grid":12,

    },


    {"varName":"destructioncallCardCardcheque",
        "type":"checkbox",
        "label":" Destruction call Card/ Card cheque",
        "grid":12,

    },

    {"varName":"cardChequeVerificationcall",
        "type":"checkbox",
        "label":"Card Cheque Verification call",
        "grid":12
    },

    {"varName":"flexiloancall",
        "type":"checkbox",
        "label":"Flexi loan call",
        "grid":12,

    },

    {"varName":"limitEnhancementCall ",
        "type":"checkbox",
        "label":"Limit Enhancement Call",
        "grid":12,

    },


    {" varName":"merchantVerification",
        "type":"checkbox",
        "label":"Merchant Verification of New Merchant Agreement",
        "grid":12,

    },



    {" varName":"cardActivation",
        "type":"checkbox",
        "label":"Card Activation",
        "grid":12,

    },


    {"varName":"citytouchDisputeAccountCards",
        "type":"checkbox",
        "label":"Citytouch Dispute (Account/Cards)",
        "grid":12,

    },
    {
        "varName": "citytouchDisputeFeedbackCall",
        "type": "checkbox",
        "label": "Citytouch Dispute Feedback Call",
        "grid": 12,
    },

    {" varName":"disputedTransaction",
        "type":"checkbox",
        "label":"Disputed Transaction",
        "grid":12,

    },

    {
        "varName": "loanSanctionLetterDelivery",
        "type": "checkbox",
        "label": "Loan Sanction Letter Delivery",
        "grid": 12,

    },

    {
        "varName": "aTMCDMDownIssues",
        "type": "checkbox",
        "label": "ATM/CDM Down issues",
        "grid": 12,

    },

    {
        "varName": "debitCardBranchChangeRequest",
        "type": "checkbox",
        "label": "Debit Card Branch Change Request",
        "grid": 12,

    },

    {
        "varName": "chequeBookBranchChangeRequest",
        "type": "checkbox",
        "label": "Cheque Book Branch Change Request",
        "grid": 12,

    },

    {
        "varName": "feedbackonBranchServices",
        "type": "checkbox",
        "label": "Feedback on Branch Services",
        "grid": 12,

    },

    {" varName":"debitCardNameCorrection",
        "type":"checkbox",
        "label":"Debit Card Name Correction",
        "grid":12,

    },

    {
        "varName": "wrongEmbossedNameonCard",
        "type": "checkbox",
        "label": "Wrong Embossed Name on Card",
        "grid": 12,

    },

    {
        "varName": "leadsgenerationandSalesrequest",
        "type": "checkbox",
        "label": "Leads generation and sales request",
        "grid": 12,

    },
/////
    {" varName":"supplementaryCardSpendingLimitNotSet",
        "type":"checkbox",
        "label":"Supplementary Card Spending Limit Not Set",
        "grid":12,

    },

    {"varName":"cardReplacement",
        "type":"checkbox",
        "label":"Card Replacement",
        "grid":12,

    },

    {" varName":"cardChequebooklostReport",
        "type":"checkbox",
        "label":"Card Cheque book lost Report/ Leaf Lost or payment stop Report/ Re-issue request",
        "grid":12,

    },


    {"varName":"limitRearrangeRequest",
        "type":"checkbox",
        "label":"Limit Re-arrange Request",
        "grid":12,

    },

    {"varName":"limitRectificationRequest",
        "type":"checkbox",
        "label":"Limit Rectification Request",
        "grid":12
    },

    {"varName":"limitTransfer",
        "type":"checkbox",
        "label":"Limit Transfer",
        "grid":12,

    },

    {"varName":"membershipRewards",
        "type":"checkbox",
        "label":"Membership Rewards (MR) Points Redemption Request",
        "grid":12,

    },


    {" varName":"eMIRequest",
        "type":"checkbox",
        "label":"EMI Request",
        "grid":12,

    },



    {" varName":"flexiloanRequest",
        "type":"checkbox",
        "label":"Flexi loan Request",
        "grid":12,

    },


    {"varName":"priorityPassissuerenewal",
        "type":"checkbox",
        "label":"Priority Pass issue/renewal",
        "grid":12,

    },
    {
        "varName": "citytouchOTPUnlock",
        "type": "checkbox",
        "label": "Citytouch OTP Unlock and OTP Creation request",
        "grid": 12,
    },

    {" varName":"citytouchstatuscheck",
        "type":"checkbox",
        "label":"Citytouch status check",
        "grid":12,

    },

    {
        "varName": "citytouchstatuschangerequest",
        "type": "checkbox",
        "label": "Citytouch status change request",
        "grid": 12,

    },

    {
        "varName": "citytouchActivationHard Copy",
        "type": "checkbox",
        "label": "Citytouch Activation (Hard Copy)",
        "grid": 12,

    },

    {
        "varName": "accountCardTaggingRequestCitytouch",
        "type": "checkbox",
        "label": "Account/Card Tagging Request in Citytouch",
        "grid": 12,

    },

    {
        "varName": "sDUpdateRequestforCitytouch",
        "type": "checkbox",
        "label": "SD Update Request for Citytouch",
        "grid": 12,

    },

    {
        "varName": "citytouchCloseDeleterequest",
        "type": "checkbox",
        "label": "Citytouch Close/Delete request",
        "grid": 12,

    },

    {" varName":"accountcreditcardTagrequest",
        "type":"checkbox",
        "label":"Account/credit card Tag request",
        "grid":12,

    },

    {
        "varName": "inwardChequeReturn",
        "type": "checkbox",
        "label": "Inward Cheque Return",
        "grid": 12,

    },

    {
        "varName": "positivePayment",
        "type": "checkbox",
        "label": "Positive Payment",
        "grid": 12,

    },
/////
    {" varName":"positivePayment",
        "type":"checkbox",
        "label":"Positive Payment",
        "grid":12,

    },

    {"varName":"cardClosure",
        "type":"checkbox",
        "label":"Card Closure",
        "grid":12,

    },

    {" varName":"productdowngradationRequest",
        "type":"checkbox",
        "label":"Product down-gradation Request",
        "grid":12,

    },


    {"varName":"limitEnhancement",
        "type":"checkbox",
        "label":"Limit Enhancement",
        "grid":12,

    },

    {"varName":"tPINChange",
        "type":"checkbox",
        "label":"TPIN Change",
        "grid":12
    },

    {"varName":"chequeLeafStop",
        "type":"checkbox",
        "label":"Cheque Leaf Stop",
        "grid":12,

    },

    {"varName":"returnCardChequeVoucherissue",
        "type":"checkbox",
        "label":"Return Card/ Cheque/Voucher issue",
        "grid":12,

    },


    {" varName":"customerretentionactivities",
        "type":"checkbox",
        "label":"Customer retention activities",
        "grid":12,

    },



    {" varName":"chequeStopPayment",
        "type":"checkbox",
        "label":"Cheque Stop Payment",
        "grid":12,

    },


    {"varName":"feeReversalCreditCard",
        "type":"checkbox",
        "label":"Fee Reversal (Credit Card)",
        "grid":12,

    },
    {
        "varName": "creditBalanceRefund",
        "type": "checkbox",
        "label": "Credit Balance Refund",
        "grid": 12,
    },

    {" varName":"paymentTransfer",
        "type":"checkbox",
        "label":"Payment Transfer",
        "grid":12,

    },

    {
        "varName": "balanceTransfer",
        "type": "checkbox",
        "label": "Balance Transfer",
        "grid": 12,

    },

    {
        "varName": "sMSAlert",
        "type": "checkbox",
        "label": "SMS Alert",
        "grid": 12,

    },

    {
        "varName": "merchantComplaint",
        "type": "checkbox",
        "label": "Merchant Complaint",
        "grid": 12,

    },

    {
        "varName": "disputeATMPOSCDM",
        "type": "checkbox",
        "label": "Dispute (ATM/POS/CDM)",
        "grid": 12,

    },
]


class CallCenterMaintenance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            varValue: [],
            getDedupData: false,
            dedupData: [[" ", " "]],
            relatedData: [[" ", " "]],
            tableData: [],
            sourceMappingData: [],
            getsearchValue: [],
            getCustomerId: '',
            getAccountType: '',
            accountOpeningFromModal: false,
            SelectedData: '',
            tabMenuSelect: 'INDIVIDUAL',
            existingAcoountOpeningModal: false,
            newAcoountOpeningModal: false,
            searchTableData: null,
            searchTableRelatedData: null,
            oldAccountData: [],

            dataNotFound: false,
            CustomerModal: false,
            uniqueId: '',
            IDENTIFICATION_NO: '',
            id: '',
            alert: false,
            value: "INDIVIDUAL",
            NonIndividualabel: "",
            individualLabel: "Individual A/C",
            content: "INDIVIDUAL",
            anchorEl: null,
            anchorE2: null,
            individualDropdownOpen: null,
            objectForJoinAccount: {},
            numberOfJointMember: "",
            notificationMessage: "CB number / NID / Passport / Birth Certificate or Driving License is Required!!",

            accountNumber: "",
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},

            csDeferalPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            caseId: "",
            title: "",
            app_uid: "-1",
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "",
            },
            fileUploadData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            maintenanceType: '',
            getDocument: false,
            customerName: 'Jamal',
            cbNumber: 12345,
            digitTIN: false,
            titleChange: false,
            nomineeUpdate: false,
            updateChangePhotoId: false,
            contactNumberChange: false,
            emailAddressChange: false,
            estatementEnrollment: false,
            addressChange: false,
            otherInformationChange: false,
            signatureCard: false,
            dormantAccountActivation: false,
            dormantAccountDataUpdate: false,
            schemeMaintenanceLinkChange: false,
            schemeMaintenance: false,
            mandateUpdateChange: false,
            cityLive: false,
            projectRelatedDataUpdateADUP: false,
            accountSchemeClose: false,
            lockerSIOpen: false,
            lockerSIClose: false,
            others: false,
            bearerApproval: '',

        }
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }

    addDeferalForm() {
        if (this.state.inputData["csDeferal"] === "YES") {
            return this.state.values.map((el, i) =>
                <React.Fragment>

                    <Grid item xs="6">
                        {
                            this.dynamicDeferral(el)
                        }
                    </Grid>
                    <Grid item xs="6">
                        {this.dynamicDeferralOther(el)}
                    </Grid>
                    <Grid item xs="6">
                        {
                            this.dynamicDate(el)
                        }
                    </Grid>


                    <Grid item xs="3">
                        <button
                            style={{float: 'right',}}
                            className="btn btn-outline-danger"
                            type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                        >
                            Remove
                        </button>
                    </Grid>

                </React.Fragment>
            )
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {
        if (this.state.inputData["csDeferal"] === "YES") {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'left',

                    }}

                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}


                >Add Deferral</button>
            )
        } else {
            return;
        }
    }
    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(Deferral));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    renderSelectMenu = () => {

        if (this.props.appId !== undefined) {
            return (

                <Grid item xs='12'>


                    <TextField

                        value="YES"
                        label="Need Deferral?"

                        InputProps={{
                            readOnly: true
                        }}
                    />


                </Grid>

            )

        } else {
            console.log(this.state.inputData["csDeferal"]);
            return (


                <Grid item xs='12'>
                    <FormLabel component="legend">Need Deferral?</FormLabel>
                    <RadioGroup aria-label="csDeferal" name="csDeferal" value={this.state.inputData["csDeferal"]}
                                onChange={this.handleChange}>
                        <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                        <FormControlLabel value="NO" control={<Radio/>} label="NO"/>

                    </RadioGroup>

                </Grid>

            )
        }


    }


    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }
    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }


    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false
        })
    }
    closeUploadModal = (data) => {
        this.setState({
            uploadModal: false,
            imgeListLinkSHow: true,
            getImageLink: data
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    bearerApproval = () => {
        if (this.state.getData) {

            return (

                <Grid item xs={12}>
                    {SelectComponent.select(this.state, this.updateComponent, bearerApproval)}
                </Grid>

            )

        }
        return;
    }

    componentDidMount() {
        this.updateComponent()
        // this.state.inputData["csDeferal"] ="NO";

        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;
            console.log(this.props.appId);
            axios.get(url,
                {withCredentials: true})
                .then((response) => {

                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,

                        showValue: true,
                        appId: this.props.appId
                    });
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            response.data.map((Deferral) => {
                                tableArray.push(this.createTableData(Deferral.id, Deferral.type, Deferral.dueDate, Deferral.appliedBy, Deferral.applicationDate, Deferral.status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })


                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            console.log(url)
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response.data)

                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        showValue: true,
                        getData: true,
                        getDocument: true,

                    });


                })
                .catch((error) => {
                    console.log(error);
                })
        }


    }


    renderSearchForm = () => {
        // !this.state.deferalNeeded &&&& this.state.getData
        if (this.state.showValue)

            return (
                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>

                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, accountMaintenanceSearch, this.updateComponent)
                            }
                        </ThemeProvider>
                    </Grid>
                    <br/><br/><br/>
                    <button
                        onClick={this.accountModal}
                        className="btn btn-danger">
                        Search
                    </button>
                </Grid>

            )

    }
    accountModal = () => {
        this.setState({
            searchTableData: true,
            accountNumber: this.state.inputData.accountNumber,

        })
    };
    renderSearchData = () => {

        if (this.state.searchTableData && this.state.showValue) {

            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>


                        <Grid container spacing={1}>
                            <ThemeProvider theme={theme}>
                                <br/><br/>
                                <Grid item xs={6}>
                                    <label for="customerName"><font size="3"><b> Customer Name :</b></font></label>

                                    {this.state.customerName}</Grid>
                                <Grid item xs={6}>
                                    <label for="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                    {this.state.cbNumber}
                                </Grid>
                                {
                                    CommonJsonFormComponent.renderJsonForm(this.state, callCenterMaintenanceList, this.updateComponent)
                                }
                            </ThemeProvider>
                        </Grid>

                        <br/><br/>
                        <Grid container spacing={3}>
                            <ThemeProvider theme={theme}>

                                <Grid item xs='12'>
                                    {this.bearerApproval()}
                                </Grid>
                                {this.renderSelectMenu()}


                                <Grid item xs='12'>
                                    {
                                        this.renderAddButtonShow()
                                    }
                                </Grid>
                                <br/>


                                {
                                    this.addDeferalForm()
                                }

                            </ThemeProvider>
                        </Grid>

                        <ThemeProvider theme={theme}>
                            <Grid container spacing={1}>
                                {this.renderImageLink()}
                            </Grid>
                        </ThemeProvider>
                        <br/><br/><br/>
                        {this.renderUploadButton()}
                        <Dialog
                            fullWidth="true"
                            maxWidth="md"
                            open={this.state.accountDetailsModal}>
                            <DialogContent>

                                <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.uploadModal}>
                            <DialogContent>

                                <LiabilityUploadModal appId={this.state.appId}
                                                      closeModal={this.closeUploadModal}/>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.imageModalBoolean}>
                            <DialogContent>

                                <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                            </DialogContent>
                        </Dialog>
                        <br/>
                        <br/>

                        <center>
                            <button
                                className="btn btn-outline-danger"
                                style={{
                                    verticalAlign: 'middle',
                                }}
                                onClick={this.handleSubmit}

                            >
                                Submit
                            </button>
                        </center>


                    </GridItem>
                </GridContainer>

            )
        }
    }


    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.deferalNeeded) {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

            }
            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.customerName = this.state.customerName;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "CallCenterMaintenance";

        if (this.state.inputData.bearerApproval === "BM Approval")
            data.cs_bearer = "BM";
        else if(this.state.inputData.bearerApproval === "BM Approval & Call Center Approval")
            data.cs_bearer = "BOTH";
        else if( this.state.inputData.bearerApproval === "Call Center Approval")
            data.cs_bearer = "CALLCENTER";


        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log("Cs Variable")
                console.log(response.data);
                this.setState({
                    appData: response.data
                })


                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",


                        })


                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 652) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }

    render() {
        console.log(this.state.appData)
        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }


        return (

            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>Account
                                Maintenance</h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                {this.renderSearchForm()}
                                <br/>
                                <br/>
                                {this.renderSearchData()}
                                <br/>
                                <br/>


                            </div>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );


    }

}

export default withStyles(styles)(CallCenterMaintenance);
