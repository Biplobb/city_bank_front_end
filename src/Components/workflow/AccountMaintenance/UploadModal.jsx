import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Grow from "@material-ui/core/Grow";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {ThemeProvider} from "@material-ui/styles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import SingleImageShow from ".././SingleImageShow";
import FileTypeComponent from "../../JsonForm/FileTypeComponent";
import CircularProgress from "@material-ui/core/CircularProgress";
import DropdownComponent from "../../JsonForm/DropdownComponent";

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});

var fileUpload = {
    "varName": "scanningFile",
    "type": "file",
    "label": "Scan and Upload File",
    "grid": 12
};
var selectFileName = {
    "varName": "fileName",
    "type": "dropdown",
    "required":true,
    "label": "This photo name",

    "grid": 6
}

class UploadModal extends Component {
    state = {
        fileUploadData: {},
        getSplitFile: [],
        ScanningphotoShow: null,
        inputData: {},
        dropdownSearchData: {}

    }

    constructor(props) {
        super(props);


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    close = () => {
        this.props.closeModal();
    }
    handleSubmit = (event) => {

        event.preventDefault();
        this.setState({
            ScanningphotoShow: false
        })
        if (!this.state.ScanningphotoShow) {
            const formData = new FormData();
            formData.append("appId",this.props.appId);
            formData.append("file", this.state.fileUploadData[fileUpload.varName]);
            axios({
                method: 'post',
                url:  backEndServerURL + '/case/split',
                data: formData,
                withCredentials: true,
                headers: {'content-type': 'multipart/form-data'}
            })
                .then((response) => {
                    console.log(response.data)
                    this.setState({
                        getSplitFile: response.data,
                        ScanningphotoShow: true
                    })
                    console.log(response.data);


                })
                .catch((error) => {
                    console.log(error)
                })
        }else {


            let url = backEndServerURL + "/case/mapFiles";
            axios.post(url, {appId: this.props.appId, fileNames: this.state.inputData}, {withCredentials: true})
                .then((response) => {
                    this.props.closeModal(response.data);
                    this.setState({
                        ScanningphotoShow:null
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }

    }
    renderDropdownData = (varName) => {

        this.state.dropdownSearchData[varName] = [

            {
                "label": "Am01",
                "value": "Am01"
            },
            {
                "label":  "Am02",
                "value": "Am02"
            }

        ];


    }
    renderSelectOption = (data) => {
        return (
            {
                "varName": data,
                "type": "dropdown",
                "label": "This photo name",
                "grid": 6,

            }
        )
    }

    renderMultipleScanningphotoShow = () => {
        if (this.state.ScanningphotoShow) {
            return (
                <Grid container spacing={1}>
                    {
                        this.state.getSplitFile.map((data) => {
                            return (

                                <Grid item xs={4}>
                                    <div>
                                        <br/>
                                        {DropdownComponent.dropdown(this.state, this.updateComponent, this.renderSelectOption(data))}
                                        <br/>
                                        {this.renderDropdownData(data)}
                                        <br/>

                                    </div>
                                    <img width='90%' src={backEndServerURL + "/file/" + data} alt=""/>

                                    <br/>
                                </Grid>

                            )
                        })

                    }
                </Grid>
            )
        } else if (this.state.ScanningphotoShow === false) {
            return (

                <CircularProgress style={{marginLeft: '33%'}}/>


            )
        } else {

        }
    }
    render() {
        const {classes} = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>Upload
                                File</h4>

                        </CardHeader>
                        <CardBody>
                            <br/>

                            <ThemeProvider theme={theme}>
                                <div>
                                    <Grid container spacing={3}>
                                        {FileTypeComponent.file(this.state, this.updateComponent, fileUpload)}
                                        {this.renderMultipleScanningphotoShow()}
                                    </Grid>
                                </div>


                                <div>

                                    <br/>
                                    <br/>
                                    <center>
                                        <button
                                            className="btn btn-outline-danger"
                                            style={{
                                                verticalAlign: 'right',

                                            }}

                                            type='button' value='add more'
                                            onClick={this.handleSubmit}
                                        >Submit
                                        </button>
                                    </center>

                                </div>
                            </ThemeProvider>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(UploadModal);