
import {

    BOMTaxCertificateMaintenance,
    BOMDebitCreditMaintenance,
    CallCenterAccountMaintenance,
    CALLFdrMaintenance,
} from "../WorkflowJsonForm3";
import React, {Component} from "react";

import VerifyCallCenter from "./VerifyCallCenter";

class CallCenterInboxCase extends Component {
    inboxCase = () => {
        if (this.props.serviceType === 'Maintenance' && this.props.subServiceType==="AccountMaintenance") {

            return (<VerifyCallCenter closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                      subServiceType={this.props.subServiceType}
                                      titleName="Account Maintenance"
                                      jsonForm={CallCenterAccountMaintenance}
                                      appId={this.props.appUid}/>)
        } else if (this.props.serviceType === 'FDRMaintenance') {

            return (<VerifyCallCenter closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                      subServiceType={this.props.subServiceType}
                                      titleName="FDR Maintenance"
                                      serviceFormType={this.props.serviceFormType}
                                      jsonForm={CALLFdrMaintenance}
                                      appId={this.props.appUid}/>)
        } else if (this.props.serviceType === 'TaxCertificateMaintenance') {

            return (<VerifyCallCenter closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="Tax Certificate Maintenance"
                                       serviceFormType={this.props.serviceFormType}
                                       jsonForm={BOMTaxCertificateMaintenance}
                                       appId={this.props.appUid}/>)
        } else if (this.props.serviceType === 'DebitCreditMaintenance') {

            return (<VerifyCallCenter closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="Debit & Credit Maintenance Maintenance"
                                       serviceFormType={this.props.serviceFormType}
                                       jsonForm={BOMDebitCreditMaintenance}
                                       appId={this.props.appUid}/>)
        }

    }


    render() {

        return (
            <div>
                {this.inboxCase()}

            </div>
        )
    }

}

export default CallCenterInboxCase;