import React, {Component} from "react";
import {ThemeProvider} from "@material-ui/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import FormControl from '@material-ui/core/FormControl';
import Notification from "../../NotificationMessage/Notification";
import Paper from '@material-ui/core/Paper';
import CardBody from "../../Card/CardBody";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {MAKERAccountMaintenance} from "../WorkflowJsonForm3";
import GridList from "@material-ui/core/GridList";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import AccountNoGenerate from "../AccountNoGenerate";
import LiabilityUploadModal from "../LiabilityUploadModal";
import SingleImageShow from "../SingleImageShow";

const maintenanceList = [

    {
        "varName": "digitTIN",
        "type": "checkbox",
        "label": "12-Digit TIN",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {
        "varName": "digitTINOld",
        "type": "text",
        "label": "Existing 12-Digit TIN",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,

    },
    {
        "varName": "digitTINNew",
        "type": "text",
        "label": "New 12-Digit TIN",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "digitTIN",
        "conditionalVarValue": true,
    },
    {
        "varName": "titleChange",
        "type": "checkbox",
        "label": "Title Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "titleChangeOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "titleChangeNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChange",
        "conditionalVarValue": true,

    },

    {
        "varName": "nomineeUpdate",
        "type": "checkbox",
        "label": "Nominee Update",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },

    {
        "varName": "nomineeUpdateOld",
        "type": "text",
        "label": "Existing Nominee",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "nomineeUpdateNew",
        "type": "text",
        "label": "New Nominee",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "nomineeUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoId",
        "type": "checkbox",
        "label": "Update/Change Photo Id",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoIdOld",
        "type": "text",
        "label": "Existing Photo Id",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },
    {
        "varName": "updateChangePhotoIdNew",
        "type": "text",
        "label": "New Photo Id",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updateChangePhotoId",
        "conditionalVarValue": true,
    },

    {
        "varName": "contactNumberChange",
        "type": "checkbox",
        "label": "Contact Number Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "residenceNumberChangeOld",
        "type": "text",
        "label": "Existing  Residence Contact Number",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "residenceNumberChangeNew",
        "type": "text",
        "label": "New Residence Contact Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "officeNumberChangeOld",
        "type": "text",
        "label": "Existing  Office Contact Number",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },

    {
        "varName": "officeNumberChangeNew",
        "type": "text",
        "label": "New Office Contact Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mobileNumberChangeOld",
        "type": "text",
        "label": "Existing  Mobile Number",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mobileNumberChangeNew",
        "type": "text",
        "label": "New Mobile Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "emailAddressChange",
        "type": "checkbox",
        "label": "Email Address Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "emailAddressChangeExisting",
        "type": "text",
        "label": "Existing Email Address",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "emailAddressChangeNew",
        "type": "text",
        "label": "New Email Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "emailAddressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollment",
        "type": "checkbox",
        "label": "Estatement Enrollment",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollmentExisting",
        "type": "text",
        "label": "Existing Estatement Enrollment",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "estatementEnrollmentNew",
        "type": "text",
        "label": "New Estatement Enrollment",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "estatementEnrollment",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressChange",
        "type": "checkbox",
        "label": "Address Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPermanentChangeExisting",
        "type": "text",
        "label": "Existing  Permanent Address",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPermanentChangeNew",
        "type": "text",
        "label": "New Permanent Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPresentChangeExisting",
        "type": "text",
        "label": "Existing  Present Address",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressPresentChangeNew",
        "type": "text",
        "label": "New Present Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressOfficeChangeExisting",
        "type": "text",
        "label": "Existing  Office Address",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "addressOfficeChangeNew",
        "type": "text",
        "label": "New Office Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "otherInformationChange",
        "type": "checkbox",
        "label": "Other Information Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "fatherMotherNameRectifyExisting",
        "type": "text",
        "label": "Existing Father/Mother Name",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "fatherMotherNameRectifyNew",
        "type": "text",
        "label": "New Father/Mother Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "spouseNameExisting",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "spouseNameNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "dateOfBirthExisting",
        "type": "text",
        "label": "Existing Date Of Birth",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "dateOfBirthNew",
        "type": "text",
        "label": "New Date Of Birth",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "employerNameExisting",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "employerNameNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "professionExisting",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "otherInformationChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivation",
        "type": "checkbox",
        "label": "Dormant Account Activation",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivationExisting",
        "type": "text",
        "label": "Existing Dormant Account Activation",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountActivationNew",
        "type": "text",
        "label": "New Dormant Account Activation",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountActivation",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "linkChangeExisting",
        "type": "text",
        "label": "Existing Link A/C",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "linkChangeNew",
        "type": "text",
        "label": "New Link A/C",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "installmentRegularizationExisting",
        "type": "text",
        "label": "Existing Installment Regularization",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "installmentRegularizationNew",
        "type": "text",
        "label": "New Installment Regularization",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChange",
        "type": "checkbox",
        "label": "Mandate Update/Change",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChangeExisting",
        "type": "text",
        "label": "Existing Mandate",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "mandateUpdateChangeNew",
        "type": "text",
        "label": "New Mandate",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateUpdateChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CTR, CIB etc",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cTRExisting",
        "type": "text",
        "label": "Existing CTR Data",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "cTRNew",
        "type": "text",
        "label": "New CTR Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,

    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "others",
        "type": "checkbox",
        "label": "Others",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
        "conditional": true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "othersExisting",
        "type": "text",
        "label": "Existing Others",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "othersNew",
        "type": "text",
        "label": "New Others",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "others",
        "conditionalVarValue": true,
    },
    {
        "varName": "title",
        "type": "title",
        "label": "AC ACM",
        "grid": 12,

    },
    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 6,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 6,
        required: true,
    },

    {
        "varName": "address1",
        "type": "text",
        "label": "Address 1",
        "grid": 6,
        required: true,
    },

    {
        "varName": "address2",
        "type": "text",
        "label": "Address 2",
        "grid": 6,

    },

    {
        "varName": "cityCode1",
        "type": "text",
        "label": "City Code",
        "grid": 6,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 6,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 6,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "label": "Country",
        "grid": 6,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 6,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "grid": 6,
        required: true,
    },

    {
        "varName": "dob",
        "type": "text",
        "label": "Dob",
        "grid": 6,

    },

    {
        "varName": "guardian'sName",
        "type": "text",
        "label": "Guardian's Name",
        "grid": 6,

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 6,

    },

    {
        "varName": "address",
        "type": "text",
        "label": "Address",
        "grid": 6,

    },

    {
        "varName": "cityCode2",
        "type": "text",
        "label": "City Code",
        "grid": 6,

    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 6,

    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 6,

    },

    {
        "varName": "country2",
        "type": "text",
        "label": "Country",
        "grid": 6,

    },

    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 6,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 6,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 6,

    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 6,

    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 6,

    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 6,
        required: true,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 6,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 6,
        required: true,
    },

    {
        "varName": "contactPhoneNumber",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 6,
        required: true,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 6,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 6,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 6,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 6,
        required: true,
    },

    {

        "type": "title",
        "label": "AC CUMM",
        "grid": 12,
    },
    {
        "varName": "customerId",
        "type": "text",
        "label": "Customer Id",
        "grid": 6,
        "length": 9,


    },

    {
        "varName": "title",
        "type": "text",
        "label": "Title",
        "grid": 6,


        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 6,
        "length": 80,


    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 6,
        "length": 10,


    },

    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 6,


        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status as on Date",
        "grid": 6,


    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "AC Manager",
        "grid": 6,


    },

    {
        "varName": "occupationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 6,


        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 6,


        required: true,
    },

    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 6,


        required: true,
    },

    {
        "varName": "staffFlag",
        "type": "text",
        "label": "Staff Flag",
        "grid": 6,


        required: true,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 6,


        required: true,
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 6,


    },

    {
        "varName": "nonResident",
        "type": "text",
        "label": "Non Resident",
        "grid": 6,


        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 6,


        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National ID Card",
        "grid": 6,


        required: true,
    },

    {
        "varName": "dateOfBirth",
        "type": "date",
        "label": "Date of Birth",
        "grid": 6,


    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer Id",
        "grid": 6,
        "length": 9,

        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 6,


    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 6,


    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "grid": 6,


        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father",
        "grid": 6,


        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 6,


        required: true,
    },

    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "grid": 6,


        required: true,
    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address1",
        "grid": 6,


    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address2",
        "grid": 6,


    },

    {
        "varName": "city1",
        "type": "text",
        "label": "City",
        "grid": 6,


        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 6,


        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 6,


    },

    {
        "varName": "country1",
        "type": "text",
        "label": "Country",
        "grid": 6,


        required: true,
    },

    {
        "varName": "phoneNo11",
        "type": "text",
        "label": "Phone No1",
        "grid": 6,
        "length": 11,

        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No2",
        "grid": 6,


    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 6,


    },

    {
        "varName": "email1",
        "type": "text",
        "label": "Email",
        "grid": 6,

        email: true,

    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address1",
        "grid": 6,


    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address2",
        "grid": 6,


    },

    {
        "varName": "city2",
        "type": "text",
        "label": "City",
        "grid": 6,


        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 6,


        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 6,


    },

    {
        "varName": "country2",
        "type": "text",
        "label": "Country",
        "grid": 6,


        required: true,
    },

    {
        "varName": "phoneNo12",
        "type": "text",
        "label": "Phone No1",
        "grid": 6,


    },

    {
        "varName": "phoneNo222",
        "type": "text",
        "label": "Phone No2",
        "grid": 6,


    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 6,


    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 6,

        email: true,

    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address1",
        "grid": 6,


    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address2",
        "grid": 6,


    },

    {
        "varName": "city3",
        "type": "text",
        "label": "City",
        "grid": 6,


        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 6,


        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 6,


    },

    {
        "varName": "country",
        "type": "text",
        "label": "Country",
        "grid": 6,


        required: true,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No1",
        "grid": 6,


    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No2",
        "grid": 6,


    },

    {
        "varName": "telexNo",
        "type": "text",
        "label": "Telex No",
        "grid": 6,


    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 6,

        email: true,

    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 6,


    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 6,


    },

    {
        "varName": "tds",
        "type": "text",
        "label": "TDS",
        "grid": 6,


    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PANGIR No",
        "grid": 6,


    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 6,


    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 6,


    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 6,


        required: true,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 6,


    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 6,


    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text2",
        "grid": 6,


    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 6,
        "length": 10,


    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 6,


    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 6,


    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text13",
        "grid": 6,


    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text14",
        "grid": 6,


        required: true,
    },

    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text15",
        "grid": 6,


        required: true,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code1",
        "grid": 6,


        required: true,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code3",
        "grid": 6,


        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 6,


        required: true,
    },

    {

        "type": "title",
        "label": "AC Kyc",
        "grid": 12,

    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid": 6,

    },

    {
        "varName": "uniqueCustomerID",
        "type": "text",
        "label": "Unique Customer ID",
        "grid": 6,

    },

    {
        "varName": "aCNo",
        "type": "text",
        "label": "AC No",
        "grid": 6,
        "length": 13,
    },

    {
        "varName": "aCTitle",
        "type": "text",
        "label": "AC Title",
        "grid": 6,

    },

    {
        "varName": "typeOfAC",
        "type": "text",
        "label": "Type of AC",
        "grid": 6,

    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "label": "Customer occupation",
        "grid": 6,

    },

    {
        "varName": "customerProbableMonthlyIncome",
        "type": "text",
        "label": "Customer probable monthly income",
        "grid": 6,

    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid": 6,

    },

    {
        "varName": "dOCCollectToEnsureSourceOfFund",
        "type": "text",
        "label": "DOC collect to Ensure Source of Fund",
        "grid": 6,

    },

    {
        "varName": "collectedDOCHaveBeenVerified",
        "type": "text",
        "label": "Collected DOC have been Verified",
        "grid": 6,

    },

    {
        "varName": "howTheAddressIsVerified",
        "type": "text",
        "label": "How the Address is verified",
        "grid": 6,

    },

    {
        "varName": "hasTheBeneficialOwnerOfTheAcBeenIdentified",
        "type": "text",
        "label": "Has the Beneficial owner of the ac been identified",
        "grid": 6,

    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport no",
        "grid": 6,

    },

    {
        "varName": "nIDNo",
        "type": "text",
        "label": "NID no",
        "grid": 6,

    },

    {
        "varName": "birthCertificateNo",
        "type": "text",
        "label": "Birth certificate no",
        "grid": 6,

    },

    {
        "varName": "eTIN",
        "type": "text",
        "label": "E-TIN",
        "grid": 6,

    },

    {
        "varName": "drivingLicenseNo",
        "type": "text",
        "label": "Driving License no",
        "grid": 6,

    },

    {
        "varName": "otherDocumentation",
        "type": "text",
        "label": "Other Documentation",
        "grid": 6,

    },

    {
        "varName": "reasonForACOpeningOfForeignCompany",
        "type": "text",
        "label": "Reason for AC opening of Foreign Company",
        "grid": 6,

    },

    {
        "varName": "typeOfVISA",
        "type": "text",
        "label": "Type of VISA",
        "grid": 6,

    },

    {
        "varName": "expDate",
        "type": "date",
        "label": "Exp date",
        "grid": 6,

    },

    {
        "varName": "workPermitAndPermission",
        "type": "text",
        "label": "Work permit and permission",
        "grid": 6,

    },

    {
        "varName": "pepip",
        "type": "text",
        "label": "PEPIP",
        "grid": 6,

    },

    {
        "varName": "ifyesThen",
        "type": "text",
        "label": "IF YES then",
        "grid": 6,

    },

    {
        "varName": "anyMatchOfTerroristActivity",
        "type": "text",
        "label": "Any match of Terrorist activity",
        "grid": 6,

    },

    {
        "varName": "whatDoesTheCustomerDoinWhatTypeOfBusinessIsTheCustomerEngaged",
        "type": "text",
        "label": "What does the customer doin what type of business is the customer engaged",
        "grid": 6,

    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "label": "Customers monthly income",
        "grid": 6,

    },

    {
        "varName": "expectedAmountOfMonthlyTotalTransaction",
        "type": "text",
        "label": "Expected Amount of Monthly Total Transaction",
        "grid": 6,

    },

    {
        "varName": "expectedAmountOfMonthlyCashTransaction",
        "type": "text",
        "label": "Expected Amount of Monthly Cash Transaction",
        "grid": 6,

    },

    {
        "varName": "totalRiskScore",
        "type": "text",
        "label": "Total Risk Score",
        "grid": 6,

    },

    {
        "varName": "comments",
        "type": "text",
        "label": "Comments",
        "grid": 6,

    },
    {

        "type": "title",
        "label": "AC Minor",
        "grid": 12,

    },
    {
        "varName": "guardian",
        "type": "text",
        "label": "Guardian",
        "grid": 6,
        required: true,
    },

    {
        "varName": "guardianName",
        "type": "text",
        "label": "Guardian Name",
        "grid": 6,
        required: true,
    },

    {
        "varName": "address",
        "type": "text",
        "label": "Address",
        "grid": 6,
        required: true,
    },

    {
        "varName": "city",
        "type": "text",
        "label": "City",
        "grid": 6,
        required: true,
    },

    {
        "varName": "state",
        "type": "text",
        "label": "State",
        "grid": 6,
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 6,
        required: true,
    },

    {
        "varName": "country",
        "type": "text",
        "label": "Country",
        "grid": 6,
        required: true,
    },

    {

        "type": "title",
        "label": "AC TP",
        "grid": 12,
    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid": 6,
    },

    {
        "varName": "uniqueCustomerID",
        "type": "text",
        "label": "Unique Customer ID",
        "grid": 6,
    },

    {
        "varName": "aCNo",
        "type": "text",
        "label": "AC No",
        "grid": 6,
    },

    {
        "varName": "aCTitle",
        "type": "text",
        "label": "AC Title",
        "grid": 6,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 6,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 6,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid": 6,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 6,
    },

    {
        "varName": "depositByTransfer",
        "type": "text",
        "label": "Deposit by Transfer",
        "grid": 6,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 6,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income from Export",
        "grid": 6,
    },

    {
        "varName": "deposittransferFromBOAC",
        "type": "text",
        "label": "DepositTransfer from BO AC",
        "grid": 6,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 6,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 6,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 6,
    },

    {
        "varName": "withdrawalThroughTransferinstrument",
        "type": "text",
        "label": "Withdrawal through TransferInstrument",
        "grid": 6,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign outward Remittance",
        "grid": 6,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment against import",
        "grid": 6,
    },

    {
        "varName": "deposittransferToBOAC",
        "type": "text",
        "label": "DepositTransfer to BO AC",
        "grid": 6,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 6,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable Withdrawal",
        "grid": 6,
    },
]
var remarks = [
    {
        "varName": "remarksMaker",
        "type": "text",
        "label": "Remarks",
        "grid": 12,


    },
];

var csBearer = [
    {
        "varName": "csBearer",
        "type": "text",
        "label": "Bearer",
        "grid": 6,
        "readOnly": true,


    }, {
        "varName": "bearerApproval",
        "type": "text",
        "label": "Bearer Approval",
        "grid": 6,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "csBearer",
        "conditionalVarValue": "YES"
    },
];
var needDeferral = [
    {
        "varName": "maker_deferal",
        "type": "radio",
        "label": "Need Deferral ",
        "enum": [
            "YES",
            "NO"
        ]
    },

    /* {
         "varName": "maker_update_all_info_send_to",
         "type": "checkbox",
         "label": "Approved ?",
         "enum": [
             "APPROVED",
             "RETURN"
         ]
     },*/
];
var deferalOther =
    {
        "varName": "defferalOther",
        "type": "text",

        "label": "Please Specify"

    };

var deferal = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ]
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
};

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    },
    gridList: {
        width: 500,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
});
const maintenanceListMaker = [
    {
        " varName": "12digitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing PAN GIR No TIN Update",
        "grid": 6,

    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New PAN GIR No TIN Update",
        "grid": 5,
        // "conditionalVarName": "12DigitTinChange",
        // "conditionalVarValue": true,
    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "grid": 6,

    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 6,

    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 6,

    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 6,

    },
    {
        "varName": "dOBNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "postal CodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 6,

    },
    {
        "varName": "postal CodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "grid": 6,

    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "grid": 6,

    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1 ",
        "grid": 6,

    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1 ",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "grid": 6,

    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "grid": 6,

    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "grid": 6,

    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "grid": 6,

    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "grid": 6,

    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Guardian Existing Country",
        "grid": 6,

    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing Postal Code",
        "grid": 6,

    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing City Code",
        "grid": 6,

    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Guardian's Existing State Code",
        "grid": 6,

    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "grid": 6,

    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "grid": 6,

    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Guardian Existing Address",
        "grid": 6,

    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditionalVarName": "gurdianUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": " Photo Id Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "grid": 6,

    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "grid": 6,

    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "grid": 6,

    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "grid": 6,

    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "grid": 6,

    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1 ",
        "grid": 6,

    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "grid": 6,

    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": "true",
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "grid": 6,

    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": "true",
    },

    {
        " varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Despatch ModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "grid": 6,

    },
    {
        "varName": "Despatch ModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": "true",
    },


    {
        " varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "grid": 6,

    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "grid": 6,

    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "grid": 6,

    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 6,

    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 6,

    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "grid": 6,

    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "grid": 6,

    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "grid": 6,

    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 6,

    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "grid": 6,

    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE ",
        "grid": 6,

    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "grid": 6,

    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "grid": 6,

    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "grid": 6,

    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": "true",
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 6,

    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditionalVarName": "dobUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "grid": 6,

    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditionalVarName": "fatherNameUpdateChange",
        "conditionalVarValue": "true",
    },

    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "grid": 6,

    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditionalVarName": "motherNameUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 6,

    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "grid": 6,

    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        // "conditionalVarName":"fatherNameUpdateChange",
        // "conditionalVarValue":"true",
    },

    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 6,

    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 6,

        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true,
    },
    {
        " varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Account StatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "grid": 6,

    },
    {
        "varName": "Account StatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 6,

        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "grid": 6,


    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "grid": 6,


    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "grid": 6,


    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "grid": 6,

    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "grid": 6,

    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "grid": 6,

    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "grid": 6,

    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "grid": 6,

    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "grid": 6,

    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 6,

        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true,
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 6,

        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        " varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Cash Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "grid": 6,

    },
    {
        "varName": "Cash Limit (DR/CR)New",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": "true",
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 6,

        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 6,

        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 6,

        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 6,

        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 6,

        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true,
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "grid": 6,

    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,

    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "grid": 6,

    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "grid": 6,

    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 6,

    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "grid": 6,

    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "grid": 6,

    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "grid": 6,

    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,

    },

    {
        " varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 6,

    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 6,

    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 6,

    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "grid": 6,

    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "grid": 6,

    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "grid": 6,

    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,

    },
    {
        " varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 6,

    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,

    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 6,

    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 6,

    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "grid": 6,

    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "grid": 6,

    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": "true",
    },
    {
        " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Clearing Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "grid": 6,

    },
    {
        "varName": "Clearing Limit (DR/CR)New",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Transfer Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "grid": 6,

    },
    {
        "varName": "Transfer Limit (DR/CR)New",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": "true",
    },

    {
        " varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Sector CodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "grid": 6,

    },
    {
        "varName": "Sector CodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Sub Sector CodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "grid": 6,

    },
    {
        "varName": "Sub Sector CodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": "true",
    },


    {
        " varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12
    },
    {
        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "Free Code 3 Old",
        "type": "text",
        "label": "Existing Free Code 3 ",
        "grid": 6,

    },
    {
        "varName": "Free Code 3 New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,

    },

]

class VarifyMakerAccountMaintenance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            appData: {},
            getData: false,
            varValue: [],
            showValue: false,
            redirectLogin: false,
            title: "",
            notificationMessage: "",
            alert: false,
            inputData: {},
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},
            values: [],
            // customerName: [],
            deferalType: [],
            expireDate: [],
            other: [],
            getCheckerList: [],
            getAllDefferal: [],
            getDeferralList: [],
            loading: false,
            jointAccountCustomerNumber: 0,
            objectForJoinAccount: [],
            getgenerateForm: false,
            renderCumModalopen: false,
            generateAccountNo: '',
            getDedupData: {},
            jointDedupData: {},
            relatedData: {},
            searchTableData: false,
            searchTableRelatedData: false,
            bearerApproval: '',
            changeUpdateCustomerAddress: true,
            changeUpdateContactNumber: true,
            changeUpdateEmail: true,
            changeUpdateCustomerOtherInformation: true,
            changeUpdateCustomerPhotoID: true,
            changeUpdateEtin: true,
            serviceFormType: '',
            maintenanceType: '',
            newMaturityInstruction: true,
            fundTransfer: "Fund Transfer to",
            payOrderFD: "Pay Order",
            payOrderDPS: "Pay Order",
            payOrderCasa: "Pay Order",
            payOrderFixed: "Pay Order",
            changeFDTenor: "Change FD Tenor",
            others: "Others",
            permanentAddressOld: "",
            residencePhoneNumberOld: "",
            officePhoneNumberOld: "",
            mobilePhoneNumberOld: "",
            previousEmail: "",
            fatherNameOld: "",
            motherNameOld: "",
            maritalStatusOld: "",
            genderOld: "",
            spouseNameOld: "",
            dobOld: "",
            countryOfBirthOld: "",
            residentStatusOld: "",
            employerNameOld: "",
            professionOld: "",
            designationOld: "",
            exitingNID: "",
            exitingPassport: "",
            exitingBirthCertificate: "",
            exitingTradeDrivingLicense: "",
            residenceAddressOld: "",
            officeBusinessAddressOld: "",
            mailForwardedAddressOld: "",


            fDDPSFaceValueCasaOld: "",

            maturityFDDPSCasaOld: "",

            fundTransferCasaOld: "",

            fDDPSFaceValueFdOld: "",

            maturityFDDPSFdOld: "",
            fundTransferFdOld: "",

            fDDPSFaceValueInterestPaymentOld: "",

            maturityFDDPSInterestPaymentOld: "",

            fundTransferInterestPaymentOld: "",

            fDDPSFaceValueDPSOld: "",

            maturityFDDPSDPSOld: "",

            fundTransferDPSOld: "",

            paymentThroughOldFD: "",

            paymentThroughDPSOld: "",

            paymentThroughCasaOld: "",

            paymentThroughFixedOld: "",


            othersCasaOld: "",

            tinCasaOld: "",

            othersFdEncashmentOld: "",

            tinFdEncashmentOld: "",

            othersFdInterestPaymentOld: "",

            tinFdInterestPaymentOld: "",

            othersDPSOld: "",

            tinDPSOld: "",

            tinFixedOld: "",

            tinFixed: "",

            reissueFDWithOld: "",

            reNewPrincipalInterestProfitOld: "",

            reNewPrincipalOnlyProfitOld: "",

            encashmentMaturityOld: "",

            creditInterestOld: "",
            numberInstallmentDueOld: "",
            detailsMonthOld: "",
            amountPerInstallmentOld: "",
            totalInstallmentAmountDueOld: "",
            debitDueInstallmentOld: "",

            branchName: "",
       
            accountNumber: '',
            csBearer: '',
            customerName:" FAISAL AHMED",
            cbNumber: " CB1401933",
        }

    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "defferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    renderBearerApproval = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, csBearer,
                    this.updateComponent)
            )

        }
        return;
    }

    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    renderJsonForm = () => {

        if (this.state.showValue) {

            return (
                <div>
                    <GridList cellHeight={800} cols={1}>
                        <React.Fragment>
                            <br/><br/>

                            <Grid item xs={6}>
                                <label htmlFor="cbNumber"><b><font size="3">CB Number :</font></b> </label>

                                {this.state.cbNumber}
                            </Grid>
                            <Grid item xs={6}>
                                <label htmlFor="accountNumber"><b><font size="3">Account Number :</font></b> </label>

                                {this.state.accountNumber}
                            </Grid>
                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, this.props.jsonForm,
                                    this.updateComponent)
                            }


                        </React.Fragment>
                    </GridList>
                </div>
            )
        }
    }

    addJsonForm() {
        if (this.state.inputData["deferalNeed"] === "YES") {
            return this.state.values.map((el, i) =>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>

                        <div key={i}>
                            <Grid item xs="12">
                                {
                                    this.dynamicDeferral(el)
                                }

                            </Grid>
                            <br/>
                            <Grid item xs="12">
                                {this.dynamicDeferralOther(el)}

                            </Grid>
                            <br/>

                            <Grid item xs="12">
                                {
                                    this.dynamicDate(el)
                                }

                                <button
                                    className="btn btn-outline-danger"
                                    style={{
                                        position: "absolute",
                                    }}
                                    type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                                >
                                    Remove Form
                                </button>
                            </Grid>

                            <br/><br/>
                        </div>

                    </GridItem>

                </GridContainer>
            )
        }
    }

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };

    componentDidMount() {


        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log("maker");
                    console.log(response.log);


                    let checkerListUrl = backEndServerURL + "/checkers";
                    axios.get(checkerListUrl, {withCredentials: true})
                        .then((response) => {
                            let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                            axios.get(deferalListUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    let tableArray = [];
                                    response.data.map((deferal) => {
                                        tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, deferal.status));

                                    });
                                    this.setState({
                                        getDeferralList: tableArray
                                    })

                                })
                                .catch((error) => {
                                    console.log(error);
                                })
                            console.log(response.data)
                            this.setState({
                                getCheckerList: response.data,

                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    console.log(response.data)
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        // varValue: varValue,
                        inputData: response.data,
                        varValue: response.data,
                        appData: response.data,
                        showValue: true,
                        loading: false
                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }

    updateComponent = () => {
        this.forceUpdate();
    };
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };
    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }
    handleSubmit = (event) => {
        event.preventDefault();


        if (this.state.deferalNeeded) {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

            }
            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }


        let data = this.state.inputData;
        this.state.inputData.next_user = this.state.inputData.maker_send_to;
        this.state.inputData.maker_update_all_info_send_to = "CHECKER";

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }
    handleSubmitReturn = (event) => {
        event.preventDefault();

        //this.state.inputData.next_user = this.state.inputData.cs_send_to;
        this.state.inputData.maker_update_all_info_send_to = "CS";

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }
    checkerList = () => {
        if (this.state.getData) {
            let checkerJsonForm = {
                "varName": "maker_send_to",
                "type": "select",
                "label": "Send To",
                "enum": []
            };
            this.state.getCheckerList.map((checker) => {

                checkerJsonForm.enum.push(checker)
            })
            return (
                SelectComponent.select(this.state, this.updateComponent, checkerJsonForm)
            )

        }

    }
    handleChange = (event) => {
        this.state.inputData["deferalNeed"] = event.target.value;
        this.updateComponent();
        if (event.target.value === "YES") {
            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values});

        } else {
            this.setState({
                values: []
            })
        }
    }
    renderDefferalData = () => {


        if (this.state.getDeferralList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Application Date", "Status"]}
                        tableData={this.state.getDeferralList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderButton = () => {
        if (this.state.getData) {
            return (

                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmit}
                    >Submit
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmitReturn}
                    >Return
                    </button>
                </div>

            )
        }
    }


    renderRemarks = () => {
        if (this.state.getData) {
            return (


                CommonJsonFormComponent.renderJsonForm(this.state, remarks, this.updateComponent)


            )
        }
    }

    render() {

        const {classes} = this.props;

        return (

            <div>
                <Grid container spacing={1}>
                    <ThemeProvider theme={theme}>
                        {this.renderJsonForm()}
                        <br/><br/> <br/> <br/>
                        {this.renderDefferalData()}

                        <br/> <br/>


                        <div>
                            {this.renderBearerApproval()}
                            <br/>
                            {this.renderRemarks()}
                            <br/>
                            <ThemeProvider theme={theme}>
                                <Grid container spacing={1}>
                                    {this.renderImageLink()}
                                </Grid>
                            </ThemeProvider>


                            <Dialog
                                fullWidth="true"
                                maxWidth="md"
                                open={this.state.accountDetailsModal}>
                                <DialogContent>

                                    <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                                </DialogContent>
                            </Dialog>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.uploadModal}>
                                <DialogContent>

                                    <LiabilityUploadModal appId={this.state.appId}
                                                          closeModal={this.closeUploadModal}/>
                                </DialogContent>
                            </Dialog>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.imageModalBoolean}>
                                <DialogContent>

                                    <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                                </DialogContent>
                            </Dialog>

                            {this.checkerList()}


                            <br/> <br/>
                            {this.renderButton()}

                        </div>


                    </ThemeProvider>
                </Grid>


            </div>

        )
    }

}

export default withStyles(styles)(VarifyMakerAccountMaintenance);
