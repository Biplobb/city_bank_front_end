import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import "../../../Static/css/RelationShipView.css";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import {ThemeProvider} from "@material-ui/styles";
import CloseIcon from '@material-ui/icons/Close';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import SingleImageShow from "../SingleImageShow";
import AccountNoGenerate from ".././AccountNoGenerate";
import LiabilityUploadModal from "../LiabilityUploadModal";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};
var csDeferral = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferral",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var csBearer = {
    "varName": "csBearer",
    "type": "select",
    "label": "Need Bearer",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther = {
    "varName": "deferalOther",
    "type": "text",
    "label": "Please Specify",
    "grid": 6
};

var Deferral = {
    "varName": "deferalType",
    "type": "select",
    "label": "Deferral Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
};

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

let JsonFormCasaIndividualDeferral = {

    "type": {
        "varName": "type",
        "label": "Deferral Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}


let accountMaintenanceSearch = [
    {
        "type": "blank",
        "grid": 2,

    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account No",

    }
]
var bearerApproval = {
    "varName": "bearerApproval",
    "type": "select",
    "label": "Bearer Approval",
    "grid": 12,
    "enum": [
        "BM Approval",
        "Call Center Approval",
        "BM & Call Center Approval"

    ]
};


const maintenanceList = [
    {
        "varName": "12digitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 2,


    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing TIN",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "12digitTinChange",
        "conditionalVarValue": true


    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New TIN",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "12digitTinChange",
        "conditionalVarValue": true

    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dOBNew",
        "type": "date",
        "label": "New DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Existing Guardian  Country",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Existing Guardian  Postal Code",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Existing Guardian  City Code",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Existing Guardian's  State Code",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Existing Guardian  Address",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": "Photo Id Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
        ,
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },

    {
        "varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },
    {
        "varName": "despatchModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },
    {
        "varName": "despatchModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },


    {
        "varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 5,
        "readOnly": true,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountStatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountStatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cashLimitDRCROld",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cashLimitDRCRNew",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },

    {
        "varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "clearingLimitDRCROld",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "clearingLimitDRCRNew",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "transferLimitDRCROld",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "transferLimitDRCRNew",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },

    {
        "varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "sectorCodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "sectorCodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "subSectorCodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "subSectorCodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },


    {
        "varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freeCode3Old",
        "type": "text",
        "label": "Existing Free Code 3",
        "readOnly": true,
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freeCode3New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },

]

class AccountMaintenance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLink: {},
            varValue: [],
            tableData: [],
            sourceMappingData: [],
            getsearchValue: [],
            getCustomerId: '',
            getAccountType: '',
            accountOpeningFromModal: false,
            SelectedData: '',
            newAcoountOpeningModal: false,
            searchTableData: null,
            dataNotFound: false,
            CustomerModal: false,
            uniqueId: '',
            IDENTIFICATION_NO: '',
            id: '',
            alert: false,
            anchorEl: null,
            anchorE2: null,
            individualDropdownOpen: null,
            notificationMessage: "CB number / NID / Passport / Birth Certificate or Driving License is Required!!",

            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},

            csDeferralPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            caseId: "",
            title: "",
            app_uid: "-1",
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferral: "",
                panGirNoTinUpdateOld: 1234,
                customerNameOld: "Raihan Rahman",
                titleOld: "Mr",
                postalCodeOld: "6654",
                nomineeNameOld: "Samin Khan",
                countryGuardianOld: "Bangladesh",
                postalGuardianCodeOld: "7788",
                cityGuardianCodeOld: "3344",
                stateGuardianCodeOld: "4455",
                relationshipOld: "Sister",
                address1Old: "Dhaka",
                address2Old: "Dkaka",
                cityCodeOld: "2345",
                stateCodeOld: "3342",
                regNoOld: "44455",
                nomineeMinorOld: "Monir",
                guardianNameOld: "Mr. Salam",
                guardianCodeOld: "3444",
                addressGuardianOld: "3344",
                expiryDateOld: "21-10-1999",
                issueDateOld: "11-10-1999",
                nationalIdCardOld: "21321432535",
                passportDetailsOld: "21424325325",
                passportNoOld: "21432535",
                phoneNo1Old: "01912345567",
                phoneNo2Old: "01813213456",
                emailOld: "mamun@gmail.com",
                despatchModeOld: "ewqrr",
                cityOld: "Dhaka",
                communicationAddress1Old: "Uttor Badda",
                communicationAddress2Old: "Gulsan",
                countryOld: "Bangladesh",
                employerAddress1Old: "Mirpur",
                employerAddress2Old: "Shamoli",
                permanentAddress1Old: "Khulna",
                permanentAddress2Old: "Khulna",

                stateOld: "dsgdsg",
                rELATIONTYPEOld: "23535346",
                rELATIONCODEOld: "325353",
                dESIGNATIONCODEOld: "23423532",
                cUStIDOld: "243252",
                dobOld: "21-10-1999",
                fatherOld: "dsgdsfh",
                motherOld: "dfdsfdsgsd",

                customerName: "FAISAL AHMED",
                cbNumber: "CB1401933",
            },
            fileUploadData: {},
            AddDeferral: false,
            showValue: false,
            getDeferralList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            getDocument: false,

            digitTIN: false,
            titleChange: false,
            nomineeUpdate: false,
            updateChangePhotoId: false,
            contactNumberChange: false,
            emailAddressChange: false,
            estatementEnrollment: false,
            addressChange: false,
            otherInformationChange: false,
            signatureCard: false,
            dormantAccountActivation: false,
            dormantAccountDataUpdate: false,
            schemeMaintenanceLinkChange: false,
            schemeMaintenance: false,
            mandateUpdateChange: false,
            cityLive: false,
            projectRelatedDataUpdateADUP: false,
            accountSchemeClose: false,
            lockerSIOpen: false,
            lockerSIClose: false,
            others: false,
            bearerApproval: '',
            csBearer: '',
            getImageBoolean: false,
            debitCard: "",
            loaderNeeded: null,
            IndividualDedupModal: false,
            JointDedupModal: false,
            individualDataSaveId: '',
            jointDataSaveId: '',
            companyDataSaveId: '',
            accountNumber: 2251401933001,
            routeCase: false,
            accountNumberNew: false
        }
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferral"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }

    addDeferralForm() {
        if (this.state.inputData["csDeferral"] === "YES") {
            return this.state.values.map((el, i) =>
                <React.Fragment>

                    <Grid item xs="6">
                        {
                            this.dynamicDeferral(el)
                        }
                    </Grid>
                    <Grid item xs="6">
                        {this.dynamicDeferralOther(el)}
                    </Grid>
                    <Grid item xs="6">
                        {
                            this.dynamicDate(el)
                        }
                    </Grid>


                    <Grid item xs="3">
                        <button
                            style={{float: 'center',}}
                            className="btn btn-outline-danger"
                            type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                        >
                            Remove
                        </button>
                    </Grid>

                </React.Fragment>
            )
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferralList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferral Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferralList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {
        // if (this.state.inputData["csDeferral"] === "YES") {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'left',

                    }}

                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}


                >Add Deferral</button>
            )
        // } else {
        //     return;
        // }
    }
    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(Deferral));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };
    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
        }
    }

    renderSelectMenu = () => {
        return (
            <Grid item xs='12'>
                <FormLabel component="legend">Need Deferral</FormLabel>
                <RadioGroup aria-label="csDeferral" name="csDeferral" value={this.state.inputData["csDeferral"]}
                            onChange={this.handleChange}>
                    <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                    <FormControlLabel value="NO" control={<Radio/>} label="NO"/>

                </RadioGroup>

            </Grid>

        )


    }

    close = () => {
        this.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

    renderUploadButton = () => {
        if (!this.state.deferalNeeded ) {
            return (
                <button
                    sty
                    className="btn btn-outline-danger"
                    style={{
                        float: 'left',
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }
    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {

        this.setState({
            imageModalBoolean: false,
            // imgeListLinkSHow: true,

        })
    }
    closeUploadModal = (data) => {

        this.setState({

            // imgeListLinkSHow: true,
            getImageLink: data,
            uploadModal: false
        })
    }
    accountDetailsModal = () => {
        this.setState({
            accountDetailsModal: false
        })
        this.closeModal();
    }
    renderImageLink = () => {

        if (this.state.imgeListLinkSHow===true && this.state.getImageLink.length > 0) {
            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }


    renderBearerApproval = () => {
        if (this.state.getData) {

            return (

                <Grid item xs={12}>
                    {SelectComponent.select(this.state, this.updateComponent, csBearer)}
                </Grid>

            )

        }
        return;
    }
    bearerApproval = () => {
        if (this.state.inputData["csBearer"] === "YES") {

            return (

                <Grid item xs={12}>
                    {SelectComponent.select(this.state, this.updateComponent, bearerApproval)}
                </Grid>

            )

        }
        return;
    }

    componentDidMount() {
        this.updateComponent()
        this.state.inputData["csDeferral"] = "YES";
        this.state.inputData["csBearer"] = "NO";

        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log(response.data)
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            var status="";
                            response.data.map((deferal) => {
                                if(deferal.status==="ACTIVE"){
                                    status="Approved"
                                }
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    this.state.inputData["csDeferal"]="YES";
                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,
                        showValue: true,
                        appId: this.props.appId,
                        loaderNeeded: true
                    });

                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            console.log(url)
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log("maintenance")
                    console.log(response.data)

                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        varValue: this.state.inputData,
                        getNewCase: true,
                        showValue: true,
                        getData: true,
                        getDocument: true,


                    });

                })
                .catch((error) => {
                    console.log(error);
                })
        }


    }


    renderSearchForm = () => {
        if (this.state.showValue && this.state.accountNumber)

            return (

                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>

                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, accountMaintenanceSearch, this.updateComponent)
                            }


                        </ThemeProvider>
                    </Grid>
                    <br/>
                    <button
                        onClick={this.accountModal}
                        className="btn btn-danger">
                        Search
                    </button>
                </Grid>

            )

    }
    accountModal = () => {

        this.setState({
            searchTableData: true,
            accountNumber: this.state.inputData.accountNumber,

        })

    };
    renderSearchData = () => {


        if (this.state.searchTableData && this.state.showValue) {

            return (
                <React.Fragment>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>
                            <br/><br/>

                            <Grid item xs={2}>
                                <label for="customerName"><font size="3"><b> Customer Name </b></font></label>
                            </Grid>
                            <Grid item xs={2}>
                                {this.state.inputData.customerName}
                            </Grid>
                            <Grid item xs={8}></Grid>
                            <Grid item xs={2}>
                                <label for="cbNumber"><b><font size="3">CB Number </font></b> </label>
                            </Grid>
                            <Grid item xs={2}>
                                {this.state.inputData.cbNumber}
                            </Grid>
                            <Grid item xs={8}></Grid>
                            {
                                CommonJsonFormComponent.renderJsonForm(this.state, maintenanceList, this.updateComponent)
                            }

                        </ThemeProvider>
                    </Grid>

                    <br/><br/>
                    <Grid container spacing={3}>
                        <ThemeProvider theme={theme}>
                            {this.renderBearerApproval()}
                            <Grid item xs='12'>
                                {this.bearerApproval()}
                            </Grid>
                            {/*{this.renderSelectMenu()}*/}


                            <Grid item xs='12'>
                                {
                                    this.renderAddButtonShow()
                                }
                            </Grid>
                            <br/>


                            {
                                this.addDeferralForm()
                            }

                        </ThemeProvider>
                    </Grid>


                    <ThemeProvider theme={theme}>
                        <Grid container spacing={1}>
                            {this.renderImageLink()}
                        </Grid>
                    </ThemeProvider>
                    <br/><br/>
                    <Grid item xs="12">
                        {this.renderUploadButton()}
                    </Grid>

                    <Dialog
                        fullWidth="true"
                        maxWidth="md"
                        open={this.state.accountDetailsModal}>
                        <DialogContent>

                            <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        open={this.state.uploadModal}>
                        <DialogContent>

                            <LiabilityUploadModal subServiceType={this.state.subServiceType} appId={this.state.appId}
                                                  closeModal={this.closeUploadModal}/>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        open={this.state.imageModalBoolean}>
                        <DialogContent>

                            <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                        </DialogContent>
                    </Dialog>
                    <Grid item xs="12"></Grid>
                    <br/>
                    <br/>

                    <center>
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'middle',
                            }}
                            onClick={this.handleSubmit}

                        >
                            Submit
                        </button>
                    </center>

                </React.Fragment>


            )
        }
    }


    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.deferalNeeded) {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

            }
            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {

                })
                .catch((error) => {
                    console.log(error);
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;


        let data = this.state.inputData;
        // data.customerName = this.state.customerName;
        data.cs_deferal = this.state.inputData["csDeferral"];
        data.serviceType = "Maintenance";
        data.subServiceType = "AccountMaintenance";


        if (this.state.inputData.bearerApproval === "BM Approval")
            data.cs_bearer = "BM";
        else if (this.state.inputData.bearerApproval === "BM Approval & Call Center Approval")
            data.cs_bearer = "BOTH";
        else if (this.state.inputData.bearerApproval === "Call Center Approval")
            data.cs_bearer = "CALL_CENTER";
            data.cs_deferal= "NO";

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {

                    var url = backEndServerURL + "/case/route/" + this.state.appId;

                    axios.get(url, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data)
                            console.log("Successfully Routed!");
                            this.setState({
                                title: "Successfull!",
                                notificationMessage: "Successfully Routed!",
                                routeCase:true,

                            })

                        })
                        .catch((error) => {
                            console.log(error);
                            if (error.response.status === 652) {
                                Functions.removeCookie();

                                this.setState({
                                    redirectLogin: true
                                })

                            }
                        });

                }
            )
            .catch((error) => {
                console.log(error)
            });

    }
    renderLocation = () => {
        if(this.state.routeCase===true)
        window.location.reload();
    }

    render() {
        console.log(this.state.inputData)
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (


            <Card>
                <CardHeader color="rose">
                    <h4><a><CloseIcon onClick={this.close} style={{
                        position: 'absolute',
                        right: 10,
                        color: "#000000"
                    }}/></a>Account
                        Maintenance</h4>
                </CardHeader>
                <CardBody>
                    <Grid container spacing={1}>
                        <Grid item xs={12}>
                            <br/>
                            {this.renderSearchForm()}
                            <br/>
                            <br/>

                            {this.renderSearchData()}
                            {this.renderLocation()}
                        </Grid>
                    </Grid>
                </CardBody>
            </Card>

        );


    }

}

export default withStyles(styles)(AccountMaintenance);
