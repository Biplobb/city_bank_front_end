import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import Grow from "@material-ui/core/Grow";
import {ThemeProvider} from "@material-ui/styles";
import theme from "../../JsonForm/CustomeTheme2";
import CloseIcon from '@material-ui/icons/Close';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import SelectComponent from "../../JsonForm/SelectComponent";
import FileTypeComponent from "../../JsonForm/FileTypeComponent";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};


let initiateBondrequest = [
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "CB Number",
        "grid": 6,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "grid": 6
    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "grid": 6

    },
    {
        "varName": "nomineeDob",
        "type": "date",
        "label": "Nominee Date Of Birth",
        "grid": 6
    },
    {
        "varName": "amount",
        "type": "text",
        "label": "Amount",
        "grid": 6
    }
];
var scanningFile = {
    "varName": "scanningFile",
    "type": "file",
    "label": "Scanning File",
    "grid": 12
};
class Bondmanagement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDate: {},
            inputData: {},
            appId: 0,
            getCheckerList: [],
            checkerListShow: false,
            fileUploadData:{},


        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateComponent = () => {
        this.forceUpdate();
    }

    componentDidMount() {
        let url = backEndServerURL + "/startCase/maker_update_all_information";
        axios.get(url, {withCredentials: true})
            .then((response) => {
                console.log(response.data)

                let checkerListUrl = backEndServerURL + "/checkers";
                axios.get(checkerListUrl, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        this.setState({
                            getCheckerList: response.data,


                        })
                    })

                    .catch((error) => {
                        console.log(error);
                    })
                this.setState({
                    appId: response.data.id,
                    checkerListShow: true
                });
            })
            .catch((error) => {
                console.log(error);
            })


    }

    checkerList = () => {
        if (this.state.checkerListShow && this.state.inputData.type!==undefined) {
            let checkerJsonForm = {
                "varName": "maker_send_to",
                "type": "select",
                "label": "Send To",
                "grid": 12,
                "enum": []
            };
            this.state.getCheckerList.map((checker) => {

                checkerJsonForm.enum.push(checker)
            })
            return (
                <Grid item xs={12}>
                    {
                        SelectComponent.select(this.state, this.updateComponent, checkerJsonForm)
                    }
                </Grid>
            )

        }

    }
    returnAccountTypeField = () => {

        return (
            <Grid item xs={12}>
                <FormControl>

                    <InputLabel>Request Type</InputLabel>
                    <Select style={{}}
                        //defaultValue={this.returnDefaultValue(field)}

                            value={this.state.inputData.type}

                            name="Type"
                            onChange={(event) => {
                                this.state.inputData.type = event.target.value;
                                this.updateComponent()
                            }}>
                        <MenuItem key="WAGE DEVELOPMENT BOND" value="WAGE DEVELOPMENT BOND">WAGE DEVELOPMENT
                            BOND</MenuItem>)
                        <MenuItem key="DIB" value="DIB">DIB</MenuItem>
                        <MenuItem key="DPB" value="DPB">DPB</MenuItem>)


                    </Select>

                </FormControl>

            </Grid>
        )

    };
    updateComponent = () => {
        this.forceUpdate();
    }
    renderJsonForm = () => {
        if (this.state.inputData.type !== undefined) {
            return (
                <React.Fragment>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, initiateBondrequest, this.updateComponent)
                    }
                </React.Fragment>
            )
        }

    }
    renderFileUpload = () => {
        if (this.state.inputData.type !== undefined) {
            return (

                <Grid item xs={12}>
                    {FileTypeComponent.file(this.state, this.updateComponent, scanningFile)}
                </Grid>

            )
        }
        return;
    }
    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.inputData)

        if(this.state.fileUploadData.scanningFile!==undefined){
            let fileUploadPath = backEndServerURL + "/case/upload";
            let types = 'Attachments';
            let files = this.state.fileUploadData.scanningFile;
            console.log(files)
            let formData=new FormData();
            formData.append("appId",this.state.appId)
            formData.append("file",files)
            formData.append("type",types)
            axios({
                method: 'post',
                url: fileUploadPath,
                data: formData,
                withCredentials: true,
                headers: {'content-type': 'multipart/form-data'}
            })
                .then((response) => {

                    console.log(response);


                })
                .catch((error) => {
                    console.log(error)
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.serviceType = "BondRequest";
        this.state.inputData.next_user = this.state.inputData.maker_send_to;
        this.state.inputData.maker_update_all_info_send_to = "CHECKER";
        data.subServiceType = this.state.inputData.type;
        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                var url = backEndServerURL + "/case/route/" + this.state.appId;
                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");

                    })
                    .catch((error) => {
                        console.log(error);

                    });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    render() {

        const {classes} = this.props;


        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">

                            <h4>Bond Management<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                <Grid container spacing={3}>
                                    <ThemeProvider theme={theme}>

                                        {this.returnAccountTypeField()}
                                        {this.renderJsonForm()}
                                        {this.checkerList()}
                                        {this.renderFileUpload()}

                                    </ThemeProvider>


                                </Grid>
                            </div>


                            <center>
                                <button
                                    className="btn btn-outline-danger"
                                    style={{
                                        verticalAlign: 'middle',
                                    }}
                                    onClick={this.handleSubmit}

                                >
                                    Submit
                                </button>
                            </center>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );
    }


}

export default withStyles(styles)(Bondmanagement);
