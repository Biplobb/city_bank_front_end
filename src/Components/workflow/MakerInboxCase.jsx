import {
    MAKERAccountMaintenance
} from "./WorkflowJsonForm3";
import {
   MAKERJsonFormForCasaIndividual,
   MAKERJsonFormForCasaJoint,
   MAKERJsonFormForCasaProprietorship,
   MAKERJsonFormForCasaCompany,
} from "./WorkflowJsonForm4";
import {
    MakerJsonFormForCasaIndividualTagFdr2
} from "./WorkflowJsonFormArin";
import React, {Component} from "react";
import VerifyMakerInboxCase from "./CASA/VerifyMakerInboxCase";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Grid from "@material-ui/core/Grid";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import ImageCrop from "./CASA/ImageCrop";
import VerifyMakerPhoto from "./VerifyMakerPhoto";
import OpeningSDM from "./fdr/OpeningSDM";
import VarifyMakerAccountMaintenance from "./AccountMaintenance/VarifyMakerAccountMaintenance";
let SearchFormIndividual = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 6,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 6,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 6,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 6,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 6,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 6,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 6,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 6,


    },];
let SearchFormNonIndividual = [


    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,
        "email": true,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone",
        "required": false,
        "grid":6

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":6


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":6


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":6


    }



];
let SearchFormJoint = [

    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Cb Number",
        "grid":6


    },
    {
        "varName": "accountSource",
        "type": "text",
        "label": "Account Source",
        "grid":6


    },
    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":6


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":6,

        "email": true,


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":6


    }
];
class MakerInboxCase extends Component {
    state = {
        sendTo: false,
        documentList: [],
        getDocument: false

    }

    constructor(props) {
        super(props);


    }


    componentDidMount() {
        let fileUrl = backEndServerURL + "/case/files/" + this.props.appUid;
        axios.get(fileUrl, {withCredentials: true})
            .then((response) => {
                console.log(":d")
                console.log(response.data)
                this.setState({
                    documentList: response.data,
                    getDocument: true
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    inboxCasePhoto = () => {
        return (<VerifyMakerPhoto closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                  subServiceType={this.props.subServiceType}

                                  titleName="Maker Update All Information"
                                  documentList={this.state.documentList}/>)



    };
    casa=()=>{

        if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') &&  (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C") && this.props.taskTitle === 'maker_update_all_information') {
            return (<VerifyMakerInboxCase closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          commonJsonForm={MAKERJsonFormForCasaIndividual}
                                          titleName="Maker Update All Information"
                                          appId={this.props.appUid}/>)
        }
        if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') && this.props.subServiceType==="Joint Account"  && this.props.taskTitle === 'maker_update_all_information') {
            return (<VerifyMakerInboxCase closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                          titleName="Maker Update All Information"
                                          commonJsonForm={MAKERJsonFormForCasaJoint}
                                          appId={this.props.appUid}/>)
        }
        else if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') && (this.props.subServiceType === "NONINDIVIDUAL" || this.props.subServiceType === "Proprietorship A/C") && this.props.taskTitle === 'maker_update_all_information') {
            return (<VerifyMakerInboxCase closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          titleName="Maker Update All Information"
                                          commonJsonForm={MAKERJsonFormForCasaProprietorship}

                                          appId={this.props.appUid}/>)
        }
        else if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') && this.props.subServiceType === "Company Account" && this.props.taskTitle === 'maker_update_all_information') {
            return (<VerifyMakerInboxCase closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          titleName="Maker Update All Information"
                                          commonJsonForm={MAKERJsonFormForCasaCompany}

                                          appId={this.props.appUid}/>)
        }
        else if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') && this.props.subServiceType === "Agent Account Opening" && this.props.taskTitle === 'maker_update_all_information') {
            return (<VerifyMakerInboxCase closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          titleName="Maker Update All Information"
                                          commonJsonForm={MAKERJsonFormForCasaCompany}

                                          appId={this.props.appUid}/>)
        }

        else if ((this.props.serviceType === "Account Opening" || this.props.serviceType === 'ExistAccountOpening') && this.props.taskTitle === 'maker_signiture_photo_upload') {
            return (<ImageCrop closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                               subServiceType={this.props.subServiceType}
                               titleName="Maker photo Upload"
                               appId={this.props.appUid}/>)
        }



    };
    fdr=()=>{
        if (this.props.serviceType ==='FDR Opening' ){
            return (<OpeningSDM closeModal={this.props.closeModal}
                                serviceType={this.props.serviceType}
                                subserviceType={this.props.subServiceType}
                                titleName="Maker Update All Information"
                                serviceForm={MakerJsonFormForCasaIndividualTagFdr2}
                                appId={this.props.appUid}/>)
        }
    }
    maintenance=()=>{
        if (this.props.serviceType === 'Maintenance' && this.props.subServiceType === 'AccountMaintenance' && this.props.taskTitle === 'maker_update_all_information') {
            return (
                <VarifyMakerAccountMaintenance closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                               subServiceType={this.props.subServiceType}
                                               titleName="Maker Update All Information"
                                               jsonForm={MAKERAccountMaintenance}
                                               appId={this.props.appUid}/>)
        }
    }
    inboxCase = () => {

        if(this.props.serviceType === "Account Opening"){
            return (
                this.casa()
            )
        }
        else if(this.props.serviceType === "FDR Opening"){
            return (
                this.fdr()
            )
        }
        else if(this.props.serviceType === "Maintenance"){
            return (
                this.maintenance()
            )
        }
        else if(this.props.serviceType === "ExistAccountOpening"){
            return (
                this.casa()
            )
        }



    };
    close = () => {
        this.props.closeModal();
    }

    render() {
        const {classes} = this.props;



           return (
               <div>

                   <Card>
                       <CardHeader color="rose">
                           <h4>{this.props.serviceType}<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                       </CardHeader>
                       <CardBody>
                           <Grid container spacing={1}>

                               <Grid  item xs={9}>

                                   {this.inboxCasePhoto()}

                               </Grid>


                               <Grid item xs={3}>
                                   {this.inboxCase()}
                               </Grid>


                           </Grid>
                       </CardBody>
                   </Card>
               </div>

           )

    }

}

export default MakerInboxCase;