//##########MainTenance##############
//CS Maintenance With Print File
function makeReadOnly(object) {
    let returnObject = JSON.parse(JSON.stringify(object));

    let returnObjectVariables = returnObject;

    for (let i = 0; i < returnObjectVariables.length; i++) {
        returnObjectVariables[i]["readOnly"] = true;
    }
    return returnObject;
}

function makeReadOnlyObject(object) {
    let returnObject = JSON.parse(JSON.stringify(object));

    let returnObjectVariables = returnObject;

    for (let i = 0; i < returnObjectVariables.length; i++) {
        returnObjectVariables[i]["readOnly"] = true;
    }
    return returnObject;
}

let csSendTo = {
    "varName": "cs_send_to",
    "type": "select",
    "label": "Send to?",
    "enum": [
        "BOM",
        "BM"
    ],
};
let bomSendTo = {
    "varName": "bom_send_to",
    "type": "select",
    "label": "Send to?",

    "enum": [
        "CS",
        "BM"
    ]
};
let bomApproval = {
    "varName": "bom_approval",
    "type": "select",
    "label": "Approve",

    "enum": [
        "APPROVED",
        "NOTAPPROVED"
    ]
};
let bmSendTo = {
    "varName": "bm_send_to",
    "type": "select",
    "label": "Send to?",

    "enum": [
        "BOM",
        "CS"
    ]
};
let bmApprove = {
    "varName": "bm_approval",
    "type": "select",
    "label": "Approve",

    "enum": [
        "APPROVED",
        "NOTAPPROVED"
    ]
};

let makerApprove = {
    "varName": "maker_approval",
    "type": "select",
    "label": "Approve",

    "enum": [
        "APPROVED",
        "NOTAPPROVED"
    ]
};
let checkerApprove = {
    "varName": "checker_approval",
    "type": "select",
    "label": "Approve",

    "enum": [
        "APPROVED",
        "NOTAPPROVED"
    ]
};
let JsonFormMaintenanceWithPrintFileAddress = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldAddress",
            "type": "text",
            "label": "Old Address",
        },
        {
            "varName": "newAddress",
            "type": "text",
            "label": "New Address",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }
    ],

};
let JsonFormMaintenanceWithPrintFileContact = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldContact",
            "type": "text",
            "label": "Old Contact",
        },
        {
            "varName": "newContact",
            "type": "text",
            "label": "New Contact",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }

    ],

};
let JsonFormMaintenanceWithPrintFileEmail = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldEmail",
            "type": "text",
            "label": "Old Email",
        },
        {
            "varName": "newEmail",
            "type": "text",
            "label": "New Email",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileNominee = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldNominee",
            "type": "text",
            "label": "Old Nominee",
        },
        {
            "varName": "newNominee",
            "type": "text",
            "label": "New Nominee",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileTenor = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldTenor",
            "type": "text",
            "label": "Old Tenor",
        },
        {
            "varName": "newTenor",
            "type": "text",
            "label": "New Tenor",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }

    ],

};
let JsonFormMaintenanceWithPrintFileScheme = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldScheme",
            "type": "text",
            "label": "Old Scheme",
        },
        {
            "varName": "newScheme",
            "type": "text",
            "label": "New Scheme",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileMaturity = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldMaturity",
            "type": "text",
            "label": "Old Maturity",
        },
        {
            "varName": "newMaturity",
            "type": "text",
            "label": "New Maturity",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileTitle = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldTitle",
            "type": "text",
            "label": "Old Title",
        },
        {
            "varName": "newTitle",
            "type": "text",
            "label": "New Title",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileLink = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldLink",
            "type": "text",
            "label": "Old Link",
        },
        {
            "varName": "newLink",
            "type": "text",
            "label": "New Link",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithPrintFileEtin = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldEtin",
            "type": "text",
            "label": "Old Etin",
        },
        {
            "varName": "newEtin",
            "type": "text",
            "label": "New Etin",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }

    ]

};
let JsonFormMaintenanceWithPrintFileDormant = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldDormant",
            "type": "text",
            "label": "Old Dormant",
        },
        {
            "varName": "newDormant",
            "type": "text",
            "label": "New Dormant",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }

    ],

};
let JsonFormMaintenanceWithPrintFileInputFiled = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },

        {
            "varName": "oldInputFiled",
            "type": "text",
            "label": "Old InputFiled",
        },
        {
            "varName": "newInputFiled",
            "type": "text",
            "label": "New InputFiled",
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};
let JsonFormMaintenanceWithFiles = {
    "variables": [

        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB number"
        },
        {
            "varName": "file",
            "type": "file",
            "label": "Attach File",
        }


    ],

};


//CS Maintenance with print file
let CSJsonFormMaintenanceWithPrintFileAddress = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileAddress));
CSJsonFormMaintenanceWithPrintFileAddress.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileContact = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileContact));
CSJsonFormMaintenanceWithPrintFileContact.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileEmail = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileEmail));
CSJsonFormMaintenanceWithPrintFileEmail.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileNominee = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileNominee));
CSJsonFormMaintenanceWithPrintFileNominee.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileTenor = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileTenor));
CSJsonFormMaintenanceWithPrintFileTenor.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileScheme = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileScheme));
CSJsonFormMaintenanceWithPrintFileScheme.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileMaturity = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileMaturity));
CSJsonFormMaintenanceWithPrintFileMaturity.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileTitle = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileTitle));
CSJsonFormMaintenanceWithPrintFileTitle.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileLink = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileLink));
CSJsonFormMaintenanceWithPrintFileLink.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileEtin = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileEtin));
CSJsonFormMaintenanceWithPrintFileEtin.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileDormant = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileDormant));
CSJsonFormMaintenanceWithPrintFileDormant.variables.push(csSendTo);
let CSJsonFormMaintenanceWithPrintFileInputFiled = JSON.parse(JSON.stringify(JsonFormMaintenanceWithPrintFileInputFiled));
CSJsonFormMaintenanceWithPrintFileInputFiled.variables.push(csSendTo);

//CS Maintenance With File
let CSJsonFormMaintenanceWithFile = JSON.parse(JSON.stringify(JsonFormMaintenanceWithFiles));
CSJsonFormMaintenanceWithFile.variables.push(csSendTo);

//BOM Maintenance With Print File
let BOMJsonFormMaintenanceWithPrintFileAddress = makeReadOnly(JsonFormMaintenanceWithPrintFileAddress);
BOMJsonFormMaintenanceWithPrintFileAddress.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileAddress.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileContact = makeReadOnly(JsonFormMaintenanceWithPrintFileContact);
BOMJsonFormMaintenanceWithPrintFileContact.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileContact.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileEmail = makeReadOnly(JsonFormMaintenanceWithPrintFileEmail);
BOMJsonFormMaintenanceWithPrintFileEmail.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileEmail.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileNominee = makeReadOnly(JsonFormMaintenanceWithPrintFileNominee);
BOMJsonFormMaintenanceWithPrintFileNominee.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileNominee.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileTenor = makeReadOnly(JsonFormMaintenanceWithPrintFileTenor);
BOMJsonFormMaintenanceWithPrintFileTenor.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileTenor.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileScheme = makeReadOnly(JsonFormMaintenanceWithPrintFileScheme);
BOMJsonFormMaintenanceWithPrintFileScheme.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileScheme.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileMaturity = makeReadOnly(JsonFormMaintenanceWithPrintFileMaturity);
BOMJsonFormMaintenanceWithPrintFileMaturity.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileMaturity.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileTitle = makeReadOnly(JsonFormMaintenanceWithPrintFileTitle);
BOMJsonFormMaintenanceWithPrintFileTitle.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileTitle.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileLink = makeReadOnly(JsonFormMaintenanceWithPrintFileLink);
BOMJsonFormMaintenanceWithPrintFileLink.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileLink.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileEtin = makeReadOnly(JsonFormMaintenanceWithPrintFileEtin);
BOMJsonFormMaintenanceWithPrintFileEtin.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileEtin.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileDormant = makeReadOnly(JsonFormMaintenanceWithPrintFileDormant);
BOMJsonFormMaintenanceWithPrintFileDormant.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileDormant.variables.push(bomSendTo);
let BOMJsonFormMaintenanceWithPrintFileInputFiled = makeReadOnly(JsonFormMaintenanceWithPrintFileInputFiled);
BOMJsonFormMaintenanceWithPrintFileInputFiled.variables.push(bomApproval);
BOMJsonFormMaintenanceWithPrintFileInputFiled.variables.push(bomSendTo);
//BOM Maintenance With File
let BOMJsonFormMaintenanceWithFile = JSON.parse(JSON.stringify(JsonFormMaintenanceWithFiles));
BOMJsonFormMaintenanceWithFile.variables.push(bomApproval);
BOMJsonFormMaintenanceWithFile.variables.push(bomSendTo);



//Maker Maintenance With Print File
let MAKERJsonFormMaintenanceWithPrintFileAddress = makeReadOnly(JsonFormMaintenanceWithPrintFileAddress);
let MAKERJsonFormMaintenanceWithPrintFileContact = makeReadOnly(JsonFormMaintenanceWithPrintFileContact);
let MAKERJsonFormMaintenanceWithPrintFileEmail = makeReadOnly(JsonFormMaintenanceWithPrintFileEmail);
let MAKERJsonFormMaintenanceWithPrintFileNominee = makeReadOnly(JsonFormMaintenanceWithPrintFileNominee);
let MAKERJsonFormMaintenanceWithPrintFileTenor = makeReadOnly(JsonFormMaintenanceWithPrintFileTenor);
let MAKERJsonFormMaintenanceWithPrintFileScheme = makeReadOnly(JsonFormMaintenanceWithPrintFileScheme);
let MAKERJsonFormMaintenanceWithPrintFileMaturity = makeReadOnly(JsonFormMaintenanceWithPrintFileMaturity);
let MAKERJsonFormMaintenanceWithPrintFileTitle = makeReadOnly(JsonFormMaintenanceWithPrintFileTitle);
let MAKERJsonFormMaintenanceWithPrintFileLink = makeReadOnly(JsonFormMaintenanceWithPrintFileLink);
let MAKERJsonFormMaintenanceWithPrintFileEtin = makeReadOnly(JsonFormMaintenanceWithPrintFileEtin);
let MAKERJsonFormMaintenanceWithPrintFileDormant = makeReadOnly(JsonFormMaintenanceWithPrintFileDormant);
let MAKERJsonFormMaintenanceWithPrintFileInputFiled = makeReadOnly(JsonFormMaintenanceWithPrintFileInputFiled);
//Maker Maintenance With File
let MAKERJsonFormMaintenanceWithFile = JSON.parse(JSON.stringify(JsonFormMaintenanceWithFiles));



//Checker Maintenance With Print File
let CHECKERJsonFormMaintenanceWithPrintFileAddress = makeReadOnly(JsonFormMaintenanceWithPrintFileAddress);
CHECKERJsonFormMaintenanceWithPrintFileAddress.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileContact = makeReadOnly(JsonFormMaintenanceWithPrintFileContact);
CHECKERJsonFormMaintenanceWithPrintFileContact.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileEmail = makeReadOnly(JsonFormMaintenanceWithPrintFileEmail);
CHECKERJsonFormMaintenanceWithPrintFileEmail.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileNominee = makeReadOnly(JsonFormMaintenanceWithPrintFileNominee);
CHECKERJsonFormMaintenanceWithPrintFileNominee.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileTenor = makeReadOnly(JsonFormMaintenanceWithPrintFileTenor);
CHECKERJsonFormMaintenanceWithPrintFileTenor.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileScheme = makeReadOnly(JsonFormMaintenanceWithPrintFileScheme);
CHECKERJsonFormMaintenanceWithPrintFileScheme.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileMaturity = makeReadOnly(JsonFormMaintenanceWithPrintFileMaturity);
CHECKERJsonFormMaintenanceWithPrintFileMaturity.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileTitle = makeReadOnly(JsonFormMaintenanceWithPrintFileTitle);
CHECKERJsonFormMaintenanceWithPrintFileTitle.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileLink = makeReadOnly(JsonFormMaintenanceWithPrintFileLink);
CHECKERJsonFormMaintenanceWithPrintFileLink.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileEtin = makeReadOnly(JsonFormMaintenanceWithPrintFileEtin);
CHECKERJsonFormMaintenanceWithPrintFileEtin.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileDormant = makeReadOnly(JsonFormMaintenanceWithPrintFileDormant);
CHECKERJsonFormMaintenanceWithPrintFileDormant.variables.push(checkerApprove);

let CHECKERJsonFormMaintenanceWithPrintFileInputFiled = makeReadOnly(JsonFormMaintenanceWithPrintFileInputFiled);
CHECKERJsonFormMaintenanceWithPrintFileInputFiled.variables.push(checkerApprove);

//CHECKER Maintenance With File
let CHECKERJsonFormMaintenanceWithFile = JSON.parse(JSON.stringify(JsonFormMaintenanceWithFiles));
CHECKERJsonFormMaintenanceWithFile.variables.push(checkerApprove);




//BM  Maintenance With print File
let BMfromCSJsonFormMaintenanceWithPrintFileAddress = makeReadOnly(JsonFormMaintenanceWithPrintFileAddress);
BMfromCSJsonFormMaintenanceWithPrintFileAddress.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileAddress = makeReadOnly(JsonFormMaintenanceWithPrintFileAddress);
BMfromBOMJsonFormMaintenanceWithPrintFileAddress.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileAddress.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileContact = makeReadOnly(JsonFormMaintenanceWithPrintFileContact);
BMfromCSJsonFormMaintenanceWithPrintFileContact.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileContact = makeReadOnly(JsonFormMaintenanceWithPrintFileContact);
BMfromBOMJsonFormMaintenanceWithPrintFileContact.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileContact.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileEmail = makeReadOnly(JsonFormMaintenanceWithPrintFileEmail);
BMfromCSJsonFormMaintenanceWithPrintFileEmail.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileEmail = makeReadOnly(JsonFormMaintenanceWithPrintFileEmail);
BMfromBOMJsonFormMaintenanceWithPrintFileEmail.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileEmail.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileNominee = makeReadOnly(JsonFormMaintenanceWithPrintFileNominee);
BMfromCSJsonFormMaintenanceWithPrintFileNominee.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileNominee = makeReadOnly(JsonFormMaintenanceWithPrintFileNominee);
BMfromBOMJsonFormMaintenanceWithPrintFileNominee.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileNominee.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileTenor = makeReadOnly(JsonFormMaintenanceWithPrintFileTenor);
BMfromCSJsonFormMaintenanceWithPrintFileTenor.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileTenor = makeReadOnly(JsonFormMaintenanceWithPrintFileTenor);
BMfromBOMJsonFormMaintenanceWithPrintFileTenor.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileTenor.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileScheme = makeReadOnly(JsonFormMaintenanceWithPrintFileScheme);
BMfromCSJsonFormMaintenanceWithPrintFileScheme.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileScheme = makeReadOnly(JsonFormMaintenanceWithPrintFileScheme);
BMfromBOMJsonFormMaintenanceWithPrintFileScheme.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileScheme.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileMaturity = makeReadOnly(JsonFormMaintenanceWithPrintFileMaturity);
BMfromCSJsonFormMaintenanceWithPrintFileMaturity.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileMaturity = makeReadOnly(JsonFormMaintenanceWithPrintFileMaturity);
BMfromBOMJsonFormMaintenanceWithPrintFileMaturity.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileMaturity.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileTitle = makeReadOnly(JsonFormMaintenanceWithPrintFileTitle);
BMfromCSJsonFormMaintenanceWithPrintFileTitle.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileTitle = makeReadOnly(JsonFormMaintenanceWithPrintFileTitle);
BMfromBOMJsonFormMaintenanceWithPrintFileTitle.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileTitle.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileLink = makeReadOnly(JsonFormMaintenanceWithPrintFileLink);
BMfromCSJsonFormMaintenanceWithPrintFileLink.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileLink = makeReadOnly(JsonFormMaintenanceWithPrintFileLink);
BMfromBOMJsonFormMaintenanceWithPrintFileLink.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileLink.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileEtin = makeReadOnly(JsonFormMaintenanceWithPrintFileEtin);
BMfromCSJsonFormMaintenanceWithPrintFileEtin.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileEtin = makeReadOnly(JsonFormMaintenanceWithPrintFileEtin);
BMfromBOMJsonFormMaintenanceWithPrintFileEtin.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileEtin.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileDormant = makeReadOnly(JsonFormMaintenanceWithPrintFileDormant);
BMfromCSJsonFormMaintenanceWithPrintFileDormant.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileDormant = makeReadOnly(JsonFormMaintenanceWithPrintFileDormant);
BMfromBOMJsonFormMaintenanceWithPrintFileDormant.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileDormant.variables.push(bmSendTo);
let BMfromCSJsonFormMaintenanceWithPrintFileInputFiled = makeReadOnly(JsonFormMaintenanceWithPrintFileInputFiled);
BMfromCSJsonFormMaintenanceWithPrintFileInputFiled.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithPrintFileInputFiled = makeReadOnly(JsonFormMaintenanceWithPrintFileInputFiled);
BMfromBOMJsonFormMaintenanceWithPrintFileInputFiled.variables.push(bmApprove);
BMfromBOMJsonFormMaintenanceWithPrintFileInputFiled.variables.push(bmSendTo);

//BM Maintenance With File
let BMfromCSJsonFormMaintenanceWithFile = JSON.parse(JSON.stringify(JsonFormMaintenanceWithFiles));
BMfromCSJsonFormMaintenanceWithFile.variables.push(bmApprove);
let BMfromBOMJsonFormMaintenanceWithFile = JsonFormMaintenanceWithFiles;
BMfromBOMJsonFormMaintenanceWithFile.variables.push(bmSendTo);
BMfromBOMJsonFormMaintenanceWithFile.variables.push(bmApprove);

//######Existing Account Opening individual#########

let CommonExistJsonFormIndividualAccountOpening = [


    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Customer Name(Old)",
        "readOnly": true,
        "grid":6
    },


    {
        "varName": "nidOld",
        "type": "text",
        "label": "NID No(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "passportOld",
        "type": "text",
        "label": "Passport Number(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "phoneOld",
        "type": "text",
        "label": "Phone No(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "birthCertificateOld",
        "type": "text",
        "label": "Birth Certificate(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "drivingLicenseOld",
        "type": "text",
        "label": "Driving License(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "emailOld",
        "type": "text",
        "label": "Email(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "comAddressOld",
        "type": "text",
        "label": "Communication Address(Old)",
        "readOnly": true,
        "grid":6
    },


    {
        "varName": "permanentAddressOld",
        "type": "text",
        "label": "Permanent Address(Old)",
        "readOnly": true,
        "grid":6
    },

    {
        "varName": "permanentAddress",
        "type": "title",
        "label": "",
        "grid":12
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
        "grid":6
    },
    {
        "varName": "introducerCB",
        "type": "text",
        "label": "Introducer CB",
        "grid":6
    }

];

let commonJsonFormIndividualAccountOpeningForm = [
    {
        "varName": "productCode",
        "type": "text",
        "label": "Product Code",
        "grid":6

    },

    /*{
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum": [
            "Instrapack",
            "NonInstrapack"
        ]
    },*/
    {
        "varName": "sbsCode",
        "type": "select",
        "label": "SBS Code",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    {
        "varName": "debitCard",
        "type": "select",
        "label": "Debit Card",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },

    {
        "varName": "cityTouchID",
        "type": "select",
        "label": "City Touch ID Request",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    {
        "varName": "lockerService",
        "type": "select",
        "label": "Locker Service Request",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    }
];
let CSExistJsonFormIndividualAccountOpening = {};
CSExistJsonFormIndividualAccountOpening = JSON.parse(JSON.stringify(CommonExistJsonFormIndividualAccountOpening));



//######New Account Opening individual#########

let CommonNewJsonFormIndividualAccountOpening = [

    {
        "varName": "cbNumber",
        "type": "text",
        "label": "CB Number  "
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name"
    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date OF Birth",
     },*/
    {
        "varName": "nid",
        "type": "text",
        "label": "NID No",
    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport Number"
    },
    {
        "varName": "drivingLicense",
        "type": "text",
        "label": "Driving License"
    },
    {
        "varName": "birthCertificate",
        "type": "text",
        "label": "Birth Certificate",
    },
    {
        "varName": "phone",
        "type": "text",
        "label": "Phone No",
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
    },
    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address",
    },


    {
        "varName": "permanentAddress",
        "type": "text",
        "label": "Permanent Address"
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
    },
    {
        "varName": "introducerCB",
        "type": "text",
        "label": "Introducer CB",
    },
    {
        "varName": "smsAlertRequest",
        "type": "select",
        "label": "Sms Alert Request",
        "enum": [
            "YES",
            "NO"
        ]
    },

    {
        "varName": "e-statement",
        "type": "select",
        "label": "E-Statement",
        "enum": [
            "YES",
            "NO"
        ]
    },
    {
        "varName": "chequeBookRequest",
        "type": "select",
        "label": "Cheque Book Request",
        "enum": [
            "YES",
            "NO"
        ]
    }

];
let CSNewJsonFormIndividualAccountOpening = {};
CSNewJsonFormIndividualAccountOpening["variables"] = JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm)));
CSNewJsonFormIndividualAccountOpening.variables.push(csSendTo);

//######BOM Account Opening individual#########
let BOMCommonjsonFormIndividualAccountOpeningSearch = [
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "CB Number",
        "grid":6

    },

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid":6

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid":6


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required":true,
        "grid":6

    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "required":true,
        "grid":6


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",

        "email": true,
        "grid":6


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required":true,
        "grid":6


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid":6


    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid":6


    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required":true,
        "grid":6


    }



];
let CommonCsToBom=[

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 6,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 6,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 6,

    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "required": true,
        "grid": 6,


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 6,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 6,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 6,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 6,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 6,


    },
    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address",
        "grid": 6,
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
        "grid": 6,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "label": "SBS Code",
        "grid": 6,
    },
    {
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 6,

    },
    {
        "varName": "instaPackAccountNo",
        "type": "text",
        "label": "InstaPack Account No",
        "grid": 6,

    },

    {
        "varName": "priority",
        "type": "select",
        "label": "URGENCY",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 6,
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Service ",
        "grid": 12

    },
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 6
    },
    {
        "varName": "debitCardText",
        "type": "text",
        "label": "Name On Card",
        "grid": 6
    },
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Ckeck Book Request",
        "grid": 6,

    },
    {

        "varName": "pageOfChequeBook",
        "type": "text",
        "label": "Page Of Ckeck Book",
        "grid": 6

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "dpsRequest",
        "type": "checkbox",
        "label": "DPS Request",
        "grid": 12
    }

]
let BOMNewJsonFormIndividualAccountOpening = {};
BOMNewJsonFormIndividualAccountOpening = makeReadOnlyObject(JSON.parse(JSON.stringify(BOMCommonjsonFormIndividualAccountOpeningSearch.concat(CommonCsToBom))));

////////////maintenance/////////////
// const maintenanceListChecker = [
//     {
//         " varName": "12digitTinChange",
//         "type": "checkbox",
//         "label": "12-Digit TIN Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "panGirNoTinUpdateOld",
//         "type": "text",
//         "label": "Existing PAN GIR No TIN Update",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "panGirNoTinUpdateNew",
//         "type": "text",
//         "label": "New PAN GIR No TIN Update",
//         "grid": 5,
//
//     },
//     {
//         "varName": "titleChangeRectificationChange",
//         "type": "checkbox",
//         "label": "Title Rectification Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "customerNameOld",
//         "type": "text",
//         "label": "Existing Customer Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "customerNameNew",
//         "type": "text",
//         "label": "New Customer Name",
//         "grid": 5,
//         "conditionalVarName": "titleChangeRectificationChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "titleOld",
//         "type": "text",
//         "label": "Existing Title",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "titleNew",
//         "type": "text",
//         "label": "New Title",
//         "grid": 5,
//         "conditionalVarName": "titleChangeRectificationChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         " varName": "nomineeUpdateChange",
//         "type": "checkbox",
//         "label": "Nominee Update Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "countryOld",
//         "type": "text",
//         "label": "Existing Country",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "countryNew",
//         "type": "text",
//         "label": "New Country",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "dOBOld",
//         "type": "date",
//         "label": "Existing DOB",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "dOBNew",
//         "type": "date",
//         "label": "New DOB",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "postal CodeOld",
//         "type": "text",
//         "label": "Existing Postal Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "postal CodeNew",
//         "type": "text",
//         "label": "New Postal Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "nomineeNameOld",
//         "type": "text",
//         "label": "Existing Nominee Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "nomineeNameNew",
//         "type": "text",
//         "label": "New Nominee Name",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "relationshipOld",
//         "type": "text",
//         "label": "Existing Relationship",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "relationshipNew",
//         "type": "text",
//         "label": "New Relationship",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "address1Old",
//         "type": "text",
//         "label": "Existing Address 1 ",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "address1New",
//         "type": "text",
//         "label": "New Address 1 ",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "address2Old",
//         "type": "text",
//         "label": "Existing Address 2",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "address2New",
//         "type": "text",
//         "label": "New Address 2",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cityCodeOld",
//         "type": "text",
//         "label": "Existing City Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "cityCodeNew",
//         "type": "text",
//         "label": "New City Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "stateCodeOld",
//         "type": "text",
//         "label": "Existing State Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "stateCodeNew",
//         "type": "text",
//         "label": "New State Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "regNoOld",
//         "type": "text",
//         "label": "Existing Reg No",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "regNoNew",
//         "type": "text",
//         "label": "New Reg No",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "nomineeMinorOld",
//         "type": "text",
//         "label": "Existing Nominee Minor",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "nomineeMinorNew",
//         "type": "text",
//         "label": "New Nominee Minor",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "varName": "guardianUpdate",
//         "type": "checkbox",
//         "label": "Guardian Update",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "countryGuardianOld",
//         "type": "text",
//         "label": "Guardian Existing Country",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "countryGuardianNew",
//         "type": "text",
//         "label": "Guardian New Country",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "postalGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian Existing Postal Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "postalGuardianCodeNew",
//         "type": "text",
//         "label": "Guardian New Postal Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cityGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian Existing City Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "CityGuardianCodeNew",
//         "type": "text",
//         "label": "Guardian New City Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "stateGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian's Existing State Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "stateCodeGuardianNew",
//         "type": "text",
//         "label": "Guardian's New State Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "guardianNameOld",
//         "type": "text",
//         "label": "Existing Guardian's Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "guardianNameNew",
//         "type": "text",
//         "label": "New Guardian's Name",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "guardianCodeOld",
//         "type": "text",
//         "label": "Existing Guardian Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "guardianCodeNew",
//         "type": "text",
//         "label": "New Guardian Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "addressGuardianOld",
//         "type": "text",
//         "label": "Guardian Existing Address",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "addressGuardianNew",
//         "type": "text",
//         "label": "Guardian New Address",
//         "grid": 5,
//
//     },
//     {
//         "varName": "updatechangePhotoIdChange",
//         "type": "checkbox",
//         "label": "Photo Id Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "expiryDateOld",
//         "type": "text",
//         "label": "Existing Expiry Date",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "expiryDateNew",
//         "type": "date",
//         "label": "New Expiry Date",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "issueDateOld",
//         "type": "text",
//         "label": "Existing Issue Date",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "issueDateNew",
//         "type": "date",
//         "label": "New Issue Date",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "nationalIdCardOld",
//         "type": "text",
//         "label": "Existing National Id Card",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "nationalIdCardNew",
//         "type": "text",
//         "label": "New National Id Card",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "passportDetailsOld",
//         "type": "text",
//         "label": "Existing Passport Details",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "passportDetailsNew",
//         "type": "text",
//         "label": "New Passport Details",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "passportNoOld",
//         "type": "text",
//         "label": "Existing Passport No",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "passportNoNew",
//         "type": "text",
//         "label": "New Passport No",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "contactNumberChangeChange",
//         "type": "checkbox",
//         "label": "Contact Number  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "phoneNo1Old",
//         "type": "text",
//         "label": "Existing Phone No 1 ",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "phoneNo1New",
//         "type": "text",
//         "label": "New Phone No 1 ",
//         "grid": 5,
//         "conditionalVarName": "contactNumberChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "phoneNo2Old",
//         "type": "text",
//         "label": "Existing Phone No 2",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "phoneNo2New",
//         "type": "text",
//         "label": "New Phone No 2",
//         "grid": 5,
//         "conditionalVarName": "contactNumberChangeChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "varName": "emailAddressChangeChange",
//         "type": "checkbox",
//         "label": "Email Address  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "emailOld",
//         "type": "text",
//         "label": "Existing Email",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "emailNew",
//         "type": "text",
//         "label": "New Email",
//         "grid": 5,
//         "conditionalVarName": "emailAddressChangeChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         " varName": "eStatementEnrollmentChange",
//         "type": "checkbox",
//         "label": "E - Statement Enrollment Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Despatch ModeOld",
//         "type": "text",
//         "label": "Existing Despatch Mode",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Despatch ModeNew",
//         "type": "text",
//         "label": "New Despatch Mode",
//         "grid": 5,
//         "conditionalVarName": "eStatementEnrollmentChange",
//         "conditionalVarValue": "true",
//     },
//
//
//     {
//         " varName": "addressChangeChange",
//         "type": "checkbox",
//         "label": "Address   Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cityOld",
//         "type": "text",
//         "label": "Existing City",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "cityNew",
//         "type": "text",
//         "label": "New City",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "communicationAddress1Old",
//         "type": "text",
//         "label": "Existing Communication Address 1",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "communicationAddress1New",
//         "type": "text",
//         "label": "New Communication Address 1",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "communicationAddress2Old",
//         "type": "text",
//         "label": "Existing Communication Address 2",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "communicationAddress2New",
//         "type": "text",
//         "label": "New Communication Address 2",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "countryOld",
//         "type": "text",
//         "label": "Existing Country",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "countryNew",
//         "type": "text",
//         "label": "New Country",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "employerAddress1Old",
//         "type": "text",
//         "label": "Existing Employer Address 1",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "employerAddress1New",
//         "type": "text",
//         "label": "New Employer Address 1",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "employerAddress2Old",
//         "type": "text",
//         "label": "Existing Employer Address 2",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "employerAddress2New",
//         "type": "text",
//         "label": "New Employer Address 2",
//         "grid": 5,
//
//     },
//
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "permanentAddress1Old",
//         "type": "text",
//         "label": "Existing Permanent Address 1",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "permanentAddress1New",
//         "type": "text",
//         "label": "New Permanent Address 1",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "permanentAddress2Old",
//         "type": "text",
//         "label": "Existing Permanent Address 2",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "permanentAddress2New",
//         "type": "text",
//         "label": "New Permanent Address 2",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "postalCodeOld",
//         "type": "text",
//         "label": "Existing Postal Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "postalCodeNew",
//         "type": "text",
//         "label": "New Postal Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "stateOld",
//         "type": "text",
//         "label": "Existing State",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "stateNew",
//         "type": "text",
//         "label": "New State",
//         "grid": 5,
//
//     },
//     {
//         " varName": "mandateSignatoryCbTaggingChange",
//         "type": "checkbox",
//         "label": "Mandate/Signatory CB Tagging Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "rELATIONTYPEOld",
//         "type": "text",
//         "label": "Existing RELATION TYPE ",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "rELATIONTYPENew",
//         "type": "text",
//         "label": "New RELATION TYPE ",
//         "grid": 5,
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "rELATIONCODEOld",
//         "type": "text",
//         "label": "Existing RELATION CODE",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "rELATIONCODENew",
//         "type": "text",
//         "label": "New RELATION CODE",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "dESIGNATIONCODEOld",
//         "type": "text",
//         "label": "Existing DESIGNATION CODE",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "dESIGNATIONCODENew",
//         "type": "text",
//         "label": "New DESIGNATION CODE",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cUStIDOld",
//         "type": "text",
//         "label": "Existing CUST. ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "cUSTIDNew",
//         "type": "text",
//         "label": "New CUST. ID",
//         "grid": 5,
//
//     },
//
//
//     {
//         "varName": "spouseNameUpdateChange",
//         "type": "checkbox",
//         "label": "Other Information  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//
//
//     {
//         "varName": "dobOld",
//         "type": "text",
//         "label": "Existing DOB",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "dobNew",
//         "type": "date",
//         "label": "New DOB",
//         "grid": 5,
//
//     },
//
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "fatherOld",
//         "type": "text",
//         "label": "Existing Father Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "fatherNew",
//         "type": "text",
//         "label": "New Father Name",
//         "grid": 5,
//         "conditionalVarName": "fatherNameUpdateChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "motherOld",
//         "type": "text",
//         "label": "Existing Mother Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "motherNew",
//         "type": "text",
//         "label": "New Mother Name",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "spouseOld",
//         "type": "text",
//         "label": "Existing Spouse Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "spouseNew",
//         "type": "text",
//         "label": "New Spouse Name",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "professionOld",
//         "type": "text",
//         "label": "Existing Profession",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "professionNew",
//         "type": "text",
//         "label": "New Profession",
//         "grid": 5,
//
//     },
//
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "employerOld",
//         "type": "text",
//         "label": "Existing Employer Name",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "employerNew",
//         "type": "text",
//         "label": "New Employer Name",
//         "grid": 5,
//
//     },
//     {
//         "varName": "signatureCard",
//         "type": "checkbox",
//         "label": "Signature Card",
//         "grid": 12,
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "signatureCardExisting",
//         "type": "text",
//         "label": "Existing Signature Card",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "signatureCardNew",
//         "type": "text",
//         "label": "New Signature Card",
//         "grid": 5,
//
//     },
//     {
//         " varName": "dormantActivitionChange",
//         "type": "checkbox",
//         "label": "Dormant Activition Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Account StatusOld",
//         "type": "text",
//         "label": "Existing Account Status",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Account StatusNew",
//         "type": "text",
//         "label": "New Account Status",
//         "grid": 5,
//
//     },
//     {
//         "varName": "dormantAccountDataUpdate",
//         "type": "checkbox",
//         "label": "Dormant Account Data Update",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "dormantAccountDataUpdateExisting",
//         "type": "text",
//         "label": "Existing Dormant Account Data",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "dormantAccountDataUpdateNew",
//         "type": "text",
//         "label": "New Dormant Account Data",
//         "grid": 5,
//
//     },
//     {
//         "varName": "schemeMaintenanceLinkChange",
//         "type": "checkbox",
//         "label": "Scheme Maintenance-Link A/C Change",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "schemeACNumberChangeExisting",
//         "type": "text",
//         "label": "Existing Scheme A/C number",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "schemeACNumberChangeNew",
//         "type": "text",
//         "label": "New Scheme A/C number",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "schemeStartDateExisting",
//         "type": "text",
//         "label": "Existing Start Date",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "schemeStartDateNew",
//         "type": "text",
//         "label": "New Start Date",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "schemeEndDateExisting",
//         "type": "text",
//         "label": "Existing End Date",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "schemeEndDateNew",
//         "type": "text",
//         "label": "New End Date",
//         "grid": 5,
//
//     },
//     {
//         "varName": "schemeMaintenance",
//         "type": "checkbox",
//         "label": "Scheme Maintenance - Installment Regularization",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentTranIDExisting",
//         "type": "text",
//         "label": "Existing Tran ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentTranIDNew",
//         "type": "text",
//         "label": "New Tran ID",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentCasaExisting",
//         "type": "text",
//         "label": "Existing CASA A/C Number (Debit)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentCasaNew",
//         "type": "text",
//         "label": "New CASA A/C Number (Debit)",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentSchemeExisting",
//         "type": "text",
//         "label": "Existing Scheme A/C Number (Credit)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentSchemeNew",
//         "type": "text",
//         "label": "New Scheme A/C Number (Credit)",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentAmountExisting",
//         "type": "text",
//         "label": "Existing Amount",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentAmountNew",
//         "type": "text",
//         "label": "New Amount",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentParticularCodeExisting",
//         "type": "text",
//         "label": "Existing Particular Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentParticularCodeNew",
//         "type": "text",
//         "label": "New Particular Code",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "installmentTransationCodeExisting",
//         "type": "text",
//         "label": "Existing Enter Transation Particulars",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "installmentTransationCodeNew",
//         "type": "text",
//         "label": "New Enter Transation Particulars",
//         "grid": 5,
//
//     },
//     {
//         "varName": "cityLive",
//         "type": "checkbox",
//         "label": "City Live",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cityLiveExisting",
//         "type": "text",
//         "label": "Existing City Live",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "cityLiveNew",
//         "type": "text",
//         "label": "New City Live",
//         "grid": 5,
//
//     },
//     {
//         "varName": "projectRelatedDataUpdateADUP",
//         "type": "checkbox",
//         "label": "Project Related Data Update ADUP, CIB",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "aDUPExisting",
//         "type": "text",
//         "label": "Existing ADUP Data",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "aDUPNew",
//         "type": "text",
//         "label": "New Project ADUP Data",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         " varName": "ctrLimitUpdateUnconfirmedAddressChange",
//         "type": "checkbox",
//         "label": "CTR Limit Update/Unconfirmed Address Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Cash Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Cash Limit (DR/CR)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Cash Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Cash Limit (DR/CR)",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "cIBExisting",
//         "type": "text",
//         "label": "Existing CIB Data",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "cIBNew",
//         "type": "text",
//         "label": "New CIB Data",
//         "grid": 5,
//
//     },
//     {
//         "varName": "accountSchemeClose",
//         "type": "checkbox",
//         "label": "Account & Scheme Close",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "accountExisting",
//         "type": "text",
//         "label": "Existing Account Number",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "accountNew",
//         "type": "text",
//         "label": "New Account Number",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "schemeExisting",
//         "type": "text",
//         "label": "Existing Scheme Number",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "schemeNew",
//         "type": "text",
//         "label": "New Scheme Number",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "lockerSIOpen",
//         "type": "checkbox",
//         "label": "Locker SI Open",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "lockerSIOpenExisting",
//         "type": "text",
//         "label": "Existing Locker SI Open",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "lockerSIOpenNew",
//         "type": "text",
//         "label": "New Locker SI Open",
//         "grid": 5,
//
//     },
//     {
//         "varName": "lockerSIClose",
//         "type": "checkbox",
//         "label": "Locker SI Close",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "lockerSICloseExisting",
//         "type": "text",
//         "label": "Existing Locker SI Close",
//         "grid": 5,
//         "readOnly": true,
//
//     },
//     {
//         "varName": "lockerSICloseNew",
//         "type": "text",
//         "label": "New Locker SI Close",
//         "grid": 5,
//
//     },
//
//     {
//         "varName": "tradeFacilitationChange",
//         "type": "checkbox",
//         "label": "Trade Facilitation Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "tradeOld",
//         "type": "text",
//         "label": "Existing Trade",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "tradeNew",
//         "type": "text",
//         "label": "New Trade",
//         "grid": 5,
//
//     },
//     {
//         "varName": "staffAccountGeneralizationChange",
//         "type": "checkbox",
//         "label": "Staff Account Generalization Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "staffFlagOld",
//         "type": "text",
//         "label": "Existing Staff Flag",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "staffFlagNew",
//         "type": "text",
//         "label": "New Staff Flag",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "staffNumberOld",
//         "type": "text",
//         "label": "Existing Staff Number",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "staffNumberNew",
//         "type": "text",
//         "label": "New Staff Number",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C Id",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C Id",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "targetSchemeCodeOld",
//         "type": "text",
//         "label": "Existing Target Scheme Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "targetSchemeCodeNew",
//         "type": "text",
//         "label": "New Target Scheme Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "trialModeOld",
//         "type": "text",
//         "label": "Existing Trial Mode",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "trialModeNew",
//         "type": "text",
//         "label": "New Trial Mode",
//         "grid": 5,
//
//     },
//
//     {
//         " varName": "freezeUnfreezeMarkChange",
//         "type": "checkbox",
//         "label": "Freeze/Unfreeze Mark Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C ID",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "customerIdOld",
//         "type": "text",
//         "label": "Existing Customer ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "customerIdNew",
//         "type": "text",
//         "label": "New Customer ID",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "freezeReasonCodeOld",
//         "type": "text",
//         "label": "Existing Freeze Reason Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "freezeReasonCodeNew",
//         "type": "text",
//         "label": "New Freeze Reason Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "freezeCodeOld",
//         "type": "text",
//         "label": "Existing Freeze  Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "freezeCodeNew",
//         "type": "text",
//         "label": "New Freeze  Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "freezeRemarksOld",
//         "type": "text",
//         "label": "Existing Freeze  Remarks",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "freezeRemarksNew",
//         "type": "text",
//         "label": "New Freeze  Remarks",
//         "grid": 5,
//
//     },
//     {
//         " varName": "cityTouchTaggingChange",
//         "type": "checkbox",
//         "label": "City Touch Tagging Change",
//         "grid": 12,
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C ID",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "customerIdOld",
//         "type": "text",
//         "label": "Existing Customer ID",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "customerIdNew",
//         "type": "text",
//         "label": "New Customer ID",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "accountNameModificationOld",
//         "type": "text",
//         "label": "Existing Account Name Modification (Y/N)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "accountNameModificationNew",
//         "type": "text",
//         "label": "New Account Name Modification (Y/N)",
//         "grid": 5,
//
//     },
//     {
//         "varName": "priorityMarkingChange",
//         "type": "checkbox",
//         "label": "Priority Marking Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "occupationCodeOld",
//         "type": "text",
//         "label": "Existing Occupation Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "occupationCodeNew",
//         "type": "text",
//         "label": "New Occupation Code",
//         "grid": 5,
//
//     },
//     {
//         " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
//         "type": "checkbox",
//         "label": "Unconfirmed AddressMarked Or Unmarked Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Clearing Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Clearing Limit (DR/CR)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Clearing Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Clearing Limit (DR/CR)",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Transfer Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Transfer Limit (DR/CR)",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Transfer Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Transfer Limit (DR/CR)",
//         "grid": 5,
//
//     },
//
//     {
//         " varName": "sbsCodeUpdateCorrectionChange",
//         "type": "checkbox",
//         "label": "SBS Code Update/Correction Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Sector CodeOld",
//         "type": "text",
//         "label": "Existing Sector Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Sector CodeNew",
//         "type": "text",
//         "label": "New Sector Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Sub Sector CodeOld",
//         "type": "text",
//         "label": "Existing Sub Sector Code",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Sub Sector CodeNew",
//         "type": "text",
//         "label": "New Sub Sector Code",
//         "grid": 5,
//
//     },
//
//
//     {
//         " varName": "relationshipManagerRmCodeChangeUpdateChange",
//         "type": "checkbox",
//         "label": "Relationship Manager (RM) Code Change/Update Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 2,
//     },
//     {
//         "varName": "Free Code 3 Old",
//         "type": "text",
//         "label": "Existing Free Code 3 ",
//         "grid": 5,
//         "readOnly": true,
//     },
//     {
//         "varName": "Free Code 3 New",
//         "type": "text",
//         "label": "New Free Code 3 ",
//         "grid": 5,
//
//     },
//
// ]


const maintenanceList = [
    {
        " varName": "12digitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing PAN GIR No TIN Update",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true



    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New PAN GIR No TIN Update",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dOBNew",
        "type": "date",
        "label": "New DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postal CodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postal CodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
         "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Guardian Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Guardian's Existing State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Guardian Existing Address",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": "Photo Id Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",

    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
          "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true


    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Despatch ModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Despatch ModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },


    {
        " varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        " varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Account StatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Account StatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        " varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Cash Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Cash Limit (DR/CR)New",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true


    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Clearing Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Clearing Limit (DR/CR)New",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Transfer Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Transfer Limit (DR/CR)New",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sector CodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sector CodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sub Sector CodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sub Sector CodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },


    {
        " varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Free Code 3 Old",
        "type": "text",
        "label": "Existing Free Code 3 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Free Code 3 New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },

]

let BOMAccountMaintenance = {};
BOMAccountMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(maintenanceList)));

let BMAccountMaintenance = {};
BMAccountMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(maintenanceList)));
let CallCenterAccountMaintenance = {};
CallCenterAccountMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(maintenanceList)));



/////maintenance chacker/////////////


const maintenanceListChecker = [
    {
        " varName": "12digitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing PAN GIR No TIN Update",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true



    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New PAN GIR No TIN Update",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "12digitTinChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "titleChangeRectificationChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dOBNew",
        "type": "date",
        "label": "New DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postal CodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postal CodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "nomineeUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Guardian Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Guardian's Existing State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Guardian Existing Address",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "guardianUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": "Photo Id Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "updatechangePhotoIdChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",

    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": "true",
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true


    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "contactNumberChangeChange",
        "conditionalVarValue" : true
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "emailAddressChangeChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Despatch ModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Despatch ModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "eStatementEnrollmentChange",
        "conditionalVarValue" : true
    },


    {
        " varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "addressChangeChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "mandateSignatoryCbTaggingChange",
        "conditionalVarValue" : true
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },

    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "spouseNameUpdateChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "signatureCard",
        "conditionalVarValue" : true
    },
    {
        " varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Account StatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Account StatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantActivitionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "dormantAccountDataUpdate",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenanceLinkChange",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true
    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "schemeMaintenance",
        "conditionalVarValue" : true

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityLive",
        "conditionalVarValue" : true
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "projectRelatedDataUpdateADUP",
        "conditionalVarValue" : true
    },
    {
        " varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Cash Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Cash Limit (DR/CR)New",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "accountSchemeClose",
        "conditionalVarValue" : true

    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true


    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIOpen",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true

    },
    {

        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "lockerSIClose",
        "conditionalVarValue" : true
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "tradeFacilitationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "staffAccountGeneralizationChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true

    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "freezeUnfreezeMarkChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "cityTouchTaggingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "priorityMarkingChange",
        "conditionalVarValue" : true
    },
    {
        " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Clearing Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Clearing Limit (DR/CR)New",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Transfer Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Transfer Limit (DR/CR)New",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue" : true
    },

    {
        " varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sector CodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sector CodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sub Sector CodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Sub Sector CodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue" : true
    },


    {
        " varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "type": "blank",
        "grid": 2,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Free Code 3 Old",
        "type": "text",
        "label": "Existing Free Code 3 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },
    {
        "varName": "Free Code 3 New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName" : "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue" : true
    },

]
// const maintenanceListMaker = [
//     {
//         " varName": "12digitTinChange",
//         "type": "checkbox",
//         "label": "12-Digit TIN Change",
//         "grid": 12,
//
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "panGirNoTinUpdateOld",
//         "type": "text",
//         "label": "Existing PAN GIR No TIN Update",
//         "grid": 6,
//
//     },
//     {
//         "varName": "panGirNoTinUpdateNew",
//         "type": "text",
//         "label": "New PAN GIR No TIN Update",
//         "grid": 5,
//         // "conditionalVarName": "12DigitTinChange",
//         // "conditionalVarValue": true,
//     },
//     {
//         "varName": "titleChangeRectificationChange",
//         "type": "checkbox",
//         "label": "Title Rectification Change",
//         "grid": 12,
//
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "customerNameOld",
//         "type": "text",
//         "label": "Existing Customer Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "customerNameNew",
//         "type": "text",
//         "label": "New Customer Name",
//         "grid": 5,
//         "conditionalVarName": "titleChangeRectificationChange",
//         "conditionalVarValue": true,
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "titleOld",
//         "type": "text",
//         "label": "Existing Title",
//         "grid": 6,
//
//     },
//     {
//         "varName": "titleNew",
//         "type": "text",
//         "label": "New Title",
//         "grid": 5,
//         "conditionalVarName": "titleChangeRectificationChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         " varName": "nomineeUpdateChange",
//         "type": "checkbox",
//         "label": "Nominee Update Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "countryOld",
//         "type": "text",
//         "label": "Existing Country",
//         "grid": 6,
//
//     },
//     {
//         "varName": "countryNew",
//         "type": "text",
//         "label": "New Country",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "dOBOld",
//         "type": "text",
//         "label": "Existing DOB",
//         "grid": 6,
//
//     },
//     {
//         "varName": "dOBNew",
//         "type": "text",
//         "label": "New DOB",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "postal CodeOld",
//         "type": "text",
//         "label": "Existing Postal Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "postal CodeNew",
//         "type": "text",
//         "label": "New Postal Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "nomineeNameOld",
//         "type": "text",
//         "label": "Existing Nominee Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "nomineeNameNew",
//         "type": "text",
//         "label": "New Nominee Name",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "relationshipOld",
//         "type": "text",
//         "label": "Existing Relationship",
//         "grid": 6,
//
//     },
//     {
//         "varName": "relationshipNew",
//         "type": "text",
//         "label": "New Relationship",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "address1Old",
//         "type": "text",
//         "label": "Existing Address 1 ",
//         "grid": 6,
//
//     },
//     {
//         "varName": "address1New",
//         "type": "text",
//         "label": "New Address 1 ",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "address2Old",
//         "type": "text",
//         "label": "Existing Address 2",
//         "grid": 6,
//
//     },
//     {
//         "varName": "address2New",
//         "type": "text",
//         "label": "New Address 2",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cityCodeOld",
//         "type": "text",
//         "label": "Existing City Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "cityCodeNew",
//         "type": "text",
//         "label": "New City Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "stateCodeOld",
//         "type": "text",
//         "label": "Existing State Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "stateCodeNew",
//         "type": "text",
//         "label": "New State Code",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "regNoOld",
//         "type": "text",
//         "label": "Existing Reg No",
//         "grid": 6,
//
//     },
//     {
//         "varName": "regNoNew",
//         "type": "text",
//         "label": "New Reg No",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "nomineeMinorOld",
//         "type": "text",
//         "label": "Existing Nominee Minor",
//         "grid": 6,
//
//     },
//     {
//         "varName": "nomineeMinorNew",
//         "type": "text",
//         "label": "New Nominee Minor",
//         "grid": 5,
//         "conditionalVarName": "nomineeUpdateChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "varName": "guardianUpdate",
//         "type": "checkbox",
//         "label": "Guardian Update",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "countryGuardianOld",
//         "type": "text",
//         "label": "Guardian Existing Country",
//         "grid": 6,
//
//     },
//     {
//         "varName": "countryGuardianNew",
//         "type": "text",
//         "label": "Guardian New Country",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "postalGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian Existing Postal Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "postalGuardianCodeNew",
//         "type": "text",
//         "label": "Guardian New Postal Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cityGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian Existing City Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "CityGuardianCodeNew",
//         "type": "text",
//         "label": "Guardian New City Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "stateGuardianCodeOld",
//         "type": "text",
//         "label": "Guardian's Existing State Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "stateCodeGuardianNew",
//         "type": "text",
//         "label": "Guardian's New State Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "guardianNameOld",
//         "type": "text",
//         "label": "Existing Guardian's Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "guardianNameNew",
//         "type": "text",
//         "label": "New Guardian's Name",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "guardianCodeOld",
//         "type": "text",
//         "label": "Existing Guardian Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "guardianCodeNew",
//         "type": "text",
//         "label": "New Guardian Code",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "addressGuardianOld",
//         "type": "text",
//         "label": "Guardian Existing Address",
//         "grid": 6,
//
//     },
//     {
//         "varName": "addressGuardianNew",
//         "type": "text",
//         "label": "Guardian New Address",
//         "grid": 5,
//         "conditionalVarName": "gurdianUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "varName": "updatechangePhotoIdChange",
//         "type": "checkbox",
//         "label": " Photo Id Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "expiryDateOld",
//         "type": "text",
//         "label": "Existing Expiry Date",
//         "grid": 6,
//
//     },
//     {
//         "varName": "expiryDateNew",
//         "type": "text",
//         "label": "New Expiry Date",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "issueDateOld",
//         "type": "text",
//         "label": "Existing Issue Date",
//         "grid": 6,
//
//     },
//     {
//         "varName": "issueDateNew",
//         "type": "text",
//         "label": "New Issue Date",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "nationalIdCardOld",
//         "type": "text",
//         "label": "Existing National Id Card",
//         "grid": 6,
//
//     },
//     {
//         "varName": "nationalIdCardNew",
//         "type": "text",
//         "label": "New National Id Card",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "passportDetailsOld",
//         "type": "text",
//         "label": "Existing Passport Details",
//         "grid": 6,
//
//     },
//     {
//         "varName": "passportDetailsNew",
//         "type": "text",
//         "label": "New Passport Details",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "passportNoOld",
//         "type": "text",
//         "label": "Existing Passport No",
//         "grid": 6,
//
//     },
//     {
//         "varName": "passportNoNew",
//         "type": "text",
//         "label": "New Passport No",
//         "grid": 5,
//         "conditionalVarName": "updatechangePhotoIdChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "contactNumberChangeChange",
//         "type": "checkbox",
//         "label": "Contact Number  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "phoneNo1Old",
//         "type": "text",
//         "label": "Existing Phone No 1 ",
//         "grid": 6,
//
//     },
//     {
//         "varName": "phoneNo1New",
//         "type": "text",
//         "label": "New Phone No 1 ",
//         "grid": 5,
//         "conditionalVarName": "contactNumberChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "phoneNo2Old",
//         "type": "text",
//         "label": "Existing Phone No 2",
//         "grid": 6,
//
//     },
//     {
//         "varName": "phoneNo2New",
//         "type": "text",
//         "label": "New Phone No 2",
//         "grid": 5,
//         "conditionalVarName": "contactNumberChangeChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "varName": "emailAddressChangeChange",
//         "type": "checkbox",
//         "label": "Email Address  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "emailOld",
//         "type": "text",
//         "label": "Existing Email",
//         "grid": 6,
//
//     },
//     {
//         "varName": "emailNew",
//         "type": "text",
//         "label": "New Email",
//         "grid": 5,
//         "conditionalVarName": "emailAddressChangeChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         " varName": "eStatementEnrollmentChange",
//         "type": "checkbox",
//         "label": "E - Statement Enrollment Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Despatch ModeOld",
//         "type": "text",
//         "label": "Existing Despatch Mode",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Despatch ModeNew",
//         "type": "text",
//         "label": "New Despatch Mode",
//         "grid": 5,
//         "conditionalVarName": "eStatementEnrollmentChange",
//         "conditionalVarValue": "true",
//     },
//
//
//     {
//         " varName": "addressChangeChange",
//         "type": "checkbox",
//         "label": "Address   Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cityOld",
//         "type": "text",
//         "label": "Existing City",
//         "grid": 6,
//
//     },
//     {
//         "varName": "cityNew",
//         "type": "text",
//         "label": "New City",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "communicationAddress1Old",
//         "type": "text",
//         "label": "Existing Communication Address 1",
//         "grid": 6,
//
//     },
//     {
//         "varName": "communicationAddress1New",
//         "type": "text",
//         "label": "New Communication Address 1",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "communicationAddress2Old",
//         "type": "text",
//         "label": "Existing Communication Address 2",
//         "grid": 6,
//
//     },
//     {
//         "varName": "communicationAddress2New",
//         "type": "text",
//         "label": "New Communication Address 2",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "countryOld",
//         "type": "text",
//         "label": "Existing Country",
//         "grid": 6,
//
//     },
//     {
//         "varName": "countryNew",
//         "type": "text",
//         "label": "New Country",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "employerAddress1Old",
//         "type": "text",
//         "label": "Existing Employer Address 1",
//         "grid": 6,
//
//     },
//     {
//         "varName": "employerAddress1New",
//         "type": "text",
//         "label": "New Employer Address 1",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "employerAddress2Old",
//         "type": "text",
//         "label": "Existing Employer Address 2",
//         "grid": 6,
//
//     },
//     {
//         "varName": "employerAddress2New",
//         "type": "text",
//         "label": "New Employer Address 2",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//
//     // {"varName":"addressChangeChange",
//     //     "type":"checkbox",
//     //     "label":"Address   Change",
//     //     "grid":12
//     // },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "permanentAddress1Old",
//         "type": "text",
//         "label": "Existing Permanent Address 1",
//         "grid": 6,
//
//     },
//     {
//         "varName": "permanentAddress1New",
//         "type": "text",
//         "label": "New Permanent Address 1",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "permanentAddress2Old",
//         "type": "text",
//         "label": "Existing Permanent Address 2",
//         "grid": 6,
//
//     },
//     {
//         "varName": "permanentAddress2New",
//         "type": "text",
//         "label": "New Permanent Address 2",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "postalCodeOld",
//         "type": "text",
//         "label": "Existing Postal Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "postalCodeNew",
//         "type": "text",
//         "label": "New Postal Code",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "stateOld",
//         "type": "text",
//         "label": "Existing State",
//         "grid": 6,
//
//     },
//     {
//         "varName": "stateNew",
//         "type": "text",
//         "label": "New State",
//         "grid": 5,
//         "conditionalVarName": "addressChangeChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         " varName": "mandateSignatoryCbTaggingChange",
//         "type": "checkbox",
//         "label": "Mandate/Signatory CB Tagging Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "rELATIONTYPEOld",
//         "type": "text",
//         "label": "Existing RELATION TYPE ",
//         "grid": 6,
//
//     },
//     {
//         "varName": "rELATIONTYPENew",
//         "type": "text",
//         "label": "New RELATION TYPE ",
//         "grid": 5,
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "rELATIONCODEOld",
//         "type": "text",
//         "label": "Existing RELATION CODE",
//         "grid": 6,
//
//     },
//     {
//         "varName": "rELATIONCODENew",
//         "type": "text",
//         "label": "New RELATION CODE",
//         "grid": 5,
//         "conditionalVarName": "mandateSignatoryCbTaggingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "dESIGNATIONCODEOld",
//         "type": "text",
//         "label": "Existing DESIGNATION CODE",
//         "grid": 6,
//
//     },
//     {
//         "varName": "dESIGNATIONCODENew",
//         "type": "text",
//         "label": "New DESIGNATION CODE",
//         "grid": 5,
//         "conditionalVarName": "mandateSignatoryCbTaggingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cUStIDOld",
//         "type": "text",
//         "label": "Existing CUST. ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "cUSTIDNew",
//         "type": "text",
//         "label": "New CUST. ID",
//         "grid": 5,
//         "conditionalVarName": "mandateSignatoryCbTaggingChange",
//         "conditionalVarValue": "true",
//     },
//
//
//     {
//         "varName": "spouseNameUpdateChange",
//         "type": "checkbox",
//         "label": "Other Information  Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//
//
//     {
//         "varName": "dobOld",
//         "type": "text",
//         "label": "Existing DOB",
//         "grid": 6,
//
//     },
//     {
//         "varName": "dobNew",
//         "type": "text",
//         "label": "New DOB",
//         "grid": 5,
//         "conditionalVarName": "dobUpdateChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "fatherOld",
//         "type": "text",
//         "label": "Existing Father Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "fatherNew",
//         "type": "text",
//         "label": "New Father Name",
//         "grid": 5,
//         "conditionalVarName": "fatherNameUpdateChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "motherOld",
//         "type": "text",
//         "label": "Existing Mother Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "motherNew",
//         "type": "text",
//         "label": "New Mother Name",
//         "grid": 5,
//         "conditionalVarName": "motherNameUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "spouseOld",
//         "type": "text",
//         "label": "Existing Spouse Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "spouseNew",
//         "type": "text",
//         "label": "New Spouse Name",
//         "grid": 5,
//         "conditionalVarName": "spouseNameUpdateChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "professionOld",
//         "type": "text",
//         "label": "Existing Profession",
//         "grid": 6,
//
//     },
//     {
//         "varName": "professionNew",
//         "type": "text",
//         "label": "New Profession",
//         "grid": 5,
//         // "conditionalVarName":"fatherNameUpdateChange",
//         // "conditionalVarValue":"true",
//     },
//
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "employerOld",
//         "type": "text",
//         "label": "Existing Employer Name",
//         "grid": 6,
//
//     },
//     {
//         "varName": "employerNew",
//         "type": "text",
//         "label": "New Employer Name",
//         "grid": 5,
//
//     },
//     {
//         "varName": "signatureCard",
//         "type": "checkbox",
//         "label": "Signature Card",
//         "grid": 12,
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "signatureCardExisting",
//         "type": "text",
//         "label": "Existing Signature Card",
//         "grid": 6,
//
//         "conditionalVarName": "signatureCard",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "signatureCardNew",
//         "type": "text",
//         "label": "New Signature Card",
//         "grid": 5,
//         "conditionalVarName": "signatureCard",
//         "conditionalVarValue": true,
//     },
//     {
//         " varName": "dormantActivitionChange",
//         "type": "checkbox",
//         "label": "Dormant Activition Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Account StatusOld",
//         "type": "text",
//         "label": "Existing Account Status",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Account StatusNew",
//         "type": "text",
//         "label": "New Account Status",
//         "grid": 5,
//         "conditionalVarName": "dormantActivitionChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "varName": "dormantAccountDataUpdate",
//         "type": "checkbox",
//         "label": "Dormant Account Data Update",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "dormantAccountDataUpdateExisting",
//         "type": "text",
//         "label": "Existing Dormant Account Data",
//         "grid": 6,
//
//         "conditionalVarName": "dormantAccountDataUpdate",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "dormantAccountDataUpdateNew",
//         "type": "text",
//         "label": "New Dormant Account Data",
//         "grid": 5,
//         "conditionalVarName": "dormantAccountDataUpdate",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "schemeMaintenanceLinkChange",
//         "type": "checkbox",
//         "label": "Scheme Maintenance-Link A/C Change",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "schemeACNumberChangeExisting",
//         "type": "text",
//         "label": "Existing Scheme A/C number",
//         "grid": 6,
//
//
//     },
//     {
//         "varName": "schemeACNumberChangeNew",
//         "type": "text",
//         "label": "New Scheme A/C number",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "schemeStartDateExisting",
//         "type": "text",
//         "label": "Existing Start Date",
//         "grid": 6,
//
//
//     },
//     {
//         "varName": "schemeStartDateNew",
//         "type": "text",
//         "label": "New Start Date",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "schemeEndDateExisting",
//         "type": "text",
//         "label": "Existing End Date",
//         "grid": 6,
//
//
//     },
//     {
//         "varName": "schemeEndDateNew",
//         "type": "text",
//         "label": "New End Date",
//         "grid": 5,
//
//     },
//     {
//         "varName": "schemeMaintenance",
//         "type": "checkbox",
//         "label": "Scheme Maintenance - Installment Regularization",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentTranIDExisting",
//         "type": "text",
//         "label": "Existing Tran ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentTranIDNew",
//         "type": "text",
//         "label": "New Tran ID",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentCasaExisting",
//         "type": "text",
//         "label": "Existing CASA A/C Number (Debit)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentCasaNew",
//         "type": "text",
//         "label": "New CASA A/C Number (Debit)",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentSchemeExisting",
//         "type": "text",
//         "label": "Existing Scheme A/C Number (Credit)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentSchemeNew",
//         "type": "text",
//         "label": "New Scheme A/C Number (Credit)",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentAmountExisting",
//         "type": "text",
//         "label": "Existing Amount",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentAmountNew",
//         "type": "text",
//         "label": "New Amount",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentParticularCodeExisting",
//         "type": "text",
//         "label": "Existing Particular Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentParticularCodeNew",
//         "type": "text",
//         "label": "New Particular Code",
//         "grid": 5,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "installmentTransationCodeExisting",
//         "type": "text",
//         "label": "Existing Enter Transation Particulars",
//         "grid": 6,
//
//     },
//     {
//         "varName": "installmentTransationCodeNew",
//         "type": "text",
//         "label": "New Enter Transation Particulars",
//         "grid": 5,
//
//     },
//     {
//         "varName": "cityLive",
//         "type": "checkbox",
//         "label": "City Live",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cityLiveExisting",
//         "type": "text",
//         "label": "Existing City Live",
//         "grid": 6,
//
//         "conditionalVarName": "cityLive",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "cityLiveNew",
//         "type": "text",
//         "label": "New City Live",
//         "grid": 5,
//         "conditionalVarName": "cityLive",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "projectRelatedDataUpdateADUP",
//         "type": "checkbox",
//         "label": "Project Related Data Update ADUP, CIB",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "aDUPExisting",
//         "type": "text",
//         "label": "Existing ADUP Data",
//         "grid": 6,
//
//         "conditionalVarName": "projectRelatedDataUpdateADUP",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "aDUPNew",
//         "type": "text",
//         "label": "New Project ADUP Data",
//         "grid": 5,
//         "conditionalVarName": "projectRelatedDataUpdateADUP",
//         "conditionalVarValue": true,
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         " varName": "ctrLimitUpdateUnconfirmedAddressChange",
//         "type": "checkbox",
//         "label": "CTR Limit Update/Unconfirmed Address Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Cash Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Cash Limit (DR/CR)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Cash Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Cash Limit (DR/CR)",
//         "grid": 5,
//         "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
//         "conditionalVarValue": "true",
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "cIBExisting",
//         "type": "text",
//         "label": "Existing CIB Data",
//         "grid": 6,
//
//         "conditionalVarName": "projectRelatedDataUpdateADUP",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "cIBNew",
//         "type": "text",
//         "label": "New CIB Data",
//         "grid": 5,
//         "conditionalVarName": "projectRelatedDataUpdateADUP",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "accountSchemeClose",
//         "type": "checkbox",
//         "label": "Account & Scheme Close",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "accountExisting",
//         "type": "text",
//         "label": "Existing Account Number",
//         "grid": 6,
//
//         "conditionalVarName": "accountSchemeClose",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "accountNew",
//         "type": "text",
//         "label": "New Account Number",
//         "grid": 5,
//         "conditionalVarName": "accountSchemeClose",
//         "conditionalVarValue": true,
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "schemeExisting",
//         "type": "text",
//         "label": "Existing Scheme Number",
//         "grid": 6,
//
//         "conditionalVarName": "accountSchemeClose",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "schemeNew",
//         "type": "text",
//         "label": "New Scheme Number",
//         "grid": 5,
//         "conditionalVarName": "accountSchemeClose",
//         "conditionalVarValue": true,
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "lockerSIOpen",
//         "type": "checkbox",
//         "label": "Locker SI Open",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "lockerSIOpenExisting",
//         "type": "text",
//         "label": "Existing Locker SI Open",
//         "grid": 6,
//
//         "conditionalVarName": "lockerSIOpen",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "lockerSIOpenNew",
//         "type": "text",
//         "label": "New Locker SI Open",
//         "grid": 5,
//         "conditionalVarName": "lockerSIOpen",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "lockerSIClose",
//         "type": "checkbox",
//         "label": "Locker SI Close",
//         "grid": 12,
//
//     },
//     {
//
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "lockerSICloseExisting",
//         "type": "text",
//         "label": "Existing Locker SI Close",
//         "grid": 6,
//
//         "conditionalVarName": "lockerSIClose",
//         "conditionalVarValue": true,
//     },
//     {
//         "varName": "lockerSICloseNew",
//         "type": "text",
//         "label": "New Locker SI Close",
//         "grid": 5,
//         "conditionalVarName": "lockerSIClose",
//         "conditionalVarValue": true,
//     },
//
//     {
//         "varName": "tradeFacilitationChange",
//         "type": "checkbox",
//         "label": "Trade Facilitation Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "tradeOld",
//         "type": "text",
//         "label": "Existing Trade",
//         "grid": 6,
//
//     },
//     {
//         "varName": "tradeNew",
//         "type": "text",
//         "label": "New Trade",
//         "grid": 5,
//
//     },
//     {
//         "varName": "staffAccountGeneralizationChange",
//         "type": "checkbox",
//         "label": "Staff Account Generalization Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "staffFlagOld",
//         "type": "text",
//         "label": "Existing Staff Flag",
//         "grid": 6,
//
//     },
//     {
//         "varName": "staffFlagNew",
//         "type": "text",
//         "label": "New Staff Flag",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "staffNumberOld",
//         "type": "text",
//         "label": "Existing Staff Number",
//         "grid": 6,
//
//     },
//     {
//         "varName": "staffNumberNew",
//         "type": "text",
//         "label": "New Staff Number",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 6,
//
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C Id",
//         "grid": 6,
//
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C Id",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "targetSchemeCodeOld",
//         "type": "text",
//         "label": "Existing Target Scheme Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "targetSchemeCodeNew",
//         "type": "text",
//         "label": "New Target Scheme Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "trialModeOld",
//         "type": "text",
//         "label": "Existing Trial Mode",
//         "grid": 6,
//
//     },
//     {
//         "varName": "trialModeNew",
//         "type": "text",
//         "label": "New Trial Mode",
//         "grid": 5,
//
//     },
//
//     {
//         " varName": "freezeUnfreezeMarkChange",
//         "type": "checkbox",
//         "label": "Freeze/Unfreeze Mark Change",
//         "grid": 12,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 6,
//
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//         "conditionalVarName": "freezeUnfreezeMarkChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C ID",
//         "grid": 5,
//         "conditionalVarName": "freezeUnfreezeMarkChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "customerIdOld",
//         "type": "text",
//         "label": "Existing Customer ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "customerIdNew",
//         "type": "text",
//         "label": "New Customer ID",
//         "grid": 5,
//         "conditionalVarName": "freezeUnfreezeMarkChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "freezeReasonCodeOld",
//         "type": "text",
//         "label": "Existing Freeze Reason Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "freezeReasonCodeNew",
//         "type": "text",
//         "label": "New Freeze Reason Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "freezeCodeOld",
//         "type": "text",
//         "label": "Existing Freeze  Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "freezeCodeNew",
//         "type": "text",
//         "label": "New Freeze  Code",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "freezeRemarksOld",
//         "type": "text",
//         "label": "Existing Freeze  Remarks",
//         "grid": 6,
//
//     },
//     {
//         "varName": "freezeRemarksNew",
//         "type": "text",
//         "label": "New Freeze  Remarks",
//         "grid": 5,
//
//     },
//     {
//         " varName": "cityTouchTaggingChange",
//         "type": "checkbox",
//         "label": "City Touch Tagging Change",
//         "grid": 12,
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "functionOld",
//         "type": "text",
//         "label": "Existing Function",
//         "grid": 6,
//
//     },
//     {
//         "varName": "functionNew",
//         "type": "text",
//         "label": "New Function",
//         "grid": 5,
//
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "acIdOld",
//         "type": "text",
//         "label": "Existing A/C ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "acIdNew",
//         "type": "text",
//         "label": "New A/C ID",
//         "grid": 5,
//         "conditionalVarName": "cityTouchTaggingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "customerIdOld",
//         "type": "text",
//         "label": "Existing Customer ID",
//         "grid": 6,
//
//     },
//     {
//         "varName": "customerIdNew",
//         "type": "text",
//         "label": "New Customer ID",
//         "grid": 5,
//         "conditionalVarName": "cityTouchTaggingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "accountNameModificationOld",
//         "type": "text",
//         "label": "Existing Account Name Modification (Y/N)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "accountNameModificationNew",
//         "type": "text",
//         "label": "New Account Name Modification (Y/N)",
//         "grid": 5,
//         "conditionalVarName": "cityTouchTaggingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "varName": "priorityMarkingChange",
//         "type": "checkbox",
//         "label": "Priority Marking Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "occupationCodeOld",
//         "type": "text",
//         "label": "Existing Occupation Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "occupationCodeNew",
//         "type": "text",
//         "label": "New Occupation Code",
//         "grid": 5,
//         "conditionalVarName": "priorityMarkingChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         " varName": "unconfirmedAddressMarkedOrUnmarkedChange",
//         "type": "checkbox",
//         "label": "Unconfirmed AddressMarked Or Unmarked Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Clearing Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Clearing Limit (DR/CR)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Clearing Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Clearing Limit (DR/CR)",
//         "grid": 5,
//         "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
//         "conditionalVarValue": "true",
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Transfer Limit (DR/CR)Old",
//         "type": "text",
//         "label": "Existing Transfer Limit (DR/CR)",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Transfer Limit (DR/CR)New",
//         "type": "text",
//         "label": "New Transfer Limit (DR/CR)",
//         "grid": 5,
//         "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
//         "conditionalVarValue": "true",
//     },
//
//     {
//         " varName": "sbsCodeUpdateCorrectionChange",
//         "type": "checkbox",
//         "label": "SBS Code Update/Correction Change",
//         "grid": 12,
//         "conditional" : true,
//         "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
//         "conditionalVarValue" : true
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//         "conditional" : true,
//         "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
//         "conditionalVarValue" : true
//     },
//     {
//         "varName": "Sector CodeOld",
//         "type": "text",
//         "label": "Existing Sector Code",
//         "grid": 6,
//         "conditional" : true,
//         "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
//         "conditionalVarValue" : true
//
//     },
//     {
//         "varName": "Sector CodeNew",
//         "type": "text",
//         "label": "New Sector Code",
//         "grid": 5,
//         "conditional" : true,
//         "conditionalVarName" : "sbsCodeUpdateCorrectionChange",
//         "conditionalVarValue" : true
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Sub Sector CodeOld",
//         "type": "text",
//         "label": "Existing Sub Sector Code",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Sub Sector CodeNew",
//         "type": "text",
//         "label": "New Sub Sector Code",
//         "grid": 5,
//         "conditionalVarName": "sbsCodeUpdateCorrectionChange",
//         "conditionalVarValue": "true",
//     },
//
//
//     {
//         " varName": "relationshipManagerRmCodeChangeUpdateChange",
//         "type": "checkbox",
//         "label": "Relationship Manager (RM) Code Change/Update Change",
//         "grid": 12
//     },
//     {
//         "type": "blank",
//         "grid": 1,
//     },
//     {
//         "varName": "Free Code 3 Old",
//         "type": "text",
//         "label": "Existing Free Code 3 ",
//         "grid": 6,
//
//     },
//     {
//         "varName": "Free Code 3 New",
//         "type": "text",
//         "label": "New Free Code 3 ",
//         "grid": 5,
//
//     },
//
// ]

const maintenanceListMaker = [
    {
        "varName": "12digitTinChange",
        "type": "checkbox",
        "label": "12-Digit TIN Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,


    },
    {
        "varName": "panGirNoTinUpdateOld",
        "type": "text",
        "label": "Existing TIN",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "12digitTinChange",
        "conditionalVarValue": true


    },
    {
        "varName": "panGirNoTinUpdateNew",
        "type": "text",
        "label": "New TIN",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "12digitTinChange",
        "conditionalVarValue": true

    },
    {
        "varName": "titleChangeRectificationChange",
        "type": "checkbox",
        "label": "Title Rectification Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerNameOld",
        "type": "text",
        "label": "Existing Customer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerNameNew",
        "type": "text",
        "label": "New Customer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "titleOld",
        "type": "text",
        "label": "Existing Title",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "titleNew",
        "type": "text",
        "label": "New Title",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "titleChangeRectificationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeUpdateChange",
        "type": "checkbox",
        "label": "Nominee Update Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dOBOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dOBNew",
        "type": "date",
        "label": "New DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postal CodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postal CodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeNameOld",
        "type": "text",
        "label": "Existing Nominee Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeNameNew",
        "type": "text",
        "label": "New Nominee Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "relationshipOld",
        "type": "text",
        "label": "Existing Relationship",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "relationshipNew",
        "type": "text",
        "label": "New Relationship",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address1Old",
        "type": "text",
        "label": "Existing Address 1 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address1New",
        "type": "text",
        "label": "New Address 1 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address2Old",
        "type": "text",
        "label": "Existing Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "address2New",
        "type": "text",
        "label": "New Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityCodeOld",
        "type": "text",
        "label": "Existing City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityCodeNew",
        "type": "text",
        "label": "New City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeOld",
        "type": "text",
        "label": "Existing State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeNew",
        "type": "text",
        "label": "New State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "regNoOld",
        "type": "text",
        "label": "Existing Reg No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "regNoNew",
        "type": "text",
        "label": "New Reg No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeMinorOld",
        "type": "text",
        "label": "Existing Nominee Minor",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "nomineeMinorNew",
        "type": "text",
        "label": "New Nominee Minor",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "nomineeUpdateChange",
        "conditionalVarValue": true
    },

    {
        "varName": "guardianUpdate",
        "type": "checkbox",
        "label": "Guardian Update",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "countryGuardianOld",
        "type": "text",
        "label": "Guardian Existing Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "countryGuardianNew",
        "type": "text",
        "label": "Guardian New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "postalGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "postalGuardianCodeNew",
        "type": "text",
        "label": "Guardian New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "cityGuardianCodeOld",
        "type": "text",
        "label": "Guardian Existing City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "CityGuardianCodeNew",
        "type": "text",
        "label": "Guardian New City Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "stateGuardianCodeOld",
        "type": "text",
        "label": "Guardian's Existing State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "stateCodeGuardianNew",
        "type": "text",
        "label": "Guardian's New State Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianNameOld",
        "type": "text",
        "label": "Existing Guardian's Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianNameNew",
        "type": "text",
        "label": "New Guardian's Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianCodeOld",
        "type": "text",
        "label": "Existing Guardian Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "guardianCodeNew",
        "type": "text",
        "label": "New Guardian Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "addressGuardianOld",
        "type": "text",
        "label": "Guardian Existing Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "addressGuardianNew",
        "type": "text",
        "label": "Guardian New Address",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "guardianUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "updatechangePhotoIdChange",
        "type": "checkbox",
        "label": "Photo Id Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
    },
    {
        "varName": "expiryDateOld",
        "type": "text",
        "label": "Existing Expiry Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
    },
    {
        "varName": "expiryDateNew",
        "type": "text",
        "label": "New Expiry Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "issueDateOld",
        "type": "text",
        "label": "Existing Issue Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,

    },
    {
        "varName": "issueDateNew",
        "type": "text",
        "label": "New Issue Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "nationalIdCardOld",
        "type": "text",
        "label": "Existing National Id Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "nationalIdCardNew",
        "type": "text",
        "label": "New National Id Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

    },
    {
        "varName": "passportDetailsOld",
        "type": "text",
        "label": "Existing Passport Details",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportDetailsNew",
        "type": "text",
        "label": "New Passport Details",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportNoOld",
        "type": "text",
        "label": "Existing Passport No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "varName": "passportNoNew",
        "type": "text",
        "label": "New Passport No",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true,
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "updatechangePhotoIdChange",
        "conditionalVarValue": true
        ,
    },
    {
        "varName": "contactNumberChangeChange",
        "type": "checkbox",
        "label": "Contact Number  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo1Old",
        "type": "text",
        "label": "Existing Phone No 1 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo1New",
        "type": "text",
        "label": "New Phone No 1 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo2Old",
        "type": "text",
        "label": "Existing Phone No 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "phoneNo2New",
        "type": "text",
        "label": "New Phone No 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "contactNumberChangeChange",
        "conditionalVarValue": true
    },

    {
        "varName": "emailAddressChangeChange",
        "type": "checkbox",
        "label": "Email Address  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "emailOld",
        "type": "text",
        "label": "Existing Email",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "emailNew",
        "type": "text",
        "label": "New Email",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "emailAddressChangeChange",
        "conditionalVarValue": true
    },

    {
        "varName": "eStatementEnrollmentChange",
        "type": "checkbox",
        "label": "E - Statement Enrollment Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Despatch ModeOld",
        "type": "text",
        "label": "Existing Despatch Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Despatch ModeNew",
        "type": "text",
        "label": "New Despatch Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "eStatementEnrollmentChange",
        "conditionalVarValue": true
    },


    {
        "varName": "addressChangeChange",
        "type": "checkbox",
        "label": "Address   Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityOld",
        "type": "text",
        "label": "Existing City",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityNew",
        "type": "text",
        "label": "New City",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress1Old",
        "type": "text",
        "label": "Existing Communication Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress1New",
        "type": "text",
        "label": "New Communication Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress2Old",
        "type": "text",
        "label": "Existing Communication Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "communicationAddress2New",
        "type": "text",
        "label": "New Communication Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryOld",
        "type": "text",
        "label": "Existing Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "countryNew",
        "type": "text",
        "label": "New Country",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress1Old",
        "type": "text",
        "label": "Existing Employer Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress1New",
        "type": "text",
        "label": "New Employer Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress2Old",
        "type": "text",
        "label": "Existing Employer Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerAddress2New",
        "type": "text",
        "label": "New Employer Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },

    // {"varName":"addressChangeChange",
    //     "type":"checkbox",
    //     "label":"Address   Change",
    //     "grid":12
    // },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress1Old",
        "type": "text",
        "label": "Existing Permanent Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress1New",
        "type": "text",
        "label": "New Permanent Address 1",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress2Old",
        "type": "text",
        "label": "Existing Permanent Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "permanentAddress2New",
        "type": "text",
        "label": "New Permanent Address 2",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeOld",
        "type": "text",
        "label": "Existing Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "postalCodeNew",
        "type": "text",
        "label": "New Postal Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateOld",
        "type": "text",
        "label": "Existing State",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "stateNew",
        "type": "text",
        "label": "New State",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "addressChangeChange",
        "conditionalVarValue": true
    },
    {
        "varName": "mandateSignatoryCbTaggingChange",
        "type": "checkbox",
        "label": "Mandate/Signatory CB Tagging Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONTYPEOld",
        "type": "text",
        "label": "Existing RELATION TYPE ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONTYPENew",
        "type": "text",
        "label": "New RELATION TYPE ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONCODEOld",
        "type": "text",
        "label": "Existing RELATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "rELATIONCODENew",
        "type": "text",
        "label": "New RELATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dESIGNATIONCODEOld",
        "type": "text",
        "label": "Existing DESIGNATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dESIGNATIONCODENew",
        "type": "text",
        "label": "New DESIGNATION CODE",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cUStIDOld",
        "type": "text",
        "label": "Existing CUST. ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cUSTIDNew",
        "type": "text",
        "label": "New CUST. ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "mandateSignatoryCbTaggingChange",
        "conditionalVarValue": true
    },


    {
        "varName": "spouseNameUpdateChange",
        "type": "checkbox",
        "label": "Other Information  Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },


    {
        "varName": "dobOld",
        "type": "text",
        "label": "Existing DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dobNew",
        "type": "text",
        "label": "New DOB",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "fatherOld",
        "type": "text",
        "label": "Existing Father Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "fatherNew",
        "type": "text",
        "label": "New Father Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "motherOld",
        "type": "text",
        "label": "Existing Mother Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "motherNew",
        "type": "text",
        "label": "New Mother Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "spouseOld",
        "type": "text",
        "label": "Existing Spouse Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "spouseNew",
        "type": "text",
        "label": "New Spouse Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "professionOld",
        "type": "text",
        "label": "Existing Profession",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "professionNew",
        "type": "text",
        "label": "New Profession",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },

    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerOld",
        "type": "text",
        "label": "Existing Employer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "employerNew",
        "type": "text",
        "label": "New Employer Name",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "spouseNameUpdateChange",
        "conditionalVarValue": true

    },
    {
        "varName": "signatureCard",
        "type": "checkbox",
        "label": "Signature Card",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "signatureCardExisting",
        "type": "text",
        "label": "Existing Signature Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "signatureCardNew",
        "type": "text",
        "label": "New Signature Card",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "signatureCard",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantActivitionChange",
        "type": "checkbox",
        "label": "Dormant Activition Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Account StatusOld",
        "type": "text",
        "label": "Existing Account Status",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Account StatusNew",
        "type": "text",
        "label": "New Account Status",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantActivitionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdate",
        "type": "checkbox",
        "label": "Dormant Account Data Update",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdateExisting",
        "type": "text",
        "label": "Existing Dormant Account Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "dormantAccountDataUpdateNew",
        "type": "text",
        "label": "New Dormant Account Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "dormantAccountDataUpdate",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeMaintenanceLinkChange",
        "type": "checkbox",
        "label": "Scheme Maintenance-Link A/C Change",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeACNumberChangeExisting",
        "type": "text",
        "label": "Existing Scheme A/C number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeACNumberChangeNew",
        "type": "text",
        "label": "New Scheme A/C number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeStartDateExisting",
        "type": "text",
        "label": "Existing Start Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeStartDateNew",
        "type": "text",
        "label": "New Start Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "schemeEndDateExisting",
        "type": "text",
        "label": "Existing End Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeEndDateNew",
        "type": "text",
        "label": "New End Date",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenanceLinkChange",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeMaintenance",
        "type": "checkbox",
        "label": "Scheme Maintenance - Installment Regularization",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTranIDExisting",
        "type": "text",
        "label": "Existing Tran ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTranIDNew",
        "type": "text",
        "label": "New Tran ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentCasaExisting",
        "type": "text",
        "label": "Existing CASA A/C Number (Debit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentCasaNew",
        "type": "text",
        "label": "New CASA A/C Number (Debit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentSchemeExisting",
        "type": "text",
        "label": "Existing Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentSchemeNew",
        "type": "text",
        "label": "New Scheme A/C Number (Credit)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentAmountExisting",
        "type": "text",
        "label": "Existing Amount",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentAmountNew",
        "type": "text",
        "label": "New Amount",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentParticularCodeExisting",
        "type": "text",
        "label": "Existing Particular Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentParticularCodeNew",
        "type": "text",
        "label": "New Particular Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTransationCodeExisting",
        "type": "text",
        "label": "Existing Enter Transation Particulars",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true
    },
    {
        "varName": "installmentTransationCodeNew",
        "type": "text",
        "label": "New Enter Transation Particulars",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "schemeMaintenance",
        "conditionalVarValue": true

    },
    {
        "varName": "cityLive",
        "type": "checkbox",
        "label": "City Live",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "cityLiveExisting",
        "type": "text",
        "label": "Existing City Live",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "cityLiveNew",
        "type": "text",
        "label": "New City Live",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityLive",
        "conditionalVarValue": true
    },
    {
        "varName": "projectRelatedDataUpdateADUP",
        "type": "checkbox",
        "label": "Project Related Data Update ADUP, CIB",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "aDUPExisting",
        "type": "text",
        "label": "Existing ADUP Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "aDUPNew",
        "type": "text",
        "label": "New Project ADUP Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "projectRelatedDataUpdateADUP",
        "conditionalVarValue": true
    },
    {
        "varName": "ctrLimitUpdateUnconfirmedAddressChange",
        "type": "checkbox",
        "label": "CTR Limit Update/Unconfirmed Address Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Cash Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Cash Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Cash Limit (DR/CR)New",
        "type": "text",
        "label": "New Cash Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cIBExisting",
        "type": "text",
        "label": "Existing CIB Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cIBNew",
        "type": "text",
        "label": "New CIB Data",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "ctrLimitUpdateUnconfirmedAddressChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountSchemeClose",
        "type": "checkbox",
        "label": "Account & Scheme Close",
        "grid": 12,

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "accountExisting",
        "type": "text",
        "label": "Existing Account Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "accountNew",
        "type": "text",
        "label": "New Account Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeExisting",
        "type": "text",
        "label": "Existing Scheme Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "schemeNew",
        "type": "text",
        "label": "New Scheme Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "accountSchemeClose",
        "conditionalVarValue": true

    },
    {
        "varName": "lockerSIOpen",
        "type": "checkbox",
        "label": "Locker SI Open",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIOpenExisting",
        "type": "text",
        "label": "Existing Locker SI Open",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIOpenNew",
        "type": "text",
        "label": "New Locker SI Open",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIOpen",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSIClose",
        "type": "checkbox",
        "label": "Locker SI Close",
        "grid": 12,


    },
    {

        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSICloseExisting",
        "type": "text",
        "label": "Existing Locker SI Close",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },
    {
        "varName": "lockerSICloseNew",
        "type": "text",
        "label": "New Locker SI Close",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "lockerSIClose",
        "conditionalVarValue": true
    },

    {
        "varName": "tradeFacilitationChange",
        "type": "checkbox",
        "label": "Trade Facilitation Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "tradeOld",
        "type": "text",
        "label": "Existing Trade",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "tradeNew",
        "type": "text",
        "label": "New Trade",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "tradeFacilitationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffAccountGeneralizationChange",
        "type": "checkbox",
        "label": "Staff Account Generalization Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffFlagOld",
        "type": "text",
        "label": "Existing Staff Flag",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffFlagNew",
        "type": "text",
        "label": "New Staff Flag",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffNumberOld",
        "type": "text",
        "label": "Existing Staff Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "staffNumberNew",
        "type": "text",
        "label": "New Staff Number",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C Id",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C Id",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "targetSchemeCodeOld",
        "type": "text",
        "label": "Existing Target Scheme Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "targetSchemeCodeNew",
        "type": "text",
        "label": "New Target Scheme Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "trialModeOld",
        "type": "text",
        "label": "Existing Trial Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },
    {
        "varName": "trialModeNew",
        "type": "text",
        "label": "New Trial Mode",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "staffAccountGeneralizationChange",
        "conditionalVarValue": true
    },

    {
        "varName": "freezeUnfreezeMarkChange",
        "type": "checkbox",
        "label": "Freeze/Unfreeze Mark Change",
        "grid": 12,


    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeReasonCodeOld",
        "type": "text",
        "label": "Existing Freeze Reason Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeReasonCodeNew",
        "type": "text",
        "label": "New Freeze Reason Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeCodeOld",
        "type": "text",
        "label": "Existing Freeze  Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeCodeNew",
        "type": "text",
        "label": "New Freeze  Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeRemarksOld",
        "type": "text",
        "label": "Existing Freeze  Remarks",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "freezeRemarksNew",
        "type": "text",
        "label": "New Freeze  Remarks",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "freezeUnfreezeMarkChange",
        "conditionalVarValue": true
    },
    {
        "varName": "cityTouchTaggingChange",
        "type": "checkbox",
        "label": "City Touch Tagging Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionOld",
        "type": "text",
        "label": "Existing Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "functionNew",
        "type": "text",
        "label": "New Function",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdOld",
        "type": "text",
        "label": "Existing A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "acIdNew",
        "type": "text",
        "label": "New A/C ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdOld",
        "type": "text",
        "label": "Existing Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "customerIdNew",
        "type": "text",
        "label": "New Customer ID",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountNameModificationOld",
        "type": "text",
        "label": "Existing Account Name Modification (Y/N)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "accountNameModificationNew",
        "type": "text",
        "label": "New Account Name Modification (Y/N)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "cityTouchTaggingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "priorityMarkingChange",
        "type": "checkbox",
        "label": "Priority Marking Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "occupationCodeOld",
        "type": "text",
        "label": "Existing Occupation Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "occupationCodeNew",
        "type": "text",
        "label": "New Occupation Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "priorityMarkingChange",
        "conditionalVarValue": true
    },
    {
        "varName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "type": "checkbox",
        "label": "Unconfirmed AddressMarked Or Unmarked Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Clearing Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Clearing Limit (DR/CR)New",
        "type": "text",
        "label": "New Clearing Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Transfer Limit (DR/CR)Old",
        "type": "text",
        "label": "Existing Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Transfer Limit (DR/CR)New",
        "type": "text",
        "label": "New Transfer Limit (DR/CR)",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "unconfirmedAddressMarkedOrUnmarkedChange",
        "conditionalVarValue": true
    },

    {
        "varName": "sbsCodeUpdateCorrectionChange",
        "type": "checkbox",
        "label": "SBS Code Update/Correction Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Sector CodeOld",
        "type": "text",
        "label": "Existing Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Sector CodeNew",
        "type": "text",
        "label": "New Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Sub Sector CodeOld",
        "type": "text",
        "label": "Existing Sub Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Sub Sector CodeNew",
        "type": "text",
        "label": "New Sub Sector Code",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "sbsCodeUpdateCorrectionChange",
        "conditionalVarValue": true
    },


    {
        "varName": "relationshipManagerRmCodeChangeUpdateChange",
        "type": "checkbox",
        "label": "Relationship Manager (RM) Code Change/Update Change",
        "grid": 12,

    },
    {
        "type": "blank",
        "grid": 1,

        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Free Code 3 Old",
        "type": "text",
        "label": "Existing Free Code 3 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },
    {
        "varName": "Free Code 3 New",
        "type": "text",
        "label": "New Free Code 3 ",
        "grid": 5,
        "conditional": true,
        "conditionalVarName": "relationshipManagerRmCodeChangeUpdateChange",
        "conditionalVarValue": true
    },

]
let MAKERAccountMaintenance = {};
MAKERAccountMaintenance = JSON.parse(JSON.stringify(maintenanceListMaker));

let CheckerAccountMaintenance = {};
CheckerAccountMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(maintenanceListChecker)));


//////fdr maintenance//////

const fDRMaintenanceList = [
    {
        "varName": "fDRMTDREncashment",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment",
        "grid": 6,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },
    {
        "varName": "fDDPSFaceValueRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },


    {
        "varName": "fDRInterestPayment",
        "type": "checkbox",
        "label": "FDR Interest Payment",
        "grid": 6,


    },
    {
        "varName": "fDRInterestPaymentRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "linkACChange",
        "type": "checkbox",
        "label": "Link A/C Change",
        "grid": 6,

    },
    {
        "varName": "linkACChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "fDRMTDREncashmentCertificate",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment Certificate",
        "grid": 6,
    },
    {
        "varName": "fDRMTDREncashmentCertificateRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "tenorChange",
        "type": "checkbox",
        "label": "Tenor Change",
        "grid": 6,
    },
    {
        "varName": "tenorChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },
    {
        "varName": "schemeChange",
        "type": "checkbox",
        "label": "Scheme Change",
        "grid": 6,

    },
    {
        "varName": "schemeChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

    {
        "varName": "duplicateAdviceChange",
        "type": "checkbox",
        "label": "Duplicate Advice Change",
        "grid": 6,
    },
    {
        "varName": "duplicateAdviceChangeRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },

    {
        "varName": "sourceTaxReversal",
        "type": "checkbox",
        "label": "Source Tax Reversal",
        "grid": 6,

    },
    {
        "varName": "sourceTaxReversalRemarks",
        "type": "text",
        "label": "Remarks",
        "grid": 6,

    },


]
let BOMFdrMaintenance = {};
BOMFdrMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(fDRMaintenanceList)));


let BMFdrMaintenance = {};
BMFdrMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(fDRMaintenanceList)));

let CALLFdrMaintenance = {};
CALLFdrMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(fDRMaintenanceList)));


// last update 11-11-19
const fDRMaintenanceListChecker =[
    {"varName":"fDNomineeUpdateChange",
        "type":"checkbox",
        "label":"FD Nominee Change",
        "grid":12,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"relationshipOld",
        "type":"text",
        "label":"Existing Relationship",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"relationshipNew",
        "type":"text",
        "label":"New Relationship",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineesNameOld",
        "type":"text",
        "label":"Existing Nominees Name",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineesNameNew",
        "type":"text",
        "label":"New Nominees Name",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"regNoOld",
        "type":"text",
        "label":"Existing Reg No",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"regNoNew",
        "type":"text",
        "label":"New Reg No",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"address1Old",
        "type":"text",
        "label":"Existing Address 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"address1New",
        "type":"text",
        "label":"New Address 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityCode1Old",
        "type":"text",
        "label":"Existing City Code 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityCode1New",
        "type":"text",
        "label":"New City Code 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateCode1Old",
        "type":"text",
        "label":"Existing State Code 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateCode1New",
        "type":"text",
        "label":"New State Code 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"countryCodeOld",
        "type":"text",
        "label":"Existing Country Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"countryCodeNew",
        "type":"text",
        "label":"New Country Code",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"postalCode1Old",
        "type":"text",
        "label":"Existing Postal Code 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"postalCode1New",
        "type":"text",
        "label":"New Postal Code 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acNoOld",
        "type":"text",
        "label":"Existing Ac No",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acNoNew",
        "type":"text",
        "label":"New Ac No",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"name1Old",
        "type":"text",
        "label":"Existing Name 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"name1New",
        "type":"text",
        "label":"New Name 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineeAge1Old",
        "type":"text",
        "label":"Existing Nominee Age 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineeAge1New",
        "type":"text",
        "label":"New Nominee Age 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomShare1Old",
        "type":"text",
        "label":"Existing Nom Share 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomShare1New",
        "type":"text",
        "label":"New Nom Share 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomRelationList1Old",
        "type":"text",
        "label":"Existing Nom Relation List 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomRelationList1New",
        "type":"text",
        "label":"New Nom Relation List 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"address3Old",
        "type":"text",
        "label":"Existing Address 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"address3New",
        "type":"text",
        "label":"New Address 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityList1Old",
        "type":"text",
        "label":"Existing City List 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityList1New",
        "type":"text",
        "label":"New City List 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateList1Old",
        "type":"text",
        "label":"Existing State List 1",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateList1New",
        "type":"text",
        "label":"New State List 1",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"name2Old",
        "type":"text",
        "label":"Existing Name 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"name2New",
        "type":"text",
        "label":"New Name 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineeAge2Old",
        "type":"text",
        "label":"Existing Nominee Age 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineeAge2New",
        "type":"text",
        "label":"New Nominee Age 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomShare2Old",
        "type":"text",
        "label":"Existing Nom Share 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomShare2New",
        "type":"text",
        "label":"New Nom Share 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomRelationList2Old",
        "type":"text",
        "label":"Existing Nom Relation List 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomRelationList2New",
        "type":"text",
        "label":"New Nom Relation List 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"address4Old",
        "type":"text",
        "label":"Existing Address 4",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"address4New",
        "type":"text",
        "label":"New Address 4",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityList2Old",
        "type":"text",
        "label":"Existing City List 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityList2New",
        "type":"text",
        "label":"New City List 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateList2Old",
        "type":"text",
        "label":"Existing State List 2",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateList2New",
        "type":"text",
        "label":"New State List 2",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"name3Old",
        "type":"text",
        "label":"Existing Name 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"name3New",
        "type":"text",
        "label":"New Name 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineeAge3Old",
        "type":"text",
        "label":"Existing Nominee Age 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineeAge3New",
        "type":"text",
        "label":"New Nominee Age 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomShare3Old",
        "type":"text",
        "label":"Existing Nom Share 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomShare3New",
        "type":"text",
        "label":"New Nom Share 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomRelationList3Old",
        "type":"text",
        "label":"Existing Nom Relation List 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomRelationList3New",
        "type":"text",
        "label":"New Nom Relation List 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"address5Old",
        "type":"text",
        "label":"Existing Address 5",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"address5New",
        "type":"text",
        "label":"New Address 5",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityList3Old",
        "type":"text",
        "label":"Existing City List 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityList3New",
        "type":"text",
        "label":"New City List 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateList3Old",
        "type":"text",
        "label":"Existing State List 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateList3New",
        "type":"text",
        "label":"New State List 3",
        "grid":5,
        "conditionalVarName":"fDNomineeUpdateChange",
        "conditionalVarValue":true,
    },

    /////////////
    {"varName":"fDTenorSchemeChange",
        "type":"checkbox",
        "label":"FD Tenor & Scheme Change",
        "grid":12,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"relationshipOld",
        "type":"text",
        "label":"Existing Relationship",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"relationshipNew",
        "type":"text",
        "label":"New Relationship",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"addressOld",
        "type":"text",
        "label":"Existing Address",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"addressNew",
        "type":"text",
        "label":"New Address",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nameOld",
        "type":"text",
        "label":"Existing Name ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nameNew",
        "type":"text",
        "label":"New Name ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acbilldisbOld",
        "type":"text",
        "label":"Existing Acbilldisb",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acbilldisbNew",
        "type":"text",
        "label":"New Acbilldisb",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"customerIdOld",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"customerIdNew",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"schemeCodeOld",
        "type":"text",
        "label":"Existing Scheme Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"schemeCodeNew",
        "type":"text",
        "label":"New Scheme Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acOpenDateOld",
        "type":"text",
        "label":"Existing Ac Open Date",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acOpenDateNew",
        "type":"text",
        "label":"New Ac Open Date",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acPrefIntCrOld",
        "type":"text",
        "label":"Existing Ac Pref Int Cr",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acPrefIntCrNew",
        "type":"text",
        "label":"New Ac Pref Int Cr",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"interestCreditAcIdOld",
        "type":"text",
        "label":"Existing Interest Credit Ac Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"interestCreditAcIdNew",
        "type":"text",
        "label":"New Interest Credit Ac Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"withHoldingTaxLevelOld",
        "type":"text",
        "label":"Existing With Holding Tax Level",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"withHoldingTaxLevelNew",
        "type":"text",
        "label":"New With Holding Tax Level",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"withholdingTaxOld",
        "type":"text",
        "label":"Existing Withholding Tax ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"withholdingTaxNew",
        "type":"text",
        "label":"New Withholding Tax ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"depositinstallAmountOld",
        "type":"text",
        "label":"Existing Depositinstall Amount",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"depositinstallAmountNew",
        "type":"text",
        "label":"New Depositinstall Amount",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"depositPeriodOld",
        "type":"text",
        "label":"Existing Deposit Period",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"depositPeriodNew",
        "type":"text",
        "label":"New Deposit Period",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"valueDateOld",
        "type":"text",
        "label":"Existing Value Date",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"valueDateNew",
        "type":"text",
        "label":"New Value Date",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"repaymentAcIdOld",
        "type":"text",
        "label":"Existing Repayment Ac Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"repaymentAcIdNew",
        "type":"text",
        "label":"New Repayment Ac Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"autoClosureOld",
        "type":"text",
        "label":"Existing Auto Closure",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"autoClosureNew",
        "type":"text",
        "label":"New Auto Closure",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"autoRenewalOld",
        "type":"text",
        "label":"Existing Auto Renewal",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"autoRenewalNew",
        "type":"text",
        "label":"New Auto Renewal",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineesNameOld",
        "type":"text",
        "label":"Existing Nominees Name",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineesNameNew",
        "type":"text",
        "label":"New Nominees Name",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"regNoOld",
        "type":"text",
        "label":"Existing Reg No",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"regNoNew",
        "type":"text",
        "label":"New Reg No",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityCodeOld",
        "type":"text",
        "label":"Existing City Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityCodeNew",
        "type":"text",
        "label":"New City Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateCodeOld",
        "type":"text",
        "label":"Existing State Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateCodeNew",
        "type":"text",
        "label":"New State Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"countryCodeOld",
        "type":"text",
        "label":"Existing Country Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"countryCodeNew",
        "type":"text",
        "label":"New Country Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"postalCodeOld",
        "type":"text",
        "label":"Existing Postal Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"postalCodeNew",
        "type":"text",
        "label":"New Postal Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"dateOfBirthOld",
        "type":"text",
        "label":"Existing Date Of Birth",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"dateOfBirthNew",
        "type":"text",
        "label":"New Date Of Birth",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"guardianNameOld",
        "type":"text",
        "label":"Existing Guardian Name ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"guardianNameNew",
        "type":"text",
        "label":"New Guardian Name ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"guardianCodeOld",
        "type":"text",
        "label":"Existing Guardian Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"guardianCodeNew",
        "type":"text",
        "label":"New Guardian Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"sectorCodeOld",
        "type":"text",
        "label":"Existing Sector Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"sectorCodeNew",
        "type":"text",
        "label":"New Sector Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"subSectorCodeOld",
        "type":"text",
        "label":"Existing Sub Sector Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"subSectorCodeNew",
        "type":"text",
        "label":"New Sub Sector Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"occupationCodeOld",
        "type":"text",
        "label":"Existing Occupation Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"occupationCodeNew",
        "type":"text",
        "label":"New Occupation Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"freeCode3Old",
        "type":"text",
        "label":"Existing Free Code 3",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"freeCode3New",
        "type":"text",
        "label":"New Free Code 3",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"freeCode6Old",
        "type":"text",
        "label":"Existing Free Code 6",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"freeCode6New",
        "type":"text",
        "label":"New Free Code 6",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"freeCode7Old",
        "type":"text",
        "label":"Existing Free Code 7",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"freeCode7New",
        "type":"text",
        "label":"New Free Code 7",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"tranCreationDuringOvOld",
        "type":"text",
        "label":"Existing Tran Creation During Ov",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"tranCreationDuringOvNew",
        "type":"text",
        "label":"New Tran Creation During Ov",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"fundingAcOld",
        "type":"text",
        "label":"Existing Funding Ac",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"fundingAcNew",
        "type":"text",
        "label":"New Funding Ac",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"typeOfTransactionOld",
        "type":"text",
        "label":"Existing Type Of Transaction ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"typeOfTransactionNew",
        "type":"text",
        "label":"New Type Of Transaction ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acNoOld",
        "type":"text",
        "label":"Existing Ac No",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acNoNew",
        "type":"text",
        "label":"New Ac No",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomineeAgeOld",
        "type":"text",
        "label":"Existing Nominee Age",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomineeAgeNew",
        "type":"text",
        "label":"New Nominee Age",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomShareOld",
        "type":"text",
        "label":"Existing Nom Share ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomShareNew",
        "type":"text",
        "label":"New Nom Share ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"nomRelationListOld",
        "type":"text",
        "label":"Existing Nom Relation List",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"nomRelationListNew",
        "type":"text",
        "label":"New Nom Relation List",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityListOld",
        "type":"text",
        "label":"Existing City List",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityListNew",
        "type":"text",
        "label":"New City List",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateListOld",
        "type":"text",
        "label":"Existing State List",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateListNew",
        "type":"text",
        "label":"New State List",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acIdOld",
        "type":"text",
        "label":"Existing Ac Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acIdNew",
        "type":"text",
        "label":"New Ac Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"relationTypeOld",
        "type":"text",
        "label":"Existing Relation Type ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"relationTypeNew",
        "type":"text",
        "label":"New Relation Type ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"relationCodeOld",
        "type":"text",
        "label":"Existing Relation Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"relationCodeNew",
        "type":"text",
        "label":"New Relation Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"designationCodeOld",
        "type":"text",
        "label":"Existing Designation Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"designationCodeNew",
        "type":"text",
        "label":"New Designation Code",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"custIdOld",
        "type":"text",
        "label":"Existing Cust Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"custIdNew",
        "type":"text",
        "label":"New Cust Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"acbilldisbIdOld",
        "type":"text",
        "label":"Existing Acbilldisb Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"acbilldisbIdNew",
        "type":"text",
        "label":"New Acbilldisb Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"adjustmentAmt1StFieldOf1StRawOld",
        "type":"text",
        "label":"Existing Adjustment Amt 1St Field Of 1St Raw ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"adjustmentAmt1StFieldOf1StRawNew",
        "type":"text",
        "label":"New Adjustment Amt 1St Field Of 1St Raw ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"drcr2NdFieldOf1StRawOld",
        "type":"text",
        "label":"Existing Drcr 2Nd Field Of 1St Raw ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"drcr2NdFieldOf1StRawNew",
        "type":"text",
        "label":"New Drcr 2Nd Field Of 1St Raw ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"runInd3RdFieldOf1StRawOld",
        "type":"text",
        "label":"Existing Run Ind 3Rd Field Of 1St Raw ",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"runInd3RdFieldOf1StRawNew",
        "type":"text",
        "label":"New Run Ind 3Rd Field Of 1St Raw ",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"remarks4ThFieldOf1StRawOld",
        "type":"text",
        "label":"Existing Remarks 4Th Field Of 1St Raw",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"remarks4ThFieldOf1StRawNew",
        "type":"text",
        "label":"New Remarks 4Th Field Of 1St Raw",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"del5ThFieldOf1StRawOld",
        "type":"text",
        "label":"Existing Del 5Th Field Of 1St Raw",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"del5ThFieldOf1StRawNew",
        "type":"text",
        "label":"New Del 5Th Field Of 1St Raw",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"principalOutflowtotalOutflowOld",
        "type":"text",
        "label":"Existing Principal Outflowtotal Outflow",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"principalOutflowtotalOutflowNew",
        "type":"text",
        "label":"New Principal Outflowtotal Outflow",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"1StFieldOfFirstLineOld",
        "type":"text",
        "label":"Existing 1St Field Of First Line",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"1StFieldOfFirstLineNew",
        "type":"text",
        "label":"New 1St Field Of First Line",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"2NdFieldOfFirstLineOld",
        "type":"text",
        "label":"Existing 2Nd Field Of First Line",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"2NdFieldOfFirstLineNew",
        "type":"text",
        "label":"New 2Nd Field Of First Line",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"3RdFieldOf3RdLineOld",
        "type":"text",
        "label":"Existing 3Rd Field Of 3Rd Line",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"3RdFieldOf3RdLineNew",
        "type":"text",
        "label":"New 3Rd Field Of 3Rd Line",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"fdAcIdOld",
        "type":"text",
        "label":"Existing Fd Ac Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"fdAcIdNew",
        "type":"text",
        "label":"New Fd Ac Id",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"valueDateOfClosureOld",
        "type":"text",
        "label":"Existing Value Date Of Closure",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"valueDateOfClosureNew",
        "type":"text",
        "label":"New Value Date Of Closure",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"useRepaymentAcOnlyOld",
        "type":"text",
        "label":"Existing Use Repayment Ac Only",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"useRepaymentAcOnlyNew",
        "type":"text",
        "label":"New Use Repayment Ac Only",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"closureRemarksOld",
        "type":"text",
        "label":"Existing Closure Remarks",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"closureRemarksNew",
        "type":"text",
        "label":"New Closure Remarks",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"intRateOld",
        "type":"text",
        "label":"Existing Int Rate",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"intRateNew",
        "type":"text",
        "label":"New Int Rate",
        "grid":5,
        "conditionalVarName":"fDTenorSchemeChange",
        "conditionalVarValue":true,
    },
    //////////
    {"varName":"sourceTaxReversalChange",
        "type":"checkbox",
        "label":"Source Tax Reversal",
        "grid":12,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"functionAvOld",
        "type":"text",
        "label":"Existing Function Av",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"functionAvNew",
        "type":"text",
        "label":"New Function Av",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"tranIdOld",
        "type":"text",
        "label":"Existing Tran Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"tranIdNew",
        "type":"text",
        "label":"New Tran Id",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"typesubtypeTBiOld",
        "type":"text",
        "label":"Existing Typesubtype T  Bi",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"typesubtypeTBiNew",
        "type":"text",
        "label":"New Typesubtype T  Bi",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"sourceTaxAcNumberDebitOld",
        "type":"text",
        "label":"Existing Source Tax  Ac Number Debit",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"sourceTaxAcNumberDebitNew",
        "type":"text",
        "label":"New Source Tax  Ac Number Debit",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"customerInstructedAcNumberCreditOld",
        "type":"text",
        "label":"Existing Customer Instructed Ac Number Credit",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"customerInstructedAcNumberCreditNew",
        "type":"text",
        "label":"New Customer Instructed Ac Number Credit",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"amountOld",
        "type":"text",
        "label":"Existing Amount",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"amountNew",
        "type":"text",
        "label":"New Amount",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"deditCreditOld",
        "type":"text",
        "label":"Existing Dedit  Credit",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"deditCreditNew",
        "type":"text",
        "label":"New Dedit  Credit",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"particularCodeOld",
        "type":"text",
        "label":"Existing Particular Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"particularCodeNew",
        "type":"text",
        "label":"New Particular Code",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"enterTransationParticularsOld",
        "type":"text",
        "label":"Existing Enter Transation Particulars",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"enterTransationParticularsNew",
        "type":"text",
        "label":"New Enter Transation Particulars",
        "grid":5,
        "conditionalVarName":"sourceTaxReversalChange",
        "conditionalVarValue":true,
    },
    ////////////
    {"varName":"fDSchemeChangeWCCChange",
        "type":"checkbox",
        "label":"FD Scheme Change without Closing",
        "grid":12,
        "conditional":true,
        "conditionalVarName":"fDSchemeChangeWCCChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"functionOld",
        "type":"text",
        "label":"Existing Function",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"functionNew",
        "type":"text",
        "label":"New Function",
        "grid":5,
        "conditionalVarName":"fDSchemeChangeWCCChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"aCIdOld",
        "type":"text",
        "label":"Existing A/C Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"aCIdNew",
        "type":"text",
        "label":"New A/C Id",
        "grid":5,
        "conditionalVarName":"fDSchemeChangeWCCChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"targetSchemeCodeOld",
        "type":"text",
        "label":"Existing Target Scheme Code",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"targetSchemeCodeNew",
        "type":"text",
        "label":"New Target Scheme Code",
        "grid":5,
        "conditionalVarName":"fDSchemeChangeWCCChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"trialModeOld",
        "type":"text",
        "label":"Existing Trial Mode",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"trialModeNew",
        "type":"text",
        "label":"New Trial Mode",
        "grid":5,
        "conditionalVarName":"fDSchemeChangeWCCChange",
        "conditionalVarValue":true,
    },
    //////
    {"varName":"fDNomineesGuardianUpdateChange",
        "type":"checkbox",
        "label":"FD Nominee's Guardian Change",
        "grid":12,
        "conditional":true,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"dateOfBirthOld",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"dateOfBirthNew",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"guardianNameOld",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"guardianNameNew",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"guardianCodeOld",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"guardianCodeNew",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"address2Old",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"address2New",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"cityCode2Old",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"cityCode2New",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"stateCode2Old",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"stateCode2New",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"countryCode2Old",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"countryCode2New",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
    {
        "type": "blank",
        "grid": 2,
    },
    {"varName":"postalCode2Old",
        "type":"text",
        "label":"Existing Customer Id",
        "grid":5,
        "readOnly": true,
    },
    {"varName":"postalCode2New",
        "type":"text",
        "label":"New Customer Id",
        "grid":5,
        "conditionalVarName":"fDNomineesGuardianUpdateChange",
        "conditionalVarValue":true,
    },
]

    let MAKERFdrMaintenance = {};
MAKERFdrMaintenance = JSON.parse(JSON.stringify(fDRMaintenanceListChecker));

let CheckerFdrMaintenance = {};
CheckerFdrMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(fDRMaintenanceListChecker)));


// taxcertificate
const taxCertificateMaintenanceList = [
    {
        "varName": "fDRMTDREncashment",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
        "conditional" : true,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,
    },
    {
        "varName": "fDDPSFaceValueFd",
        "type": "text",
        "label": "FD/DPS Face Value",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },
    {
        "varName": "maturityFDFdDPS",
        "type": "checkbox",
        "label": "To Be En-cashed before maturity for FD/DPS",
        "grid": 6,
        "conditional" : true,
        "conditionalVarName": "fDRMTDREncashment",
        "conditionalVarValue": true,

    },
    {
        "varName": "fDRInterestPayment",
        "type": "radio",
        "label": "FDR Interest Payment",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "fDRInterestPayment",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 1,
        "conditional" : true,
        "conditionalVarName": "fDRInterestPayment",
        "conditionalVarValue": true,
    },
    {
        "varName": "fDDPSFaceValueFdr",
        "type": "text",
        "label": "FD/DPS Face Value",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "fDRInterestPayment",
        "conditionalVarValue": true,

    },
    {
        "varName": "maturityFDRFdDPS",
        "type": "checkbox",
        "label": "To Be En-cashed before maturity for FD/DPS",
        "grid": 6,
        "conditional" : true,
        "conditionalVarName": "fDRInterestPayment",
        "conditionalVarValue": true,

    },


    {

        "type": "blank",
        "grid": 1,
        "conditional" : true,
        "conditionalVarName": "maturityFDRFdDPS",
        "conditionalVarValue": true,
    },
    {
        "varName": "fundFdr",
        "type": "checkbox",
        "label": "Fund Transfer to",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "fundFdr",
        "conditionalVarValue": true,

    },

    {
        "varName": "fundTransferFdr",
        "type": "text",
        "label": "Account No",
        "grid": 6,

    },
    {

        "type": "blank",
        "grid": 1,
    },


    {
        "varName": "payOrderFdr",
        "type": "checkbox",
        "label": "Pay Order",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "payOrderFdr",
        "conditionalVarValue": true,

    },

    {
        "varName": "paymentThroughFdr",
        "type": "select",
        "label": "Payment Through",
        "grid": 6,

        "enum": [
            "EFT",
            "RTGS"
        ]
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "othersFdr",
        "type": "checkbox",
        "label": "Others",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "othersFdr",
        "conditionalVarValue": true,

    },
    {
        "varName": "othersFdrSpecify",
        "type": "text",
        "label": "Specify",
        "grid": 6,
    },

    {

        "type": "blank",
        "grid": 6,
    },
    {
        "varName": "tinFdr",
        "type": "select",
        "label": "E-Tin",
        "grid": 6,
        "enum": [
            "YES",
            "NO"
        ],

    },

    {
        "varName": "linkACChange",
        "type": "checkbox",
        "label": "Link A/C Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "linkACChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "linkACOld",
        "type": "text",
        "label": "Existing Link A/C",
        "grid": 5,
        "readOnly": true,

    },
    {
        "varName": "linkACNew",
        "type": "text",
        "label": "New Link A/C",
        "grid": 6,

    },
    {
        "varName": "fDRMTDREncashmentCertificate",
        "type": "checkbox",
        "label": "FDR/MTDR Encashment Certificate",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "fDRMTDREncashmentCertificate",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },

    {
        "varName": "tenorChange",
        "type": "checkbox",
        "label": "Tenor Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "tenorChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "reissueFDWith",
        "type": "select",
        "label": "Reissue FD with",
        "grid": 11,
        "enum": [
            "Principal Amount",
            "Principal Amount with Accrued(earned) Interest",

        ]
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "newTenorFd",
        "type": "text",
        "label": "New Tenor",
        "grid": 5,
    },
    {
        "varName": "newTenorFdSelect",
        "type": "select",
        "selectedValue": "Days",
        "label": "",
        "grid": 6,
        "enum": [
            "Days",
            "Months",
            "Years"
        ],
    },


    {
        "varName": "schemeChange",
        "type": "checkbox",
        "label": "Scheme Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "schemeChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "fDDPSFaceValueDPS",
        "type": "text",
        "label": "FD/DPS Face Value",
        "grid": 5,

    },
    {
        "varName": "maturityFDRFdDPSDPS",
        "type": "checkbox",
        "label": "To Be En-cashed before maturity for FD/DPS",
        "grid": 6,
        "conditional" : true,
        "conditionalVarName": "maturityFDRFdDPSDPS",
        "conditionalVarValue": true,

    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "fundDPS",
        "type": "checkbox",
        "label": "Fund Transfer to",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "fundDPS",
        "conditionalVarValue": true,

    },

    {
        "varName": "fundTransferDPS",
        "type": "text",
        "label": "Account No",
        "grid": 6,

    },

    {

        "type": "blank",
        "grid": 1,
    },

    {
        "varName": "payOrderDPS",
        "type": "checkbox",
        "label": "Pay Order",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "payOrderDPS",
        "conditionalVarValue": true,

    },

    {
        "varName": "paymentThroughDPS",
        "type": "select",
        "label": "Payment Through",
        "grid": 6,

        "enum": [
            "EFT",
            "RTGS"
        ]
    },
    {

        "type": "blank",
        "grid": 1,
    },
    {
        "varName": "othersDPS",
        "type": "checkbox",
        "label": "Others",
        "grid": 5,
        "conditional" : true,
        "conditionalVarName": "othersDPS",
        "conditionalVarValue": true,
    },
    {
        "varName": "othersDPSSpecify",
        "type": "text",
        "label": "Specify",
        "grid": 5,
    },

    {

        "type": "blank",
        "grid": 6,
    },
    {
        "varName": "tinDPS",
        "type": "select",
        "label": "E-Tin",
        "grid": 6,
        "enum": [
            "YES",
            "NO"
        ],

    },


    {

        "type": "blank",
        "grid": 2,
    },

    {
        "varName": "duplicateAdviceChange",
        "type": "checkbox",
        "label": "Duplicate Advice Change",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "duplicateAdviceChange",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },

    {
        "varName": "sourceTaxReversal",
        "type": "checkbox",
        "label": "Source Tax Reversal",
        "grid": 12,
        "conditional" : true,
        "conditionalVarName": "sourceTaxReversal",
        "conditionalVarValue": true,
    },
    {

        "type": "blank",
        "grid": 2,
    },


]
let BOMTaxCertificateMaintenance = {};
BOMTaxCertificateMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(taxCertificateMaintenanceList)));


let BMTaxCertificateMaintenance = {};
BMTaxCertificateMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(taxCertificateMaintenanceList)));


let MAKERTaxCertificateMaintenance = {};
MAKERTaxCertificateMaintenance = JSON.parse(JSON.stringify(taxCertificateMaintenanceList));

let CheckerTaxCertificateMaintenance = {};
CheckerTaxCertificateMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(taxCertificateMaintenanceList)));


// debit credit maintenance

const debitCreditMaintenanceList = [
    {" varName":"amexGoldUpgradation",
        "type":"checkbox",
        "label":"Amex Gold Up-gradation",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftPriority",
        "type":"checkbox",
        "label":"Amex Platinum Gift - 6 Priority Pass Free Visits",
        "grid":12,

    },

    {" varName":"amexPlatinumGiftSquare",
        "type":"checkbox",
        "label":"Amex Platinum Gift  Square Hospital Voucher",
        "grid":12,

    },


    {"varName":"amexPlatinumGiftAirlines",
        "type":"checkbox",
        "label":"Amex Platinum gift- Airlines voucher",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftMRpoint",
        "type":"checkbox",
        "label":"Amex platinum gift-25000 MR point",
        "grid":12
    },

    {"varName":"amexPlatinumGiftAviation",
        "type":"checkbox",
        "label":"Amex Platinum Gift-Partex Aviation",
        "grid":12,

    },

    {"varName":"amexPlatinumGiftResortvoucher",
        "type":"checkbox",
        "label":"Amex Platinum gift-Resort voucher",
        "grid":12,

    },


    {" varName":"amexPlatinumUpgradation",
        "type":"checkbox",
        "label":"Amex Platinum Up-gradation",
        "grid":12,

    },



    {" varName":"applicationstatusquery",
        "type":"checkbox",
        "label":"Application status query",
        "grid":12,

    },


    {"varName":"aTMdisputeadjustment",
        "type":"checkbox",
        "label":"ATM dispute adjustment",
        "grid":12,

    },

    {
        "varName": "autoDebitEnrollment",
        "type": "checkbox",
        "label": "Auto Debit Enrollment",
        "grid": 12,
    },

    {" varName":"autodebitstandinginstructioncancellation",
        "type":"checkbox",
        "label":"Auto debit/standing instruction cancellation",
        "grid":12,

    },

    {
        "varName": "balanceTransferAmex",
        "type": "checkbox",
        "label": "Balance Transfer (Amex)",
        "grid": 12,

    },

    {
        "varName": "balanceTransferVisa",
        "type": "checkbox",
        "label": "Balance Transfer (Visa)",
        "grid": 12,

    },

    {
        "varName": "captureCreditCardReplacement",
        "type": "checkbox",
        "label": "Capture Credit Card Replacement",
        "grid": 12,

    },

    {
        "varName": "cardchequereissue",
        "type": "checkbox",
        "label": "Card cheque reissue",
        "grid": 12,

    },

    {
        "varName": "cardChequereturnnotreceived",
        "type": "checkbox",
        "label": "Card Cheque return/not received",
        "grid": 12,

    },

    {" varName":"cardnotreceivedAmex",
        "type":"checkbox",
        "label":"Card not received (Amex)",
        "grid":12,

    },

    {
        "varName": "cardnotreceivedVisa",
        "type": "checkbox",
        "label": "Card not received (Visa)",
        "grid": 12,

    },

    {
        "varName": "cardprintingcorrection",
        "type": "checkbox",
        "label": "Card printing correction",
        "grid": 12,

    },
    //////////
    {" varName":"certificatefeesrealization",
        "type":"checkbox",
        "label":"Certificate fees realization",
        "grid":12,

    },

    {"varName":"cityTouchbillpaymentDispute",
        "type":"checkbox",
        "label":"City Touch bill payment Dispute",
        "grid":12,

    },

    {" varName":"cityTouchCreditCardpaymentdispute",
        "type":"checkbox",
        "label":"City Touch Credit Card payment dispute",
        "grid":12,

    },


    {"varName":"cityTouchonlineshoppingdispute",
        "type":"checkbox",
        "label":"City Touch online shopping dispute",
        "grid":12,

    },

    {"varName":"citytouchDisputeAccounts",
        "type":"checkbox",
        "label":"Citytouch Dispute-Accounts",
        "grid":12
    },

    {"varName":"annualFeereversalunderCorporateAgreement",
        "type":"checkbox",
        "label":"Annual Fee reversal under Corporate Agreement",
        "grid":12,

    },

    {"varName":"creditbalancetransferAmex",
        "type":"checkbox",
        "label":"Credit balance transfer (Amex)",
        "grid":12,

    },


    {" varName":"creditbalancetransferVisa",
        "type":"checkbox",
        "label":"Credit balance transfer (Visa)",
        "grid":12,

    },



    {" varName":"creditCardRenewalRequest",
        "type":"checkbox",
        "label":"Credit Card Renewal Request",
        "grid":12,

    },


    {"varName":"creditCardReplaceRequest",
        "type":"checkbox",
        "label":"Credit Card Replace Request",
        "grid":12,

    },

    {
        "varName": "creditCardUpgradationRequest",
        "type": "checkbox",
        "label": "Credit Card Upgradation Request",
        "grid": 12,
    },

    {" varName":"carddatacorrection",
        "type":"checkbox",
        "label":"Card data correction",
        "grid":12,

    },

    {
        "varName": "delinquentCreditCardReissueRenewal",
        "type": "checkbox",
        "label": "Delinquent Credit Card Reissue/Renewal",
        "grid": 12,

    },

    {
        "varName": "eCommerceOTPIssue",
        "type": "checkbox",
        "label": "E-Commerce OTP Issue",
        "grid": 12,

    },

    {
        "varName": "estatementnotreceived",
        "type": "checkbox",
        "label": "E-statement not received",
        "grid": 12,

    },

    {
        "varName": "earlysettlementforFlexibuyEMIFlexiloan",
        "type": "checkbox",
        "label": "Early settlement for Flexi buy /EMI/ Flexi loan",
        "grid": 12,

    },

    {
        "varName": "eFTNCancelationRequestRegular",
        "type": "checkbox",
        "label": "EFTN Cancelation Request (Regular)",
        "grid": 12,

    },

    {" varName":"eFTNRequestUSDBDT",
        "type":"checkbox",
        "label":"EFTN Request USD & BDT",
        "grid":12,

    },

    {
        "varName": "eFTNRequestUSDBDTDone",
        "type": "checkbox",
        "label": "EFTN Request USD (BDT Done)",
        "grid": 12,

    },

    {
        "varName": "eMIrequest",
        "type": "checkbox",
        "label": "EMI request",
        "grid": 12,

    },
    {" varName":"fDRencashment",
        "type":"checkbox",
        "label":"FDR encashment",
        "grid":12,

    },

    {"varName":"fDRencashmentClosurerequest",
        "type":"checkbox",
        "label":"FDR encashment & Closure request",
        "grid":12,

    },

    {" varName":"feereversalforCreditCardAmex",
        "type":"checkbox",
        "label":"Fee reversal for Credit Card (Amex)",
        "grid":12,

    },


    {"varName":"feereversalforCreditCardVisa",
        "type":"checkbox",
        "label":"Fee reversal for Credit Card (Visa)",
        "grid":12,

    },

    {"varName":"fundtransferVisa",
        "type":"checkbox",
        "label":"Fund transfer (Visa)",
        "grid":12
    },

    {"varName":"fundtransferAmex",
        "type":"checkbox",
        "label":"Fund transfer (Amex)",
        "grid":12,

    },

    {"varName":"giftCardReplacementRequest",
        "type":"checkbox",
        "label":"Gift Card Replacement Request",
        "grid":12,

    },


    {" varName":"hardCopySoftCopyEnableRequest",
        "type":"checkbox",
        "label":"Hard Copy / Soft Copy Enable Request",
        "grid":12,

    },



    {" varName":"hardcopystatementrelatedissue",
        "type":"checkbox",
        "label":"Hard copy statement related issue",
        "grid":12,

    },


    {"varName":"insuranceSchemeDeEnrollmentrequest",
        "type":"checkbox",
        "label":"Insurance Scheme De-Enrollment request",
        "grid":12,

    },

    {
        "varName": "insuranceSchemeenrollmentRequest",
        "type": "checkbox",
        "label": "Insurance Scheme enrollment Request",
        "grid": 12,
    },

    {" varName":"lienMarking",
        "type":"checkbox",
        "label":"Lien Marking",
        "grid":12,

    },

    {
        "varName": "lienwithdraw",
        "type": "checkbox",
        "label": "Lien withdraw",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawcarddueadjustment",
        "type": "checkbox",
        "label": "Lien withdraw & card dues adjustment",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawforinteresttransfer",
        "type": "checkbox",
        "label": "Lien withdraw for interest transfer",
        "grid": 12,

    },

    {
        "varName": "lienwithdrawadjustmentRelien",
        "type": "checkbox",
        "label": "Lien withdraw-adjustment- Re-lien",
        "grid": 12,

    },

    {
        "varName": "limitRearrange",
        "type": "checkbox",
        "label": "Limit Re-arrange",
        "grid": 12,

    },

    {" varName":"limitRectificationRequest",
        "type":"checkbox",
        "label":"Limit Rectification Request",
        "grid":12,

    },

    {
        "varName": "mRCardFeeRedemption",
        "type": "checkbox",
        "label": "MR Card Fee Redemption",
        "grid": 12,

    },

    {
        "varName": "mRPointsReversalIssues",
        "type": "checkbox",
        "label": "MR Points Reversal Issues",
        "grid": 12,

    },
    //////////
    {" varName":"mRRedemptionforCapturedcardFees",
        "type":"checkbox",
        "label":"MR Redemption for Captured card Fees",
        "grid":12,

    },

    {"varName":"mRRedemptionforCardReplacementFees",
        "type":"checkbox",
        "label":"MR Redemption for Card Replacement Fees",
        "grid":12,

    },

    {" varName":"mRRedemptionforCertificateFees",
        "type":"checkbox",
        "label":"MR Redemption for Certificate Fees",
        "grid":12,

    },


    {"varName":"mRRedemptionforChequeReturnFees",
        "type":"checkbox",
        "label":"MR Redemption for Cheque Return Fees",
        "grid":12,

    },

    {"varName":"mRRedemptionforCIBFees",
        "type":"checkbox",
        "label":"MR Redemption for CIB Fees",
        "grid":12
    },

    {"varName":"mRredemptionforGiftCard",
        "type":"checkbox",
        "label":"MR redemption for Gift Card",
        "grid":12,

    },

    {"varName":"mRRedemptionforLatePaymentFees",
        "type":"checkbox",
        "label":"MR Redemption for Late Payment Fees",
        "grid":12,

    },


    {" varName":"mRRedemptionforOutstandingBDT",
        "type":"checkbox",
        "label":"MR Redemption for Outstanding (BDT)",
        "grid":12,

    },



    {" varName":"mRRedemptionforOutstandingUSD",
        "type":"checkbox",
        "label":"MR Redemption for Outstanding (USD)",
        "grid":12,

    },


    {"varName":"mRRedemptionforOverLimitFees",
        "type":"checkbox",
        "label":"MR Redemption for Over Limit Fees",
        "grid":12,

    },

    {
        "varName": "mRRedemptionPINReplacementFees",
        "type": "checkbox",
        "label": "MR Redemption for PIN Replacement Fees",
        "grid": 12,
    },

    {" varName":"mRRedemptionSMSFees",
        "type":"checkbox",
        "label":"MR Redemption for SMS Fees",
        "grid":12,

    },

    {
        "varName": "mRRedemptionStatementRetrievalFees",
        "type": "checkbox",
        "label": "MR Redemption for Statement Retrieval Fees",
        "grid": 12,

    },

    {
        "varName": "mRVoucherReissue",
        "type": "checkbox",
        "label": "MR Voucher Reissue",
        "grid": 12,

    },

    {
        "varName": "passportInformationupdaterequest",
        "type": "checkbox",
        "label": "Passport Information update request",
        "grid": 12,

    },

    {
        "varName": "paymentUpdatewrongPart",
        "type": "checkbox",
        "label": "Payment Update wrong Part",
        "grid": 12,

    },

    {
        "varName": "personalcallverification",
        "type": "checkbox",
        "label": "Personal detail update request without call verification",
        "grid": 12,

    },

    {" varName":"photoSignatureupload",
        "type":"checkbox",
        "label":"Photo & Signature upload for Card Cheque Activation",
        "grid":12,

    },

    {
        "varName": "pODrequestonCard",
        "type": "checkbox",
        "label": "POD request on Card",
        "grid": 12,

    },

    {
        "varName": "pODrequestonVoucher",
        "type": "checkbox",
        "label": "POD request on Voucher",
        "grid": 12,

    },
    ///////////////////////////////////////////////
    {" varName":"prepaidcardcontactinformationupdate",
        "type":"checkbox",
        "label":"Prepaid card contact information update",
        "grid":12,

    },

    {"varName":"priorityPassCardnotreceived",
        "type":"checkbox",
        "label":"Priority Pass Card not received",
        "grid":12,

    },

    {" varName":"prioritypassissuingrequestforGoldPrimary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Gold Primary",
        "grid":12,

    },


    {"varName":"prioritypassissuingrequestPlatinumPrimary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Platinum Primary",
        "grid":12,

    },

    {"varName":"prioritypassissuingrequestPlatinumSupplementary",
        "type":"checkbox",
        "label":"Priority pass issuing request for Platinum Supplementary",
        "grid":12
    },

    {"varName":"prioritypassReplacementPlatinumPrimary",
        "type":"checkbox",
        "label":"Priority pass Replacement for Platinum Primary",
        "grid":12,

    },

    {"varName":"prioritypassReplacementPlatinumSupplementary",
        "type":"checkbox",
        "label":"Priority pass Replacement for Platinum Supplementary",
        "grid":12,

    },


    {" varName":"prioritypassReplacementGoldPrimary",
        "type":"checkbox",
        "label":"Priority pass Replacement Gold Primary",
        "grid":12,

    },



    {" varName":"queryCardPindeliveryAmex",
        "type":"checkbox",
        "label":"Query on Card/Pin delivery (Amex)",
        "grid":12,

    },


    {"varName":"queryCardPindeliveryVisa",
        "type":"checkbox",
        "label":"Query on Card/Pin delivery (Visa)",
        "grid":12,

    },

    {
        "varName": "queryDebitCardPINdelivery",
        "type": "checkbox",
        "label": "Query on Debit Card/PIN delivery",
        "grid": 12,
    },

    {" varName":"queryUDC",
        "type":"checkbox",
        "label":"Query on UDC",
        "grid":12,

    },

    {
        "varName": "returnCardchequeresendrequest",
        "type": "checkbox",
        "label": "Return Card cheque resend request",
        "grid": 12,

    },

    {
        "varName": "returnCardresendrequestAmex",
        "type": "checkbox",
        "label": "Return Card resend request (Amex)",
        "grid": 12,

    },

    {
        "varName": "returnCardresendrequestVisa",
        "type": "checkbox",
        "label": "Return Card resend request (Visa)",
        "grid": 12,

    },

    {
        "varName": "returnPINresendrequestAmex",
        "type": "checkbox",
        "label": "Return PIN resend request (Amex)",
        "grid": 12,

    },

    {
        "varName": "returnVoucherresendrequestBirthdayVoucher",
        "type": "checkbox",
        "label": "Return Voucher resend request (Birthday Voucher)",
        "grid": 12,

    },

    {" varName":"returnVoucherresendrequestMRVoucher",
        "type":"checkbox",
        "label":"Return Voucher resend request (MR Voucher)",
        "grid":12,

    },

    {
        "varName": "returnVoucherresendrequestSpendingVoucher",
        "type": "checkbox",
        "label": "Return Voucher resend request (Spending Voucher)",
        "grid": 12,

    },

    {
        "varName": "returnedCardResend AMEXUpdatedAddress",
        "type": "checkbox",
        "label": "Returned Card Resend  AMEX (Updated Address)",
        "grid": 12,

    },
    //////////
    {" varName":"returnedCardResendVISAUpdatedAddress",
        "type":"checkbox",
        "label":"Returned Card Resend  VISA (Updated Address)",
        "grid":12,

    },

    {"varName":"returnedPriorityPassCardresend",
        "type":"checkbox",
        "label":"Returned Priority Pass Card resend",
        "grid":12,

    },

    {" varName":"securedCardClosureSecuredUnsecured",
        "type":"checkbox",
        "label":"Secured Card Closure_Secured to Unsecured",
        "grid":12,

    },


    {"varName":"securedCardClosureSecurityClosureRelease",
        "type":"checkbox",
        "label":"Secured Card Closure_Security Closure/Release",
        "grid":12,

    },

    {"varName":"signaturePhotoUpdate",
        "type":"checkbox",
        "label":"Signature / Photo Update",
        "grid":12
    },

    {"varName":"statementCycleChange",
        "type":"checkbox",
        "label":"Statement Cycle Change",
        "grid":12,

    },

    {"varName":"statementFeeAmex",
        "type":"checkbox",
        "label":"Statement Fee for Amex",
        "grid":12,

    },


    {" varName":"statementFeeVisa",
        "type":"checkbox",
        "label":"Statement Fee for Visa",
        "grid":12,

    },



    {" varName":"supplementarycardlimitsetrequest",
        "type":"checkbox",
        "label":"Supplementary card limit set request",
        "grid":12,

    },


    {"varName":"uDCreturnrequest",
        "type":"checkbox",
        "label":"UDC return request",
        "grid":12,

    },

    {
        "varName": "citytouchDisputeAccountsCreditCardBillPayment",
        "type": "checkbox",
        "label": "Citytouch Dispute-Accounts » Credit Card Bill Payment",
        "grid": 12,
    },

    {" varName":"citytouchDisputeAccountsVisaInstantPayment",
        "type":"checkbox",
        "label":"Citytouch Dispute-Accounts » Visa Instant Payment",
        "grid":12,

    },

    {
        "varName": "accountTaggingDebitCard",
        "type": "checkbox",
        "label": "Account Tagging with Debit Card",
        "grid": 12,

    },

    {
        "varName": "captureDebitCardReplacement",
        "type": "checkbox",
        "label": "Capture Debit Card Replacement",
        "grid": 12,

    },

    {
        "varName": "debitCardCashwithdrawallimitmodification",
        "type": "checkbox",
        "label": "Debit Card Cash withdrawal limit modification",
        "grid": 12,

    },

    {
        "varName": "debitCardFeesRealization",
        "type": "checkbox",
        "label": "Debit Card Fees Realization",
        "grid": 12,

    },

    {
        "varName": "debitCardPINReplace",
        "type": "checkbox",
        "label": "Debit Card PIN Replace",
        "grid": 12,

    },

    {" varName":"debitCardPODcopy",
        "type":"checkbox",
        "label":"Debit Card POD copy",
        "grid":12,

    },

    {
        "varName": "debitCardRenewalRequest",
        "type": "checkbox",
        "label": "Debit Card Renewal Request",
        "grid": 12,

    },

    {
        "varName": "debitCardReplaceRequest",
        "type": "checkbox",
        "label": "Debit Card Replace Request",
        "grid": 12,

    },
    {" varName":"debitCardPINnotreceived",
        "type":"checkbox",
        "label":"Debit Card/PIN not received",
        "grid":12,

    },

    {"varName":"complainagainstSalespersonCreditCard",
        "type":"checkbox",
        "label":"Complain against Sales person (Credit Card)",
        "grid":12,

    },

    {" varName":"officeQuotaEndorsementCorporateCards",
        "type":"checkbox",
        "label":"Office Quota Endorsement  Corporate Cards",
        "grid":12,

    },


    {"varName":"advanceendorsementupdaterequest",
        "type":"checkbox",
        "label":"Advance endorsement update request",
        "grid":12,

    },

    {"varName":"amexCreditCardAcceptanceissueInternational",
        "type":"checkbox",
        "label":"Amex Credit Card Acceptance issue (International)",
        "grid":12
    },

    {"varName":"autoLimitEnhancement",
        "type":"checkbox",
        "label":"Auto Limit Enhancement",
        "grid":12,

    },

    {"varName":"cardLimitComplainbyRFC",
        "type":"checkbox",
        "label":"Card Limit Complain (by RFC)",
        "grid":12,

    },


    {" varName":"cardSurrenderAmex",
        "type":"checkbox",
        "label":"Card Surrender (Amex)",
        "grid":12,

    },



    {" varName":"cardSurrenderVisa",
        "type":"checkbox",
        "label":"Card Surrender (Visa)",
        "grid":12,

    },


    {"varName":"cardSurrenderDowngradeAmexPlatinum",
        "type":"checkbox",
        "label":"Card Surrender/Down grade (Amex Platinum)",
        "grid":12,

    },

    {
        "varName": "cardmembersAppreciation",
        "type": "checkbox",
        "label": "Cardmembers Appreciation",
        "grid": 12,
    },

    {" varName":"certificaterequestRegularCard",
        "type":"checkbox",
        "label":"Certificate request for Regular Card",
        "grid":12,

    },

    {
        "varName": "endorsementCancellationrequest",
        "type": "checkbox",
        "label": "Endorsement Cancellation request",
        "grid": 12,

    },

    {
        "varName": "endorsementUpdateRequest",
        "type": "checkbox",
        "label": "Endorsement Update Request",
        "grid": 12,

    },

    {
        "varName": "limitDecreaseRequest",
        "type": "checkbox",
        "label": "Limit Decrease Request",
        "grid": 12,

    },

    {
        "varName": "limitEnhancement",
        "type": "checkbox",
        "label": "Limit Enhancement",
        "grid": 12,

    },

    {
        "varName": "lPCInterestEOLwaiverrequest",
        "type": "checkbox",
        "label": "LPC/Interest/EOL waiver request",
        "grid": 12,

    },

    {" varName":"priorityPassComplain",
        "type":"checkbox",
        "label":"Priority Pass Complain",
        "grid":12,

    },

    {
        "varName": "securedCardClosure",
        "type": "checkbox",
        "label": "Secured Card Closure",
        "grid": 12,

    },

    {
        "varName": "securedCardClosureRetentionCall",
        "type": "checkbox",
        "label": "Secured Card Closure_Retention Call",
        "grid": 12,

    },
    //////////
    {" varName":"unBilledStatement",
        "type":"checkbox",
        "label":"Un-Billed Statement",
        "grid":12,

    },

    {"varName":"debitCardSurrender",
        "type":"checkbox",
        "label":"Debit Card Surrender",
        "grid":12,

    },

    {" varName":"eCommerceMerchantonboardingrequest",
        "type":"checkbox",
        "label":"E:Commerce Merchant on-boarding request",
        "grid":12,

    },


    {"varName":"aTMdisputeoffus",
        "type":"checkbox",
        "label":"ATM dispute (off us)",
        "grid":12,

    },

    {"varName":"aTMDisputeoverseas",
        "type":"checkbox",
        "label":"ATM Dispute (overseas)",
        "grid":12
    },

    {"varName":"disputedtransactionrequest",
        "type":"checkbox",
        "label":"Disputed transaction request",
        "grid":12,

    },

    {"varName":"fraudCounterfeittransaction",
        "type":"checkbox",
        "label":"Fraud & Counterfeit transaction",
        "grid":12,

    },


    {" varName":"pOSdiputeOffUS",
        "type":"checkbox",
        "label":"POS dipute (Off US)",
        "grid":12,

    },



    {" varName":"amexCreditCardAcceptanceissueLocal",
        "type":"checkbox",
        "label":"Amex Credit Card Acceptance issue (Local)",
        "grid":12,

    },


    {"varName":"citytouchDisputeMerchant",
        "type":"checkbox",
        "label":"Citytouch Dispute-Merchant",
        "grid":12,

    },

    {
        "varName": "flexibuyrequest",
        "type": "checkbox",
        "label": "Flexi buy request",
        "grid": 12,
    },

    {" varName":"merchantServicesComplaint",
        "type":"checkbox",
        "label":"Merchant Services Complaint",
        "grid":12,

    },

    {
        "varName": "mRcomplainMerchant",
        "type": "checkbox",
        "label": "MR complain (Merchant)",
        "grid": 12,

    },

    {
        "varName": "creditCardActivationRegularCard",
        "type": "checkbox",
        "label": "Credit Card Activation for Regular Card",
        "grid": 12,

    },

    {
        "varName": "delinquentCreditCardActivationOutbound",
        "type": "checkbox",
        "label": "Delinquent Credit Card Activation by Outbound",
        "grid": 12,

    },

    {
        "varName": "personaldetailupdaterequestwithcallverification",
        "type": "checkbox",
        "label": "Personal detail update request with call verification",
        "grid": 12,

    },

    {
        "varName": "easypayDeEnrollment",
        "type": "checkbox",
        "label": "Easypay De-Enrollment",
        "grid": 12,

    },

    {" varName":"easypayEnrollment",
        "type":"checkbox",
        "label":"Easypay Enrollment",
        "grid":12,

    },

    {
        "varName": "mRDisputeComplaint",
        "type": "checkbox",
        "label": "MR Dispute & Complaint",
        "grid": 12,

    },

    {
        "varName": "delinquentAMEXCardClosureConfirmation",
        "type": "checkbox",
        "label": "Delinquent AMEX Card Closure Confirmation (above 6 aging card)",
        "grid": 12,

    },
    {" varName":"delinquentCardOSConfirmation",
        "type":"checkbox",
        "label":"Delinquent Card O/S Confirmation (above 6 aging card)",
        "grid":12,

    },


    {"varName":"delinquentCreditCardActive",
        "type":"checkbox",
        "label":"Delinquent Credit Card Active / Reissue Feedback from Collection",
        "grid":12,

    },

    {
        "varName": "delinquentVISACardClosureConfirmation",
        "type": "checkbox",
        "label": "Delinquent VISA Card Closure Confirmation (above 6 aging card)",
        "grid": 12,
    },

    {" varName":"eFTNCancelationRequest",
        "type":"checkbox",
        "label":"EFTN Cancelation Request (Delinquent)",
        "grid":12,

    },

    {
        "varName": "nOCNODrequestirregularCard",
        "type": "checkbox",
        "label": "NOC/NOD request for irregular Card (state 4 to 14, 92,98,99)",
        "grid": 12,

    },

    {
        "varName": "nOCNODrequestwrittenoffCard",
        "type": "checkbox",
        "label": "NOC/NOD request for written off Card (state -91)",
        "grid": 12,

    },

    {
        "varName": "aLEbyBackOffice",
        "type": "checkbox",
        "label": "ALE by Back Office",
        "grid": 12,

    },

    {
        "varName": "contractclosureBackOffice",
        "type": "checkbox",
        "label": "Contract closure of Back Office",
        "grid": 12,

    },

    {
        "varName": "pGCBackOffice",
        "type": "checkbox",
        "label": "PGC by Back Office",
        "grid": 12,

    },

    {" varName":"pGCBackofficefromAmexlounge",
        "type":"checkbox",
        "label":"PGC by Back office from Amex lounge",
        "grid":12,

    },

    {
        "varName": "amexIntlLoungeFee",
        "type": "checkbox",
        "label": "Amex Int l Lounge Fee",
        "grid": 12,

    },
]

let BOMDebitCreditMaintenance = {};
BOMDebitCreditMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(debitCreditMaintenanceList)));


let BMDebitCreditMaintenance = {};
BMDebitCreditMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(debitCreditMaintenanceList)));


let MAKERDebitCreditMaintenance = {};
MAKERDebitCreditMaintenance = JSON.parse(JSON.stringify(debitCreditMaintenanceList));

let CheckerDebitCreditMaintenance = {};
CheckerDebitCreditMaintenance = makeReadOnlyObject(JSON.parse(JSON.stringify(debitCreditMaintenanceList)));


////////////maintenence/////////


//######BOM Account Opening NOnindividual#########
let BOMCommonjsonFormNonIndividualAccountOpeningSearch = [
    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,
        "email": true,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone",
        "required": false,
        "grid":6

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":6


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":6


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":6


    }


];
let BOMNewCommonjsonFormNonIndividualAccountOpeningSearch = {};
BOMNewCommonjsonFormNonIndividualAccountOpeningSearch = makeReadOnlyObject(JSON.parse(JSON.stringify(BOMCommonjsonFormNonIndividualAccountOpeningSearch.concat(CommonCsToBom))));


//######BM Account Opening individual#########
let BMJsonFormIndividualAccountOpening = {};
BMJsonFormIndividualAccountOpening["variables"] = makeReadOnlyObject(JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm))));
BMJsonFormIndividualAccountOpening.variables.push(bmApprove);
BMJsonFormIndividualAccountOpening.variables.push(bmSendTo);



//####SD Account Opening individual#######last update
let BMCommonjsonFormIndividualAccountOpeningSearch = [


    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid":6,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid":6,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required":true,
        "grid":6,

    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "required":true,
        "grid":6,


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",

        "email": true,
        "grid":6,


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required":true,
        "grid":6,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid":6,


    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid":6,


    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required":true,
        "grid":6,


    } ,




];
let BMjsonFormIndividualAccountOpeningSearch=makeReadOnlyObject(JSON.parse(JSON.stringify(BMCommonjsonFormIndividualAccountOpeningSearch)));
let SDCommonNewJsonFormIndividualAccountOpeningFinacle1 = [

    {
        // seal
        "varName": "relationshipBranchName",
        "type": "text",
        "label": "Relationship Branch Name",
        "grid":"12"

    },

    {
        "varName": "aOFDate",
        "type": "date",
        "label": "Date",
        "grid":"12"

    }, {
        "varName": "aOFBranchName",
        "type": "text",
        "label": "Branch Name",
        "grid":"12"

    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":"12"

    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "Account No",
        "grid":"12"

    },
    {
        "varName": "title",
        "type": "text",
        "label": "Title ",
        "grid":"12"

    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Short Name",
        "grid":"12"

    },
    {
        // tick
        "varName": "accountType",
        "type": "text",
        "label": "Type Of Account",
        "grid":"12"

    },
    {

        "varName": "purposeOfOpening",
        "type": "text",
        "label": "Purpose Of Opening",
        "grid":"12"

    },
    {
        // tick
        "varName": "natureOfAccount",
        "type": "text",
        "label": "Nature Of Account",
        "grid":"12"

    },
    {
        // tick
        "varName": "currency",
        "type": "text",
        "label": "Currency",
        "grid":"12"

    },
    {
        // Numeric & Check Box
        "varName": "initialDeposit",
        "type": "text",
        "label": "Initial Deposit",
        "grid":"12"

    },
    {
        "varName": "depositType",
        "type": "text",
        "label": "Deposit Type",
        "grid":"12"

    },
    {
        "varName": "commAddressSelection",
        "type": "text",
        "label": "Comm. Address Selection",
        "grid":"12"

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Desig. Email ID",
        "grid":"12"

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Desig. Mobile Number",
        "grid":"12"

    },
    {

        "varName": "adcRequest",
        "type": "title",
        "label": "ADC Request",
        "grid":"12"
    },
    {
        // tick
        "varName": "requiredService",
        "type": "text",
        "label": "Required Service(s)",
        "grid":"12"

    },
    {
        "varName": "emailForCityTouchService",
        "type": "text",
        "label": "Email for City Touch Service",
        "grid":"12"

    },
    {
        "varName": "preferredCityTouchUserID",
        "type": "text",
        "label": "Preferred City Touch User ID",
        "grid":"12"

    },
    {
        "varName": "desiredCreditCardNumberToBeTaggedWithCitytouch",
        "type": "text",
        "label": "Desired Credit Card Number to be tagged with Citytouch",
        "grid":"12"

    },
    {
        "varName": "facilitiesDetails",
        "type": "title",
        "label": "Facilities Details",
        "grid":"12"
    },
    {
        // tick
        "varName": "chequeBook",
        "type": "text",
        "label": "Cheque Book",
        "grid":"12"

    }, {
        // tick
        "varName": "debitCrad",
        "type": "text",
        "label": "Debit Crad",
        "grid":"12"

    }, {
        // tick
        "varName": "lockerFacility",
        "type": "text",
        "label": "Locker Facility",
        "grid":"12"

    },
    {
        "varName": "statementFacilities",
        "type": "text",
        "label": "Statement Facilities",
        "grid":"12"

    },
    {
        "varName": "cardDetails",
        "type": "title",
        "label": "Card Details",
        "grid":"12"
    },
    {
        // tick
        "varName": "cardApplicant",
        "type": "text",
        "label": "Card Applicant",
        "grid":"12"

    },
    {
        // tick
        "varName": "cardType",
        "type": "text",
        "label": "Card Type",
        "grid":"12"

    },
    {
        // tick
        "varName": "customerCategory",
        "type": "text",
        "label": "Customer Category",
        "grid":"12"

    },
    {
        "varName": "nameOfCard",
        "type": "text",
        "label": "Name of Card",
        "grid":"12"

    },
    {
        "varName": "operatingOn",
        "type": "text",
        "label": "Operating On",
        "grid":"12"

    },
    {
        "varName": "cardReceiveThrough",
        "type": "text",
        "label": "Card Receive Through",
        "grid":"12"

    },
    {
        "varName": "introducerInformation",
        "type": "title",
        "label": "Introducer Information",
        "grid":"12"
    },
    {
        "varName": "introName",
        "type": "text",
        "label": "Introducer Name",
        "grid":"12"

    },
    {
        "varName": "introAccountNumber",
        "type": "text",
        "label": "Introducer Account Number",
        "grid":"12"

    },
    {
        // Alpha Numeric
        "varName": "introducerCB",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid":"12"

    },
    {
        "varName": "relationshipWithApplicant",
        "type": "text",
        "label": "Relationship with Applicant",
        "grid":"12"

    },
    {
        "varName": "nomineeInformation",
        "type": "title",
        "label": "Nominee Information",
        "grid":"12"
    },
    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid":"12"

    },
    {
        "varName": "nomineeFathersName",
        "type": "text",
        "label": "Nominee Fathers Name",
        "grid":"12"

    }, {
        "varName": "nomineeMothersName",
        "type": "text",
        "label": "Nominee Mothers Name",
        "grid":"12"

    },
    {
        "varName": "nomineeSopuseName",
        "type": "text",
        "label": "Nominee Sopuse Name",
        "grid":"12"

    },
    {
        "varName": "nomineeOccupation",
        "type": "text",
        "label": "Nominee Occupation",
        "grid":"12"

    },
    {
        "varName": "nomineeRelationship",
        "type": "text",
        "label": "Relationship",
        "grid":"12"

    },
    {
        "varName": "nomineeDob",
        "type": "date",
        "label": "DOB",
        "grid":"12"

    }, {
        "varName": "nomineeBirthCertificate",
        "type": "text",
        "label": "Birth Certificate",
        "grid":"12"

    }, {
        "varName": "nomineeNid",
        "type": "text",
        "label": "NID",
        "grid":"12"

    },
    {
        "varName": "nomineePassport",
        "type": "text",
        "label": "Passport",
        "grid":"12"

    },
    {
        "varName": "nomineePassportExpDate",
        "type": "text",
        "label": "Passport Exp Date",
        "grid":"12"

    },
    {
        "varName": "nomineeDrivingLicense",
        "type": "text",
        "label": "Driving License",
        "grid":"12"

    },
    {
        "varName": "DLExpDate",
        "type": "text",
        "label": "DL. Exp Date",
        "grid":"12"

    },
    {
        "varName": "nomineeOtherPhotoID",
        "type": "text",
        "label": "Other Photo ID",
        "grid":"12"

    },
    {
        "varName": "nomineePresentAddress",
        "type": "text",
        "label": "Permanent/Present Address",
        "grid":"12"

    },
    {
        "varName": "nomineePostcode",
        "type": "text",
        "label": "Postcode",
        "grid":"12"

    },
    {
        "varName": "nomineePhoneNumber",
        "type": "text",
        "label": "Phone Number",
        "grid":"12"

    },
    {
        "varName": "gaurdianInformation",
        "type": "title",
        "label": "Gaurdian Information",
        "grid":"12"
    },
    {
        "varName": "gaurdianName",
        "type": "text",
        "label": "Gaurdian Name",
        "grid":"12"

    },
    {
        "varName": "Fathers/HusbandName",
        "type": "text",
        "label": "Fathers/Husband Name",
        "grid":"12"

    },
    {
        "varName": "gaurdianDob",
        "type": "date",
        "label": "DOB",
        "grid":"12"

    },
    {
        "varName": "relationshipWithNominee",
        "type": "text",
        "label": "Relationship With Nominee",
        "grid":"12"

    },
    {
        "varName": "legalGaurdianPhotoInformation",
        "type": "text",
        "label": "Legal Gaurdian Photo Information",
        "grid":"12"

    },
    {
        "varName": "forBankUseOnly",
        "type": "title",
        "label": "For Bank Use Only",
        "grid":"12"
    },
    {
        "varName": "RelationshipNoCustomerID",
        "type": "text",
        "label": "Relationship No/ Customer ID",
        "grid":"12"

    },
    {
        "varName": "forBankUseOnlySectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid":"12"

    },
    {
        "varName": "depositTypeCode",
        "type": "text",
        "label": "Deposit Type Code",
        "grid":"12"

    },
    {
        "varName": "accountOpeningChecklist",
        "type": "text",
        "label": "Account Opening Checklist",
        "grid":"12"

    }, {
        "varName": "identityVerifiedBy",
        "type": "text",
        "label": "Identity Verified By ",
        "grid":"12"

    }, {
        "varName": "addressVerifiedBy",
        "type": "text",
        "label": "Address Verified By",
        "grid":"12"

    },
    {
        "varName": "resultofSanctionBlackListScreening",
        "type": "text",
        "label": "Result of Sanction/Black List Screening",
        "grid":"12"

    },
    {
        // tick
        "varName": "howWasTheACOpened",
        "type": "text",
        "label": "How was the A/C opened",
        "grid":"12"

    },
    {
        "varName": "nameOfDMEBrEmployee",
        "type": "text",
        "label": "Name of DME/Br. Employee",
        "grid":"12"

    },
    {
        // tick
        "varName": "taxApplicable",
        "type": "text",
        "label": "Tax Applicable",
        "grid":"12"

    },
    {
        "varName": "forBranch",
        "type": "title",
        "label": "For Branch",
        "grid":"12"
    },
    {
        "varName": "savings/CurrentProductCode",
        "type": "text",
        "label": "Savings/Current Product Code",
        "grid":"12"

    },
    {
        "varName": "valueDate",
        "type": "date",
        "label": "Value Date",
        "grid":"12"

    },
    {
        "varName": "forBranchOthers",
        "type": "text",
        "label": "Others",
        "grid":"12"

    },
    {
        "varName": "fDProductCode",
        "type": "text",
        "label": "FD Product Code",
        "grid":"12"

    },
    {
        "varName": "branchSol",
        "type": "text",
        "label": "Branch Sol",
        "grid":"12"

    },
    {
        "varName": "workflowInput",
        "type": "title",
        "label": "Workflow Input",
        "grid":"12"
    },
    {
        "varName": "debitCard",
        "type": "text",
        "label": "Debit Card",
        "grid":"12"

    },
    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone 1",
        "grid":"12"

    },
    {
        "varName": "phone2",
        "type": "text",
        "label": "Phone 2",
        "grid":"12"

    },
    {
        "varName": "smsAlertRequest",
        "type": "select",
        "label": "Sms Alert Request",
        "grid":"12",
        "enum": [
            "YES",
            "NO"
        ]

    }, {
        "varName": "companyCode",
        "type": "text",
        "label": "Company Code",
        "grid":"12"

    },
    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "grid":"12"

    },
    {
        "varName": "kyc",
        "type": "title",
        "label": "KYC",
        "grid":"12"

    },
    {
        "varName": "kycSourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid":"12"

    }, {
        "varName": "dOCCollect",
        "type": "text",
        "label": "DOC Collect",
        "grid":"12"

    }, {
        "varName": "collectedDOCHaveBeenVerified ",
        "type": "text",
        "label": "Collected DOC have been Verified",
        "grid":"12"

    },
    {
        "varName": "howTheAddressIsVerified",
        "type": "text",
        "label": "How the Address is verified",
        "grid":"12"

    },
    {
        "varName": "hasBeneficialOwnerBeenIdentified",
        "type": "text",
        "label": "Has the Beneficial Owner of the a/c been identified",
        "grid":"12"

    },
    {
        "varName": "identification",
        "type": "title",
        "label": "Identification",
        "grid":"12"

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport No",
        "grid":"12"

    },
    {
        "varName": "nid",
        "type": "text",
        "label": "NID No",
        "grid":"12"

    },
    {
        "varName": "birthCertificateNo",
        "type": "text",
        "label": "Birth Certificate No",
        "grid":"12"

    },
    {
        "varName": "identificationEtin",
        "type": "text",
        "label": "E-TIN",
        "grid":"12"

    },
    {
        "varName": "drivingLicense",
        "type": "text",
        "label": "Driving License No",
        "grid":"12"

    },
    {
        "varName": "identificationOtherDocumentation",
        "type": "text",
        "label": "Other Documentation",
        "grid":"12"

    },
    {

        "varName": "reasonForA/COpeningOfForeignCompany ",
        "type": "text",
        "label": "Reason for A/C opening of Foreign Company",
        "grid":"12"

    },
    {
        "varName": "typeOfVISA",
        "type": "text",
        "label": "Type Of VISA",
        "grid":"12"

    },
    {
        "varName": "identificationExpDate",
        "type": "date",
        "label": "Exp Date",

        "grid":"12"

    },
    {
        "varName": "workPermitAndPermission",
        "type": "text",
        "label": "Work Permit and Permission",
        "grid":"12"

    },

    {
        "varName": "PEP/IP",
        "type": "text",
        "label": "PEP/IP ?",
         "grid":"12"


    },
    {
        "varName": "iFYESThen",
        "type": "text",
        "label": "IF YES Then",
        "grid":"12"


    },

    {
        "varName": "anyMatchOfTerroristActivity",
        "type": "text",
        "label": "Any Match Of Terrorist Activity",
        "grid":"12"

    },
    {
        "varName": "riskRating",
        "type": "title",
        "label": "Risk Rating",
        "grid":"12"
    },
    {
        // tick
        "varName": "businessCustomerEngaged",
        "type": "text",
        "label": "What does the Customer do/in What Type of Business Is The Customer Engaged",
        "grid":"12"
    },
    {
        // tick
        "varName": "customerMonthlyIncome",
        "type": "text",
        "label": "Customer's Monthly Income",
        "grid":"12"

    },
    {
        "varName": "expectedAmountOfMonthlyTotalTransaction",
        "type": "text",
        "label": "Expected Amount of Monthly Total Transaction",
        "grid":"12"

    },
    {
        "varName": "expectedAmountOfMonthlyCashTransaction",
        "type": "text",
        "label": "Expected Amount of Monthly Cash Transaction",
        "grid":"12"

    },
    {
        "varName": "totalRiskScore",
        "type": "text",
        "label": "Total Risk Score",
        "grid":"12"

    },
    {
        "varName": "riskRatingComments",
        "type": "text",
        "label": "Comments",
        "grid":"12"

    },
    {
        "varName": "tp",
        "type": "title",
        "label": "TP",
        "grid":"12"
    },
    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid":"12"

    },
    {
        "varName": "deposit",
        "type": "title",
        "label": "Deposit",
        "grid":"12"
    },
    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid":"12"

    }, {
        "varName": "depositByTransfer",
        "type": "text",
        "label": "Deposit by Transfer",
        "grid":"12"

    },
    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid":"12"

    },
    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income from Export",
        "grid":"12"

    },
    {
        "varName": "Deposit/TransferFromBOA/C",
        "type": "text",
        "label": "Deposit/Transfer from BO A/C",
        "grid":"12"

    },
    {
        "varName": "depositOthers",
        "type": "text",
        "label": "Others",
        "grid":"12"

    },
    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid":"12"

    },
    {
        "varName": "withdraw",
        "type": "title",
        "label": "Withdraw",
        "grid":"12"
    },
    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid":"12"

    },
    {
        "varName": "withdrawalThroughTransfer/Instrument",
        "type": "text",
        "label": "Withdrawal Through Transfer/Instrument",
        "grid":"12"

    },
    {

        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid":"12"

    },
    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid":"12"

    },
    {
        "varName": "deposit/TransferToBO",
        "type": "text",
        "label": "Deposit/Transfer to BO A/C",
        "grid":"12"

    },
    {

        "varName": "withdrawOthers",
        "type": "text",
        "label": "Others",
        "grid":"12"

    },
    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid":"12"

    }


];
let SDCommonNewJsonFormIndividualAccountOpeningFinacle = [
    {
        // seal
        "varName": "relationshipBranchName",
        "type": "text",
        "label": "Relationship Branch Name",
        "grid":6
    },
    {
        "varName": "aOFDate",
        "type": "date",
        "label": "Date",
        "grid":6
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":6
    },
    {
        "varName": "aOFBranchName",
        "type": "text",
        "label": "Branch Name",
        "grid":6
    },

    {
        "varName": "accountNo",
        "type": "text",
        "label": "Account No",
        "grid":6
    },
    {
        "varName": "title",
        "type": "text",
        "label": "Title ",
        "grid":6
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Short Name",
        "grid":6
    },
    {
        // tick
        "varName": "accountType",
        "type": "select",
        "label": "Type Of Account",
        "grid":6,
        "enum": [
            "General Savings A/C",
            "Current A/C",
            "SND A/C",
            "Alo General Savings A/C",
            "Alo Savings Delight",
            "Alo High Value Savings",
            "Seniors' Savings A/C",
            "Savings Delight A/C",
            "Fixed Deposit A/C",
            "RFCD A/C",
            "Foreign Currency A/C",
            "Students Savings A/C School Plan",
            "Students Savings A/C College Plan",
            "Others"
        ],
    },
    {

        "varName": "purposeOfOpening",
        "type": "text",
        "label": "Purpose Of Opening",
        "grid":6
    },
    {
        // tick
        "varName": "natureOfAccount",
        "type": "select",
        "label": "Nature Of Account",
        "grid":6,
        "enum": [
            "Individual",
            "Joint"
        ]
    },
    {
        // tick
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "grid":6,
        "enum": [
            "BDT",
            "USD",
            "Euro",
            "GBP",
            "Others"
        ]
    },
    {
        // Numeric & Check Box
        "varName": "initialDeposit",
        "type": "text",
        "label": "Initial Deposit",
        "grid":6
    },
    {
        "varName": "depositType",
        "type": "select",
        "label": "Deposit Type",
        "grid":6,
        "enum": [
            "Cash",
            "Cheque",
            "Transfer"
        ]
    },
    {
        "varName": "commAddressSelection",
        "type": "select",
        "label": "Comm. Address Selection",
        "grid":6,
        "enum": [
            "Present Address(Residence)",
            "Office/Business Address",
            "Permanent Address"
        ]
    },
    {
        "varName": "email",
        "type": "text",
        "label": "Desig. Email ID",
        "grid":6
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Desig. Mobile Number",
        "grid":6
    },
    {

        "varName": "adcRequest",
        "type": "title",
        "label": "ADC Request",
        "grid":12
    },
    {
        // tick
        "varName": "requiredService",
        "type": "select",
        "label": "Required Service(s)",
        "grid":6,
        "enum": [
            "City Touch",
            "Call Center",
            "SMS Alert",
            "Others"
        ],
    },
    {
        "varName": "emailForCityTouchService",
        "type": "text",
        "label": "Email for City Touch Service",
        "grid":6
    },
    {
        "varName": "preferredCityTouchUserID",
        "type": "text",
        "label": "Preferred City Touch User ID",
        "grid":6
    },
    {
        "varName": "desiredCreditCardNumberToBeTaggedWithCitytouch",
        "type": "title",
        "label": "Desired Credit Card Number to be tagged with CityTouch Id",
        "grid":12
    },
    {
        "varName": "creditCardNumber",
        "type": "text",
        "label": "Credit Card Number(s)",
        "grid":6
    },
    {
        "varName": "clientID",
        "type": "text",
        "label": "Client ID",
        "grid":6
    },
    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid":6,
        "enum": [
            "AMEX",
            "VISA",
            "Others"
        ]
    },
    {
        "varName": "facilitiesDetails",
        "type": "title",
        "label": "Facilities Details",
        "grid":12
    },
    {
        // tick
        "varName": "chequeBook",
        "type": "select",
        "label": "Cheque Book",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    }, {
        // tick
        "varName": "debitCrad",
        "type": "select",
        "label": "Debit Crad",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    }, {
        // tick
        "varName": "lockerFacility",
        "type": "select",
        "label": "Locker Facility",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    {
        "varName": "statementFacilities",
        "type": "select",
        "label": "Statement Facilities",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    /////////end of 1 page \\\\\\\\\\\\\\\
    {
        "varName": "cardDetails",
        "type": "title",
        "label": "Card Details",
        "grid":12
    },
    {
        // tick
        "varName": "cardApplicant",
        "type": "select",
        "label": "Card Applicant",
        "grid":6,
        "enum": [
            "1st Applicant",
            "2nd Applicant",
            "Others"
        ]
    },
    {
        // tick
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid":6,
        "enum": [
            "MC Debit Card",
            "VISA Debit Card",
            "City Max Card",
            "Others"
        ]
    },
    {
        // tick
        "varName": "customerCategory",
        "type": "select",
        "label": "Customer Category",
        "grid":6,
        "enum": [
            "Individual",
            "Staff",
            "Corporate-Name of the Organization"
        ]
    },
    {
        "varName": "nameOfCard",
        "type": "text",
        "label": "Name of Card",
        "grid":6
    },
    {
        "varName": "operatingOn",
        "type": "text",
        "label": "Operating On",
        "grid":6
    },
    {
        "varName": "cardReceiveThrough",
        "type": "text",
        "label": "Card Receive Through",
        "grid":6
    },
    {
        "varName": "introducerInformation",
        "type": "title",
        "label": "Introducer Information",
        "grid":12
    },
    {
        "varName": "introName",
        "type": "text",
        "label": "Introducer Name",
        "grid":6
    },
    {
        "varName": "introAccountNumber",
        "type": "text",
        "label": "Account Number",
        "grid":6
    },
    {
        // Alpha Numeric
        "varName": "introducerCB",
        "type": "text",
        "label": "Customer ID",
        "grid":6
    },
    {
        "varName": "relationshipWithApplicant",
        "type": "text",
        "label": "Relationship with Applicant",
        "grid":6
    },
    {
        "varName": "nomineeInformation",
        "type": "title",
        "label": "Nominee Information",
        "grid":12
    },
    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid":6
    },
    {
        "varName": "nomineeFathersName",
        "type": "text",
        "label": "Fathers Name",
        "grid":6
    }, {
        "varName": "nomineeMothersName",
        "type": "text",
        "label": "Mothers Name",
        "grid":6
    },
    {
        "varName": "nomineeSopuseName",
        "type": "text",
        "label": "Sopuse Name",
        "grid":6
    },
    {
        "varName": "nomineeOccupation",
        "type": "text",
        "label": "Occupation",
        "grid":6
    },
    {
        "varName": "nomineeRelationship",
        "type": "text",
        "label": "Relationship",
        "grid":6
    },
    {
        "varName": "nomineeDob",
        "type": "date",
        "label": "DOB",
        "grid":6
    }, {
        "varName": "nomineeBirthCertificate",
        "type": "text",
        "label": "Birth Certificate",
        "grid":6
    }, {
        "varName": "nomineeNid",
        "type": "text",
        "label": "NID",
        "grid":6
    },
    {
        "varName": "nomineePassport",
        "type": "text",
        "label": "Passport",
        "grid":6
    },
    {
        "varName": "nomineePassportExpDate",
        "type": "text",
        "label": "Passport Exp Date",
        "grid":6
    },
    {
        "varName": "nomineeDrivingLicense",
        "type": "text",
        "label": "Driving License",
        "grid":6
    },
    {
        "varName": "DLExpDate",
        "type": "text",
        "label": "DL. Exp Date",
        "grid":6
    },
    {
        "varName": "nomineeOtherPhotoID",
        "type": "text",
        "label": "Other Photo ID",
        "grid":6
    },
    {
        "varName": "nomineePresentAddress",
        "type": "text",
        "label": "Permanent/Present Address",
        "grid":6
    },
    {
        "varName": "nomineePostcode",
        "type": "text",
        "label": "Postcode",
        "grid":6
    },
    {
        "varName": "nomineePhoneNumber",
        "type": "text",
        "label": "Phone Number",
        "grid":6
    },
    {
        "varName": "gaurdianInformation",
        "type": "title",
        "label": "Gaurdian Information",
        "grid":12
    },
    {
        "varName": "gaurdianName",
        "type": "text",
        "label": "Gaurdian Name",
        "grid":6
    },
    {
        "varName": "Fathers/HusbandName",
        "type": "text",
        "label": "Fathers/Husband Name",
        "grid":6
    },
    {
        "varName": "gaurdianDob",
        "type": "date",
        "label": "DOB",
        "grid":6
    },
    {
        "varName": "relationshipWithNominee",
        "type": "text",
        "label": "Relationship With Nominee",
        "grid":6
    },
    {
        "varName": "legalGaurdianPhotoInformation",
        "type": "text",
        "label": "Legal Gaurdian Photo Information",
        "grid":6
    },
    //////////  end 2 page //////////
    {
        "varName": "forBankUseOnly",
        "type": "title",
        "label": "For Bank Use Only",
        "grid":12
    },
    {
        "varName": "RelationshipNoCustomerID",
        "type": "text",
        "label": "Relationship No/ Customer ID",
        "grid":6
    },
    {
        "varName": "forBankUseOnlySectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid":6
    },
    {
        "varName": "depositTypeCode",
        "type": "text",
        "label": "Deposit Type Code",
        "grid":6
    },
    {
        "varName": "accountOpeningChecklist",
        "type": "text",
        "label": "Account Opening Checklist",
        "grid":6
    }, {
        "varName": "identityVerifiedBy",
        "type": "select",
        "label": "Identity Verified By ",
        "grid":6,
        "enum": [
            "Passport",
            "Birth Certificate",
            "National ID Card",
            "Other"
        ],
    }, {
        "varName": "addressVerifiedBy",
        "type": "text",
        "label": "Address Verified By",
        "grid":6
    },
    {
        "varName": "resultofSanctionBlackListScreening",
        "type": "title",
        "label": "Result of Sanction/Black List Screening",
        "grid":12
    },
    {
        "varName": "uNSecurityCouncil",
        "type": "select",
        "label": "UN Security Council Sanction list",
        "grid":6,
        "enum": [
            "Match Found",
            "No Match Found"]
    },
    {
        "varName": "blackListCreditDepartment",
        "type": "select",
        "label": "Black List provided by the Credit Department",
        "grid":6,
        "enum": [
            "Match Found",
            "No Match Found"]
    },
    {
        "varName": "restrictionByLocalLaw",
        "type": "select",
        "label": "Restriction by Local Law/Bangladesh Bank to Open an A/C",
        "grid":6,
        "enum": [
            "Match Found",
            "No Match Found"]
    },
    {
        // tick
        "varName": "howWasTheACOpened",
        "type": "select",
        "label": "How was the A/C opened",
        "grid":6,
        "enum": [
            "By RM/Branch Employee",
            "By DME",
            "Internet",
            "Walk-in/Unsolicited"
        ],
    },
    {
        "varName": "nameOfDMEBrEmployee",
        "type": "text",
        "label": "Name of DME/Br. Employee",
        "grid":6
    },
    {
        // tick
        "varName": "taxApplicable",
        "type": "select",
        "label": "Tax Applicable",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    ///  for branch  ////////////
    {
        "varName": "forBranch",
        "type": "title",
        "label": "For Branch",
        "grid":12
    },
    {
        "varName": "savings/CurrentProductCode",
        "type": "checkbox",
        "label": "Savings/Current Product Code",
        "grid":6
    },
    {
        "varName": "fDProductCode",
        "type": "checkbox",
        "label": "FD Product Code",
        "grid":6
    },
    {
        "varName": "valueDate",
        "type": "checkbox",
        "label": "Value Date",
        "grid":4
    },
    {
        "varName": "branchSol",
        "type": "checkbox",
        "label": "Branch Sol",
        "grid":4
    },
    {
        "varName": "forBranchOthers",
        "type": "checkbox",
        "label": "Others",
        "grid":4
    },


    /// end 4 page  ///
    {
        "varName": "tp",
        "type": "title",
        "label": "TP",
        "grid":12
    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid":6
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":6
    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "A/C No.",
        "grid":6
    },
    {
        "varName": "AcTitle",
        "type": "text",
        "label": "A/C Title",
        "grid":6
    },
    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid":6
    },
    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid":6
    },
    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid":6
    },
    {
        "varName": "deposit",
        "type": "title",
        "label": "Deposit",
        "grid":12
    },
    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid":6
    }, {
        "varName": "depositByTransfer",
        "type": "text",
        "label": "Deposit by Transfer",
        "grid":6
    },
    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid":6
    },
    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income from Export",
        "grid":6
    },
    {
        "varName": "Deposit/TransferFromBOA/C",
        "type": "text",
        "label": "Deposit/Transfer from BO A/C",
        "grid":6
    },
    {
        "varName": "depositOthers",
        "type": "text",
        "label": "Others",
        "grid":6
    },
    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid":6
    },
    {
        "varName": "withdraw",
        "type": "title",
        "label": "Withdraw",
        "grid":12
    },
    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid":6
    },
    {
        "varName": "withdrawalThroughTransfer/Instrument",
        "type": "text",
        "label": "Withdrawal Through Transfer/Instrument",
        "grid":6
    },
    {

        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid":6
    },
    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid":6
    },
    {
        "varName": "deposit/TransferToBO",
        "type": "text",
        "label": "Deposit/Transfer to BO A/C",
        "grid":6
    },
    {

        "varName": "withdrawOthers",
        "type": "text",
        "label": "Others",
        "grid":6
    },
    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid":6
    },
    ////end page//////////////
    {
        "varName": "title",
        "type": "title",
        "label": "Individual Information Form",
        "grid":12
    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid":6
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":6
    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "A/C No.",
        "grid":6
    },
    {
        "varName": "relationWithTheAC",
        "type": "select",
        "label": "Relation with the A/C",
        "grid":6,
        "enum": [
            "1st Applicant",
            "2nd Applicant",
            "3rd Applicant",
            "director",
            "A/C Operator",
            "Partner",
            "Beneficial Owner",
            "Minor",
            "Guardian",
            "Trustee",
            "Attorney Holder",
            "Proprietor",
            "Others"
        ],
    },
    {
        "varName": "accountName",
        "type": "text",
        "label": "Account Name",
        "grid":6
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer's Name",
        "grid":6
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Relation",
        "grid":12
    },
    {
        "varName": "fatherName",
        "type": "text",
        "label": "Father's Name",
        "grid":6
    },
    {
        "varName": "motherName",
        "type": "text",
        "label": "Mother's Name",
        "grid":6
    },
    {
        "varName": "spouseName",
        "type": "text",
        "label": "Spouse' Name",
        "grid":6
    }, {
        "varName": "dob",
        "type": "date",
        "label": "DOB",
        "grid":6
    },
    {
        "varName": "religion",
        "type": "text",
        "label": "Religion",
        "grid":6
    },
    {
        "varName": "gender",
        "type": "select",
        "label": "Gender",
        "grid":6,
        "enum": [
            "Male",
            "Female",
            "Third Gender",
            "Address proof"
        ],
    },
    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid":6
    },
    {
        "varName": "birthCertificate",
        "type": "text",
        "label": "Birth certificate",
        "grid":6
    },
    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident status",
        "grid":6,
        "enum": [
            "Resident",
            "Non-Resident"
        ],
    },
    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid":6
    },
    {
        "varName": "passportIssueDate",
        "type": "date",
        "label": "Issue Date",
        "grid":6
    },
    {
        "varName": "passportExpDate",
        "type": "date",
        "label": "Exp Date",
        "grid":6
    },
    {
        "varName": "passportPlaceOfIssue",
        "type": "text",
        "label": "Place of issue",
        "grid":6
    },
    {
        "varName": "drivingLicenseNo",
        "type": "text",
        "label": "Driving License no",
        "grid":6
    },
    {
        "varName": "drivingLicenseExpDate",
        "type": "date",
        "label": "Exp Date",
        "grid":6
    },
    {
        "varName": "otherPhotoId",
        "type": "text",
        "label": "Other photo id",
        "grid":6
    },
    {
        "varName": "etin",
        "type": "text",
        "label": "E-TIN",
        "grid":6
    },
    {
        "varName": "nationality",
        "type": "select",
        "label": "Nationality",
        "grid":6,
        "enum": [
            "Bangladeshi",
            "Others"
        ],
    },
    {
        "varName": "placeOfBirth",
        "type": "text",
        "label": "Place of Birth",
        "grid":6
    },
    {
        "varName": "countryOfBirth",
        "type": "text",
        "label": "Country of Birth",
        "grid":6
    },
    {
        "varName": "profession",
        "type": "text",
        "label": "Profession",
        "grid":6
    },
    {
        "varName": "nameOfTheOrganization",
        "type": "text",
        "label": "Name of the Organization",
        "grid":6
    },
    {
        "varName": "designation",
        "type": "text",
        "label": "Designation",
        "grid":6
    },
    {
        "varName": "natureOfBusiness",
        "type": "text",
        "label": "Nature of Business",
        "grid":6
    },
    ////end page//////
    {
        "varName": "presentAddress",
        "type": "text",
        "label": "Present Address",
        "grid":6
    },
    {
        "varName": "presentAddressPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":6
    },
    {
        "varName": "presentAddressCountry",
        "type": "text",
        "label": "Country",
        "grid":6
    },
    {

        "varName": "presentAddressLandmark",
        "type": "text",
        "label": "Landmark",
        "grid":6
    },
    {
        "varName": "professionalAddress",
        "type": "text",
        "label": "Professional Address",
        "grid":6
    },
    {

        "varName": "professionalAddressPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":6
    },
    {
        "varName": "professionalAddressCountry",
        "type": "text",
        "label": "Country",
        "grid":6
    },
    {
        "varName": "professionalAddressLandmark",
        "type": "text",
        "label": "Landmark",
        "grid":6
    },
    {
        "varName": "permanentAddress",
        "type": "text",
        "label": "Permanent Address",
        "grid":6
    },
    {
        "varName": "permanentAddressPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":6
    },
    {
        "varName": "permanentAddressCountry",
        "type": "text",
        "label": "Country",
        "grid":6
    },
    {
        "varName": "permanentAddressLandmark",
        "type": "text",
        "label": "Landmark",
        "grid":6
    },
    {
        "varName": "phoneNumberResidence",
        "type": "text",
        "label": "Phone Number(Residence)",
        "grid":6
    },
    {
        "varName": "phoneNumberOffice",
        "type": "text",
        "label": "Phone Number(Office)",
        "grid":6
    },
    {
        "varName": "mobile",
        "type": "text",
        "label": "Mobile",
        "grid":6
    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ID",
        "grid":6
    },
    {
        "varName": "emergencyContact",
        "type": "title",
        "label": "Emergency Contact",
        "grid":12
    },
    {
        "varName": "emergencyContactName",
        "type": "text",
        "label": "Name",
        "grid":6
    },
    {
        "varName": "emergencyContactAddress",
        "type": "text",
        "label": "Address",
        "grid":6
    },
    {
        "varName": "emergencyContactEmailId",
        "type": "text",
        "label": "Email ID",
        "grid":6
    },
    {
        "varName": "emergencyContactMobile",
        "type": "text",
        "label": "Mobile",
        "grid":6
    },
    {
        "varName": "emergencyContactRelationshipWithAcHolder",
        "type": "text",
        "label": "Relationship With A/C Holder",
        "grid":6
    },
    {
        "varName": "creditCardInformation",
        "type": "title",
        "label": "Credit Card Information of Account Holder",
        "grid":12
    },
    {
        "varName": "issuedByLocal",
        "type": "text",
        "label": "Issued By (Local)",
        "grid":6
    },
    {
        "varName": "issuedByInternational",
        "type": "text",
        "label": "Issued By(International)",
        "grid":6
    },
    {
        "varName": "fatcaInformation",
        "type": "text",
        "label": "FATCA Information",
        "grid":6
    },
    ////end page //////////////
    {
        "varName": "kyc",
        "type": "title",
        "label": "KYC",
        "grid":12
    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid":6
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":6
    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "A/C No.",
        "grid":6
    },
    {
        "varName": "AcTitle",
        "type": "text",
        "label": "A/C Title",
        "grid":6
    },
    {
        "varName": "typeOfAc",
        "type": "select",
        "label": "Type of A/C",
        "grid":6,
        "enum": [
            "Savings A/C",
            "Current A/C",
            "SND A/C",
            "NFCD A/C",
            "RFCD A/C",
            "Foreign Currency A/c",
            "other"
        ],
    },
    {
        "varName": "customerOccupation",
        "type": "text",
        "label": "Customer occupation",
        "grid":6
    },
    {
        "varName": "customerProbableMonthlyIncome ",
        "type": "text",
        "label": "Customer Probable Monthly Income",
        "grid":6
    },
    {
        "varName": "kycSourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid":6
    },
    {
        "varName": "dOCCollect",
        "type": "text",
        "label": "DOC collect to Ensure Source of Fund",
        "grid":6
    },
    {
        "varName": "collectedDOCHaveBeenVerified ",
        "type": "select",
        "label": "Collected DOC have been Verified",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    {
        "varName": "hasBeneficialOwnerBeenIdentified",
        "type": "select",
        "label": "Has the Beneficial Owner of the a/c been identified",
        "grid":6,
        "enum": [
            "YES",
            "NO",
            "Not Applicable"
        ]
    },
    {
        "varName": "howTheAddressIsVerified",
        "type": "text",
        "label": "How the Address is verified",
        "grid":6
    },

    {
        "varName": "identification",
        "type": "title",
        "label": "Identification",
        "grid":12
    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport No",
        "grid":12
    },
    {
        "varName": "passportReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "passportVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {
        "varName": "nid",
        "type": "text",
        "label": "NID No",
        "grid":12
    },
    {
        "varName": "nidReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "nidVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {
        "varName": "birthCertificateNo",
        "type": "text",
        "label": "Birth Certificate No",
        "grid":12
    },
    {
        "varName": "birthCertificateReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "birthCertificateVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {
        "varName": "etin",
        "type": "text",
        "label": "E-TIN",
        "grid":12
    },
    {
        "varName": "etinReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "etinVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {
        "varName": "drivingLicense",
        "type": "text",
        "label": "Driving License No",
        "grid":12
    },
    {
        "varName": "drivingLicenseReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "drivingLicenseVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {
        "varName": "otherDocumentation",
        "type": "text",
        "label": "Other Documentation",
        "grid":12
    },
    {
        "varName": "otherDocumentationReceivedCopy",
        "type": "checkbox",
        "label": "Received Copy",
        "grid":6
    },
    {
        "varName": "otherDocumentationVerified",
        "type": "checkbox",
        "label": "Verified",
        "grid":6
    },
    {

        "varName": "reasonForA/COpeningOfForeignCompany ",
        "type": "text",
        "label": "Reason for A/C opening of Foreign Company",
        "grid":6
    },
    {
        "varName": "typeOfVISA",
        "type": "text",
        "label": "Type Of VISA",
        "grid":6
    },
    {
        "varName": "identificationExpDate",
        "type": "date",
        "label": "Exp Date",
        "grid":6
    },
    {
        "varName": "workPermitAndPermission",
        "type": "select",
        "label": "Work Permit and Permission",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },

    {
        "varName": "PEP/IP",
        "type": "text",
        "label": "PEP/IP ?",
        "grid":6
    },
    {
        "varName": "iFYESThen",
        "type": "text",
        "label": "IF YES Then",
        "grid":6
    },

    {
        "varName": "anyMatchOfTerroristActivity",
        "type": "text",
        "label": "Any Match Of Terrorist Activity",
        "grid":6,
        "enum": [
            "YES",
            "NO"
        ]
    },
    ///// end page //////////////
    {
        "varName": "riskRating",
        "type": "title",
        "label": "Risk Rating",
        "grid":12,

    },
    {
        // tick
        "varName": "businessCustomerEngaged",
        "type": "select",
        "label": "What does the Customer do/in What Type of Business Is The Customer Engaged",
            "grid":6,
    "enum": [
    "Jewelry/ Gold Business/Valuable Metal Business",
    "Money Changer/ Courier Service/Mobile Banking Agent",
    "Real Estate Developer/Agent",
    "Promoter of Construction Project/Constructor",
    "Art/Antic Dealer",
    "Restaurant/Bar/Night club/Residential Hotel/Parlor Business",
    "Import/Export",
    "Manpower Export Business",
    "Arms Business",
    "Garments Business/Garments Accessories/Buying House",
    "Pilot/Flight Attended",
    "Trusty",
    "Share/Stock Business Investor",
    "Software Business/information & Technology Business",
    "Expatriate(Foreign Working in Bangladesh)",
    "Travel Agent",
    "Investor with Annual More than 1 Crore Investment",
    "Freight/Shipping/Cargo Agent",
    "Auto Business (New/Reconditioned Car) Business",
    "Business(Leather and Leather Related Goods)",
    "House Construction Materials Business",
    "Professional (journalist,Lawyer,Doctor,Engineer,Chartered Accountant)",
"Director(Private/Public Limited Company)",
    "High level Official of Multinational Organization",
    "Housewives",
    "Employment in Information & Technology",
    "Player/Media Celebrity/Producer/Director",
    "Freelance Software Developer",
    "Business Agent",
    "Government Job",
    "Landlord",
    "Thread Business/Jute Business",
    "Transport Operator",
    "Tobacco & Cigarette Business",
    "Amusement Organization/Park",
    "Motor Parts/Workshop Business",
    "Private Service Managerial",
    "Teacher(Government/Private/Autonomous Educational Institution)",
    "Service(Private)",
    "Small Business(Yearly Turnover Under 50 Lakh)",
    "Self-employed Professional",
    "Computer/Mobile Phone Dealer",
    "Manufacturer(Except Firearms)",
    "Student",
    "Retired from Service",
    "Farmer/Worker/Fisher",
    "Other"

]
},
{
    // tick
    "varName": "customerMonthlyIncome",
    "type": "select",
    "label": "Customer's Monthly Income",
    "grid":6,
    "enum": [
    "Upto 1 lakh",
    ">1 to 3 lakh",
    "more than 3 lakh"
]
},
{
    "varName": "expectedAmountOfMonthlyTotalTransaction",
    "type": "title",
    "label": "Expected Amount of Monthly Total Transactions",
    "grid":12,

},
{
    "varName": "expectedAmountOfMonthlyTotalTransaction",
    "type": "select",
    "label": "Amount of Transaction in Current A/C (in Lacs)",
    "grid":6,
    "enum": [
    "0-10",
    ">10-20",
    ">20"
]


},
{
    "varName": "expectedAmountOfMonthlyTotalTransaction",
    "type": "select",
    "label": "Amount of Transaction in Savings A/C (in Lacs)",
    "grid":6,
    "enum": [
    "0-5",
    ">5-10",
    ">10"
]

},
{
    "varName": "expectedAmountOfMonthlyCashTransaction",
    "type": "title",
    "label": "Expected Amount of Monthly Cash Transaction",
    "grid":12
},
{
    "varName": "expectedAmountOfMonthlyTotalTransaction",
    "type": "select",
    "label": "Amount of Cash Transaction in Current A/C (in Lacs)",
    "grid":6,
    "enum": [
    "0-5",
    ">5-10",
    ">10"
]


},
{
    "varName": "expectedAmountOfMonthlyTotalTransaction",
    "type": "select",
    "label": "Amount of Cash Transaction in Saving A/C (in Lacs)",
    "grid":6,
    "enum": [
    "0-2",
    ">2-5",
    ">5"
]


},
{
    "varName": "totalRiskScore",
    "type": "select",
    "label": "Total Risk Score",
    "grid":6,
    "enum": [
    "HIGH",
    "LOW"
]
},
{
    "varName": "riskRatingComments",
    "type": "text",
    "label": "Comments",
    "grid":6
}


];



let MAKERJsonFormIndividualAccountOpening = {};
MAKERJsonFormIndividualAccountOpening=JSON.parse(JSON.stringify(SDCommonNewJsonFormIndividualAccountOpeningFinacle));


let CHECKERNewJsonFormIndividualAccountOpeningFinacle = {};
CHECKERNewJsonFormIndividualAccountOpeningFinacle= makeReadOnlyObject(JSON.parse(JSON.stringify(SDCommonNewJsonFormIndividualAccountOpeningFinacle)));
//CHECKERNewJsonFormIndividualAccountOpeningFinacle.variables.push(checkerApprove)


//####SD Account Opening Non-individual#######last update
let BMCommonjsonFormNonIndividualAccountOpeningSearch = [

    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,
        "email": true,
        "readOnly": false,
        "grid":6


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone",
        "required": false,
        "grid":6

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":6


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":6


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":6


    }




];
let BMjsonFormNonIndividualAccountOpeningSearch=makeReadOnlyObject(JSON.parse(JSON.stringify(BMCommonjsonFormNonIndividualAccountOpeningSearch)));
let SDCommonNewJsonFormNonIndividualAccountOpeningFinacle =   [


    {
        // seal
        "varName": "relationshipBranchName",
        "type": "text",
        "label": "Relationship Branch Name",
        "grid":12

    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid":12

    },
    {
        "varName": "branchName",
        "type": "text",
        "label": "Branch Name",
        "grid":12
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":12
    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "Account No",
        "grid":12
    },
    {
        "varName": "title",
        "type": "text",
        "label": "Title",
        "grid":12
    },
    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid":12
    },
    {
        "varName": "purposeOfOpening",
        "type": "text",
        "label": "Purpose of Opening",
        "grid":12
    },
    {
        // tick
        "varName": "accountType",
        "type": "checkbox",
        "label": "Type Of Account",
        "grid":12
    },
    {

        "varName": "typeOfOrganization",
        "type": "checkbox",
        "label": "Type of Organization",
        "grid":12
    },
    {

        "varName": "currency",
        "type": "checkbox",
        "label": "Currency",
        "grid":12
    },
    {
        // tick
        "varName": "natureOfBusiness",
        "type": "checkbox",
        "label": "Nature Of Business",
        "grid":12
    },
    {
        // seal
        "varName": "typeOfProductService",
        "type": "text",
        "label": "Type of Product/Service",
        "grid":12

    },
    {
        "varName": "totalManpower",
        "type": "text",
        "label": "Total Manpower",
        "grid":12

    }, {
        "varName": "yearlyTurnover",
        "type": "text",
        "label": "Yearly Turnover",
        "grid":12
    },
    {
        "varName": "netAsset",
        "type": "text",
        "label": "Net Asset",
        "grid":12
    },
    {
        "varName": "otherInformation",
        "type": "text",
        "label": "Other Information(if any)",
        "grid":12
    },
    {
        "varName": "comRegAddress",
        "type": "text",
        "label": "Com. Reg. Address",
        "grid":12
    },
    {
        "varName": "comRegUpazila",
        "type": "text",
        "label": "Upazila",
        "grid":12
    },
    {
        "varName": "comRegDistrict",
        "type": "text",
        "label": "District",
        "grid":12
    },
    {
        // tick
        "varName": "comRegPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":12
    },
    {

        "varName": "comRegCountry",
        "type": "text",
        "label": "Country",
        "grid":12
    },
    {

        "varName": "bussOffcAddress",
        "type": "text",
        "label": "Buss/offc. Address",
        "grid":12
    },
    {
        // tick
        "varName": "bussOffcUpazila",
        "type": "text",
        "label": "Upazila",
        "grid":12
    },
    {
        // tick
        "varName": "bussOffcDistrict",
        "type": "text",
        "label": "District",
        "grid":12
    },
    {
        // Numeric & Check Box
        "varName": "bussOffcPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":12
    },
    {
        "varName": "bussOffcCountry",
        "type": "text",
        "label": "Country",
        "grid":12
    },
    {
        "varName": "factAddress",
        "type": "text",
        "label": "Fact. Address",
        "grid":12
    },
    {
        "varName": "factUpazila",
        "type": "text",
        "label": "Upazila",
        "grid":12
    },

    {
        "varName": "factDistrict",
        "type": "text",
        "label": "District",
        "grid":12
    },
    {
        "varName": "factPostCode",
        "type": "text",
        "label": "Post Code",
        "grid":12
    },
    {
        "varName": "factCountry",
        "type": "text",
        "label": "Country",
        "grid":12
    },

    {
        "varName": "initialDeposit ",
        "type": "checkbox",
        "label": "Initial Deposit",
        "grid":12
    },
    {

        "varName": "otherInformation",
        "type": "title",
        "label": "Other Information",
        "grid":12
    },
    {
        // tick
        "varName": "tradeLicenseNo",
        "type": "text",
        "label": "Trade License No.",
        "grid":12
    },
    {
        "varName": "tradeLicenseDate",
        "type": "date",
        "label": "Date",
        "grid":12
    },
    {
        "varName": "issuingAuthority",
        "type": "text",
        "label": "Issuing Authority",
        "grid":12
    },
    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Registration No",
        "grid":12
    },
    {
        // tick
        "varName": "registrationNoDate",
        "type": "date",
        "label": "Date",
        "grid":12
    },
    {
        // Numeric & Check Box
        "varName": "registrationAuthority",
        "type": "text",
        "label": "Registration Authority",
        "grid":12
    },
    {
        "varName": "country",
        "type": "text",
        "label": "Country",
        "grid":12
    },
    {
        "varName": "etin",
        "type": "text",
        "label": "E-TIN No.",
        "grid":12
    },
    {
        "varName": "vATRegistrationNo",
        "type": "text",
        "label": "VAT Registration No",
        "grid":12
    },
    {

        "varName": "introducerInformation",
        "type": "title",
        "label": "Introducer's Information",
        "grid":12
    },

    {
        "varName": "introName",
        "type": "text",
        "label": "Introducer's Name",
        "grid":12
    },
    {
        "varName": "relationshipWithApplicant",
        "type": "text",
        "label": "Relationship with Applicant(s)",
        "grid":12
    },
    {
        "varName": "introAccountNumber",
        "type": "text",
        "label": "Account Number",
        "grid":12
    },
    {
        // Alpha Numeric
        "varName": "introducerCB",
        "type": "text",
        "label": "Customer ID",
        "grid":12
    },
    {
        "varName": "introducerContactNumber",
        "type": "text",
        "label": "Contact Number",
        "grid":12
    },
    {
        "varName": "introAccountName",
        "type": "text",
        "label": "Account Name",
        "grid":12
    },
    {
        // Alpha Numeric
        "varName": "introducerBranchName",
        "type": "text",
        "label": "Branch Name",
        "grid":12
    },
    {
        // Alpha Numeric
        "varName": "introducerSourceOfFund",
        "type": "text",
        "label": "Source of Fund",
        "grid":12
    },
    {
        "varName": "forBankUseOnly",
        "type": "title",
        "label": "For Bank Use Only",
        "grid":12
    },
    {
        "varName": "nameOfDMERMBrStaff",
        "type": "text",
        "label": "Name of DME/RM/Br Staff",
        "grid":12
    },
    {
        "varName": "sBSSectorCode",
        "type": "text",
        "label": "SBS Sector Code",
        "grid":12
    },
    {
        "varName": "sBSDepositCode",
        "type": "text",
        "label": "SBS Deposit Code",
        "grid":12
    },
    {
        "varName": "rMDMEBrStaffCode",
        "type": "text",
        "label": "RM/DME/Br Staff Code",

    }, {
        "varName": "productSchemeCode",
        "type": "text",
        "label": "Product Scheme Code",
        "grid":12
    },
    {
        "varName": "aCOccupationCode",
        "type": "text",
        "label": "A/C Occupation Code",
        "grid":12
    },
    {
        "varName": "workflowInput",
        "type": "title",
        "label": "Workflow Input",
        "grid":12
    },
    {
        "varName": "debitCard",
        "type": "text",
        "label": "Debit Card",
        "grid":12
    },
    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone 1",
        "grid":12
    },
    {
        "varName": "phone2",
        "type": "text",
        "label": "Phone 2",
        "grid":12
    },
    {
        "varName": "smsAlertRequest",
        "type": "select",
        "label": "Sms Alert Request",
        "grid":12,
        "enum": [
            "YES",
            "NO"
        ]

    }, {
        "varName": "companyCode",
        "type": "text",
        "label": "Company Code",
        "grid":12
    },
    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "grid":12
    },

    {
        "varName": "title",
        "type": "title",
        "label": "IIF",
        "grid":12
    },
    {
        "varName": "date",
        "type": "date",
        "label": "Date",
        "grid":12
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Unique Customer ID",
        "grid":12
    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "A/C No.",
        "grid":12
    },
    {
        "varName": "relationWithTheAC",
        "type": "text",
        "label": "Relation with the A/C",
        "grid":12
    },
    {
        "varName": "accountName",
        "type": "text",
        "label": "Account Name",
        "grid":12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer's Name",
        "grid":12
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Relation",
        "grid":12
    },
    {
        "varName": "fatherName",
        "type": "text",
        "label": "Father's Name",
        "grid":12
    },
    {
        "varName": "motherName",
        "type": "text",
        "label": "Mother's Name",
        "grid":12
    },
    {
        "varName": "spouseName",
        "type": "text",
        "label": "Spouse' Name",
        "grid":12
    }, {
        "varName": "dob",
        "type": "date",
        "label": "DOB",
        "grid":12
    },
    {
        "varName": "religion",
        "type": "text",
        "label": "Religion",
        "grid":12
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid":12
    },
    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid":12
    },
    {
        "varName": "birthCertificate",
        "type": "text",
        "label": "Birth certificate",
        "grid":12
    },
    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident status",
        "grid":12
    },
    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid":12
    },
    {
        "varName": "passportIssueDate",
        "type": "date",
        "label": "Issue Date",
        "grid":12
    },
    {
        "varName": "passportExpDate",
        "type": "date",
        "label": "Exp Date",
        "grid":12
    },
    {
        "varName": "passportPlaceOfIssue",
        "type": "text",
        "label": "Place of issue",
        "grid":12
    }
    ];




let MAKERJsonFormNonIndividualAccountOpening = {};
MAKERJsonFormNonIndividualAccountOpening=JSON.parse(JSON.stringify(SDCommonNewJsonFormNonIndividualAccountOpeningFinacle));


let CHECKERNewJsonFormNonIndividualAccountOpeningFinacle = {};
CHECKERNewJsonFormNonIndividualAccountOpeningFinacle= makeReadOnlyObject(JSON.parse(JSON.stringify(SDCommonNewJsonFormNonIndividualAccountOpeningFinacle)));
//CHECKERNewJsonFormIndividualAccountOpeningFinacle.variables.push(checkerApprove)
//######Existing Account Opening Joint#########


let CSExistJsonFormJointAccountOpening = {};
CSExistJsonFormJointAccountOpening["variables"] = JSON.parse(JSON.stringify(CommonExistJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm)));
CSExistJsonFormJointAccountOpening.variables.push(csSendTo);




let CSNewJsonFormJointAccountOpening = {};
CSNewJsonFormJointAccountOpening["variables"] = JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm)));
CSNewJsonFormJointAccountOpening.variables.push(csSendTo);

//######BOM Account Opening Joint#########
let BOMNewJsonFormJointAccountOpening = {};
BOMNewJsonFormJointAccountOpening["variables"] = makeReadOnlyObject(JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm))));
BOMNewJsonFormJointAccountOpening.variables.push(bomApproval);
BOMNewJsonFormJointAccountOpening.variables.push(bomSendTo);

//######BM Account Opening Joint#########
let BMJsonFormJointAccountOpening = {};
BMJsonFormJointAccountOpening["variables"] = makeReadOnlyObject(JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm))));
BMJsonFormJointAccountOpening.variables.push(bmApprove);
BMJsonFormJointAccountOpening.variables.push(bmSendTo);


//######Existing Account Opening Proprietorship#########


let CSExistJsonFormProprietorshipAccountOpening = {};
CSExistJsonFormProprietorshipAccountOpening["variables"] = JSON.parse(JSON.stringify(CommonExistJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm)));
CSExistJsonFormProprietorshipAccountOpening.variables.push(csSendTo);



let CSNewJsonFormProprietorshipAccountOpening = {};
CSNewJsonFormProprietorshipAccountOpening["variables"] = JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm)));
CSNewJsonFormProprietorshipAccountOpening.variables.push(csSendTo);

//######BOM Account Opening Proprietorship#########
let BOMNewJsonFormProprietorshipAccountOpening = {};
BOMNewJsonFormProprietorshipAccountOpening["variables"] = makeReadOnlyObject(JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm))));
BOMNewJsonFormProprietorshipAccountOpening.variables.push(bomApproval);
BOMNewJsonFormProprietorshipAccountOpening.variables.push(bomSendTo);

//######BM Account Opening Proprietorship#########
let BMJsonFormProprietorshipAccountOpening = {};
BMJsonFormProprietorshipAccountOpening["variables"] = makeReadOnlyObject(JSON.parse(JSON.stringify(CommonNewJsonFormIndividualAccountOpening.concat(commonJsonFormIndividualAccountOpeningForm))));
BMJsonFormProprietorshipAccountOpening.variables.push(bmApprove);
BMJsonFormProprietorshipAccountOpening.variables.push(bmSendTo);

//##DebitCard  Finacle #####
let CommonJsonFormForDebitCard = [


    {
        "varName": "name",
        "type": "text",
        "label": "Customer Name",

    },
    {
        "varName": "gender",
        "type": "select",
        "label": "Gender ?",
        "enum":[
            "MALE",
            "FEMALE",

        ]

    },
    {
        "varName": "document",
        "type": "text",
        "label": "Document",

    },
    {
        "varName": "dateOfBirth",
        "type": "date",
        "label": "Date of birth",

    },
    {
        "varName": "accountNo",
        "type": "text",
        "label": "Account No",

    },

    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type ?",
        "enum":[
            "SAVINGS",
            "CURRENT",
            "RFCD",
            "OTHER"

        ]

    },
    {
        "varName": "Transliteration",
        "type": "text",
        "label": "Transliteration",

    },
    {
        "varName": "address",
        "type": "text",
        "label": "Address",

    },
    {
        "varName": "email",
        "type": "text",
        "label": "E-mail",
        "email":true
    },
    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile phone"
    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality"
    },
    {
        "varName": "dse/rm",
        "type": "text",
        "label": "DSE/RM Code"
    },
    {
        "varName": "applicationSource",
        "type": "text",
        "label": "Application Source"
    },
    {
        "varName": "branchNameReceiveCard",
        "type": "text",
        "label": "Branch Name to Receive Card"
    },
    {
        "varName": "corporatename",
        "type": "text",
        "label": "Corporate Name"
    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport Number"
    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport name"
    },
    {
        "varName": "passportExpiredate",
        "type": "date",
        "label": "Passport Expiry Date"
    },
    {
        "varName": "passportIssuedate",
        "type": "date",
        "label": "Passport Issue Date"
    },
    {
        "varName": "limitProfile",
        "type": "date",
        "label": "Limit profile"
    },
    {
        "varName": "cardType",
        "type": "select",
        "label": "Card type ",
        "enum": [
            "MASTER",
            "VISA",
            "MAXX"
        ]
    },
    {
        "varName": "productType",
        "type": "select",
        "label": "Product type",
        "enum": [
            "GENERAL",
            "STAFF",
            "CORPORATE",
            "OTHER"
        ]
    },
    {
        "varName": "initialBranchSolId",
        "type": "text",
        "label": "Initiating Branch SOL ID"
    },
    {
        "varName": "casaSolId",
        "type": "text",
        "label": "CASA SOL ID "
    }


];
//## CS DebitCard ####
let CSJsonFormForDebitCard={};
CSJsonFormForDebitCard["variables"]=JSON.parse(JSON.stringify(CommonJsonFormForDebitCard));
CSJsonFormForDebitCard.variables.push(csSendTo);

//## BOM DebitCard ####
let BOMJsonFormForDebitCard={};
BOMJsonFormForDebitCard["variables"]=makeReadOnlyObject(JSON.parse(JSON.stringify(CommonJsonFormForDebitCard)));
BOMJsonFormForDebitCard.variables.push(bomApproval);
BOMJsonFormForDebitCard.variables.push(bomSendTo);

//## BM DebitCard ####
let BMJsonFormForDebitCard={};
BMJsonFormForDebitCard["variables"]=makeReadOnlyObject(JSON.parse(JSON.stringify(CommonJsonFormForDebitCard)));
BMJsonFormForDebitCard.variables.push(bmApprove);
BMJsonFormForDebitCard.variables.push(bmSendTo);

//## Maker DebitCard ####
let MAKERJsonFormForDebitCard={};
MAKERJsonFormForDebitCard["variables"]=makeReadOnlyObject(JSON.parse(JSON.stringify(CommonJsonFormForDebitCard)));

//## Checker DebitCard ####
let CHECKERJsonFormForDebitCard={};
CHECKERJsonFormForDebitCard["variables"]=makeReadOnlyObject(JSON.parse(JSON.stringify(CommonJsonFormForDebitCard)));
CHECKERJsonFormForDebitCard.variables.push(checkerApprove);

//#####DDeup Search Individual Form#############
let CSjsonFormIndividualAccountOpeningSearch = {
    "variables": [
        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",

        },

        {
            "varName": "nid",
            "type": "text",
            "label": "NID",

        },
        {
            "varName": "passport",
            "type": "text",
            "label": "Passport",


        },
        {
            "varName": "customerName",
            "type": "text",
            "label": "Customer Name",
            "required":true

        },
        {
            "varName": "dob",
            "type": "date",
            "label": "Date Of Birth",
            "required":true


        },
        {
            "varName": "email",
            "type": "text",
            "label": "Email",

            "email": true,


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone Number",
            "required":true


        },

        {
            "varName": "tin",
            "type": "text",
            "label": "eTin",


        },


        {
            "varName": "registrationNo",
            "type": "text",
            "label": "Birth Certificate/Driving License",


        },
        {
            "varName": "nationality",
            "type": "select",
            "label": "Nationality",
            "enum":[
                "BANGLADESH",
                "JAPAN",
                "INDIA"
            ],
            "required":true


        },


    ],

};
//#####DDeup Search Individual Form#############
let CSjsonFormIndividualAccountOpeningSearchForwardTo = [


        {
            "varName": "nid",
            "type": "text",
            "label": "NID",
            "grid":6

        },
        {
            "varName": "passport",
            "type": "text",
            "label": "Passport",
            "grid":6


        },
        {
            "varName": "customerName",
            "type": "text",
            "label": "Customer Name",
            "required":true,
            "grid":6

        },
        {
            "varName": "dob",
            "type": "date",
            "label": "Date Of Birth",
            "required":true,
            "grid":6


        },
        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "grid":6,

            "email": true,


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone Number",
            "required":true,
            "grid":6


        },

        {
            "varName": "tin",
            "type": "text",
            "label": "eTin",
            "grid":6


        },


        {
            "varName": "registrationNo",
            "type": "text",
            "label": "Birth Certificate/Driving License",
            "grid":6


        },
        {
            "varName": "nationality",
            "type": "text",
            "label": "Nationality",
            "required":true,
            "grid":6


        },


    ];

//#####DDeup Search NonIndividual Form#############
let CSjsonFormNonIndividualAccountOpeningSearch =[


        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",
            "required": false,
            "readOnly": false
        },

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false

        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false

        },

        {
            "varName": "companyEtin",
            "type": "text",
            "label": "Company ETin",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,
            "readOnly": false


        },
        {
            "varName": "certificate",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,


        }




];
//#####DDeup Search NonIndividual Form#############
let CSjsonFormNonIndividualAccountOpeningSearchForwardTo =[

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false,
            "grid":6


        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false,
            "grid":6


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false,
            "grid":6

        },

        {
            "varName": "companyEtin",
            "type": "text",
            "label": "Company ETin",
            "required": false,
            "grid":6


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,
            "readOnly": false,
            "grid":6


        },
        {
            "varName": "certificate",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,
            "grid":6


        }




];
//#####DDeup Search NonIndividual Form#############
let CSjsonFormIndividualJointAccountOpeningSearch = {
    "variables": [

        {
            "varName": "numberOfCustomer",
            "type": "text",
            "label": "Number Of Customer ?",
            "required": true,
            "number": true,
        }
    ]

};


//#####DDeup Search NonIndividual/Propritorship Form#############
let CSjsonFormNonIndividualProprietorshipAccountOpeningSearch = {
    "variables": [
        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",
            "required": false,
            "readOnly": false
        },

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false

        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false

        },

        {
            "varName": "tin",
            "type": "text",
            "label": "Company ETin",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,


        },


    ],

};

//#####DDeup Search NonIndividual/Partnership Form#############
let CSjsonFormNonIndividualPartnershipAccountOpeningSearch = {
    "variables": [
        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",
            "required": true,
            "readOnly": false
        },

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false

        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false

        },

        {
            "varName": "tin",
            "type": "text",
            "label": "Company ETin",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,


        },


    ],

};

//#####DDeup Search NonIndividual/Limited Company Acc Form#############
let CSjsonFormNonIndividualLimitedAccountOpeningSearch = {
    "variables": [
        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",
            "required": false,
            "readOnly": false
        },

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false

        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false

        },

        {
            "varName": "tin",
            "type": "text",
            "label": "Company ETin",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,


        },


    ],

};

//#####DDeup Search NonIndividual/Others  Form#############
let CSjsonFormNonIndividualOthersAccountOpeningSearch = {
    "variables": [
        {
            "varName": "cbNumber",
            "type": "text",
            "label": "CB Number",
            "required": false,
            "readOnly": false
        },

        {
            "varName": "companyName",
            "type": "text",
            "label": "Company Name",
            "required": false,
            "readOnly": false

        },

        {
            "varName": "email",
            "type": "text",
            "label": "Email",
            "required": false,
            "email": true,
            "readOnly": false


        },

        {
            "varName": "phone",
            "type": "text",
            "label": "Phone",
            "required": false

        },

        {
            "varName": "tin",
            "type": "text",
            "label": "Company ETin",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Trade License",
            "required": false,


        },
        {
            "varName": "tradeLicense",
            "type": "text",
            "label": "Certificate Of Incorporation",
            "required": false,


        },


    ],

};



//13-october-2019
//Sd maker Update All information Json Form #######
let MAKERUpdateAllInformation = {
    "variables": [

        {
            "varName": "discripancy",
            "type": "Select",
            "label": "Discripancy",
            "enum": [
                "YES",
                "NO"
            ]
        },

        {
            "varName": "deferal",
            "type": "Select",
            "label": "Deferral",
            "enum": [
                "YES",
                "NO"
            ]
        },

        {
            "varName": "deferalType",
            "type": "text",
            "label": "Deferral Type",
        },
        {
            "varName": "expireDate",
            "type": "date",
            "label": "Expire Date Type",
        } ,
        {
            "varName": "maker_update_all_information_send_tO",
            "type": "Select",
            "label": "Approved ?",
            "enum": [
                "APPROVED",
                "RETURN"
            ]
        },

    ]

};

//Sd maker Signiture and photo upload Json Form #######
let MAKERsigniturePhotoUpload = {
    "variables": [

        {
            "varName": "MAKERsigniturePhotoUpload",
            "type": "text",
            "label": "Signiture and Photo Upload ?",
        } ,

    ]

};


//Sd Checker Update All information Json Form #######
let CHECKERupdateAllInformation = {
    "variables": [
        {
            "varName": "checker_update_all_information_approval",
            "type": "Select",
            "label": "Approved ?",
            "enum": [
                "APPROVED",
                "NOAPPROVED"
            ]
        },

    ]

};

//Sd Checker Signiture and photo upload Json Form #######

let checkersigniturePhotoUpload = {
    "variables": [
        {
            "varName": "checker_signiture_photo_upload_approval",
            "type": "Select",
            "label": "Approved ?",
            "enum": [
                "APPROVED",
                "NOAPPROVED"
            ]
        },

    ]

};


module.exports = {
    CSJsonFormMaintenanceWithPrintFileAddress,
    CSJsonFormMaintenanceWithPrintFileContact,
    CSJsonFormMaintenanceWithPrintFileEmail,
    CSJsonFormMaintenanceWithPrintFileNominee,
    CSJsonFormMaintenanceWithPrintFileTenor,
    CSJsonFormMaintenanceWithPrintFileScheme,
    CSJsonFormMaintenanceWithPrintFileMaturity,
    CSJsonFormMaintenanceWithPrintFileTitle,
    CSJsonFormMaintenanceWithPrintFileLink,
    CSJsonFormMaintenanceWithPrintFileEtin,
    CSJsonFormMaintenanceWithPrintFileDormant,
    CSJsonFormMaintenanceWithPrintFileInputFiled,
    CSJsonFormMaintenanceWithFile,


    BOMJsonFormMaintenanceWithPrintFileAddress,
    BOMJsonFormMaintenanceWithPrintFileContact,
    BOMJsonFormMaintenanceWithPrintFileEmail,
    BOMJsonFormMaintenanceWithPrintFileNominee,
    BOMJsonFormMaintenanceWithPrintFileTenor,
    BOMJsonFormMaintenanceWithPrintFileScheme,
    BOMJsonFormMaintenanceWithPrintFileMaturity,
    BOMJsonFormMaintenanceWithPrintFileTitle,
    BOMJsonFormMaintenanceWithPrintFileLink,
    BOMJsonFormMaintenanceWithPrintFileEtin,
    BOMJsonFormMaintenanceWithPrintFileDormant,
    BOMJsonFormMaintenanceWithPrintFileInputFiled,
    BOMJsonFormMaintenanceWithFile,

    BMfromCSJsonFormMaintenanceWithPrintFileAddress,
    BMfromCSJsonFormMaintenanceWithPrintFileContact,
    BMfromCSJsonFormMaintenanceWithPrintFileEmail,
    BMfromCSJsonFormMaintenanceWithPrintFileNominee,
    BMfromCSJsonFormMaintenanceWithPrintFileTenor,
    BMfromCSJsonFormMaintenanceWithPrintFileScheme,
    BMfromCSJsonFormMaintenanceWithPrintFileMaturity,
    BMfromCSJsonFormMaintenanceWithPrintFileTitle,
    BMfromCSJsonFormMaintenanceWithPrintFileLink,
    BMfromCSJsonFormMaintenanceWithPrintFileEtin,
    BMfromCSJsonFormMaintenanceWithPrintFileDormant,
    BMfromCSJsonFormMaintenanceWithPrintFileInputFiled,
    BMfromCSJsonFormMaintenanceWithFile,

    BMfromBOMJsonFormMaintenanceWithPrintFileAddress,
    BMfromBOMJsonFormMaintenanceWithPrintFileContact,
    BMfromBOMJsonFormMaintenanceWithPrintFileEmail,
    BMfromBOMJsonFormMaintenanceWithPrintFileNominee,
    BMfromBOMJsonFormMaintenanceWithPrintFileTenor,
    BMfromBOMJsonFormMaintenanceWithPrintFileScheme,
    BMfromBOMJsonFormMaintenanceWithPrintFileMaturity,
    BMfromBOMJsonFormMaintenanceWithPrintFileTitle,
    BMfromBOMJsonFormMaintenanceWithPrintFileLink,
    BMfromBOMJsonFormMaintenanceWithPrintFileEtin,
    BMfromBOMJsonFormMaintenanceWithPrintFileDormant,
    BMfromBOMJsonFormMaintenanceWithPrintFileInputFiled,
    BMfromBOMJsonFormMaintenanceWithFile,
    BMjsonFormNonIndividualAccountOpeningSearch,
    MAKERJsonFormMaintenanceWithPrintFileAddress,
    MAKERJsonFormMaintenanceWithPrintFileContact,
    MAKERJsonFormMaintenanceWithPrintFileEmail,
    MAKERJsonFormMaintenanceWithPrintFileNominee,
    MAKERJsonFormMaintenanceWithPrintFileTenor,
    MAKERJsonFormMaintenanceWithPrintFileScheme,
    MAKERJsonFormMaintenanceWithPrintFileMaturity,
    MAKERJsonFormMaintenanceWithPrintFileTitle,
    MAKERJsonFormMaintenanceWithPrintFileLink,
    MAKERJsonFormMaintenanceWithPrintFileEtin,
    MAKERJsonFormMaintenanceWithPrintFileDormant,
    MAKERJsonFormMaintenanceWithPrintFileInputFiled,
    MAKERJsonFormMaintenanceWithFile,
    MAKERJsonFormNonIndividualAccountOpening,

    CHECKERJsonFormMaintenanceWithPrintFileAddress,
    CHECKERJsonFormMaintenanceWithPrintFileContact,
    CHECKERJsonFormMaintenanceWithPrintFileEmail,
    CHECKERJsonFormMaintenanceWithPrintFileNominee,
    CHECKERJsonFormMaintenanceWithPrintFileTenor,
    CHECKERJsonFormMaintenanceWithPrintFileScheme,
    CHECKERJsonFormMaintenanceWithPrintFileMaturity,
    CHECKERJsonFormMaintenanceWithPrintFileTitle,
    CHECKERJsonFormMaintenanceWithPrintFileLink,
    CHECKERJsonFormMaintenanceWithPrintFileEtin,
    CHECKERJsonFormMaintenanceWithPrintFileDormant,
    CHECKERJsonFormMaintenanceWithPrintFileInputFiled,
    CHECKERJsonFormMaintenanceWithFile,

    CHECKERNewJsonFormNonIndividualAccountOpeningFinacle,

    MAKERJsonFormIndividualAccountOpening,
    CHECKERNewJsonFormIndividualAccountOpeningFinacle,

    CSExistJsonFormIndividualAccountOpening,
    CSNewJsonFormIndividualAccountOpening,
    BOMNewJsonFormIndividualAccountOpening,
    BMJsonFormIndividualAccountOpening,


    CSExistJsonFormJointAccountOpening,
    CSNewJsonFormJointAccountOpening,
    BOMNewJsonFormJointAccountOpening,
    BMJsonFormJointAccountOpening,


    CSExistJsonFormProprietorshipAccountOpening,
    CSNewJsonFormProprietorshipAccountOpening,
    BOMNewJsonFormProprietorshipAccountOpening,
    BMJsonFormProprietorshipAccountOpening,


    CSjsonFormIndividualAccountOpeningSearch,
    CSjsonFormIndividualAccountOpeningSearchForwardTo,
    CSjsonFormIndividualJointAccountOpeningSearch,
    CSjsonFormNonIndividualAccountOpeningSearch,
    CSjsonFormNonIndividualAccountOpeningSearchForwardTo,
    CSjsonFormNonIndividualProprietorshipAccountOpeningSearch,
    CSjsonFormNonIndividualPartnershipAccountOpeningSearch,
    CSjsonFormNonIndividualLimitedAccountOpeningSearch,
    CSjsonFormNonIndividualOthersAccountOpeningSearch,



    CSJsonFormForDebitCard,
    BOMJsonFormForDebitCard,
    BMJsonFormForDebitCard,
    MAKERJsonFormForDebitCard,
    CHECKERJsonFormForDebitCard,
    BOMNewCommonjsonFormNonIndividualAccountOpeningSearch,


    MAKERUpdateAllInformation,
    MAKERsigniturePhotoUpload,

    CHECKERupdateAllInformation,
    BMjsonFormIndividualAccountOpeningSearch,


    BMAccountMaintenance,
    BOMAccountMaintenance,
    CheckerAccountMaintenance,
    maintenanceListChecker,
    MAKERAccountMaintenance,


    BOMFdrMaintenance,
    BMFdrMaintenance,
    CallCenterAccountMaintenance,
    CALLFdrMaintenance,
    CheckerFdrMaintenance,
    MAKERFdrMaintenance,


    BOMTaxCertificateMaintenance,
    BMTaxCertificateMaintenance,
    MAKERTaxCertificateMaintenance,
    CheckerTaxCertificateMaintenance,

    BOMDebitCreditMaintenance,
    BMDebitCreditMaintenance,
    MAKERDebitCreditMaintenance,
    CheckerDebitCreditMaintenance
};

