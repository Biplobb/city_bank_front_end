import {
    BMjsonFormIndividualAccountOpeningSearch,
    BMjsonFormNonIndividualProprietorshipAccountOpeningSearch,
    BMCommonjsonFormJointAccountOpeningSearch,
} from "../WorkflowJsonForm2";
import {
    BMJsonFormForDstIndividual,
    BMJsonFormForDstJoint,
    BMJsonFormForDstProprietorship,
    BMJsonFormForDstCompany,
} from "../WorkflowJsonForm4";
import React, {Component} from "react";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";
import CardBody from "../../Card/CardBody";
import withStyles from "@material-ui/core/styles/withStyles";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import VerifyApprovalOfficer from "./VerifyApprovalOfficer";


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});
const filteringJsonForm1 = {
    "variables": [
        {
            "varName": "customer_name",
            "type": "empty",
            "label": "Customer Name",
            "number": false,
        },
        {
            "varName": "customer_name",
            "type": "empty",
            "label": "Customer Name",
            "number": false,
        },
        {
            "varName": "cb_number",
            "type": "empty",
            "label": "CB Number",
        }


    ],

};

class ApprovalOfficerInbox extends Component {
    state = {
        sendTo: false,

        getDocument: false

    }

    constructor(props) {
        super(props);


    }

    DST=()=>{

        console.log(this.props)
        if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForDstIndividual}
                                appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="Joint Account" || this.props.subServiceType==="Individual A/C")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForDstJoint}
                                appId={this.props.appUid}/>)
        } else if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForDstProprietorship}
                                appId={this.props.appUid}/>)
        } else if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType === "Company Account")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForDstCompany}
                                appId={this.props.appUid}/>)
        }



    };
    renderExistAccountOpening=()=>{
        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMjsonFormIndividualAccountOpeningSearch}
                                appId={this.props.appUid}/>)
        }
        if (this.props.serviceType === 'ExistAccountOpening' &&   this.props.subServiceType==="Joint Account" ) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMCommonjsonFormJointAccountOpeningSearch}
                                appId={this.props.appUid}/>)
        }
        else  if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMjsonFormNonIndividualProprietorshipAccountOpeningSearch}
                                appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType === "Company Account")) {
            return (<VerifyApprovalOfficer closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMjsonFormNonIndividualProprietorshipAccountOpeningSearch}
                                appId={this.props.appUid}/>)
        }

    };
    inboxCase = () => {
        if (this.props.serviceType === "Account Opening") {

            return(
                this.DST()
            )
        }
        else if(this.props.serviceType==="ExistAccountOpening"){
            return(
                this.renderExistAccountOpening()
            )
        }

    };
    close = () => {
        this.props.closeModal();
    }

    render() {
        const {classes} = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">

                            <h4>{this.props.serviceType}<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>
                        </CardHeader>
                        <CardBody>

                            {this.inboxCase()}

                            {/*<Grid item xs={9}>


                            </Grid>*/}
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(ApprovalOfficerInbox);