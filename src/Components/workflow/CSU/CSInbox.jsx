import React, {Component} from "react";
import Liability from "../CASA/Liability";
import CSRejectedPage from "../CASA/CSRejectedPage";
import LiabilityExist from "../CASA/LiabilityExist";
import AgentBanking from "../AGENT/AgentBanking";
import DST from "../CSU/DST";
import {
    CSjsonFormIndividualAccountOpeningSearchForwardTo,
    CSjsonFormJointAccountOpeningSearchForwardTo, CSjsonFormNonIndividualAccountOpeningSearchForwardTo
} from "../WorkflowJsonForm2";
import {
    CSJsonFormForCasaIndividual,
    CSJsonFormForCasaJoint,
    CSJsonFormForCasaProprietorship,
    CSJsonFormForCasaCompany,
} from "../WorkflowJsonForm4";
let SearchForm = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 4,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 4,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 4,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 4,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 4,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 4,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 4,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 4,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 4,


    },];
let SearchFormIndividual = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 4,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 4


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 4,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 4,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 4,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 4,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 4,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 4,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 4,


    },];
let SearchFormNonIndividual = [


    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":4


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,
        "email": true,
        "readOnly": false,
        "grid":4


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone",
        "required": false,
        "grid":4

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":4


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":4


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":4


    }



];
let SearchFormJoint = [

    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Cb Number",
        "grid":4


    },
    {
        "varName": "accountSource",
        "type": "text",
        "label": "Account Source",
        "grid":4


    },
    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":4


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":4,

        "email": true,


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":4


    }
];
class CSInbox extends Component {

    dst=()=>{

        if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C") &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<DST closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaIndividual}
                               appId={this.props.appUid}/>)
        }

        if (this.props.serviceType === 'Account Opening' && this.props.subServiceType==="Joint Account" &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<DST closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaJoint}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL") &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<DST closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaProprietorship}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'Account Opening' && this.props.subServiceType==="Company Account" &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<DST closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaCompany}
                               appId={this.props.appUid}/>)
        }


        else if (this.props.serviceType === 'Account Opening'  && this.props.taskTitle==='cs_case_rejected' ) {
            return (<CSRejectedPage closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                                    appId={this.props.appUid}/>)
        }
    };


    renderExistAccountOpeningcase=()=>{

        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C") &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<LiabilityExist closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormIndividual}
                                    appId={this.props.appUid}/>)
        }
        else  if (this.props.serviceType === 'ExistAccountOpening' &&  this.props.subServiceType==="JointForAccountOpening"  &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<LiabilityExist closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormJoint}
                                    appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL") &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Partnership A/C" &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Limited Company A/C" &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Others" &&  this.props.taskTitle==='csu_cso_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening'  && this.props.taskTitle==='cs_case_rejected' ) {
            return (<CSRejectedPage closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                                    appId={this.props.appUid}/>)
        }
        else{
            alert("External Problem")
        }


    };
    inboxCase = () => {

        //alert(this.props.serviceType)
        //alert(this.props.subServiceType)

         if (this.props.serviceType === "Account Opening") {
           

            return (
                this.dst()
            )
        }


        else if (this.props.serviceType === "ExistAccountOpening") {
            return (
                this.renderExistAccountOpeningcase()
            )
        }

    }

    close=()=>{
        this.props.closeModal();
    }
    render() {



        return (

            this.inboxCase()



        );

    }

}

export default CSInbox;