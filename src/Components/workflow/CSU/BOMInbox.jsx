import VerifyDocumentBOM from "../CASA/VerifyDocumentBOM";

import {
    BOMJsonFormForDstIndividual,
    BOMJsonFormForDstJoint,
    BOMJsonFormForDstProprietorship,
    BOMJsonFormForDstCompany,
} from "../WorkflowJsonForm4";

import React, {Component} from "react";
import VerifyBom from "./VerifyBom";
import Card from "../../Card/Card";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../../Card/CardBody";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import VerifyMakerPhoto from "../VerifyMakerPhoto";

class BOMInbox extends Component {
    state={
        documentList: [],
        getDocument: false
    }
    DST=()=>{

        if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {

            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstIndividual}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'Account Opening' && this.props.subServiceType==="Joint Account") {

            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstJoint}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'Account Opening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL")) {
            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstProprietorship}
                                       appId={this.props.appUid}/>)
        }

        else if (this.props.serviceType === 'Account Opening' && this.props.subServiceType==="Company Account") {
            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstCompany}
                                       appId={this.props.appUid}/>)
        }


        else {


        }

    };
    DSTExist=()=>{

        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {

            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstIndividual}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Joint Account") {

            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstJoint}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL")) {
            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstProprietorship}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Company Account") {
            return (<VerifyBom closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForDstCompany}
                                       appId={this.props.appUid}/>)
        }

        else {


        }

    }
    inboxCase = () => {
        //alert(this.props.serviceType)
        if(this.props.serviceType === "Account Opening"){
            return (
                this.DST()
            )
        }
        else if(this.props.serviceType === "ExistAccountOpening"){
            return (
                this.DSTExist()
            )
        }


    }
    componentDidMount() {
        let fileUrl = backEndServerURL + "/case/files/" + this.props.appUid;
        axios.get(fileUrl, {withCredentials: true})
            .then((response) => {
                console.log(":d")
                console.log(response.data)
                this.setState({
                    documentList: response.data,
                    getDocument: true
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }
    inboxCasePhoto = () => {
        return (<VerifyMakerPhoto closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                  subServiceType={this.props.subServiceType}

                                  titleName="Maker Update All Information"
                                  documentList={this.state.documentList}/>)



    };
    close=()=>{
        this.props.closeModal()
    }
    render() {
        return (
            <div>

                <Card>
                    <CardHeader color="rose">
                        <h4>{this.props.serviceType}<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                    </CardHeader>
                    <CardBody>
                        <Grid container spacing={1}>

                            <Grid  item xs={8}>

                                {this.inboxCasePhoto()}

                            </Grid>


                            <Grid item xs={4}>
                                {this.inboxCase()}
                            </Grid>


                        </Grid>
                    </CardBody>
                </Card>
            </div>

        )
    }

}

export default BOMInbox;