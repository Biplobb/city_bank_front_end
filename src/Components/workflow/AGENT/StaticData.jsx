import React from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Tooltip from "@material-ui/core/Tooltip";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import PropTypes from "prop-types";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from "../../../Common/Functions";
import AgentBanking from "./AgentBanking";
import FormSample from "../../JsonForm/FormSample";
import Paper from "@material-ui/core/Paper";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import CardHeader from "../../Card/CardHeader";
import Table from "@material-ui/core/Table";
import {Dialog, withStyles} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import TableBody from "@material-ui/core/TableBody";
import {ThemeProvider} from "@material-ui/styles";
import theme from "../../JsonForm/CustomeTheme";
import Button from "@material-ui/core/Button";
import TablePagination from "@material-ui/core/TablePagination";

const filteringJsonForm = {
    "variables": [
        {
            "varName": "cb_number",
            "type": "text",
            "label": "CB Number",
        },
        {
            "varName": "branch_id",
            "type": "select",
            "label": "Sol Id",
            "enum":[
                "100",
                "101",
                "102",
            ]
        },
        {
            "varName": "service_type",
            "type": "select",
            "label": "Service Type",
            "enum":[
                "AccountOpening",
                "FdrOpening",

            ]
        },


    ],

};



let counter = 0;

function createData(customer_name, cb_number, account_number, appUid, service_type, subservice_type, branch_id,taskTitle,jointAccountCustomerNumber) {

    counter += 1;
    return {id: counter, customer_name, cb_number, account_number, appUid, service_type, subservice_type, branch_id,taskTitle,jointAccountCustomerNumber};
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: 'id', numeric: false, disablePadding: true, label: 'Serial No'},
    {id: 'customer_name', numeric: false, disablePadding: false, label: 'Name'},
    {id: 'cb_number', numeric: false, disablePadding: false, label: 'CB Number'},
    {id: 'account_number', numeric: false, disablePadding: false, label: 'Account Number'},
    {id: 'service_type', numeric: false, disablePadding: false, label: 'Category'},
    {id: 'subservice_type', numeric: false, disablePadding: false, label: 'Sub Category'},
    {id: 'sol_id', numeric: true, disablePadding: false, label: 'Sol Id'},
    {id: 'View', numeric: false, disablePadding: false, label: 'View'}
];

class TableContentHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">

                    </TableCell>
                    {rows.map(
                        row => (
                            <TableCell
                                key={row.id}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        ),
                        this,
                    )}
                </TableRow>
            </TableHead>
        );
    }
}

TableContentHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};



const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 2,
    },
    table: {
        minWidth: 1020,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    container: {
        overflow: 'auto',
        padding: '10px'
    },

    left: {
        float: 'left',
        width: '200px'
    },

    right: {
        marginLeft: '210px'
    }
});

class Inbox extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'id',
        selected: [],
        data: [],
        getInboxCaseData: [],
        page: 0,
        rowsPerPage: 25,
        renderModal: false,
        appUid: '',
        inboxModal: false,
        serviceType: '',
        subserviceType: '',
        jointAccountCustomerNumber:'',
        taskTitle: '',
        redirectLogin:false,
    };


    componentDidMount() {


        const data = [];




                    data.push(createData("Md Biplob Hossain","59841433", "1234567891011", "","CASA","INDIVIDUAL","100","",""));
                    data.push(createData("Md Biplob Hossain","59841433", "1234567891011", "","CASA","Company Account","100","",""));



                this.setState({data: data, getInboxCaseData: data});


    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({selected: state.data.map(n => n.id)}));
            return;
        }
        this.setState({selected: []});
    };

    handleClick = (event, id) => {
        const {selected} = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({selected: newSelected});
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    viewCase = (appUid, serviceType, subserviceType,taskTitle,jointAccountCustomerNumber) => {
        this.setState({
            serviceType: serviceType,
            subserviceType: subserviceType,
            inboxModal: true,
            appUid: appUid,
            taskTitle:taskTitle,
            jointAccountCustomerNumber:jointAccountCustomerNumber
        })

    }
    closeModal = () => {
        this.setState({
            inboxModal: false,


        })
    }

    renderInboxCase = () => {
        return (
                <AgentBanking  serviceType={this.state.serviceType}   subServiceType={this.state.subserviceType}   closeModal={this.closeModal}
                        />
            )
    }
    renderFilterForm = () => {
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filter" onSubmit={this.getSubmitedForm}
                        jsonForm={filteringJsonForm}/>
        )
    }

    getSubmitedForm = (object) => {
        counter = 0;
        let objectTable = {};
        let tableArray = [];

        for (let variable in object) {
            let trimData = object[variable].trim();
            if (trimData !== '')
                objectTable[variable] = trimData;
        }

        this.state.getInboxCaseData.map((inboxCase) => {
            let showable = true;
            for (let variable in objectTable) {
                if (objectTable[variable] !== inboxCase[variable])
                    showable = false;
            }

            if (showable)

                tableArray.push(createData(inboxCase.customer_name, inboxCase.cb_number, inboxCase.account_number, inboxCase.appUid, inboxCase.service_type, inboxCase.subservice_type, inboxCase.branch_id,inboxCase.taskTitle,inboxCase.jointAccountCustomerNumber));



        })
        this.setState({
            data: tableArray
        })
    };

    returnModalSizeOnRole(){
        let role = localStorage.getItem("roles");
        if (role !==undefined){
            if (role.indexOf("MAKER") !== -1 || role.indexOf("CHECKER") !== -1)
                return true;
            return false;
        }
        return false;
    }

    render() {
        const {classes} = this.props;
        const {data, order, orderBy, selected, rowsPerPage, page} = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <section>


                <Paper className={classes.root}>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>

                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Agent Banking Data</h4>

                            </CardHeader>
                            <br/>

                            <div className={classes.tableWrapper}>
                                <Table className={classes.table} aria-labelledby="tableTitle">
                                    <TableContentHead
                                        numSelected={selected.length}
                                        order={order}
                                        orderBy={orderBy}
                                        onSelectAllClick={this.handleSelectAllClick}
                                        onRequestSort={this.handleRequestSort}
                                        rowCount={data.length}
                                    />
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        fullScreen = {this.returnModalSizeOnRole()}
                                        open={this.state.inboxModal}>
                                        <DialogContent>

                                            {this.renderInboxCase()}
                                        </DialogContent>
                                    </Dialog>
                                    <TableBody>
                                        {stableSort(data, getSorting(order, orderBy))
                                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                            .map(n => {
                                                const isSelected = this.isSelected(n.id);
                                                return (
                                                    <TableRow
                                                        hover
                                                        /* onClick={event => this.handleClick(event, n.id)}*/
                                                        role="checkbox"
                                                        aria-checked={isSelected}
                                                        tabIndex={-1}
                                                        key={n.id}
                                                        selected={isSelected}
                                                    >
                                                        <TableCell padding="none">

                                                        </TableCell>
                                                        <TableCell padding="none">
                                                            {n.id}
                                                        </TableCell>
                                                        <TableCell align="left">
                                                            {n.customer_name}
                                                        </TableCell>
                                                        <TableCell align="left">{n.cb_number}</TableCell>
                                                        <TableCell align="left">{n.account_number}</TableCell>
                                                        <TableCell align="left">{n.service_type}</TableCell>
                                                        <TableCell align="left">{n.subservice_type}</TableCell>
                                                        <TableCell align="right">{n.branch_id}</TableCell>
                                                        <TableCell align="left">

                                                            <ThemeProvider theme={theme}>
                                                                <Button
                                                                    onClick={() => this.viewCase(n.appUid, n.service_type, n.subservice_type, n.taskTitle,n.jointAccountCustomerNumber)}
                                                                    variant="contained" color="secondary" className={classes.margin}>
                                                                    View
                                                                </Button>
                                                            </ThemeProvider>
                                                        </TableCell>


                                                    </TableRow>
                                                );
                                            })}
                                        {emptyRows > 0 && (
                                            <TableRow style={{height: 49 * emptyRows}}>
                                                <TableCell colSpan={6}/>
                                            </TableRow>
                                        )}
                                    </TableBody>
                                </Table>
                            </div>

                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25]}
                                component="div"
                                count={data.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                backIconButtonProps={{
                                    'aria-label': 'Previous Page',
                                }}
                                nextIconButtonProps={{
                                    'aria-label': 'Next Page',
                                }}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            />

                        </GridItem>
                    </GridContainer>
                </Paper>
            </section>
        );

    }
}

Inbox.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Inbox);
