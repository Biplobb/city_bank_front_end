import Grow from "@material-ui/core/Grow";
import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Functions from "../../../Common/Functions";
import Table from "../../Table/Table";
import SelectComponent from "../../JsonForm/SelectComponent";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import Notification from "../../NotificationMessage/Notification";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../../Card/CardBody";
import CircularProgress from "@material-ui/core/CircularProgress";
import {ThemeProvider} from "@material-ui/styles";
import theme from "../../JsonForm/CustomeTheme2";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import AccountNoGenerate from "../AccountNoGenerate";
import CSImageLinkEdit from "../CASA/CSImageLinkEdit";
import DSTUploadModal from "../CASA/LiabilityUploadModal";
import SingleImageShow from "../CASA/SingleImageShow";
import withStyles from "@material-ui/core/styles/withStyles";
import {
    AbhMakerJsonFormForAgentIndividual,
    AbhMakerJsonFormForAgentNonIndividual,

} from '../WorkflowJsonForm4';

let abhMaker = [
    {
        "varName": "abhMaker",
        "type": "textArea",
        "label": "Abh Maker Remarks",
        "grid": 12
    }];

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};

function Transition(props) {
    return <Grow in={true} timeout="auto" {...props} />;
}

var deferalOther =
    {
        "varName": "deferalOther",
        "type": "text",
        "label": "Others",

    };

var deferal =
    {
        "varName": "deferalType",
        "type": "select",
        "label": "Deferral Type",
        "enum": [
            "Applicant Photograph",
            "Nominee Photograph",
            "Passport",
            "Address proof",
            "Transaction profile",
            "other"
        ],

    };

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",

};

class AgentBanking extends Component {

    constructor(props) {
        super(props);
        this.state = {
            SelectedData: false,
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            varValue: [],
            caseId: "",
            title: "",
            notificationMessage: "",
            app_uid: "-1",
            alert: false,
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                abs_deferal: "NO",
                accountType: "NON-INSTAPACK",
                priority: "GENERAL"
            },
            fileUploadData: [],
            selectedDate: {},
            dropdownSearchData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            getMappingAllImage: false,
            accountDetailsModal: false,
            loaderNeeded: null,
            IndividualDedupModal: false,
            JointDedupModal: false,
            individualDataSaveId: '',
            jointDataSaveId: '',
            companyDataSaveId: '',
            numberOfCustomer: 0,
            err: false,
            errorArray: {},
            errorMessages: {},
            getRemarks: [],
            uploadButtonClick: false,
            getAllApprovalOfficer: []


        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event, value) => {

        this.state.inputData["abs_deferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }


    addDeferalForm() {


        return this.state.values.map((el, i) =>
            <React.Fragment>
                <Grid item xs={3}>
                    {
                        this.dynamicDeferral(el)
                    }
                </Grid>

                <Grid item xs={3}>
                    {
                        this.dynamicDate(el)
                    }

                </Grid>
                <Grid item xs={3}>
                    {this.dynamicDeferralOther(el)}

                </Grid>
                <Grid item xs={3}>
                    <button
                        style={{float: "left"}}
                        className="btn btn-outline-danger"
                        type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                    >
                        Remove
                    </button>


                </Grid>
            </React.Fragment>
        )

    }

    getSearchvalue = (jsonObject) => {
        var clone = JSON.parse(JSON.stringify(jsonObject))
        for (var prop in clone)
            if (clone[prop] === '')
                delete clone[prop];
        return clone;
    }
    DedupDataSaveApi = (subServiceType) => {
        if (subServiceType === "INDIVIDUAL" || subServiceType === "Individual A/C") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "individualDedupData": this.props.individualDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Joint Account") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Proprietorship A/C" || subServiceType === "NONINDIVIDUAL") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "individualDedupData": this.props.individualDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {
                        "companyDedupData": this.props.companyDedupData,
                        "dedupType": subServiceType
                    }, {withCredentials: true})
                        .then((response) => {
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Company Account") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {
                        "companyDedupData": this.props.companyDedupData,
                        "dedupType": subServiceType
                    }, {withCredentials: true})
                        .then((response) => {
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else {
            alert("please type select")
        }
    }
    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }

    componentDidMount() {
        var remarksArray = [];
        this.setState({
            loaderNeeded: false
        })

        let getCommentsUrl = backEndServerURL + "/appRemarkGet/" + this.props.appId;
        axios.get(getCommentsUrl, {withCredentials: true})
            .then((response) => {

                console.log(response.data);
                response.data.map((data) => {

                    remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                })
                this.setState({
                    getRemarks: remarksArray
                })
            })
            .catch((error) => {
                console.log(error)
            })

        this.state.inputData["abs_deferal"] = "NO";
        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log(response.data)
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {
                            let approvalOfficerUrl = backEndServerURL + "/user/absCheckers";
                            axios.get(approvalOfficerUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    this.setState({
                                        getAllApprovalOfficer: response.data
                                    })
                                })
                                .catch((error) => {
                                    console.log(error);
                                })
                            console.log(response.data);
                            let tableArray = [];
                            var status = "";
                            response.data.map((deferal) => {
                                if (deferal.status === "ACTIVE") {
                                    status = "Approved"
                                } else {
                                    status = deferal.status
                                }
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                            console.log(this.state.getDeferalList)
                        })
                        .catch((error) => {
                            console.log(error);
                        })


                    console.log(remarksArray)
                    this.state.inputData = response.data;
                    this.state.inputData["abs_deferal"] = "NO";
                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,
                        showValue: true,
                        appId: this.props.appId,
                        loaderNeeded: true
                    });

                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/abs_maker_account_opening";
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    let approvalOfficerUrl = backEndServerURL + "/user/absCheckers";
                    axios.get(approvalOfficerUrl, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data);
                            this.setState({
                                getAllApprovalOfficer: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    /*  {
                          this.DedupDataSaveApi(this.props.subServiceType)
                      }*/
                    console.log(response.data)
                    // let inputData = this.getSearchvalue(this.props.searchValue);
                    let inputData = {};
                    inputData.abs_deferal = "NO";
                    inputData.accountSource = "FINACLE";
                    inputData.accountType = "SAVINGS";
                    inputData.accType = "SAVINGS";
                    inputData.cbNumber = "CB5984143";
                    inputData.accountNumber = 2101430595001;
                    inputData.customerName = "Md Biplob Hossain .";
                    inputData.passport = 12345676;
                    inputData.phone = "01714740719";
                    inputData.email = "a@gmail.com";
                    inputData.nid = "1996234567546";

                    inputData.tin = "1234567";
                    inputData.registrationNo = "87654321";
                    inputData.nationality = "BANGLADESH";
                    inputData.comAddress = "dhaka";
                    inputData.schemeCode = "12345";
                    inputData.currency = "BDT";
                    inputData.rmCode = "1234543";
                    inputData.ccepCompanyCode = "876543456";
                    inputData.ccepCompanyCode = "876543456";
                    inputData.currency = "BDT";
                    inputData.certificate = "234567567";
                    inputData.companyEtin = "654322";
                    inputData.sbsCode = "345";
                    inputData.priority = "GENERAL";

                    if (this.props.subServiceType === "Company Account") {
                        inputData.accountType = "NON-INSTAPACK";

                    } else {
                        inputData.accountType = "NON-INSTAPACK";
                        // inputData.accountType = "INSTAPACK";
                    }

                    inputData.priority = "GENERAL";
                    inputData.smsAlertRequest = true;
                    inputData.instaCardType = "CITYMAXX";
                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        //inputData:JSON.parse(JSON.stringify(this.props.searchValue)),
                        inputData: inputData,
                        varValue: JSON.parse(JSON.stringify(inputData)),
                        showValue: true,
                        getData: true,
                        loaderNeeded: true


                    });
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <div style={{"border-style": "groove", "border-width": "1px"}}>
                        <Table

                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["Deferal Type", "Expire Date", "Raise By", "Raise Date", "Status"]}
                            tableData={this.state.getDeferalList}
                            tableAllign={['left', 'left']}
                        />

                        <br/>

                    </div>
                </div>

            )
        }

    };
    renderRemarksData = () => {


        if (this.state.getRemarks.length > 0) {

            return (
                <div>
                    <div style={{"border-style": "groove", "border-width": "1px"}}>
                        <Table

                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["Remarks", "Raised By", "Date", "Role"]}
                            tableData={this.state.getRemarks}
                            tableAllign={['left', 'left', 'left', 'left']}
                        />

                        <br/>
                    </div>

                </div>

            )
        }

    }
    renderAddButtonShow = () => {

        return (
            <button
                className="btn btn-outline-danger"

                style={{
                    width: 150,
                    float: 'right',
                    verticalAlign: 'right',

                }}

                type='button' value='add more'
                onClick={this.addClick.bind(this)}


            >Add Deferral</button>
        )

    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };

    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        } else {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    updateComponent = () => {
        this.forceUpdate();
    };
    handleChangeComments = () => {
        alert("Ok")
    }
    returnJsonForm = () => {
        if (this.state.showValue) {
            if(this.props.subServiceType==="INDIVIDUAL"){
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, AbhMakerJsonFormForAgentIndividual, this.updateComponent)
                )
            }
            else{
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, AbhMakerJsonFormForAgentNonIndividual, this.updateComponent)
                )
            }



        }
    };


    /*handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.state.inputData)
        /!* let error = MyValidation.defaultValidation(AbhMakerJsonFormForAgentIndividual, this.state)
         this.forceUpdate();
         console.log(this.state.inputData);

         console.log("Not working");

         if(error===true){
             return 0;
         }
 *!/

        if (this.state.inputData["abs_deferal"] === "YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.abs_deferal = this.state.inputData["abs_deferal"];
        data.serviceType = "Account Opening";
        data.individualDedupData = this.state.individualDataSaveId;
        data.jointDedupData = this.state.jointDataSaveId;
        data.companyDedupData = this.state.companyDataSaveId;
        data.jointAccountCustomerNumber = this.props.jointAccountCustomerNumber;
        data.subServiceType = this.props.subServiceType;
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                if (this.state.inputData.accountType === "NON-INSTAPACK" ) {
                    this.setState({
                        accountDetailsModal: true
                    })
                }
                if (this.state.inputData.accountType !== "NON-INSTAPACK") {
                    var url = backEndServerURL + "/case/route/" + this.state.appId;

                    axios.get(url, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data);
                            console.log("Successfully Routed!");

                            {
                                this.close();
                            }
                            this.setState({
                                title: "Successfull!",
                                notificationMessage: "Successfully Routed!",
                                alert: true

                            })
                            //




                        })
                        .catch((error) => {
                            console.log(error);

                        });
                }
            })
            .catch((error) => {
                console.log(error)
            });

    };*/
    ;
    handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.state.inputData);
        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.abhMaker + "/" + this.state.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        if (this.state.inputData["abs_deferal"] === "YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }

        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.abs_deferal = this.state.inputData["abs_deferal"];
        data.serviceType = "Account Opening";
        data.abhMaker = undefined;
        data.individualDedupData ="NxkaoW4BUazH1mA-_HyL";
        data.jointDedupData ="NxkaoW4BUazH1mA-_HyL";
        data.companyDedupData = "NxkaoW4BUazH1mA-_HyL";
        data.jointAccountCustomerNumber = this.props.jointAccountCustomerNumber;
        data.subServiceType = this.props.subServiceType;
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);


                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        console.log("Successfully Routed!");

                        {
                            this.props.closeModal();
                        }
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true

                        })
                        //


                    })
                    .catch((error) => {
                        console.log(error);

                    });

            })
            .catch((error) => {
                console.log(error)
            });

    }
    handleSubmitDraft = (event) => {
        event.preventDefault();


        if (this.state.inputData["abs_deferal"] === "YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var variableSetUrl = backEndServerURL + "/save/" + this.state.appId;

        let data = this.state.inputData;
        data.abs_deferal = this.state.inputData["abs_deferal"];
        data.serviceType = "Account Opening";
        data.individualDedupData = this.state.individualDataSaveId;
        data.jointDedupData = this.state.jointDataSaveId;
        data.companyDedupData = this.state.companyDataSaveId;
        data.jointAccountCustomerNumber = this.props.jointAccountCustomerNumber;
        data.subServiceType = this.props.subServiceType;
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);

                this.setState({
                    title: "Successfull!",
                    notificationMessage: "Successfully Draft!",
                    alert: true

                })
                this.close()
                //


            })
            .catch((error) => {
                console.log(error)
            });

    }
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))

        this.state.inputData["abs_deferal"] = "YES";
    }

    renderRemarks = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, abhMaker, this.updateComponent)

            )
        }
        return;
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
            if (this.state.values.length > 0) {
                this.state.inputData["abs_deferal"] = "YES"
            } else {
                this.state.inputData["abs_deferal"] = "NO"
            }
        }


    }


    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    style={{
                        width: 150,

                    }}
                    className="btn btn-outline-danger"

                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    };
    renderAssignedImage = () => {
        if (this.state.uploadButtonClick || this.state.fileUploadData.length > 1) {
            return (
                <button
                    style={{
                        width: 150,

                    }}
                    className="btn btn-outline-danger"

                    onClick={this.mappingAllImage}

                >
                    Assigned Image
                </button>
            )
        }
    };

    closeModal = () => {
        this.setState({

            getMappingAllImage: false,
            IndividualDedupModal: false,
            JointDedupModal: false,

        })
    }
    closeUploadModal = (data) => {
        this.setState({
            uploadButtonClick: true,
            uploadModal: false
        })
    }

    accountDetailsModal = () => {
        this.setState({
            accountDetailsModal: false
        })
        var url = backEndServerURL + "/case/route/" + this.state.appId;

        axios.get(url, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                console.log("Successfully Routed!");
                this.setState({
                    title: "Successfull!",
                    notificationMessage: "Successfully Routed!",
                    alert: true

                })
                this.props.closeModal();


            })
            .catch((error) => {
                console.log(error);

            });


    }

    IndividualDedupModal = () => {
        this.setState({
            IndividualDedupModal: true
        })
    }
    JointDedupModal = () => {
        this.setState({
            JointDedupModal: true
        })
    }
    mappingAllImage = (event) => {
        event.preventDefault();
        this.setState({
            getMappingAllImage: true
        })
    }
    renderApprovalOfficerList = () => {
        if (this.state.getData && this.state.inputData["abs_deferal"] === "NO") {


            let ApprovalOfficerJsonForm = {
                "varName": "next_user",
                "type": "select",
                "label": "Send To",
                "grid": 12,
                "enum": []
            };
            this.state.getAllApprovalOfficer.map((data) => {

                ApprovalOfficerJsonForm.enum.push(data)
            })
            return (
                SelectComponent.select(this.state, this.updateComponent, ApprovalOfficerJsonForm)
            )

        }
    }

    render() {

        const {classes} = this.props;


        if (this.state.loaderNeeded === false) {
            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4>New
                                    Customer
                                    Account Opening<a><CloseIcon onClick={this.close} style={{
                                        position: 'absolute',
                                        right: 10,
                                        color: "#000000"
                                    }}/></a></h4>

                            </CardHeader>
                            <CardBody>
                                <CircularProgress style={{marginLeft: '50%'}}/>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>


            )
        } else if (this.state.inputData.accountType === "INSTAPACK" && this.props.subServiceType !== "Company Account") {
            return (


                <Card>
                    <CardHeader color="rose">
                        <h4>New
                            Customer
                            Account Opening<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>

                    </CardHeader>
                    <CardBody>
                        <div>

                            <ThemeProvider theme={theme}>

                                <Grid container spacing={1}>
                                    {this.renderNotification()}


                                    {this.returnJsonForm()}

                                    <Grid item xs='12'>
                                    </Grid>
                                    {this.renderAddButtonShow()}


                                    <Grid item xs='12'>

                                    </Grid>
                                    {
                                        this.addDeferalForm()
                                    }


                                    <Grid item xs='12'>
                                    </Grid>


                                    <Grid item xs={12}>

                                        {this.renderDefferalData()}


                                    </Grid>
                                    <Grid item xs={12}>

                                        {this.renderRemarksData()}

                                    </Grid>
                                    <Grid item xs='12'>
                                    </Grid>
                                    <Grid item xs={2}>
                                        {this.renderUploadButton()}

                                    </Grid>
                                    <Grid item xs={2}>
                                        {this.renderAssignedImage()}
                                    </Grid>
                                    <Grid item xs='12'>
                                    </Grid>
                                    {this.renderRemarks()}

                                    <Grid item xs='12'>
                                    </Grid>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="md"
                                        open={this.state.accountDetailsModal}>
                                        <DialogContent>

                                            <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                                        </DialogContent>
                                    </Dialog>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.getMappingAllImage}>
                                        <DialogContent>
                                            <CSImageLinkEdit appId={this.state.appId} closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.uploadModal}>
                                        <DialogContent>
                                            <DSTUploadModal

                                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                                subServiceType={this.props.subServiceType} appId={this.state.appId}
                                                closeModal={this.closeUploadModal}/>
                                        </DialogContent>
                                    </Dialog>


                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.imageModalBoolean}>
                                        <DialogContent>

                                            <SingleImageShow data={this.state.selectImage}
                                                             closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>
                                    {this.renderApprovalOfficerList()}
                                    <Grid item xs='12'>
                                    </Grid>
                                    <center>
                                        <button
                                            className="btn btn-outline-danger"
                                            style={{}}
                                            onClick={this.handleSubmit}

                                        >
                                            Submit
                                        </button>
                                        &nbsp;&nbsp;&nbsp;
                                        <button
                                            className="btn btn-outline-info"
                                            style={{}}
                                            onClick={this.handleSubmitDraft}

                                        >
                                            Draft
                                        </button>

                                    </center>
                                </Grid>

                            </ThemeProvider>


                        </div>


                    </CardBody>
                </Card>


            );
        } else if (this.state.inputData.accountType === "NON-INSTAPACK" || this.props.subServiceType === "Company Account") {
            return (


                <Card>
                    <CardHeader color="rose">
                        <h4>Account Opening Agent Banking<a><CloseIcon onClick={this.close} style={{
                            position: 'absolute',
                            right: 10,
                            color: "#000000"
                        }}/></a></h4>

                    </CardHeader>
                    <CardBody>
                        <div>
                            <Dialog
                                fullWidth="true"
                                maxWidth="md"
                                open={this.state.accountDetailsModal}>
                                <DialogContent>

                                    <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                                </DialogContent>
                            </Dialog>
                            <ThemeProvider theme={theme}>

                                <Grid container spacing={1}>
                                    {this.renderNotification()}


                                    {this.returnJsonForm()}

                                    <Grid item xs='12'>
                                    </Grid>
                                    {this.renderAddButtonShow()}

                                    <Grid item xs='12'>
                                    </Grid>

                                    {
                                        this.addDeferalForm()
                                    }


                                    <Grid item xs='12'>
                                    </Grid>


                                    <Grid item xs={12}>

                                        {this.renderDefferalData()}


                                    </Grid>
                                    <Grid item xs={12}>

                                        {this.renderRemarksData()}


                                    </Grid>
                                    <Grid item xs='12'>
                                    </Grid>
                                    <Grid item xs={2}>
                                        {this.renderUploadButton()}

                                    </Grid>
                                    <Grid item xs={2}>
                                        <Grid item xs={2}>
                                            {this.renderAssignedImage()}
                                        </Grid>
                                    </Grid>
                                    <Grid item xs='12'>
                                    </Grid>
                                    {this.renderRemarks()}

                                    <Grid item xs='12'>
                                    </Grid>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.getMappingAllImage}>
                                        <DialogContent>
                                            <CSImageLinkEdit appId={this.state.appId} closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.uploadModal}>
                                        <DialogContent>
                                            <DSTUploadModal
                                                subServiceType={this.props.subServiceType}
                                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                                subServiceType={this.state.subServiceType} appId={this.state.appId}
                                                closeModal={this.closeUploadModal}/>
                                        </DialogContent>
                                    </Dialog>


                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.imageModalBoolean}>
                                        <DialogContent>

                                            <SingleImageShow data={this.state.selectImage}
                                                             closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>

                                    {this.renderApprovalOfficerList()}
                                    <Grid item xs='12'>
                                    </Grid>
                                    <center>
                                        <button
                                            className="btn btn-outline-danger"
                                            style={{}}
                                            onClick={this.handleSubmit}

                                        >
                                            Submit
                                        </button>
                                        &nbsp;
                                        <button
                                            className="btn btn-outline-info"
                                            style={{}}
                                            onClick={this.handleSubmitDraft}

                                        >
                                            Draft
                                        </button>

                                    </center>
                                </Grid>

                            </ThemeProvider>


                        </div>


                    </CardBody>
                </Card>


            );
        }


    }

}

export default withStyles(styles)(AgentBanking);
