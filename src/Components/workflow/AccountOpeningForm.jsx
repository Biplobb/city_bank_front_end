import React, {Component} from "react";
import Liability from "./CASA/Liability";

import DST from "./CSU/DST";
import {
    CSJsonFormForCasaIndividual,
    CSJsonFormForCasaJoint,
    CSJsonFormForCasaProprietorship,
    CSJsonFormForCasaCompany,


    CSJsonFormForDstIndividual,
    CSJsonFormForDstJoint,
    CSJsonFormForDstProprietorship,
    CSJsonFormForDstCompany,
} from "./WorkflowJsonForm4";

class AccountOpeningForm extends Component {
    state = {
        inputData: {},
        selectedDate: {},
        SelectedDropdownSearchData: null,
        dropdownSearchData: {},
        accountGenerateModal: null
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    close = () => {
        this.props.closeModal();
    }
    closeModal = () => {
        this.setState({
            accountGenerateModal: false
        })
        this.props.closeModal();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            accountGenerateModal: true
        })

    }
    CSU=()=>{

        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "INDIVIDUAL" || this.props.subServiceType === "Individual A/C")) {
            return (
                <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     commonJsonForm={CSJsonFormForDstIndividual}
                     serviceType={this.props.serviceType}
                     subServiceType={this.props.subServiceType}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && this.props.subServiceType === "Joint Account") {
            return (
               <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                     commonJsonForm={CSJsonFormForDstJoint}
                     serviceType={this.props.serviceType}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     subServiceType={this.props.subServiceType}
                     searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (
               <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     commonJsonForm={CSJsonFormForDstProprietorship}
                     serviceType={this.props.serviceType}
                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     subServiceType={this.props.subServiceType}
                     searchValue={this.props.searchValue}
                />
            )
        }else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Company Account")) {
            return (
                <DST closeModal={this.props.closeModal}
                           getAccountType={this.props.getAccountType}
                           accountType={this.props.accountType}
                           commonJsonForm={CSJsonFormForDstCompany}
                           serviceType={this.props.serviceType}
                           jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                           individualDedupData={this.props.individualDedupData}
                           jointDedupData={this.props.jointDedupData}
                           companyDedupData={this.props.companyDedupData}
                           subServiceType={this.props.subServiceType}
                           searchValue={this.props.searchValue}
                />
            )
        } else {
            alert("Please select Tab")
        }
    };
    casa = () => {
        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "INDIVIDUAL" || this.props.subServiceType === "Individual A/C")) {
            return (
                <Liability closeModal={this.props.closeModal}
                           getAccountType={this.props.getAccountType}
                           accountType={this.props.accountType}
                           commonJsonForm={CSJsonFormForCasaIndividual}
                           serviceType={this.props.serviceType}
                           subServiceType={this.props.subServiceType}
                           individualDedupData={this.props.individualDedupData}
                           jointDedupData={this.props.jointDedupData}
                           companyDedupData={this.props.companyDedupData}
                           searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && this.props.subServiceType === "Joint Account") {
            return (
                <Liability closeModal={this.props.closeModal}
                           getAccountType={this.props.getAccountType}
                           accountType={this.props.accountType}
                           jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                           commonJsonForm={CSJsonFormForCasaJoint}
                           serviceType={this.props.serviceType}
                           individualDedupData={this.props.individualDedupData}
                           jointDedupData={this.props.jointDedupData}
                           companyDedupData={this.props.companyDedupData}
                           subServiceType={this.props.subServiceType}
                           searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (
                <Liability closeModal={this.props.closeModal}
                           getAccountType={this.props.getAccountType}
                           accountType={this.props.accountType}
                           commonJsonForm={CSJsonFormForCasaProprietorship}
                           serviceType={this.props.serviceType}
                           jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                           individualDedupData={this.props.individualDedupData}
                           jointDedupData={this.props.jointDedupData}
                           companyDedupData={this.props.companyDedupData}
                           subServiceType={this.props.subServiceType}
                           searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Company Account")) {
            return (
                <Liability closeModal={this.props.closeModal}
                           getAccountType={this.props.getAccountType}
                           accountType={this.props.accountType}
                           commonJsonForm={CSJsonFormForCasaCompany}
                           serviceType={this.props.serviceType}
                           jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                           individualDedupData={this.props.individualDedupData}
                           jointDedupData={this.props.jointDedupData}
                           companyDedupData={this.props.companyDedupData}
                           subServiceType={this.props.subServiceType}
                           searchValue={this.props.searchValue}
                />
            )
        } else {
            alert("Please select Tab")
        }
    };

    casaExist = () => {
        if (this.props.serviceType === "ExistAccountOpening" && (this.props.subServiceType === "INDIVIDUAL" || this.props.subServiceType === "Individual A/C")) {
            return (
                <Liability closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                commonJsonForm={CSJsonFormForCasaIndividual}
                                serviceType={this.props.serviceType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                individualDedupData={this.props.individualDedupData}
                                jointDedupData={this.props.jointDedupData}
                                companyDedupData={this.props.companyDedupData}
                                subServiceType={this.props.subServiceType}
                                searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "ExistAccountOpening" && this.props.subServiceType === "Joint Account") {
            return (
                <Liability closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                commonJsonForm={CSJsonFormForCasaJoint}
                                serviceType={this.props.serviceType}
                                individualDedupData={this.props.individualDedupData}
                                jointDedupData={this.props.jointDedupData}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                companyDedupData={this.props.companyDedupData}
                                subServiceType={this.props.subServiceType}
                                searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "ExistAccountOpening" && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (
                <Liability closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                commonJsonForm={CSJsonFormForCasaProprietorship}
                                serviceType={this.props.serviceType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                individualDedupData={this.props.individualDedupData}
                                jointDedupData={this.props.jointDedupData}
                                companyDedupData={this.props.companyDedupData}
                                subServiceType={this.props.subServiceType}
                                searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "ExistAccountOpening" && this.props.subServiceType === "Company Account") {
            return (
                <Liability closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                commonJsonForm={CSJsonFormForCasaCompany}
                                serviceType={this.props.serviceType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                individualDedupData={this.props.individualDedupData}
                                jointDedupData={this.props.jointDedupData}
                                companyDedupData={this.props.companyDedupData}
                                subServiceType={this.props.subServiceType}
                                searchValue={this.props.searchValue}
                />
            )
        } else {
            alert("Please select Tab");
        }
    };
    renderInboxCase = () => {

        //alert(this.props.serviceType)
        //alert(this.props.subServiceType)

        if (this.props.serviceType === "Account Opening" && this.props.workplace==="BRANCH") {

            return (
                this.casa()
            )
        }
        else if (this.props.serviceType === "Account Opening" && this.props.workplace==="CSU") {

            return (
                this.CSU()
            )
        }

        else if (this.props.serviceType === "ExistAccountOpening") {
            return (
                this.casaExist()
            )
        }

    }

    render() {
        return (
            this.renderInboxCase()
        )
    }

}

export default AccountOpeningForm;