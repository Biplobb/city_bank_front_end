function makeReadOnlyObject(object) {
    if(object.hide!==true){
        let returnObject = JSON.parse(JSON.stringify(object));

        let returnObjectVariables = returnObject;

        for (let i = 0; i < returnObjectVariables.length; i++) {

            returnObjectVariables[i]["readOnly"] = true;
        }
        return returnObject;
    }

}

//#####################CASA#####################################################

//###CS ,BM  ,BOM ,MAKER,CHECKER     INDIVIDUAL#####################
let CSJsonFormForCasaIndividual = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 3,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 3,


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid":3
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3
    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];
let BOMJsonFormForCasaIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 12,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 12,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 12,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 12,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 12,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 12,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 12,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 12,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid":12
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid":12
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12
    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12,

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12,

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12,

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12,
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let BOMJsonFormForDSTIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 12,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 12,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 12,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 12,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 12,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 12,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 12,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 12,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12,

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12,

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12,

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12,
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));

 let BMJsonFormForCasaIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 3,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 3,


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid":3
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3
    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let MAKERJsonFormForCasaIndividual =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForCasaIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        //"enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));
//###CS ,BM  ,BOM ,MAKER,CHECKER     JOINT#####################
let CSJsonFormForCasaJoint = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid":3


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":3


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },



    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


];
let BMJsonFormForCasaJoint = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid":3


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":3


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },



    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


])));
let BOMJsonFormForCasaJoint = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },



    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid": 12,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },



    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


])));
let MAKERJsonFormForCasaJoint =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForCasaJoint = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));

//###CS ,BM  ,BOM ,MAKER,CHECKER     PROPRIETORSHIP#####################
let CSJsonFormForCasaProprietorship = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },


    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


];
let BMJsonFormForCasaProprietorship =makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },


    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


])));
let BOMJsonFormForCasaProprietorship = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },


    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":12


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":12


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":12

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":12


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":12


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":12


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },


    {
        "varName": "dpsAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsTenor",
        "type": "text",
        "label": "Tenor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsInterest",
        "type": "text",
        "label": "Interest %",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSi",
        "type": "text",
        "label": "SI Instruction",

        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSchemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "12",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


])));
let MAKERJsonFormForCasaProprietorship=[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForCasaProprietorship= makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));

//###CS ,BM  ,BOM ,MAKER,CHECKER     COMPANY#####################
let CSJsonFormForCasaCompany = [

    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];
let BMJsonFormForCasaCompany = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let BOMJsonFormForCasaCompany = makeReadOnlyObject(JSON.parse(JSON.stringify([

    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":12


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":12


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":12

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":12


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":12


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":12


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },


    {
        "varName": "dpsAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsTenor",
        "type": "text",
        "label": "Tenor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsInterest",
        "type": "text",
        "label": "Interest %",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSi",
        "type": "text",
        "label": "SI Instruction",

        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSchemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let MAKERJsonFormForCasaCompany =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForCasaCompany = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));

//#####################DST#####################################################

//###CS ,BM  ,BOM ,MAKER,CHECKER     INDIVIDUAL#####################
let CSJsonFormForDstIndividual = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 3,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 3,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];
let BOMJsonFormForDstIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },


    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 12,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 12,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 12,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 12,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 12,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 12,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 12,

    },
    {
        "varName": "nationality",
        "type": "select",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 12,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12,

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12,

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12,

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12,
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 12,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

    /////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

    ///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
 let BMJsonFormForDstIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
     {
         "varName": "accountSource",
         "type": "select",
         "label": "Account Source",
         "grid": 3,
         "enum": [
             "FINACLE",
             "ABABIL"
         ]

     },
     {
         "varName": "accountType",
         "type": "select",
         "label": "Account Type",
         "enum":[
             "INSTAPACK",
             "NON-INSTAPACK"
         ],
         "grid": 3,
     },
     {
         "varName": "accountNumber",
         "type": "text",
         "label": "Account Number",

         "conditional": true,
         "conditionalVarName": "accountType",
         "conditionalVarValue": "INSTAPACK",
         "grid": 3,
     },
     {
         "varName": "accType",
         "type": "select",
         "required": true,
         "errorMessage" : "Error",
         "label": "Account Type",
         "enum":[
             "SAVINGS",
             "CURRENT"
         ],
         "grid": 3,
     },


     {
         "varName": "nid",
         "type": "text",
         "errorMessage" : "Error",
         "required": true,
         "validation" : "nid",
         "label": "NID",
         "grid": 3,

     },
     {
         "varName": "passport",
         "type": "text",
         "errorMessage" : "Error",
         "label": "Passport",
         "grid": 3,


     },
     {
         "varName": "customerName",
         "type": "text",
         "errorMessage" : "Error",
         "label": "Customer Name",
         "required": true,
         "grid": 3,

     },
     /* {
          "varName": "dob",
          "type": "date",
          "label": "Date Of Birth",
          "required": true,
          "grid": 6,


      },*/
     {
         "varName": "email",
         "type": "text",
         "label": "Email ",
         "errorMessage" : "Error",
         "validation" : "email",
         "required": true,

         "grid": 3,


     },

     {
         "varName": "phone",
         "type": "text",
         "errorMessage" : "Error",
         "validation" : "phone",
         "required": true,
         "label": "Mobile Number",
         "grid": 3,


     },

     {
         "varName": "tin",
         "type": "text",
         "label": "E-Tin",
         "grid": 3,

     },


     {
         "varName": "registrationNo",
         "type": "text",
         "label": "Birth Certificate/Driving License",
         "grid": 3,

     },
     {
         "varName": "nationality",
         "type": "select",
         "errorMessage" : "Error",
         "label": "Nationality",
         "enum":[
             "BANGLADESH",
             "JAPAN",
             "OTHER",
         ],
         "required": true,
         "grid": 3,


     },
     {
         "varName": "communicationAddress1",
         "type": "text",
         "label": "Communication Address 1",
         "grid": 3,

     },

     {
         "varName": "communicationAddress2",
         "type": "text",
         "label": "Communication Address 2",
         "grid": 3,
     },

     {
         "varName": "city2",
         "label": "City",
         "type": "select",
         "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
         "grid": 3,

     },

     {
         "varName": "state2",
         "type": "text",
         "label": "State",
         "grid": 3,

     },

     {
         "varName": "postalCode3",
         "type": "text",
         "label": "Postal Code",
         "grid": 3,

     },
     {
         "varName": "schemeCode",
         "type": "text",
         "required": true,
         "label": "Scheme Code",
         "grid": 3,
     },
     {
         "varName": "currency",
         "type": "select",
         "label": "Currency",
         "required": true,
         "grid": 3,
         "enum": [
             "BDT",
             "USD",
             "EUR",
             "GBP"
         ]
     },
     {
         "varName": "rmCode",
         "type": "text",
         "required": true,
         "label": "RM Code",
         "grid": 3,
     },
     {
         "varName": "sbsCode",
         "type": "text",
         "required": true,
         "label": "SBS Code",
         "grid": 3,
     },
     /*{
         "varName": "occupationCode",
         "type": "text",
         "label": "Occupation Code",
         "grid": 6,

     },*/
     {
         "varName": "ccepCompanyCode",
         "type": "text",
         "label": "CCEP Company Code",
         "grid": 3,

     },
     {
         "varName": "priority",
         "type": "select",
         "label": "Priority",
         "enum": [
             "GENERAL",
             "HIGH",
         ],
         "grid": 3,
     } ,
     {
         "varName": "statementFacility",
         "type": "select",
         "grid": 3,
         "enum":[
             "Printed Statement",
             "E-Statement",
             "NO",
         ],
         "label":"Statement",

     },
     {
         "varName": "smsAlertRequest",
         "type": "checkbox",
         "label": "SMS Alert Request",
         "grid": 3

     },
     {
         "varName": "callCenterRegistration",
         "type": "checkbox",
         "label": "Call Center Registration",
         "grid": 3

     },



     {
         "varName": "lockerFacility",
         "type": "checkbox",
         "label": "Locker Facility",
         "grid": 3

     },
     {
         "varName": "loanRequest",
         "type": "checkbox",
         "label": "Loan Request",
         "grid": 3
     },


     /*
 {
     "varName": "dpsAmount",
     "type": "text",
     "label": "Amount",
     "grid": 3,
     "conditional": true,
     "conditionalVarName": "dpsRequest",
     "conditionalVarValue": true,
 },
 {
     "varName": "dpsTenor",
     "type": "text",
     "label": "Tenor",
     "grid": 3,
     "conditional": true,
     "conditionalVarName": "dpsRequest",
     "conditionalVarValue": true,
 },
 {
     "varName": "dpsInterest",
     "type": "text",
     "label": "Interest %",
     "grid": 3,
     "conditional": true,
     "conditionalVarName": "dpsRequest",
     "conditionalVarValue": true,
 },
 {
     "varName": "dpsSi",
     "type": "text",
     "label": "SI Instruction",

     "grid": 3,
     "conditional": true,
     "conditionalVarName": "dpsRequest",
     "conditionalVarValue": true,
 },
 {
     "varName": "dpsSchemeCode",
     "type": "text",
     "label": "Scheme Code",
     "grid": 3,
     "conditional": true,
     "conditionalVarName": "dpsRequest",
     "conditionalVarValue": true,
 },*/

     {
         "varName": "fdrRequest",
         "type": "checkbox",
         "label": "FDR Request",
         "grid": 12
     },
     {
         "varName": "accountsType",
         "type": "select",
         "grid": 3,
         "enum":[
             "Fix General",
             "Double",
             "Monthly",
         ],
         "label":"Type Of Account",
         "conditional": true,
         "conditionalVarName": "fdrRequest",
         "conditionalVarValue": true,

     },
     {
         "varName": "doubleTennor",
         "type": "select",
         "grid": 3,
         "enum":[
             "Monthly"
         ],
         "label":"Tennor Type",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Double",
     },{
         "varName": "tennordDouble",
         "type": "text",
         "label": "Tennor",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Double",
     },{
         "varName": "doubleAmount",
         "type": "text",
         "label": "Amount",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Double",
     },{
         "varName": "doubleInterest",
         "type": "text",
         "label": "Interest",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Double",
     },{
         "varName": "doubleSiMaturity",
         "type": "select",
         "grid": 3,
         "enum":[
             "No"
         ],
         "label":"SI Maturity",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Double",
     },

/////  Account Type Fix General /////
     {
         "varName": "fixGTennor",
         "type": "select",
         "grid": 3,
         "enum":[
             "Day",
             "Monthly"
         ],
         "label":"Tennor Type",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Fix General",
     },{
         "varName": "tennorFixG",
         "type": "text",
         "label": "Tennor",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Fix General",
     },{
         "varName": "fixGAmount",
         "type": "text",
         "label": "Amount",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Fix General",
     },{
         "varName": "fixGInterest",
         "type": "text",
         "label": "Interest",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Fix General",
     },{
         "varName": "fixGSiMaturity",
         "type": "select",
         "grid": 3,
         "enum":[
             "Yes",
             "No",
             "Encash at Maturity to my / Our Account No",
         ],
         "label":"SI Maturity",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Fix General",
     },{
         "varName": "fixdGMaturityYes",
         "type": "select",
         "label": "Maturity Yes",
         "enum":[
             "Renew Principal Only and Credit Interest to the Account No",
             "Renew Both Principal and Interest"
         ],
         "grid": 6,
         "conditional": true,
         "conditionalVarName": "fixGSiMaturity",
         "conditionalVarValue": "Yes",
     },

///////  Month Tenor /////
     {
         "varName": "monthTennor",
         "type": "select",
         "grid": 3,
         "enum":[
             "Monthly"
         ],
         "label":"Tennor Type",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     }, {
         "varName": "tennorMonth",
         "type": "text",
         "label": "Tennor",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     },{
         "varName": "monthAmount",
         "type": "text",
         "label": "Amount",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     },{
         "varName": "monthInterest",
         "type": "text",
         "label": "Interest",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     },{
         "varName": "monthSiMaturity",
         "type": "select",
         "grid": 3,
         "enum":[
             "Yes",
         ],
         "label":"SI Maturity",
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     },{
         "varName": "monthMaturityYes",
         "type": "select",
         "label": "Maturity Yes",
         "enum":[
             "Renew Principal Only and Credit Interest to the Account No"
             // "Renew Principal Only "
         ],
         "grid": 6,
         "conditional": true,
         "conditionalVarName": "accountsType",
         "conditionalVarValue": "Monthly",
     },
     {
         "varName": "cityTouchRequest",
         "type": "checkbox",
         "label": "City Touch",
         "grid": 12

     },



     //cheque book
     {
         "varName": "chequeBookRequest",
         "type": "checkbox",
         "label": "Cheque Book Request",
         "grid": 12,

     },
     {"varName": "pageOfChequeBook",
         "type": "select",
         "enum":[
             "25",
             "50",
             "100",
         ],
         "label": "Page Of Cheque Book",
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,
         "grid": 3

     },
     {"varName": "numberOfChequeBookRequest",
         "type": "select",
         "enum":[
             "1",
             "2",
             "3",
             "4",
             "5",
         ],
         "label": "Number Of Cheque Book",
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,
         "grid": 3,


     },
     {
         "varName": "chequeBookDeliveryType",
         "type": "select",
         "label": "Delivery Type",
         "enum": [
             "Courier",
             "Branch"
         ],
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,

     },
     {
         "varName": "chequeBookBranch",
         "type": "select",
         "label": "Branch Name",
         "enum": [,
             "GULSHAN 1",
             "MOTHIJHEEL 1",
             "DHANMONDI",
         ],
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "chequeBookDeliveryType",
         "conditionalVarValue": "Branch",

     },
     {
         "varName": "chequeBookDesign",
         "type": "select",
         "label": "Cheque Book Design",
         "enum": [
             "Sapphire",
             "Citygem",
             "City Alo",
             "Other"
         ],
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,


     },
     {
         "varName": "chequeBookDesignOther",
         "type": "text",
         "label": "Other",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": "Other",


     },
//Debit Card
     {
         "varName": "debitCard",
         "type": "checkbox",
         "label": "Debit Card",
         "grid": 12
     },
     {
         "varName": "customerName",
         "type": "text",
         "label": "Name On Card",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "cardType",
         "type": "select",
         "label": "Card Type",
         "grid": 3,
         "enum": [
             "VISA",
             "MASTER",
             "CITYMAXX",
         ],
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },
     {
         "varName": "debitCardDeliveryType",
         "type": "select",
         "label": "Delivery Type",
         "enum": [
             "Courier",
             "Branch"
         ],
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "debitRequestkBranch",
         "type": "select",
         "label": "Branch Name",
         "enum": [,
             "GULSHAN 1",
             "MOTHIJHEEL 1",
             "DHANMONDI",
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "debitCardDeliveryType",
         "conditionalVarValue": "Branch",

     },
     {
         "varName": "instaDebitCard",
         "type": "checkbox",
         "label": "Insta Debit Card",
         "grid": 12
     },
     {
         "varName": "customerName",
         "type": "text",
         "label": "Name On Card",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "instaCardType",
         "type": "select",
         "label": "Card Type",
         "enum":[
             "CITYMAXX"
         ],
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },
     {
         "varName": "instaClientId",
         "type": "text",
         "label": "Client ID",
         "grid": 3,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },
 ])));
let MAKERJsonFormForDstIndividual =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForDstIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        //"enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));
//###CS ,BM  ,BOM ,MAKER,CHECKER     JOINT#####################
let CSJsonFormForDstJoint = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },


    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":3


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":3


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


];
let BMJsonFormForDstJoint = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":3


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":3


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


])));
let BOMJsonFormForDstJoint = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },


    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":12


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":12,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":12


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },


    {
        "varName": "dpsAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsTenor",
        "type": "text",
        "label": "Tenor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsInterest",
        "type": "text",
        "label": "Interest %",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSi",
        "type": "text",
        "label": "SI Instruction",

        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSchemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },


])));
let MAKERJsonFormForDstJoint =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForDstJoint = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));


//###CS ,BM  ,BOM ,MAKER,CHECKER     PROPRIETORSHIP#####################
let CSJsonFormForDstProprietorship = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


];
let BMJsonFormForDstProprietorship =makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },


    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


])));
let BOMJsonFormForDstProprietorship = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":12


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":12


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":12

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":12


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":12


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":12


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },


    {
        "varName": "dpsAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsTenor",
        "type": "text",
        "label": "Tenor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsInterest",
        "type": "text",
        "label": "Interest %",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSi",
        "type": "text",
        "label": "SI Instruction",

        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSchemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "12",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    }


])));
let MAKERJsonFormForDstProprietorship=[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForDstProprietorship= makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));

//###CS ,BM  ,BOM ,MAKER,CHECKER     COMPANY#####################
let CSJsonFormForDstCompany = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },


    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];
let BMJsonFormForDstCompany = makeReadOnlyObject(JSON.parse(JSON.stringify([

    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let BOMJsonFormForDstCompany = makeReadOnlyObject(JSON.parse(JSON.stringify([

    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 12,
        "enum": [
            "FINACLE",
            "ABABIL"
        ]

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 12,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",

        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 12,
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 12,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":12


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":12


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":12

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":12


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":12


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":12


    },

    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,

    },

    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 12,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 12,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 12,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 12,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 12,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 12,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 12,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 12

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    },


    {
        "varName": "dpsAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsTenor",
        "type": "text",
        "label": "Tenor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsInterest",
        "type": "text",
        "label": "Interest %",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSi",
        "type": "text",
        "label": "SI Instruction",

        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "dpsSchemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "dpsRequest",
        "conditionalVarValue": true,
    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 12,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 12,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 12,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 12,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 12,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
let MAKERJsonFormForDstCompany =[
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "select",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

];
let CHECKERJsonFormForDstCompany = makeReadOnlyObject(JSON.parse(JSON.stringify( [
    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "CbNumber",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone1",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
    },

    {
        "varName": "statement",
        "type": "text",
        "label": "Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "despatchMode",
        "type": "text",
        "label": "Despatch Mode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Contact Phone Number",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Ac ID",
        "grid": 12,
        "length": 13,
        required: true,
    },

    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeName",
        "type": "text",
        "label": "Nominee Name",
        "grid": 12,
        required: true,
    },

    {
        "varName": "relationship",
        "type": "text",
        "label": "Relationship",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob1",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address11",
        "type": "text",
        "label": "Address 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "address22",
        "type": "text",
        "label": "Address 2",
        "grid": 12,
    },

    {
        "varName": "cityCode1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "stateCode1",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode1",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country1",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "regNo",
        "type": "text",
        "label": "Reg No",
        "grid": 12,
        required: true,
    },

    {
        "varName": "nomineeMinor",
        "type": "text",
        "label": "Nominee Minor",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "guardiansName",
        "type": "text",
        "label": "Guardians Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",

    },

    {
        "varName": "guardianCode",
        "type": "text",
        "label": "Guardian Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address4",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "cityCode2",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "stateCode2",
        "type": "text",
        "label": "State Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postalCode2",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country2",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "nomineeMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "city1",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "varName": "country3",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
    },

    {
        "label": "AOF 3",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "modeOfOperation",
        "type": "text",
        "label": "Mode Of Operation",
        "grid": 12,
        required: true,
    },

    {
        "label": "AOF 4",
        "type": "title",
        "grid": 12,
    },

    {
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode31",
        "type": "text",
        "label": "Free Code 3 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "targetSchemeCode",
        "type": "text",
        "label": "Target Scheme Code",
        "grid": 12,
        required: true,
    },

    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
    },

    {
        "varName": "residentStatus",
        "type": "text",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
    },

    {
        "varName": "dob2",
        "type": "date",
        "label": "D.O.B",
        "grid": 12,
        required: true,
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
    },

    {
        "varName": "maritialStatus",
        "type": "text",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
    },

    {
        "varName": "issueDate",
        "type": "date",
        "label": "Issue Date",
        "grid": 12,
    },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
    },

    {
        "varName": "expiryDate",
        "type": "date",
        "label": "Expiry Date",
        "grid": 12,
    },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
    },
    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country4",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo4",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
    },

    {
        "varName": "city3",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "country5",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
    },

    {
        "varName": "city4",
        "type": "text",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
    },

    {
        "varName": "country6",
        "type": "text",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
    },

    {
        "varName": "customerOccupation",
        "type": "text",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "Doc Collect To Ensure SOF",
        "grid": 12,
        required: true,
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "howTheAddress",
        "type": "text",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Beneficial Owner",
        "grid": 12,
        required: true,
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "text",
        "enum":["Yes","No"],
        "label": "Customer's Occupation or Business",
        "grid": 12,
        required: true,
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "text",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
    },

    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid": 12,
        required: true,
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid": 12,
        required: true,
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        "label": "Withdrawal (Transfer/instrument)",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid": 12,
        required: true,
        numeric: true,
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid": 12,
        required: true,
        numeric: true,
    },
    {
        "label": "Others",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
    },

    {
        "varName": "statusAsOnDate",
        "type": "date",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "label":"Staff Flag",
        "enum":["Yes","No"],
        "grid": 12,
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,
        required: true,
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "accountManager",
        "type": "text",
        "label": "Account Manager",
        "grid": 12,
        required: true,
    },

    {
        "varName": "cashLimit",
        "type": "text",
        "label": "Cash Limit",
        "grid": 12,
    },

    {
        "varName": "clearingLimit",
        "type": "text",
        "label": "Clearing Limit",
        "grid": 12,
    },

    {
        "varName": "transferLimit",
        "type": "text",
        "label": "Transfer Limit",
        "grid": 12,
    },

    {
        "varName": "remarks",
        "type": "text",
        "label": "Remarks",
        "grid": 12,
    },

    {
        "varName": "statementFrequency",
        "type": "text",
        "label": "Statement Frequency",
        "grid": 12,
        required: true,
    },

    {
        "varName": "occupationOode",
        "type": "text",
        "label": "Occupation Oode",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode2",
        "type": "text",
        "label": "Free Code 1 ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode7",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode9",
        "type": "text",
        "label": "Free Code 9",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeCode10",
        "type": "text",
        "label": "Free Code 10",
        "grid": 12,
        required: true,
    },

    {
        "varName": "freeText11",
        "type": "text",
        "label": "Free Text 11",
        "grid": 12,
        required: true,
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
    },

    {
        "varName": "function",
        "type": "text",
        "label": "Function",
        "grid": 12,
        required: true,
    },

    {
        "varName": "trialMode",
        "type": "text",
        "label": "Trial Mode",
        "grid": 12,
        required: true,
    },

])));


//#####################Agent Banking#####################################################

//###CS ,BM  ,BOM ,MAKER,CHECKER     INDIVIDUAL#####################
let AbhMakerJsonFormForAgentIndividual = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],
        "readOnly":true


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
        "readOnly":true,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
        "readOnly":true
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "readOnly":true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "nid",
        "type": "text",
        "readOnly":true,
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "readOnly":true,
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "readOnly":true,
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "readOnly":true,
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 3,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "readOnly":true,
        "label": "Mobile Number",
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "readOnly":true,
        "label": "E-Tin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "readOnly":true,
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "select",
        "readOnly":true,
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 3,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "readOnly":true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "readOnly":true,
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "readOnly":true,
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "readOnly":true,
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },

     
    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/
 
   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];
let AbhMakerJsonFormForAgentNonIndividual = [
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],
        "readOnly":true


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
        "readOnly":true,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
        "readOnly":true
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "readOnly":true,
        "grid": 3,
    },
    {
        "varName": "cbNumber",
        "type": "text",
        "label": "CB Number",
        "required": true,
        "readOnly":true,
        "grid": 3,
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly":true,

        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "readOnly":true,
        "required": false,


        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "readOnly":true,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "readOnly":true,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly":true,

        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "readOnly":true,
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    },


    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "readOnly":true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "readOnly":true,
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "readOnly":true,
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "readOnly":true,
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },


    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/

    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "select",
        "grid": 3,
        "enum":[
            "Fix General",
            "Double",
            "Monthly",
        ],
        "label":"Type Of Account",
        "conditional": true,
        "conditionalVarName": "fdrRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "doubleTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },{
        "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "No"
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
    },

/////  Account Type Fix General /////
    {
        "varName": "fixGTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Day",
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
            "No",
            "Encash at Maturity to my / Our Account No",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
    },{
        "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No",
            "Renew Both Principal and Interest"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
    },

///////  Month Tenor /////
    {
        "varName": "monthTennor",
        "type": "select",
        "grid": 3,
        "enum":[
            "Monthly"
        ],
        "label":"Tennor Type",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    }, {
        "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
            "Yes",
        ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },{
        "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
            "Renew Principal Only and Credit Interest to the Account No"
            // "Renew Principal Only "
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
    },
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },



    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "select",
        "enum":[
            "25",
            "50",
            "100",
        ],
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "select",
        "enum":[
            "1",
            "2",
            "3",
            "4",
            "5",
        ],
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,


    },
    {
        "varName": "chequeBookDesignOther",
        "type": "text",
        "label": "Other",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": "Other",


    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
];

let AbhApprovalJsonFormForAgentIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
    {
        "varName": "accountSource",
        "type": "select",
        "label": "Account Source",
        "grid": 3,
        "errorMessage" : "Error",
        "required": true,
        "enum": [
            "FINACLE",
            "ABABIL"
        ],


    },
    {
        "varName": "accountType",
        "type": "select",
        "errorMessage" : "Error",
        "label": "CB Type",
        "enum":[
            "INSTAPACK",
            "NON-INSTAPACK"
        ],
        "grid": 3,
        "required": true,
        "conditional": true,
        "conditionalVarName": "accountSource",
        "conditionalVarValue": "FINACLE",
    },
    {
        "varName": "accType",
        "type": "select",
        "required": true,
        "errorMessage" : "Error",
        "label": "Account Type",
        "enum":[
            "SAVINGS",
            "CURRENT"
        ],
        "grid": 3,
    },
    {
        "varName": "accountNumber",
        "type": "text",
        "label": "Account Number",
        "required": true,
        "errorMessage" : "Error",
        "conditional": true,
        "conditionalVarName": "accountType",
        "conditionalVarValue": "INSTAPACK",
        "grid": 3,
    },



    {
        "varName": "nid",
        "type": "text",
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "errorMessage" : "Error",
        "validation" : "email",
        "required": true,

        "grid": 3,


    },

    {
        "varName": "phone",
        "type": "text",
        "errorMessage" : "Error",
        "validation" : "phone",
        "required": true,
        "label": "Mobile Number",
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "E-Tin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "text",
        "errorMessage" : "Error",
        "label": "Nationality",
        "enum":[
            "BANGLADESH",
            "JAPAN",
            "OTHER",
        ],
        "required": true,
        "grid": 3,


    },
    {
        "varName": "communicationAddress1",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 3,

    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 3,
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 3,

    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 3,

    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 3,

    },
    {
        "varName": "schemeCode",
        "type": "text",
        "required": true,
        "label": "Scheme Code",
        "grid": 3,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "required": true,
        "grid": 3,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "required": true,
        "label": "RM Code",
        "grid": 3,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "required": true,
        "label": "SBS Code",
        "grid": 3,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 3,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 3,
    } ,
    {
        "varName": "statementFacility",
        "type": "select",
        "grid": 3,
        "enum":[
            "Printed Statement",
            "E-Statement",
            "NO",
        ],
        "label":"Statement",

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 3

    },
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 3

    },



    {
        "varName": "lockerFacility",
        "type": "checkbox",
        "label": "Locker Facility",
        "grid": 3

    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 3
    },

     
    /*
{
    "varName": "dpsAmount",
    "type": "text",
    "label": "Amount",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsTenor",
    "type": "text",
    "label": "Tenor",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsInterest",
    "type": "text",
    "label": "Interest %",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSi",
    "type": "text",
    "label": "SI Instruction",

    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},
{
    "varName": "dpsSchemeCode",
    "type": "text",
    "label": "Scheme Code",
    "grid": 3,
    "conditional": true,
    "conditionalVarName": "dpsRequest",
    "conditionalVarValue": true,
},*/
 
   {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    


    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {"varName": "pageOfChequeBook",
        "type": "text",
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3

    },
    {"varName": "numberOfChequeBookRequest",
        "type": "text",
        "label": "Number Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 3,


    },
    {
        "varName": "chequeBookDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "chequeBookBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 3,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardDeliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "debitRequestkBranch",
        "type": "select",
        "label": "Branch Name",
        "enum": [,
            "GULSHAN 1",
            "MOTHIJHEEL 1",
            "DHANMONDI",
        ],
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "debitCardDeliveryType",
        "conditionalVarValue": "Branch",

    },
    {
        "varName": "instaDebitCard",
        "type": "checkbox",
        "label": "Insta Debit Card",
        "grid": 12
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Name On Card",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "instaCardType",
        "type": "select",
        "label": "Card Type",
        "enum":[
            "CITYMAXX"
        ],
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "instaClientId",
        "type": "text",
        "label": "Client ID",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "instaDebitCard",
        "conditionalVarValue": true,

    },
])));
 let AbhCheckerJsonFormForAgentIndividual = makeReadOnlyObject(JSON.parse(JSON.stringify([
     {
         "varName": "accountSource",
         "type": "select",
         "label": "Account Source",
         "grid": 12,
         "errorMessage" : "Error",
         "required": true,
         "enum": [
             "FINACLE",
             "ABABIL"
         ],


     },
     {
         "varName": "accountType",
         "type": "select",
         "errorMessage" : "Error",
         "label": "CB Type",
         "enum":[
             "INSTAPACK",
             "NON-INSTAPACK"
         ],
         "grid": 12,
         "required": true,
         "conditional": true,
         "conditionalVarName": "accountSource",
         "conditionalVarValue": "FINACLE",
     },
     {
         "varName": "accType",
         "type": "select",
         "required": true,
         "errorMessage" : "Error",
         "label": "Account Type",
         "enum":[
             "SAVINGS",
             "CURRENT"
         ],
         "grid": 12,
     },
     {
         "varName": "accountNumber",
         "type": "text",
         "label": "Account Number",
         "required": true,
         "errorMessage" : "Error",
         "conditional": true,
         "conditionalVarName": "accountType",
         "conditionalVarValue": "INSTAPACK",
         "grid": 12,
     },



     {
         "varName": "nid",
         "type": "text",
         "errorMessage" : "Error",
         "required": true,
         "validation" : "nid",
         "label": "NID",
         "grid": 12,

     },
     {
         "varName": "passport",
         "type": "text",
         "errorMessage" : "Error",
         "label": "Passport",
         "grid": 12,


     },
     {
         "varName": "customerName",
         "type": "text",
         "errorMessage" : "Error",
         "label": "Customer Name",
         "required": true,
         "grid": 12,

     },
     /* {
          "varName": "dob",
          "type": "date",
          "label": "Date Of Birth",
          "required": true,
          "grid": 6,


      },*/
     {
         "varName": "email",
         "type": "text",
         "label": "Email ",
         "errorMessage" : "Error",
         "validation" : "email",
         "required": true,

         "grid": 12,


     },

     {
         "varName": "phone",
         "type": "text",
         "errorMessage" : "Error",
         "validation" : "phone",
         "required": true,
         "label": "Mobile Number",
         "grid": 12,


     },

     {
         "varName": "tin",
         "type": "text",
         "label": "E-Tin",
         "grid": 12,

     },


     {
         "varName": "registrationNo",
         "type": "text",
         "label": "Birth Certificate/Driving License",
         "grid": 12,

     },
     {
         "varName": "nationality",
         "type": "text",
         "errorMessage" : "Error",
         "label": "Nationality",
         "enum":[
             "BANGLADESH",
             "JAPAN",
             "OTHER",
         ],
         "required": true,
         "grid": 12,


     },
     {
         "varName": "communicationAddress1",
         "type": "text",
         "label": "Communication Address 1",
         "grid": 12,

     },

     {
         "varName": "communicationAddress2",
         "type": "text",
         "label": "Communication Address 2",
         "grid": 12,
     },

     {
         "varName": "city2",
         "label": "City",
         "type": "select",
         "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
         "grid": 12,

     },

     {
         "varName": "state2",
         "type": "text",
         "label": "State",
         "grid":12,

     },

     {
         "varName": "postalCode3",
         "type": "text",
         "label": "Postal Code",
         "grid": 12,

     },
     {
         "varName": "schemeCode",
         "type": "text",
         "required": true,
         "label": "Scheme Code",
         "grid": 12,
     },
     {
         "varName": "currency",
         "type": "select",
         "label": "Currency",
         "required": true,
         "grid": 12,
         "enum": [
             "BDT",
             "USD",
             "EUR",
             "GBP"
         ]
     },
     {
         "varName": "rmCode",
         "type": "text",
         "required": true,
         "label": "RM Code",
         "grid": 12,
     },
     {
         "varName": "sbsCode",
         "type": "text",
         "required": true,
         "label": "SBS Code",
         "grid": 12,
     },
     /*{
         "varName": "occupationCode",
         "type": "text",
         "label": "Occupation Code",
         "grid": 6,

     },*/
     {
         "varName": "ccepCompanyCode",
         "type": "text",
         "label": "CCEP Company Code",
         "grid": 12,

     },
     {
         "varName": "priority",
         "type": "select",
         "label": "Priority",
         "enum": [
             "GENERAL",
             "HIGH",
         ],
         "grid": 12,
     } ,
     {
         "varName": "statementFacility",
         "type": "select",
         "grid": 12,
         "enum":[
             "Printed Statement",
             "E-Statement",
             "NO",
         ],
         "label":"Statement",

     },
     {
         "varName": "smsAlertRequest",
         "type": "checkbox",
         "label": "SMS Alert Request",
         "grid": 3

     },
     {
         "varName": "callCenterRegistration",
         "type": "checkbox",
         "label": "Call Center Registration",
         "grid": 3

     },



     {
         "varName": "lockerFacility",
         "type": "checkbox",
         "label": "Locker Facility",
         "grid": 3

     },
     {
         "varName": "loanRequest",
         "type": "checkbox",
         "label": "Loan Request",
         "grid": 3
     },

      
     {
         "varName": "dpsAmount",
         "type": "text",
         "label": "Amount",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "dpsRequest",
         "conditionalVarValue": true,
     },
     {
         "varName": "dpsTenor",
         "type": "text",
         "label": "Tenor",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "dpsRequest",
         "conditionalVarValue": true,
     },
     {
         "varName": "dpsInterest",
         "type": "text",
         "label": "Interest %",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "dpsRequest",
         "conditionalVarValue": true,
     },
     {
         "varName": "dpsSi",
         "type": "text",
         "label": "SI Instruction",

         "grid": 12,
         "conditional": true,
         "conditionalVarName": "dpsRequest",
         "conditionalVarValue": true,
     },
     {
         "varName": "dpsSchemeCode",
         "type": "text",
         "label": "Scheme Code",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "dpsRequest",
         "conditionalVarValue": true,
     },
     {
    "varName": "fdrRequest",
    "type": "checkbox",
    "label": "FDR Request",
    "grid": 12
},
{
    "varName": "accountsType",
    "type": "select",
    "grid": 3,
    "enum":[
    "Fix General",
    "Double",
    "Monthly",
],
    "label":"Type Of Account",
    "conditional": true,
    "conditionalVarName": "fdrRequest",
    "conditionalVarValue": true,

},
{
    "varName": "doubleTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Double",
},{
    "varName": "tennordDouble",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},{
    "varName": "doubleSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "No"
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Double",
},

/////  Account Type Fix General /////
{
    "varName": "fixGTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Day",
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Fix General",
},{
    "varName": "tennorFixG",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixGSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
        "No",
        "Encash at Maturity to my / Our Account No",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Fix General",
},{
    "varName": "fixdGMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No",
        "Renew Both Principal and Interest"
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "fixGSiMaturity",
        "conditionalVarValue": "Yes",
},

///////  Month Tenor /////
{
    "varName": "monthTennor",
    "type": "select",
    "grid": 3,
    "enum":[
    "Monthly"
],
    "label":"Tennor Type",
    "conditional": true,
    "conditionalVarName": "accountsType",
    "conditionalVarValue": "Monthly",
}, {
    "varName": "tennorMonth",
        "type": "text",
        "label": "Tennor",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthAmount",
        "type": "text",
        "label": "Amount",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthInterest",
        "type": "text",
        "label": "Interest",
        "grid": 3,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthSiMaturity",
        "type": "select",
        "grid": 3,
        "enum":[
        "Yes",
    ],
        "label":"SI Maturity",
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},{
    "varName": "monthMaturityYes",
        "type": "select",
        "label": "Maturity Yes",
        "enum":[
        "Renew Principal Only and Credit Interest to the Account No"
        // "Renew Principal Only "
    ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "accountsType",
        "conditionalVarValue": "Monthly",
},
    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    
     {
         "varName": "cityTouchRequest",
         "type": "checkbox",
         "label": "City Touch",
         "grid": 12

     },
     {
         "varName": "email",
         "type": "text",
         "label": "Email ",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "cityTouchRequest",
         "conditionalVarValue": true,

     },


     //cheque book
     {
         "varName": "chequeBookRequest",
         "type": "checkbox",
         "label": "Cheque Book Request",
         "grid": 12,

     },
     {"varName": "pageOfChequeBook",
         "type": "text",
         "label": "Page Of Cheque Book",
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,
         "grid": 3

     },
     {"varName": "numberOfChequeBookRequest",
         "type": "text",
         "label": "Number Of Cheque Book",
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,
         "grid": 12,


     },
     {
         "varName": "chequeBookDeliveryType",
         "type": "select",
         "label": "Delivery Type",
         "enum": [
             "Courier",
             "Branch"
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,

     },
     {
         "varName": "chequeBookBranch",
         "type": "select",
         "label": "Branch Name",
         "enum": [,
             "GULSHAN 1",
             "MOTHIJHEEL 1",
             "DHANMONDI",
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "chequeBookDeliveryType",
         "conditionalVarValue": "Branch",

     },
     {
         "varName": "chequeBookDesign",
         "type": "select",
         "label": "Cheque Book Design",
         "enum": [
             "Sapphire",
             "Citygem",
             "City Alo",
             "Other"
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "chequeBookRequest",
         "conditionalVarValue": true,

     },
//Debit Card
     {
         "varName": "debitCard",
         "type": "checkbox",
         "label": "Debit Card",
         "grid": 12
     },
     {
         "varName": "customerName",
         "type": "text",
         "label": "Name On Card",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "cardType",
         "type": "select",
         "label": "Card Type",
         "grid": 12,
         "enum": [
             "VISA",
             "MASTER",
             "CITYMAXX",
         ],
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },
     {
         "varName": "debitCardDeliveryType",
         "type": "select",
         "label": "Delivery Type",
         "enum": [
             "Courier",
             "Branch"
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "debitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "debitRequestkBranch",
         "type": "select",
         "label": "Branch Name",
         "enum": [,
             "GULSHAN 1",
             "MOTHIJHEEL 1",
             "DHANMONDI",
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "debitCardDeliveryType",
         "conditionalVarValue": "Branch",

     },
     {
         "varName": "instaDebitCard",
         "type": "checkbox",
         "label": "Insta Debit Card",
         "grid": 12
     },
     {
         "varName": "customerName",
         "type": "text",
         "label": "Name On Card",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },

     {
         "varName": "instaCardType",
         "type": "select",
         "label": "Card Type",
         "enum":[
             "CITYMAXX"
         ],
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },
     {
         "varName": "instaClientId",
         "type": "text",
         "label": "Client ID",
         "grid": 12,
         "conditional": true,
         "conditionalVarName": "instaDebitCard",
         "conditionalVarValue": true,

     },

 ])));
 
module.exports = {
    ///casa
    CSJsonFormForCasaIndividual,
    BMJsonFormForCasaIndividual,
    BOMJsonFormForCasaIndividual,
    MAKERJsonFormForCasaIndividual,
    CHECKERJsonFormForCasaIndividual,

    CSJsonFormForCasaJoint,
    BMJsonFormForCasaJoint,
    BOMJsonFormForCasaJoint,
    MAKERJsonFormForCasaJoint,
    CHECKERJsonFormForCasaJoint,

    CSJsonFormForCasaProprietorship,
    BMJsonFormForCasaProprietorship,
    BOMJsonFormForCasaProprietorship,
    MAKERJsonFormForCasaProprietorship,
    CHECKERJsonFormForCasaProprietorship,


    CSJsonFormForCasaCompany,
    BMJsonFormForCasaCompany,
    BOMJsonFormForCasaCompany,
    MAKERJsonFormForCasaCompany,
    CHECKERJsonFormForCasaCompany,




    ///DST
    CSJsonFormForDstIndividual,
    BMJsonFormForDstIndividual,
    BOMJsonFormForDstIndividual,
    MAKERJsonFormForDstIndividual,
    CHECKERJsonFormForDstIndividual,

    CSJsonFormForDstJoint,
    BMJsonFormForDstJoint,
    BOMJsonFormForDstJoint,
    MAKERJsonFormForDstJoint,
    CHECKERJsonFormForDstJoint,

    CSJsonFormForDstProprietorship,
    BMJsonFormForDstProprietorship,
    BOMJsonFormForDstProprietorship,
    MAKERJsonFormForDstProprietorship,
    CHECKERJsonFormForDstProprietorship,


    CSJsonFormForDstCompany,
    BMJsonFormForDstCompany,
    BOMJsonFormForDstCompany,
    MAKERJsonFormForDstCompany,
    CHECKERJsonFormForDstCompany,
    BOMJsonFormForDSTIndividual,


///Agent
    AbhMakerJsonFormForAgentIndividual,
    AbhMakerJsonFormForAgentNonIndividual,
    AbhApprovalJsonFormForAgentIndividual,
    AbhCheckerJsonFormForAgentIndividual,



}
