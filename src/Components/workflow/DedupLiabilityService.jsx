import React,{Component} from 'react';
import Liability from "../workflow/CASA/Liability";
import OpeningCS from "./fdr/OpeningCS";
import {Menu, MenuItem} from "@material-ui/core";
import GridItem from "../Grid/GridItem";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../Card/CardBody";
import GridContainer from "../Grid/GridContainer";
import AccountOpeningForm from "./AccountOpeningForm";
import {
    CSJsonFormForCasaIndividualFdr,
    CSJsonFormForCasaJoint,
    CSJsonFormForCasaProprietorship,
    CSJsonFormForCasaCompany,
} from "./WorkflowJsonFormArin";
class DedupLiabilityService extends Component{
    constructor(props) {
        super(props);
        this.state =  {
            selectedMenu : "NOTSELECTED"
        }
    }

    handleMenuSelect(menu){
        this.setState({selectedMenu : menu})
    }

    renderServiceMenu() {
        if(localStorage.getItem("workplace").indexOf('CSU') !== -1){
           return(
               <AccountOpeningForm   closeModal={this.props.closeModal}
                                     getAccountType={this.props.getAccountType}
                                     accountType={this.props.accountType}
                                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                     workplace={this.props.workplace}
                                     serviceType={this.props.serviceType}
                                     subServiceType={this.props.subServiceType}
                                     individualDedupData={this.props.individualDedupData}
                                     jointDedupData={this.props.jointDedupData}
                                     companyDedupData={this.props.companyDedupData}
                                     searchValue={this.props.searchValue}/>

           )
        }
       else{
            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4>Customer Services<a><CloseIcon onClick={this.props.closeModal} style={{
                                    position: 'absolute',
                                    right: 10,
                                    color: "#000000"
                                }}/></a></h4>

                            </CardHeader>
                            <CardBody>
                                <div>
                                    <MenuItem onClick={(event) => {
                                        this.handleMenuSelect("accountOpening")
                                    }}>Account Opening</MenuItem>
                                    <MenuItem onClick={(event) => {
                                        this.handleMenuSelect("fdrOpening")
                                    }}>FDR Opening</MenuItem>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            )
        }
    }

    render() {
        if (this.state.selectedMenu === "accountOpening"){
            return (
                <div>
                    <AccountOpeningForm   closeModal={this.props.closeModal}
                                          getAccountType={this.props.getAccountType}
                                          accountType={this.props.accountType}
                                          jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                          workplace={this.props.workplace}
                                          serviceType={this.props.serviceType}
                                          subServiceType={this.props.subServiceType}
                                          individualDedupData={this.props.individualDedupData}
                                          jointDedupData={this.props.jointDedupData}
                                          companyDedupData={this.props.companyDedupData}
                                          searchValue={this.props.searchValue}/>
                </div>
            );

        }
        else if (this.state.selectedMenu === "fdrOpening"){
            return (
                <div>
                    <OpeningCS  closeModal={this.props.closeModal}
                                getAccountType={this.props.getAccountType}
                                accountType={this.props.accountType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                workplace={this.props.workplace}
                                serviceType='FDR Opening'
                                subServiceType='New FDR Opening'
                                commonJsonForm={CSJsonFormForCasaIndividualFdr}
                                individualDedupData={this.props.individualDedupData}
                                jointDedupData={this.props.jointDedupData}
                                companyDedupData={this.props.companyDedupData}
                                searchValue={this.props.searchValue}/>
                </div>
            );

        }
        else{
            return this.renderServiceMenu();
        }
    }
}

export default DedupLiabilityService;