import {
    BMAccountMaintenance
} from "./WorkflowJsonForm3";

import {
    BMJsonFormForCasaIndividual,
    BMJsonFormForCasaJoint,
    BMJsonFormForCasaProprietorship,
    BMJsonFormForCasaCompany,
} from "./WorkflowJsonForm4";
import {
    BMJsonFormForCasaIndividualFdr,
    BMJsonFormForCasaIndividualTagFdr,
    BMJsonFormForCasaIndividualTagFdr2,
} from "./WorkflowJsonFormArin";
import React, {Component} from "react";
import GridContainer from "../Grid/GridContainer";
import GridItem from "../Grid/GridItem";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import withStyles from "@material-ui/core/styles/withStyles";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import BMApproval from "./CASA/BMApproval";
import OpeningBM from "./fdr/OpeningBM";
import ExistingBM from "./fdr/ExistingBM";


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});
 

class BMInboxCase extends Component {
    state = {
        sendTo: false,

        getDocument: false

    }

    constructor(props) {
        super(props);


    }

    casa=()=>{
        console.log(this.props)
        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaIndividual}
                                appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="Joint Account" || this.props.subServiceType==="Individual A/C")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaJoint}
                                appId={this.props.appUid}/>)
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaProprietorship}
                                appId={this.props.appUid}/>)
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Company Account")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaCompany}
                                appId={this.props.appUid}/>)
        }
         else if (this.props.serviceType === "AccountOpening" && this.props.subServiceType ==="Agent Account Opening") {

            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaIndividual}
                                appId={this.props.appUid}/>)
        }


    };
    casaExist=()=>{
        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaIndividual}
                                appId={this.props.appUid}/>)
        }
        if (this.props.serviceType === 'ExistAccountOpening' &&   this.props.subServiceType==="Joint Account" ) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaJoint}
                                appId={this.props.appUid}/>)
        }
        else  if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaProprietorship}
                                appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType === "Company Account")) {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName={this.props.serviceType}
                                jsonForm={BMJsonFormForCasaCompany}
                                appId={this.props.appUid}/>)
        }

    };
    fdr=()=>{

        if(this.props.serviceType==='FDR Opening' && this.props.subServiceType === 'New FDR Opening'){
            return (<OpeningBM closeModal={this.props.closeModal}
                               serviceType={this.props.serviceType}
                               subserviceType={this.props.subServiceType}
                               commonJsonForm={BMJsonFormForCasaIndividualFdr}
                               titleName={this.props.serviceType}
                               appId={this.props.appUid}/>)
        }else if(this.props.serviceType==='FDR Opening' && this.props.subServiceType === 'Tag FDR Opening'){
            return (<ExistingBM closeModal={this.props.closeModal}
                                serviceType={this.props.serviceType}
                                subserviceType={this.props.subServiceType}
                                commonJsonForm={BMJsonFormForCasaIndividualTagFdr}
                                serviceJsonForm={ BMJsonFormForCasaIndividualTagFdr2}
                                titleName={this.props.serviceType}
                                appId={this.props.appUid}/>)
        }


    }
    maintenance=()=>{
        if (this.props.serviceType === 'Maintenance' && this.props.subServiceType === 'AccountMaintenance') {
            return (<BMApproval closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                subServiceType={this.props.subServiceType}
                                titleName="Account Maintenance"
                                maintenanceType={this.props.maintenanceType}
                                jsonForm={BMAccountMaintenance}
                                appId={this.props.appUid}/>)
        }
    }
    inboxCase = () => {
        if (this.props.serviceType === "Account Opening") {

            return(
                this.casa()
            )
        }
        else if (this.props.serviceType === "FDR Opening") {

            return(
                this.fdr()
            )
        }
        else if (this.props.serviceType === "Maintenance") {

            return(
                this.maintenance()
            )
        }
        else if(this.props.serviceType==="ExistAccountOpening"){
            return(
                this.casaExist()
            )
        }

    };
    close = () => {
        this.props.closeModal();
    }

    render() {
        const {classes} = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">

                            <h4>{this.props.serviceType}<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>
                        </CardHeader>
                        <CardBody>

                            {this.inboxCase()}

                            {/*<Grid item xs={9}>


                            </Grid>*/}
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(BMInboxCase);