import VerifyDocumentBOM from "./CASA/VerifyDocumentBOM";
import {
    BOMNewJsonFormIndividualAccountOpening,
    BOMNewCommonjsonFormNonIndividualAccountOpeningSearch,
    BOMNewJsonFormJointAccountOpening

} from "./WorkflowJsonForm2";
import {
    BOMAccountMaintenance,


} from "./WorkflowJsonForm3";

import {
    BOMJsonFormForCasaIndividual,
    BOMJsonFormForCasaJoint,
    BOMJsonFormForCasaProprietorship,
    BOMJsonFormForCasaCompany,
} from "./WorkflowJsonForm4";
import {
    BOMJsonFormForCasaIndividualFdr,
    BOMJsonFormForCasaIndividualTagFdr
} from "./WorkflowJsonFormArin";
import React, {Component} from "react";
import OpeningBom from "./fdr/OpeningBom";
import {backEndServerURL} from "../../Common/Constant";
import axios from "axios";
import VerifyMakerPhoto from "./VerifyMakerPhoto";
import Grid from "@material-ui/core/Grid";
import CardBody from "../Card/CardBody";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';

class BOMInboxCase extends Component {
    state={
        documentList: [],
        getDocument: false
    }
    casa=()=>{

        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {

            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForCasaIndividual}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Joint Account") {

            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForCasaJoint}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL")) {
            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForCasaProprietorship}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Agent Account Opening") {
            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForCasaIndividual}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Company Account") {
            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMJsonFormForCasaCompany}
                                       appId={this.props.appUid}/>)
        }


        else {


        }

    };
    renderExistAccountOpeningcase=()=>{

        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C")) {

            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMNewJsonFormIndividualAccountOpening}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Joint Account") {

            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                                       titleName="New Account Opening"
                                       jsonForm={BOMNewJsonFormJointAccountOpening}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL")) {
            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMNewCommonjsonFormNonIndividualAccountOpeningSearch}
                                       appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Company Account") {
            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="New Account Opening"
                                       jsonForm={BOMNewCommonjsonFormNonIndividualAccountOpeningSearch}
                                       appId={this.props.appUid}/>)
        }

        else {


        }

    };
    fdr=()=>{
        if (this.props.serviceType === 'FDR Opening' && this.props.subServiceType === 'New FDR Opening') {
            return (<OpeningBom closeModal={this.props.closeModal}
                                serviceType={this.props.serviceType}
                                subserviceType={this.props.subServiceType}
                                commonJsonForm={BOMJsonFormForCasaIndividualFdr}
                                titleName={this.props.serviceType}
                                appId={this.props.appUid}/>)
        }else if (this.props.serviceType === 'FDR Opening' && this.props.subServiceType === 'Tag FDR Opening') {
            return (<OpeningBom closeModal={this.props.closeModal}
                                serviceType={this.props.serviceType}
                                subserviceType="Tag FDR Opening"
                                commonJsonForm={BOMJsonFormForCasaIndividualTagFdr}
                                titleName={this.props.serviceType}
                                appId={this.props.appUid}/>)
        }

    }
    maintenance=()=>{
        if (this.props.serviceType === 'Maintenance'&& this.props.subServiceType==="AccountMaintenance") {

            return (<VerifyDocumentBOM closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                       subServiceType={this.props.subServiceType}
                                       titleName="Account Maintenance"
                                       maintenanceType={this.props.maintenanceType}
                                       jsonForm={BOMAccountMaintenance}
                                       appId={this.props.appUid}/>)
        }
    }
    inboxCase = () => {
        //alert(this.props.serviceType)
        if(this.props.serviceType === "Account Opening"){
            return (
                this.casa()
            )
        }
        else if(this.props.serviceType === "FDR Opening"){
            return (
                this.fdr()
            )
        }
        else if(this.props.serviceType === "Maintenance"){
            return (
                this.maintenance()
            )
        }
        else if(this.props.serviceType === "ExistAccountOpening"){
            return (
                this.renderExistAccountOpeningcase()
            )
        }


    }
    componentDidMount() {
        let fileUrl = backEndServerURL + "/case/files/" + this.props.appUid;
        axios.get(fileUrl, {withCredentials: true})
            .then((response) => {
                console.log(":d")
                console.log(response.data)
                this.setState({
                    documentList: response.data,
                    getDocument: true
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }
    close = () => {
        this.props.closeModal();
    }
    inboxCasePhoto = () => {
        return (<VerifyMakerPhoto closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                  subServiceType={this.props.subServiceType}

                                  titleName="Maker Update All Information"
                                  documentList={this.state.documentList}/>)



    };

    render() {
        return (
            <div>

                <Card>
                    <CardHeader color="rose">
                        <h4>{this.props.serviceType}<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                    </CardHeader>
                    <CardBody>
                        <Grid container spacing={1}>

                            <Grid  item xs={8}>

                                {this.inboxCasePhoto()}

                            </Grid>


                            <Grid item xs={4}>
                                {this.inboxCase()}
                            </Grid>


                        </Grid>
                    </CardBody>
                </Card>
            </div>

        )
    }

}

export default BOMInboxCase;