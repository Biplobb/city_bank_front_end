
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import Functions from '../../../Common/Functions';
import Notification from "../../NotificationMessage/Notification";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {ThemeProvider} from "@material-ui/styles/index";
import Grid from "@material-ui/core/Grid/index";
import theme from "../../JsonForm/CustomeTheme";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import DialogContent from "@material-ui/core/DialogContent/index";
import loader from "../../../Static/loader.gif";
import {Dialog} from "@material-ui/core/index";
import CardHeader from "../../Card/CardHeader";
import Card from "../../Card/Card";
import GridList from "@material-ui/core/GridList/index";
import MakerCumInput from "./MakerCumInput";
import AssignedCropImage from "./AssignedCropImage";
import DedupResultFunction from "../../DedupResultFunction";
import ImageCrop from "./ImageCrop";
import Link from "@material-ui/core/Link/index";
import SecondaryCbFormDetails from "./SecondaryCbFormDetails";

let makerRemarks = [
    {
        "varName": "makerRemarks",
        "type": "textArea",
        "label": "Maker Remarks",
        "grid": 12
    }]
;



const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    gridList: {
        width: 500,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },

};


class VerifyMakerInboxCase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            appData: {},
            getData: false,
            varValue: [],
            showValue: false,
            redirectLogin: false,
            title: "",
            notificationMessage: "",
            alert: false,
            inputData: {tagList: {}},
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},
            values: [],
            customerName: [],
            deferalType: [],
            expireDate: [],
            other: [],
            getCheckerList: [],
            getAllDefferal: [],
            getDeferalList: [],
            loading: false,
            jointAccountCustomerNumber: 0,
            objectForJoinAccount: [],
            getgenerateForm: false,
            renderCumModalopen: false,
            generateAccountNo: '',
            getDedupData: {},
            jointDedupData: {},
            jointSearchTableData: [],
            propritorshipData: [],
            dedupData: [],
            tagClick: false,
            getTaggingList: {},
            getTaggingCustomerList: {},
            taggingData: [],
            relatedData: {},
            searchTableData: false,
            searchTableRelatedData: false,
            imageCropModal: false,
            getRemarks: [], err: false,
            errorArray: {},
            errorMessages: {},
            getMappingCropImage: false,



        };


    }


    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }
    renderRemarksData = () => {


        if (this.state.getRemarks.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Remarks", "Raised By", "Date", "Role"]}
                        tableData={this.state.getRemarks}
                        tableAllign={['left', 'left', 'left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    JointAccountForm = () => {
        if (this.state.getData) {

            let objectForJoinAccount = [];
            var sl;
            for (var i = 0; i < 2; i++) {
                sl = i + 1;
                objectForJoinAccount.push(
                    {
                        "varName": "Customer " + i,
                        "type": "title",
                        "label": "Customer: " + sl

                    },

                    {
                        "varName": "title" + i,
                        "type": "title",
                        "label": "Individual Information Form",
                        "grid": 12
                    },
                    {
                        "varName": "date" + i,
                        "type": "date",
                        "label": "Date",
                        "grid": 6
                    },
                    {
                        "varName": "cbNumber" + i,
                        "type": "text",
                        "label": "Unique Customer ID",
                        "grid": 6
                    },
                    {
                        "varName": "accountNo" + i,
                        "type": "text",
                        "label": "A/C No.",
                        "grid": 6
                    },
                    {
                        "varName": "relationWithTheAC" + i,
                        "type": "select",
                        "label": "Relation with the A/C",
                        "grid": 6,
                        "enum": [
                            "1st Applicant",
                            "2nd Applicant",
                            "3rd Applicant",
                            "director",
                            "A/C Operator",
                            "Partner",
                            "Beneficial Owner",
                            "Minor",
                            "Guardian",
                            "Trustee",
                            "Attorney Holder",
                            "Proprietor",
                            "Others"
                        ],
                    },
                    {
                        "varName": "accountName" + i,
                        "type": "text",
                        "label": "Account Name",
                        "grid": 6
                    },
                    {
                        "varName": "customerName" + i,
                        "type": "text",
                        "label": "Customer's Name",
                        "grid": 6
                    },
                    {
                        "varName": "title" + i,
                        "type": "title",
                        "label": "Relation",
                        "grid": 12
                    },
                    {
                        "varName": "fatherName" + i,
                        "type": "text",
                        "label": "Father's Name",
                        "grid": 6
                    },
                    {
                        "varName": "motherName" + i,
                        "type": "text",
                        "label": "Mother's Name",
                        "grid": 6
                    },
                    {
                        "varName": "spouseName" + i,
                        "type": "text",
                        "label": "Spouse' Name",
                        "grid": 6
                    }, {
                        "varName": "dob" + i,
                        "type": "date",
                        "label": "DOB",
                        "grid": 6
                    },
                    {
                        "varName": "religion" + i,
                        "type": "text",
                        "label": "Religion",
                        "grid": 6
                    },
                    {
                        "varName": "gender" + i,
                        "type": "select",
                        "label": "Gender",
                        "grid": 6,
                        "enum": [
                            "Male",
                            "Female",
                            "Third Gender",
                            "Address proof"
                        ],
                    },
                    {
                        "varName": "nid" + i,
                        "type": "text",
                        "label": "NID",
                        "grid": 6
                    },
                    {
                        "varName": "birthCertificate" + i,
                        "type": "text",
                        "label": "Birth certificate",
                        "grid": 6
                    },
                    {
                        "varName": "residentStatus" + i,
                        "type": "select",
                        "label": "Resident status",
                        "grid": 6,
                        "enum": [
                            "Resident",
                            "Non-Resident"
                        ],
                    },
                    {
                        "varName": "passportNo" + i,
                        "type": "text",
                        "label": "Passport No",
                        "grid": 6
                    },
                    {
                        "varName": "passportIssueDate" + i,
                        "type": "date",
                        "label": "Issue Date",
                        "grid": 6
                    },
                    {
                        "varName": "passportExpDate" + i,
                        "type": "date",
                        "label": "Exp Date",
                        "grid": 6
                    },
                    {
                        "varName": "passportPlaceOfIssue" + i,
                        "type": "text",
                        "label": "Place of issue",
                        "grid": 6
                    },
                    {
                        "varName": "drivingLicenseNo" + i,
                        "type": "text",
                        "label": "Driving License no",
                        "grid": 6
                    },
                    {
                        "varName": "drivingLicenseExpDate" + i,
                        "type": "date",
                        "label": "Exp Date",
                        "grid": 6
                    },
                    {
                        "varName": "otherPhotoId" + i,
                        "type": "text",
                        "label": "Other photo id",
                        "grid": 6
                    },
                    {
                        "varName": "etin" + i,
                        "type": "text",
                        "label": "E-TIN",
                        "grid": 6
                    },
                    {
                        "varName": "nationality" + i,
                        "type": "select",
                        "label": "Nationality",
                        "grid": 6,
                        "enum": [
                            "Bangladeshi",
                            "Others"
                        ],
                    },
                    {
                        "varName": "placeOfBirth" + i,
                        "type": "text",
                        "label": "Place of Birth",
                        "grid": 6
                    },
                    {
                        "varName": "countryOfBirth" + i,
                        "type": "text",
                        "label": "Country of Birth",
                        "grid": 6
                    },
                    {
                        "varName": "profession" + i,
                        "type": "text",
                        "label": "Profession",
                        "grid": 6
                    },
                    {
                        "varName": "nameOfTheOrganization" + i,
                        "type": "text",
                        "label": "Name of the Organization",
                        "grid": 6
                    },
                    {
                        "varName": "designation" + i,
                        "type": "text",
                        "label": "Designation",
                        "grid": 6
                    },
                    {
                        "varName": "natureOfBusiness" + i,
                        "type": "text",
                        "label": "Nature of Business",
                        "grid": 6
                    },
                    ////end page//////
                    {
                        "varName": "presentAddress" + i,
                        "type": "text",
                        "label": "Present Address",
                        "grid": 6
                    },
                    {
                        "varName": "presentAddressPostCode" + i,
                        "type": "text",
                        "label": "Post Code",
                        "grid": 6
                    },
                    {
                        "varName": "presentAddressCountry" + i,
                        "type": "text",
                        "label": "Country",
                        "grid": 6
                    },
                    {

                        "varName": "presentAddressLandmark" + i,
                        "type": "text",
                        "label": "Landmark",
                        "grid": 6
                    },
                    {
                        "varName": "professionalAddress" + i,
                        "type": "text",
                        "label": "Professional Address",
                        "grid": 6
                    },
                    {

                        "varName": "professionalAddressPostCode" + i,
                        "type": "text",
                        "label": "Post Code",
                        "grid": 6
                    },
                    {
                        "varName": "professionalAddressCountry" + i,
                        "type": "text",
                        "label": "Country",
                        "grid": 6
                    },
                    {
                        "varName": "professionalAddressLandmark" + i,
                        "type": "text",
                        "label": "Landmark",
                        "grid": 6
                    },
                    {
                        "varName": "permanentAddress" + i,
                        "type": "text",
                        "label": "Permanent Address",
                        "grid": 6
                    },
                    {
                        "varName": "permanentAddressPostCode" + i,
                        "type": "text",
                        "label": "Post Code",
                        "grid": 6
                    },
                    {
                        "varName": "permanentAddressCountry" + i,
                        "type": "text",
                        "label": "Country",
                        "grid": 6
                    },
                    {
                        "varName": "permanentAddressLandmark" + i,
                        "type": "text",
                        "label": "Landmark",
                        "grid": 6
                    },
                    {
                        "varName": "phoneNumberResidence" + i,
                        "type": "text",
                        "label": "Mobile Number(Residence)",
                        "grid": 6
                    },
                    {
                        "varName": "phoneNumberOffice" + i,
                        "type": "text",
                        "label": "Mobile Number(Office)",
                        "grid": 6
                    },
                    {
                        "varName": "mobile" + i,
                        "type": "text",
                        "label": "Mobile",
                        "grid": 6
                    },
                    {
                        "varName": "email" + i,
                        "type": "text",
                        "label": "Email ID",
                        "grid": 6
                    },
                    {
                        "varName": "emergencyContact" + i,
                        "type": "title",
                        "label": "Emergency Contact",
                        "grid": 12
                    },
                    {
                        "varName": "emergencyContactName" + i,
                        "type": "text",
                        "label": "Name",
                        "grid": 6
                    },
                    {
                        "varName": "emergencyContactAddress" + i,
                        "type": "text",
                        "label": "Address",
                        "grid": 6
                    },
                    {
                        "varName": "emergencyContactEmailId" + i,
                        "type": "text",
                        "label": "Email ID",
                        "grid": 6
                    },
                    {
                        "varName": "emergencyContactMobile" + i,
                        "type": "text",
                        "label": "Mobile",
                        "grid": 6
                    },
                    {
                        "varName": "emergencyContactRelationshipWithAcHolder" + i,
                        "type": "text",
                        "label": "Relationship With A/C Holder",
                        "grid": 6
                    },
                    {
                        "varName": "creditCardInformation" + i,
                        "type": "title",
                        "label": "Credit Card Information of Account Holder",
                        "grid": 12
                    },
                    {
                        "varName": "issuedByLocal" + i,
                        "type": "text",
                        "label": "Issued By (Local)",
                        "grid": 6
                    },
                    {
                        "varName": "issuedByInternational" + i,
                        "type": "text",
                        "label": "Issued By(International)",
                        "grid": 6
                    },
                    {
                        "varName": "fatcaInformation" + i,
                        "type": "text",
                        "label": "FATCA Information",
                        "grid": 6
                    }
                )

            }

            this.setState({
                objectForJoinAccount: objectForJoinAccount,
                getgenerateForm: true

            })
            console.log(objectForJoinAccount);

            this.renderJsonFormCommon();
            return;

        }

    }

    componentDidMount() {

        this.setState({
            loading: true
        })
        var remarksArray = []

        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log("llll")
                    console.log(response.data.jointDedupData)
                    let objectForJoinAccount = [];
                    var sl;
                    for (var i = 0; i < response.data.jointAccountCustomerNumber; i++) {
                        sl = i + 1;
                        objectForJoinAccount.push(
                            {
                                "varName": "Customer " + i,
                                "type": "title",
                                "label": "Customer: " + sl,
                                "grid": 12

                            },

                            {
                                "varName": "title" + i,
                                "type": "title",
                                "label": "Individual Information Form",
                                "grid": 12
                            },
                            {
                                "varName": "date" + i,
                                "type": "date",
                                "label": "Date",
                                "grid": 6
                            },
                            {
                                "varName": "cbNumber" + i,
                                "type": "text",
                                "label": "Unique Customer ID",
                                "grid": 6
                            },
                            {
                                "varName": "accountNo" + i,
                                "type": "text",
                                "label": "A/C No.",
                                "grid": 6
                            },
                            {
                                "varName": "relationWithTheAC" + i,
                                "type": "select",
                                "label": "Relation with the A/C",
                                "grid": 6,
                                "enum": [
                                    "1st Applicant",
                                    "2nd Applicant",
                                    "3rd Applicant",
                                    "director",
                                    "A/C Operator",
                                    "Partner",
                                    "Beneficial Owner",
                                    "Minor",
                                    "Guardian",
                                    "Trustee",
                                    "Attorney Holder",
                                    "Proprietor",
                                    "Others"
                                ],
                            },
                            {
                                "varName": "accountName" + i,
                                "type": "text",
                                "label": "Account Name",
                                "grid": 6
                            },
                            {
                                "varName": "customerName" + i,
                                "type": "text",
                                "label": "Customer's Name",
                                "grid": 6
                            },
                            {
                                "varName": "title" + i,
                                "type": "title",
                                "label": "Relation",
                                "grid": 12
                            },
                            {
                                "varName": "fatherName" + i,
                                "type": "text",
                                "label": "Father's Name",
                                "grid": 6
                            },
                            {
                                "varName": "motherName" + i,
                                "type": "text",
                                "label": "Mother's Name",
                                "grid": 6
                            },
                            {
                                "varName": "spouseName" + i,
                                "type": "text",
                                "label": "Spouse' Name",
                                "grid": 6
                            }, {
                                "varName": "dob" + i,
                                "type": "date",
                                "label": "DOB",
                                "grid": 6
                            },
                            {
                                "varName": "religion" + i,
                                "type": "text",
                                "label": "Religion",
                                "grid": 6
                            },
                            {
                                "varName": "gender" + i,
                                "type": "select",
                                "label": "Gender",
                                "grid": 6,
                                "enum": [
                                    "Male",
                                    "Female",
                                    "Third Gender",
                                    "Address proof"
                                ],
                            },
                            {
                                "varName": "nid" + i,
                                "type": "text",
                                "label": "NID",
                                "grid": 6
                            },
                            {
                                "varName": "birthCertificate" + i,
                                "type": "text",
                                "label": "Birth certificate",
                                "grid": 6
                            },
                            {
                                "varName": "residentStatus" + i,
                                "type": "select",
                                "label": "Resident status",
                                "grid": 6,
                                "enum": [
                                    "Resident",
                                    "Non-Resident"
                                ],
                            },
                            {
                                "varName": "passportNo" + i,
                                "type": "text",
                                "label": "Passport No",
                                "grid": 6
                            },
                            {
                                "varName": "passportIssueDate" + i,
                                "type": "date",
                                "label": "Issue Date",
                                "grid": 6
                            },
                            {
                                "varName": "passportExpDate" + i,
                                "type": "date",
                                "label": "Exp Date",
                                "grid": 6
                            },
                            {
                                "varName": "passportPlaceOfIssue" + i,
                                "type": "text",
                                "label": "Place of issue",
                                "grid": 6
                            },
                            {
                                "varName": "drivingLicenseNo" + i,
                                "type": "text",
                                "label": "Driving License no",
                                "grid": 6
                            },
                            {
                                "varName": "drivingLicenseExpDate" + i,
                                "type": "date",
                                "label": "Exp Date",
                                "grid": 6
                            },
                            {
                                "varName": "otherPhotoId" + i,
                                "type": "text",
                                "label": "Other photo id",
                                "grid": 6
                            },
                            {
                                "varName": "etin" + i,
                                "type": "text",
                                "label": "E-TIN",
                                "grid": 6
                            },
                            {
                                "varName": "nationality" + i,
                                "type": "select",
                                "label": "Nationality",
                                "grid": 6,
                                "enum": [
                                    "Bangladeshi",
                                    "Others"
                                ],
                            },
                            {
                                "varName": "placeOfBirth" + i,
                                "type": "text",
                                "label": "Place of Birth",
                                "grid": 6
                            },
                            {
                                "varName": "countryOfBirth" + i,
                                "type": "text",
                                "label": "Country of Birth",
                                "grid": 6
                            },
                            {
                                "varName": "profession" + i,
                                "type": "text",
                                "label": "Profession",
                                "grid": 6
                            },
                            {
                                "varName": "nameOfTheOrganization" + i,
                                "type": "text",
                                "label": "Name of the Organization",
                                "grid": 6
                            },
                            {
                                "varName": "designation" + i,
                                "type": "text",
                                "label": "Designation",
                                "grid": 6
                            },
                            {
                                "varName": "natureOfBusiness" + i,
                                "type": "text",
                                "label": "Nature of Business",
                                "grid": 6
                            },
                            ////end page//////
                            {
                                "varName": "presentAddress" + i,
                                "type": "text",
                                "label": "Present Address",
                                "grid": 6
                            },
                            {
                                "varName": "presentAddressPostCode" + i,
                                "type": "text",
                                "label": "Post Code",
                                "grid": 6
                            },
                            {
                                "varName": "presentAddressCountry" + i,
                                "type": "text",
                                "label": "Country",
                                "grid": 6
                            },
                            {

                                "varName": "presentAddressLandmark" + i,
                                "type": "text",
                                "label": "Landmark",
                                "grid": 6
                            },
                            {
                                "varName": "professionalAddress" + i,
                                "type": "text",
                                "label": "Professional Address",
                                "grid": 6
                            },
                            {

                                "varName": "professionalAddressPostCode" + i,
                                "type": "text",
                                "label": "Post Code",
                                "grid": 6
                            },
                            {
                                "varName": "professionalAddressCountry" + i,
                                "type": "text",
                                "label": "Country",
                                "grid": 6
                            },
                            {
                                "varName": "professionalAddressLandmark" + i,
                                "type": "text",
                                "label": "Landmark",
                                "grid": 6
                            },
                            {
                                "varName": "permanentAddress" + i,
                                "type": "text",
                                "label": "Permanent Address",
                                "grid": 6
                            },
                            {
                                "varName": "permanentAddressPostCode" + i,
                                "type": "text",
                                "label": "Post Code",
                                "grid": 6
                            },
                            {
                                "varName": "permanentAddressCountry" + i,
                                "type": "text",
                                "label": "Country",
                                "grid": 6
                            },
                            {
                                "varName": "permanentAddressLandmark" + i,
                                "type": "text",
                                "label": "Landmark",
                                "grid": 6
                            },
                            {
                                "varName": "phoneNumberResidence" + i,
                                "type": "text",
                                "label": "Mobile Number(Residence)",
                                "grid": 6
                            },
                            {
                                "varName": "phoneNumberOffice" + i,
                                "type": "text",
                                "label": "Mobile Number(Office)",
                                "grid": 6
                            },
                            {
                                "varName": "mobile" + i,
                                "type": "text",
                                "label": "Mobile",
                                "grid": 6
                            },
                            {
                                "varName": "email" + i,
                                "type": "text",
                                "label": "Email ID",
                                "grid": 6
                            },
                            {
                                "varName": "emergencyContact" + i,
                                "type": "title",
                                "label": "Emergency Contact",
                                "grid": 12
                            },
                            {
                                "varName": "emergencyContactName" + i,
                                "type": "text",
                                "label": "Name",
                                "grid": 6
                            },
                            {
                                "varName": "emergencyContactAddress" + i,
                                "type": "text",
                                "label": "Address",
                                "grid": 6
                            },
                            {
                                "varName": "emergencyContactEmailId" + i,
                                "type": "text",
                                "label": "Email ID",
                                "grid": 6
                            },
                            {
                                "varName": "emergencyContactMobile" + i,
                                "type": "text",
                                "label": "Mobile",
                                "grid": 6
                            },
                            {
                                "varName": "emergencyContactRelationshipWithAcHolder" + i,
                                "type": "text",
                                "label": "Relationship With A/C Holder",
                                "grid": 6
                            },
                            {
                                "varName": "creditCardInformation" + i,
                                "type": "title",
                                "label": "Credit Card Information of Account Holder",
                                "grid": 12
                            },
                            {
                                "varName": "issuedByLocal" + i,
                                "type": "text",
                                "label": "Issued By (Local)",
                                "grid": 6
                            },
                            {
                                "varName": "issuedByInternational" + i,
                                "type": "text",
                                "label": "Issued By(International)",
                                "grid": 6
                            },
                            {
                                "varName": "fatcaInformation" + i,
                                "type": "text",
                                "label": "FATCA Information",
                                "grid": 6
                            }
                        )

                    }

                    this.setState({
                        getDedupData: response.data,
                        objectForJoinAccount: objectForJoinAccount,
                        getgenerateForm: true

                    })
                    console.log(response.data)
                    let checkerListUrl = backEndServerURL + "/checkers";
                    axios.get(checkerListUrl, {withCredentials: true})
                        .then((response) => {
                            let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                            axios.get(deferalListUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    var status = "";
                                    var tableArray = [];
                                    response.data.map((deferal) => {
                                        if (deferal.status === "ACTIVE") {
                                            status = "Approved By BM"
                                        }
                                        else{
                                            status = deferal.status
                                        }
                                        tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                                    });
                                    this.setState({
                                        getDeferalList: tableArray
                                    })
                                    let getCommentsUrl = backEndServerURL + "/appRemarkGet/" + this.props.appId;
                                    axios.get(getCommentsUrl, {withCredentials: true})
                                        .then((response) => {

                                            console.log(response.data);
                                            response.data.map((data) => {

                                                remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                                            })
                                            this.setState({
                                                getRemarks: remarksArray
                                            })
                                        })
                                        .catch((error) => {
                                            console.log(error)
                                        })


                                })
                                .catch((error) => {
                                    console.log(error);
                                })
                            console.log(response.data)
                            this.setState({
                                getCheckerList: response.data,

                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    console.log(response.data)
                    /*  let varValue = response.data;
                      let listOfTag = response.data.tagging.substring(1, response.data.tagging.length - 1).split(",");
                      let array = [];
                      for (var i = 0; i < listOfTag.length; i++) {
                          array.push(listOfTag[i]);
                      }
                      console.log(array[0])
                      console.log(response.data)
                      let tableArrayData=[];
                      array.map((data, index) => {
                          tableArrayData.push(this.createTaggingDataShow(data));

                      })
  */

                    let tableArrayData=[]
                    let cbTagUrl = backEndServerURL + "/secondaryCB/" + this.props.appId;
                    axios.get(cbTagUrl, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data);
                            response.data.map((data, index) => {
                                tableArrayData.push(this.createTaggingData(data));

                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    let varValue=response.data;
                    this.setState({
                        getTagList: tableArrayData,
                        jointAccountCustomerNumber: response.data.jointAccountCustomerNumber,
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        showValue: true,
                        loading: false
                    });
                })
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        loading: false
                    })
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.getTaggingList)

        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.makerRemarks + "/" + this.props.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        if (this.state.inputData["deferalNeed"] === "YES") {
            var defType = [];
            var expDate = [];
            var defOther = [];
            let appId = this.props.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["defferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["defferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/raisedSd/Bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        this.state.inputData.maker_update_all_info_send_to = "CHECKER";

        let array = [];
        let arrayCustomerList = [];


        for (let Tagging in  this.state.getTaggingList) {

            array.push(this.state.getTaggingList[Tagging]);
        }

        this.state.inputData.tagging = this.state.getTaggingList;

        this.state.inputData.makerRemarks =undefined;
        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()
                        //
                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }
    renderCumModalopen = () => {
        this.setState({
            renderCumModalopen: true
        })
    }
    tagging = () => {
        this.setState({
            generateAccountNo: 59841433
        })
    }


    handleSubmitReturn = (event) => {
        event.preventDefault();

        //this.state.inputData.next_user = this.state.inputData.cs_send_to;
        this.state.inputData.maker_update_all_info_send_to = "CS";

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, {maker_update_all_info_send_to: "CS"}, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }
    checkerList = () => {
        if (this.state.getData) {
            let checkerJsonForm = {
                "varName": "next_user",
                "type": "select",
                "label": "Send To",
                "grid": 12,
                "enum": []
            };
            this.state.getCheckerList.map((checker) => {

                checkerJsonForm.enum.push(checker)
            })
            return (
                SelectComponent.select(this.state, this.updateComponent, checkerJsonForm)
            )

        }

    }

    renderJsonFormFirst = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.commonJsonForm, this.updateComponent)


            )
        } else {
            return;
        }
    }


    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Expire Date", "Raise By", "Raise Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderButton = () => {
        if (this.state.getData) {
            return (

                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmit}
                    >Submit
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',

                        }}

                        type='button' value='add more'
                        onClick={this.handleSubmitReturn}
                    >Return
                    </button>
                </div>

            )
        }
    }
    renderBomForwardForm = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.commonJsonForm, this.updateComponent)


            )
        } else {
            return;
        }
    }
    closeModal = (account) => {

        this.setState({
            renderCumModalopen: false,
            getMappingCropImage: false,

            generateAccountNo: account,
            imageCropModal: false
        })
        this.renderImageCrop()
    }

    renderDedupComponent = () => {
        if (this.props.subServiceType !== "INDIVIDUAL" && this.state.getData) {
            return (
                <DedupResultFunction removingTaggingData={this.removingTaggingData} getTaggingData={this.getTaggingData}
                                     subServiceType={this.props.subServiceType} appId={this.props.appId}/>
            )
        }
    }


    removingTaggingData = (event, index) => {
        event.preventDefault();
        let arrayList = [];

        for (let Tagging in  this.state.getTaggingList) {

            if ("customertagging"+index === this.state.getTaggingList[Tagging]) {

                delete this.state.getTaggingList[Tagging];
            }

        }
        this.forceUpdate();
        this.renderTagging();
        /*if (index > -1) {

            this.state.getTaggingList.splice(index, 1);
            this.updateComponent();
        }*/
    }
    TaggingDataView = (event, index) => {
        event.preventDefault();
        let arrayList = [];

        for (let Tagging in  this.state.getTaggingList) {

            if (index === this.state.getTaggingList[Tagging]) {

                delete this.state.getTaggingList[Tagging];
            }

        }
        this.forceUpdate();
        this.renderTagging();
        /*if (index > -1) {

            this.state.getTaggingList.splice(index, 1);
            this.updateComponent();
        }*/
    }
    createTaggingData = (data) => {
        return (
            ["Customer "+data.customer,data.cbNumber,  [<button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'right',
                }}
                type='button' value='add more'
                onClick={(event) => this.tagCbDetailsModal(event, data.customer)}
            >View
            </button>]]


        )
    }
    /* createTaggingDataShow = (taggingname) => {

         return ([taggingname, [<button
                 className="btn btn-outline-danger"
                 style={{
                     verticalAlign: 'right',

                 }}

                 type='button' value='add more'
                 onClick={(event) => this.tagCbDetailsModal(event, taggingname)}
             >view
             </button>]]


         )
     }*/
    renderTagging = () => {
        let tableArrayData=[]
        let cbTagUrl = backEndServerURL + "/secondaryCB/" + this.props.appId;
        axios.get(cbTagUrl, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                response.data.map((data, index) => {
                    tableArrayData.push(this.createTaggingData(data));

                })
            })
            .catch((error) => {
                console.log(error);
            })

        this.setState({
            getTagList: tableArrayData})
        this.forceUpdate();
        /*
                let tableArray = [];
                Object.keys(this.state.getTaggingList).map((obj, i) => {

                    if (this.state.getTaggingList[obj] !== undefined && this.state.getTaggingList[obj] !== null) {

                        tableArray.push(this.createTaggingData(this.state.getTaggingList[obj]));

                    }

                })
                this.setState({
                    taggingData: tableArray
                })*/


    }



    renderTagList = () => {

        if (this.state.getData && (this.props.subServiceType !== "INDIVIDUAL" && this.props.subServiceType !== "Individual A/C") && this.state.getTagList.length > 0) {

            return (

                <div style={{marginBottom: 40}}>
                    {/*<paper>
                        <CardHeader color="rose">
                            <h4>Tag CB List</h4>

                        </CardHeader>
                    </paper>
*/}
                    <div>
                        {/*  <center>
                        <h4>Tag CB List</h4>
                    </center>*/}
                        <Table

                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["Customer No","CB Number", "View"]}

                            tableData={this.state.getTagList}
                            tableAllign={['left', 'left', 'left']}
                        />

                        <br/>


                    </div>

                </div>

            )
        }

    }
    renderImageCrop = () => {
        if ((backEndServerURL + "/signaturePhoto/" + this.props.appId + "/signature" !== undefined)) {
            console.log(backEndServerURL + "/signaturePhoto/" + this.props.appId + "/signiture")
            return (

                <img width='100%' src={backEndServerURL + "/signaturePhoto/" + this.props.appId + "/signature"}
                     alt=""/>
            )
        }


    }
    getTaggingData = (index, data,customerNo) => {
        /*  console.log(index)
          console.log(data)
          console.log(customerNo);
          let customerList=[];
          customerList.push(data);
          customerList.push(customerNo);
          this.state.getTaggingList["customer"+index] = customerList;
          //this.state.getTaggingCustomerList[index] = customerNo;
          this.setState({
              tagClick: true
          })
          console.log(this.state.getTaggingList);*/
        this.renderTagging();
        this.forceUpdate();
    }
    /*  renderTagList = () => {

          if (this.state.getData && (this.props.subServiceType !== "INDIVIDUAL" && this.props.subServiceType !== "Individual A/C") && this.state.getTagList.length > 0) {

              return (

                  <div style={{marginBottom: 40}}>

                      <div>

                          <Table

                              tableHovor="yes"
                              tableHeaderColor="primary"
                              tableHead={["Customer Number","CB Number", "View"]}

                              tableData={this.state.getTagList}
                              tableAllign={['left', 'left', 'left']}
                          />

                          <br/>


                      </div>

                  </div>

              )
          }

      };*/
    cropImage = (event) => {
        event.preventDefault();
        this.setState({
            imageCropModal: true
        })
    }
    renderRemarks = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, makerRemarks, this.updateComponent)

            )
        }
        return;
    }
    renderCropedImage = () => {
        if (backEndServerURL + "/signaturePhoto/" + this.props.appId) {
            return (
                <button
                    style={{}}
                    className="btn btn-outline-danger"

                    onClick={this.mappingCropImage}>
                    Croped Image
                </button>
            )
        }
    }
    mappingCropImage = (event) => {
        event.preventDefault();
        this.setState({
            getMappingCropImage: true
        })
    }
    closeCropImage = () => {
        this.setState({
            getMappingCropImage: false
        })
    }
    tagCbDetailsModalClose=()=>{
        this.setState({
            tagCbDetailsModal: false

        })
    }
    tagCbDetailsModal = (event, cb) => {

        event.preventDefault();
        this.setState({
            tagCbDetailsModal: true,
            tagCb: cb
        })

    }
    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }


        return (
            <GridList cellHeight={800} cols={1}>
                <div>
                    <Dialog
                        fullWidth="true"
                        maxWidth="sm"
                        className={classes.modal}
                        classes={{paper: classes.dialogPaper}}
                        open={this.state.loading}>
                        <DialogContent className={classes.dialogPaper}>

                            <center>
                                <img src={loader} alt=""/>
                            </center>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        fullScreen={true}
                        open={this.state.renderCumModalopen}>
                        <DialogContent  >
                            <MakerCumInput closeModal={this.closeModal}/>

                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        fullScreen={true}
                        open={this.state.tagCbDetailsModal}>
                        <DialogContent className={classes.dialogPaper}>
                            <SecondaryCbFormDetails customerNumber={this.state.tagCb} appId={this.props.appId} tagCb={this.state.tagCb} closeModal={this.tagCbDetailsModalClose}/>
                            {/*  <MakerCumInput customerNumber={this.state.getCustomerNumber} appId={this.props.app_uid} generateAccountNo={this.state.generateAccountNo} tagingModalCbnumber={this.state.tagCb} taging={this.taging} closeModal={this.tagCbDetailsModalClse}/>*/}
                        </DialogContent>

                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        className={classes.modal}
                        classes={{paper: classes.dialogPaper}}
                        open={this.state.imageCropModal}>
                        <DialogContent className={classes.dialogPaper}>
                            <ImageCrop subServiceType={this.props.subServiceType} appId={this.props.appId}
                                       closeModal={this.closeModal}/>

                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        className={classes.modal}
                        classes={{paper: classes.dialogPaper}}
                        open={this.state.getMappingCropImage}>
                        <DialogContent className={classes.dialogPaper}>
                            <AssignedCropImage subServiceType={this.props.subServiceType} appId={this.props.appId}
                                               closeModal={this.closeModal}/>

                        </DialogContent>
                    </Dialog>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>
                            {this.renderNotification()}
                            {this.renderJsonFormFirst()}
                            {this.renderDefferalData()}
                        </ThemeProvider>
                    </Grid>
                    <br/>
                    <br/>

                    {this.renderDedupComponent()}

                    {/* <center>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>


                                {this.renderTaggingData()}
                            </Grid>


                        </Grid>
                    </center>*/}
                    {this.renderTagList()}

                    <ThemeProvider theme={theme}>
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',

                            }}

                            type='button' value='add more'
                            onClick={this.cropImage}
                        >Signature Card Crop
                        </button>
                        &nbsp;
                        {this.renderCropedImage()}
                        <br/>
                        {this.renderRemarksData()}
                        <br/>
                        <br/>
                        {this.renderRemarks()}
                        <br/>
                        {this.checkerList()}
                        <br/>
                        <br/>

                    </ThemeProvider>
                    {this.renderButton()}

                </div>
            </GridList>
        )

    }


}

export default withStyles(styles)(VerifyMakerInboxCase);

