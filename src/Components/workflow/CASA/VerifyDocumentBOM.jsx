import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import Functions from '../../../Common/Functions';
import Notification from "../../NotificationMessage/Notification";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import Grid from "@material-ui/core/Grid/index";
import {ThemeProvider} from "@material-ui/styles/index";
import theme from "../../JsonForm/CustomeTheme";
import {Dialog} from "@material-ui/core/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import SingleImageShow from "./SingleImageShow";
import GridList from "@material-ui/core/GridList/index";
import CSImageLinkEdit from "./CSImageLinkEdit";
import Link from "@material-ui/core/Link/index";

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};
let bomRemarks = [
    {
        "varName": "bomRemarks",
        "type": "textArea",
        "label": "BOM Remarks",
        "grid": 12
    }]
;

class VerifyDocumentBOM extends React.Component {
    state = {

        message: "",
        appData: {},
        getData: false,
        varValue: [],
        redirectLogin: false,
        title: "",
        notificationMessage: "",

        alert: false,
        getDeferalList: [],
        inputData: {},
        getImageLink: [],
        getImageBoolean: false,
        imageModalBoolean: false,
        selectImage: "",
        err: false,
        errorArray: {},
        errorMessages: {},
        getRemarks: [],
        getMappingAllImage: false
    }

    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }
    renderRemarksData = () => {


        if (this.state.getRemarks.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Remarks", "Raised By", "Date", "Role"]}
                        tableData={this.state.getRemarks}
                        tableAllign={['left', 'left', 'left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }

    componentDidMount() {
        var remarksArray=[]
        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response.data)
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {
                            let imageUrl = backEndServerURL + "/case/files/" + this.props.appId;
                            axios.get(imageUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    this.setState({
                                        getImageLink: response.data,
                                        getImageBoolean: true
                                    })
                                })
                                .catch((error) => {
                                    console.log(error);
                                })
                            console.log(response.data);
                            let tableArray = [];
                            var status = "";
                            response.data.map((deferal) => {
                                if (deferal.status === "ACTIVE") {
                                    status = "Approved By BM"
                                }
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })
                            let getCommentsUrl = backEndServerURL + "/appRemarkGet/"+this.props.appId;
                            axios.get(getCommentsUrl, {withCredentials: true})
                                .then((response) => {

                                    console.log(response.data);
                                    response.data.map((data) => {

                                        remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                                    })
                                    this.setState({
                                        getRemarks: remarksArray
                                    })
                                })
                                .catch((error) => {
                                    console.log(error)
                                })



                        })
                        .catch((error) => {
                            console.log(error);
                        })

                    this.setState({
                        getData: true,
                        showValue: true,
                        inputData: response.data,
                        varValue: response.data,
                        appData: response.data


                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Expire Date", "Raise By", "Raise Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }

    handleSubmit = (event, data) => {
        event.preventDefault();
        this.state.inputData.bom_approval = data;
        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.bomRemarks + "/" + this.props.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        this.state.inputData.bomRemarks=undefined;
        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {

                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()
                       //

                    })
                    .catch((error) => {
                        console.log(error);
                        /* if(error.response.status===452){
                             Functions.removeCookie();

                             this.setState({
                                 redirectLogin:true
                             })

                         }*/
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }
    renderEditForm = (() => {

        if (this.state.getData) {


            return (

                CommonJsonFormComponent.renderJsonForm(this.state, this.props.jsonForm, this.updateComponent)

            )
        }
        return;
    })

    handleChange = (event) => {
        event.target.name = event.target.value;

    };
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }
    renderSubmitButton = () => {
        if (this.state.getData) {
            return (
                <center>
                    <div>
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',

                            }}

                            type='button' value='add more'

                            onClick={(event) => this.handleSubmit(event, "APPROVED")}
                        >Approve
                        </button>
                        &nbsp;&nbsp;&nbsp;
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',

                            }}

                            type='button' value='add more'
                            onClick={(event) => this.handleSubmit(event, "RETURN")}

                        >Return
                        </button>
                    </div>
                </center>

            )
        }
    }
    updateComponent = () => {
        this.forceUpdate();
    };

    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {

            return (
                this.state.getImageLink.map((data) => {
                    return (

                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>

                    )
                })


            )
        }

    }
    renderRemarks = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, bomRemarks, this.updateComponent)

            )
        }
        return;
    }
    mappingAllImage = (event) => {
        event.preventDefault();
        this.setState({
            getMappingAllImage: true
        })
    }
    closeModal = () => {
        this.setState({
            getMappingAllImage: false
        })
    }
    renderCropedButton = () => {
       if(this.state.getData){
           return (
               <button
                   style={{
                       width: 150,

                   }}
                   className="btn btn-outline-danger"

                   onClick={this.mappingAllImage}

               >
                   Assigned Image
               </button>
           )
       }
    }

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (

            <GridList cellHeight={800} cols={1}>
                <Card>

                    <CardBody>

                        <ThemeProvider theme={theme}>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.imageModalBoolean}>
                                <DialogContent>

                                    <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                                </DialogContent>
                            </Dialog>
                            <div>


                                <Grid container spacing={1}>
                                    {this.renderEditForm()}


                                    <Grid item xs={12}>
                                        {this.renderDefferalData()}
                                    </Grid>
                                    <Grid item xs={12}>
                                        {this.renderRemarksData()}
                                    </Grid>


                                    {this.renderCropedButton()}
                                    <br/>
                                    <br/>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.getMappingAllImage}>
                                        <DialogContent>
                                            <CSImageLinkEdit appId={this.props.appId} closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>

                                    {this.renderNotification()}
                                    {this.renderRemarks()}

                                    <br/>
                                    <br/>
                                    <br/>

                                    {this.renderSubmitButton()}
                                </Grid>
                            </div>

                        </ThemeProvider>

                    </CardBody>
                </Card>
            </GridList>

        )
    }

}

export default withStyles(styles)(VerifyDocumentBOM);
