import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import React,{Component} from "react";
import Card from "../../Card/Card";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../../Card/CardBody";
import Grid from "@material-ui/core/Grid";
import DialogContent from "@material-ui/core/DialogContent";
import SingleImageShow from "./SingleImageShow";
import {Dialog} from "@material-ui/core";
class AssignedCropImage extends Component{
    state={
        getImageLink:[],
        getImageBoolean:false,
        imageModalBoolean:false,
        selectImage:""

    }

    close=()=>{
        this.props.closeModal();
    }
    closeModal=()=>{
        this.setState({
            imageModalBoolean:false
        })
    }
    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    mappingPhoto=()=>{
        console.log(backEndServerURL+"/signaturePhoto/"+this.props.appId+"/signature")
                    return(
                        <img  src={ backEndServerURL+"/signaturePhoto/"+this.props.appId+"/signature"} alt=""/>
                    )





    }
    render(){
        return(
            <div>
                <Card>
                    <CardHeader color="rose">
                        <h4>Croped Image<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                    </CardHeader>
                    <CardBody>
                        <Dialog
                            fullWidth="true"
                            maxWidth="xl"
                            open={this.state.imageModalBoolean}>
                            <DialogContent>

                                <SingleImageShow data={this.state.selectImage}
                                                 closeModal={this.closeModal}/>
                            </DialogContent>
                        </Dialog>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                {this.mappingPhoto()}

                            </Grid>

                        </Grid>
                    </CardBody>
                </Card>
            </div>
        )
    }
}
export default AssignedCropImage;