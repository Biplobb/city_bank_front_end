import GridList from "@material-ui/core/GridList";
import React from "react";
import {backEndServerURL} from "../../../Common/Constant";

import withStyles from "@material-ui/core/styles/withStyles";
const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    gridList: {
        width: 500,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },

};


class SecondaryCBImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            appData: {},
            getData: false,
            varValue: [],
            showValue: false,
            redirectLogin: false,
            title: "",
            notificationMessage: "",
            alert: false,
            inputData: {},
            selectedDate: {},
            SelectedDropdownSearchData: null,
            dropdownSearchData: {},
            values: [],
            customerName: [],
            deferalType: [],
            expireDate: [],
            other: [],
            getCheckerList: [],
            getAllDefferal: [],
            getDeferalList: [],
            loading: false,
            jointAccountCustomerNumber: 0,
            objectForJoinAccount: [],
            getgenerateForm: false,
        };


    }


    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };





    renderImage=()=>{

            let iif1, iif2;
          var customerNumber="Customer "+this.props.customerNumber;
          this.props.documentList.map((document) => {
                if (document.indexOf(customerNumber+" IIF1") > -1)
                    iif1 = document;
                else if (document.indexOf(customerNumber+" IIF2") > -1)
                    iif2 = document;



            })
            return (

                <React.Fragment>

                    <img width='100%' src={backEndServerURL + "/file/" + iif1} alt=""/>
                    <img width='100%' src={backEndServerURL + "/file/" + iif2} alt=""/>


                </React.Fragment>

            )


    }





    render() {
        const {classes} = this.props;

        return (
            <GridList  cellHeight={800} cols={1} >
                <div>
                    {this.renderImage()}
                </div>
            </GridList>

        )

    }


}

export default withStyles(styles)(SecondaryCBImage);
