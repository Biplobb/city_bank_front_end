import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import Functions from '../../../Common/Functions';
import Notification from "../../NotificationMessage/Notification";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {ThemeProvider} from "@material-ui/styles/index";
import Grid from "@material-ui/core/Grid/index";
import theme from "../../JsonForm/CustomeTheme";
import SelectComponent from "../../JsonForm/SelectComponent";
import Table from "../../Table/Table";
import FileTypeComponent from "../../JsonForm/FileTypeComponent";
import DialogContent from "@material-ui/core/DialogContent/index";
import {Dialog} from "@material-ui/core/index";
import CheckerTagCBDetails from "./CheckerTagCBDetails";
import GridList from "@material-ui/core/GridList/index";
import loader from "../../../Static/loader.gif";
import AssignedCropImage from "./AssignedCropImage";
import SecondaryCbFormDetails from "./SecondaryCbFormDetails";

let checkerRemarks = [
    {
        "varName": "checkerRemarks",
        "type": "textArea",
        "label": "Checker Remarks",
        "grid": 12
    }];

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};
var fileUpload = {
    "varName": "scanningFile",
    "type": "file",
    "label": "Upload File",
    "grid": 12
};


class VerifyMakerInboxCase extends React.Component {
    state = {
        message: "",
        appData: {},
        getData: false,
        varValue: [],
        redirectLogin: false,
        title: "",
        notificationMessage: "",
        alert: false,
        inputData: {},
        selectedDate: {},
        SelectedDropdownSearchData: null,
        dropdownSearchData: {},
        values: [],
        showValue: false,
        customerName: [],
        deferalType: [],
        expireDate: [],
        other: [],
        getCheckerList: [],
        getDeferalList: [],
        fileUploadData: {},
        loaderNeeded: null,
        objectForJoinAccount: [],
        getgenerateForm: false,
        getTagList: [],
        tagCbDetailsModal: false,
        customerNumber: "",
        loading: false,
        getRemarks: [],
        err: false,
        errorArray: {},
        errorMessages: {},
        getCropedImage: false

    }

    handleChange = (event) => {

        event.preventDefault();

        this.state.inputData[event.target.name] = event.target.value;


    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }
    renderRemarksData = () => {
        if (this.state.getRemarks.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Remarks", "Raised By", "Date", "Role"]}
                        tableData={this.state.getRemarks}
                        tableAllign={['left', 'left', 'left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }

    componentDidMount() {
        var remarksArray = [];
        this.setState({})
        this.setState({
            loading: true,

        })
        let objectForJoinAccount = [];
        var sl;
        for (var i = 0; i < 2; i++) {
            sl = i + 1;
            objectForJoinAccount.push(
                {
                    "varName": "Customer " + i,
                    "type": "title",
                    "label": "Customer: " + sl,
                    "grid": 12

                },

                {
                    "varName": "title" + i,
                    "type": "title",
                    "label": "Individual Information Form",
                    "grid": 12
                },
                {
                    "varName": "date" + i,
                    "type": "date",
                    "label": "Date",
                    "grid": 6
                },
                {
                    "varName": "cbNumber" + i,
                    "type": "text",
                    "label": "Unique Customer ID",
                    "grid": 6
                },
                {
                    "varName": "accountNo" + i,
                    "type": "text",
                    "label": "A/C No.",
                    "grid": 6
                },
                {
                    "varName": "relationWithTheAC" + i,
                    "type": "select",
                    "label": "Relation with the A/C",
                    "grid": 6,
                    "enum": [
                        "1st Applicant",
                        "2nd Applicant",
                        "3rd Applicant",
                        "director",
                        "A/C Operator",
                        "Partner",
                        "Beneficial Owner",
                        "Minor",
                        "Guardian",
                        "Trustee",
                        "Attorney Holder",
                        "Proprietor",
                        "Others"
                    ],
                },
                {
                    "varName": "accountName" + i,
                    "type": "text",
                    "label": "Account Name",
                    "grid": 6
                },
                {
                    "varName": "customerName" + i,
                    "type": "text",
                    "label": "Customer's Name",
                    "grid": 6
                },
                {
                    "varName": "title" + i,
                    "type": "title",
                    "label": "Relation",
                    "grid": 12
                },
                {
                    "varName": "fatherName" + i,
                    "type": "text",
                    "label": "Father's Name",
                    "grid": 6
                },
                {
                    "varName": "motherName" + i,
                    "type": "text",
                    "label": "Mother's Name",
                    "grid": 6
                },
                {
                    "varName": "spouseName" + i,
                    "type": "text",
                    "label": "Spouse' Name",
                    "grid": 6
                }, {
                    "varName": "dob" + i,
                    "type": "date",
                    "label": "DOB",
                    "grid": 6
                },
                {
                    "varName": "religion" + i,
                    "type": "text",
                    "label": "Religion",
                    "grid": 6
                },
                {
                    "varName": "gender" + i,
                    "type": "select",
                    "label": "Gender",
                    "grid": 6,
                    "enum": [
                        "Male",
                        "Female",
                        "Third Gender",
                        "Address proof"
                    ],
                },
                {
                    "varName": "nid" + i,
                    "type": "text",
                    "label": "NID",
                    "grid": 6
                },
                {
                    "varName": "birthCertificate" + i,
                    "type": "text",
                    "label": "Birth certificate",
                    "grid": 6
                },
                {
                    "varName": "residentStatus" + i,
                    "type": "select",
                    "label": "Resident status",
                    "grid": 6,
                    "enum": [
                        "Resident",
                        "Non-Resident"
                    ],
                },
                {
                    "varName": "passportNo" + i,
                    "type": "text",
                    "label": "Passport No",
                    "grid": 6
                },
                {
                    "varName": "passportIssueDate" + i,
                    "type": "date",
                    "label": "Issue Date",
                    "grid": 6
                },
                {
                    "varName": "passportExpDate" + i,
                    "type": "date",
                    "label": "Exp Date",
                    "grid": 6
                },
                {
                    "varName": "passportPlaceOfIssue" + i,
                    "type": "text",
                    "label": "Place of issue",
                    "grid": 6
                },
                {
                    "varName": "drivingLicenseNo" + i,
                    "type": "text",
                    "label": "Driving License no",
                    "grid": 6
                },
                {
                    "varName": "drivingLicenseExpDate" + i,
                    "type": "date",
                    "label": "Exp Date",
                    "grid": 6
                },
                {
                    "varName": "otherPhotoId" + i,
                    "type": "text",
                    "label": "Other photo id",
                    "grid": 6
                },
                {
                    "varName": "etin" + i,
                    "type": "text",
                    "label": "E-TIN",
                    "grid": 6
                },
                {
                    "varName": "nationality" + i,
                    "type": "select",
                    "label": "Nationality",
                    "grid": 6,
                    "enum": [
                        "Bangladeshi",
                        "Others"
                    ],
                },
                {
                    "varName": "placeOfBirth" + i,
                    "type": "text",
                    "label": "Place of Birth",
                    "grid": 6
                },
                {
                    "varName": "countryOfBirth" + i,
                    "type": "text",
                    "label": "Country of Birth",
                    "grid": 6
                },
                {
                    "varName": "profession" + i,
                    "type": "text",
                    "label": "Profession",
                    "grid": 6
                },
                {
                    "varName": "nameOfTheOrganization" + i,
                    "type": "text",
                    "label": "Name of the Organization",
                    "grid": 6
                },
                {
                    "varName": "designation" + i,
                    "type": "text",
                    "label": "Designation",
                    "grid": 6
                },
                {
                    "varName": "natureOfBusiness" + i,
                    "type": "text",
                    "label": "Nature of Business",
                    "grid": 6
                },
                ////end page//////
                {
                    "varName": "presentAddress" + i,
                    "type": "text",
                    "label": "Present Address",
                    "grid": 6
                },
                {
                    "varName": "presentAddressPostCode" + i,
                    "type": "text",
                    "label": "Post Code",
                    "grid": 6
                },
                {
                    "varName": "presentAddressCountry" + i,
                    "type": "text",
                    "label": "Country",
                    "grid": 6
                },
                {

                    "varName": "presentAddressLandmark" + i,
                    "type": "text",
                    "label": "Landmark",
                    "grid": 6
                },
                {
                    "varName": "professionalAddress" + i,
                    "type": "text",
                    "label": "Professional Address",
                    "grid": 6
                },
                {

                    "varName": "professionalAddressPostCode" + i,
                    "type": "text",
                    "label": "Post Code",
                    "grid": 6
                },
                {
                    "varName": "professionalAddressCountry" + i,
                    "type": "text",
                    "label": "Country",
                    "grid": 6
                },
                {
                    "varName": "professionalAddressLandmark" + i,
                    "type": "text",
                    "label": "Landmark",
                    "grid": 6
                },
                {
                    "varName": "permanentAddress" + i,
                    "type": "text",
                    "label": "Permanent Address",
                    "grid": 6
                },
                {
                    "varName": "permanentAddressPostCode" + i,
                    "type": "text",
                    "label": "Post Code",
                    "grid": 6
                },
                {
                    "varName": "permanentAddressCountry" + i,
                    "type": "text",
                    "label": "Country",
                    "grid": 6
                },
                {
                    "varName": "permanentAddressLandmark" + i,
                    "type": "text",
                    "label": "Landmark",
                    "grid": 6
                },
                {
                    "varName": "phoneNumberResidence" + i,
                    "type": "text",
                    "label": "Mobile Number(Residence)",
                    "grid": 6
                },
                {
                    "varName": "phoneNumberOffice" + i,
                    "type": "text",
                    "label": "Mobile Number(Office)",
                    "grid": 6
                },
                {
                    "varName": "mobile" + i,
                    "type": "text",
                    "label": "Mobile",
                    "grid": 6
                },
                {
                    "varName": "email" + i,
                    "type": "text",
                    "label": "Email ID",
                    "grid": 6
                },
                {
                    "varName": "emergencyContact" + i,
                    "type": "title",
                    "label": "Emergency Contact",
                    "grid": 12
                },
                {
                    "varName": "emergencyContactName" + i,
                    "type": "text",
                    "label": "Name",
                    "grid": 6
                },
                {
                    "varName": "emergencyContactAddress" + i,
                    "type": "text",
                    "label": "Address",
                    "grid": 6
                },
                {
                    "varName": "emergencyContactEmailId" + i,
                    "type": "text",
                    "label": "Email ID",
                    "grid": 6
                },
                {
                    "varName": "emergencyContactMobile" + i,
                    "type": "text",
                    "label": "Mobile",
                    "grid": 6
                },
                {
                    "varName": "emergencyContactRelationshipWithAcHolder" + i,
                    "type": "text",
                    "label": "Relationship With A/C Holder",
                    "grid": 6
                },
                {
                    "varName": "creditCardInformation" + i,
                    "type": "title",
                    "label": "Credit Card Information of Account Holder",
                    "grid": 12
                },
                {
                    "varName": "issuedByLocal" + i,
                    "type": "text",
                    "label": "Issued By (Local)",
                    "grid": 6
                },
                {
                    "varName": "issuedByInternational" + i,
                    "type": "text",
                    "label": "Issued By(International)",
                    "grid": 6
                },
                {
                    "varName": "fatcaInformation" + i,
                    "type": "text",
                    "label": "FATCA Information",
                    "grid": 6
                }
            )

        }

        this.setState({
            objectForJoinAccount: objectForJoinAccount,
            getgenerateForm: true

        })

        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    let checkerListUrl = backEndServerURL + "/checkers";
                    axios.get(checkerListUrl, {withCredentials: true})
                        .then((response) => {

                            let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                            axios.get(deferalListUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);

                                    let tableArray = [];
                                    let status = "";
                                    response.data.map((deferal) => {
                                        if (deferal.status === "ACTIVE") {
                                            status = "Approved By BM"
                                        } else {
                                            status = deferal.status
                                        }
                                        tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                                    });
                                    this.setState({
                                        getDeferalList: tableArray
                                    })

                                })
                                .catch((error) => {
                                    console.log(error);
                                })


                            this.setState({
                                getCheckerList: response.data
                            })
                            let getCommentsUrl = backEndServerURL + "/appRemarkGet/" + this.props.appId;
                            axios.get(getCommentsUrl, {withCredentials: true})
                                .then((response) => {

                                    console.log(response.data);
                                    response.data.map((data) => {

                                        remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                                    })
                                    this.setState({
                                        getRemarks: remarksArray
                                    })
                                })
                                .catch((error) => {
                                    console.log(error)
                                })


                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    let tableArrayData=[]
                    let cbTagUrl = backEndServerURL + "/secondaryCB/" + this.props.appId;
                    axios.get(cbTagUrl, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data);
                            response.data.map((data, index) => {
                                tableArrayData.push(this.createTaggingData(data));

                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    let varValue=response.data;
                    this.setState({
                        getTagList: tableArrayData,
                        getData: true,
                        showValue: true,
                        varValue: varValue,
                        appData: response.data,
                        loaderNeeded: true,
                        loading: false,
                    });

                })
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        loading: false
                    })
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }


    updateComponent = () => {
        this.forceUpdate();
    };


    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }
    closeModal = () => {
        this.setState({
            getCropedImage: false
        })
    }
    handleSubmit = (event, checker_approval) => {
        event.preventDefault();
        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.checkerRemarks + "/" + this.props.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        this.state.inputData.checker_approval = checker_approval;
        if (this.state.fileUploadData.scanningFile !== undefined) {
            let fileUploadPath = backEndServerURL + "/case/upload";
            let types = 'Attachments';
            let files = this.state.fileUploadData.scanningFile;
            console.log("dfghjk")
            console.log(files)
            let formData = new FormData();
            formData.append("appId", this.props.appId)
            formData.append("file", files)
            formData.append("type", types)
            axios({
                method: 'post',
                url: fileUploadPath,
                data: formData,
                withCredentials: true,
                headers: {'content-type': 'multipart/form-data'}
            })
                .then((response) => {

                    console.log(response);


                })
                .catch((error) => {
                    console.log(error)
                })
        }


        this.state.inputData.checkerRemarks = undefined;
        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {


                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()
                        //
                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });


    }


    /*renderForm = () => {
         if (this.state.getData) {

             return (
                 CommonJsonFormComponent.renderJsonForm(this.state, this.props.jsonForm, this.updateComponent)

             )
         }
         return;
     } */

    renderFileUpload = () => {
        if (this.state.getData) {
            return (

                <Grid item xs={12}>
                    {FileTypeComponent.file(this.state, this.updateComponent, fileUpload)}
                </Grid>

            )
        }
        return;
    }
    renderSubmitButton = () => {
        if (this.state.getData) {
            return (
                <Grid item xs={12}>

                    <div>
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',
                                marginTop: 20

                            }}

                            type='button' value='add more'
                            onClick={(event) => this.handleSubmit(event, "APPROVED")}
                        >Approve
                        </button>
                        &nbsp;&nbsp;&nbsp;
                        <button
                            className="btn btn-outline-danger"
                            style={{
                                verticalAlign: 'right',
                                marginTop: 20

                            }}
                            checkerApproval

                            type='button' value='add more'
                            onClick={(event) => this.handleSubmit(event, "NOTAPPROVED")}
                        >Return
                        </button>
                    </div>


                </Grid>

            )
        }
    }
    renderImageCrop = (event) => {
        event.preventDefault();
        this.setState({
            getCropedImage: true
        })


    }
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Due Date", "Created By", "Application Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    tagCbDetailsModalClse = () => {
        this.setState({
            tagCbDetailsModal: false
        })
    }
    tagCbDetailsModal = (event, customerNumber) => {
        event.preventDefault();
        this.setState({
            tagCbDetailsModal: true,
            customerNumber: customerNumber
        })

    }
    createTaggingData = (data) => {
        return (
            ["Customer "+data.customer,data.cbNumber,  [<button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'right',
                }}
                type='button' value='add more'
                onClick={(event) => this.tagCbDetailsModal(event, data.customer)}
            >View
            </button>]]


        )
    }


    renderTagList = () => {

        if (this.state.getData && (this.props.subServiceType !== "INDIVIDUAL" && this.props.subServiceType !== "Individual A/C") && this.state.getTagList.length > 0) {

            return (

                <div style={{marginBottom: 40}}>
                    {/*<paper>
                        <CardHeader color="rose">
                            <h4>Tag CB List</h4>

                        </CardHeader>
                    </paper>
*/}
                    <div>
                        {/*  <center>
                        <h4>Tag CB List</h4>
                    </center>*/}
                        <Table

                            tableHovor="yes"
                            tableHeaderColor="primary"
                            tableHead={["Customer No","CB Number", "View"]}

                            tableData={this.state.getTagList}
                            tableAllign={['left', 'left', 'left']}
                        />

                        <br/>


                    </div>

                </div>

            )
        }

    }


    renderJsonFormFirst = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.commonJsonForm, this.updateComponent)


            )
        } else {
            return;
        }
    }


    renderRemarks = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, checkerRemarks, this.updateComponent)

            )
        }
        return;
    }

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }


        return (
            <GridList cellHeight={800} cols={1}>
                <div>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        fullScreen={true}
                        open={this.state.tagCbDetailsModal}>
                        <DialogContent className={classes.dialogPaper}>
                            <SecondaryCbFormDetails appId={this.props.appId} customerNumber={this.state.customerNumber}
                                                    closeModal={this.tagCbDetailsModalClse}/>
                            {/*  <MakerCumInput customerNumber={this.state.getCustomerNumber} appId={this.props.app_uid} generateAccountNo={this.state.generateAccountNo} tagingModalCbnumber={this.state.tagCb} taging={this.taging} closeModal={this.tagCbDetailsModalClse}/>*/}
                        </DialogContent>

                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="xl"
                        className={classes.modal}
                        classes={{paper: classes.dialogPaper}}
                        open={this.state.getCropedImage}>
                        <DialogContent className={classes.dialogPaper}>
                            <AssignedCropImage subServiceType={this.props.subServiceType} appId={this.props.appId}
                                               closeModal={this.closeModal}/>

                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth="true"
                        maxWidth="sm"
                        className={classes.modal}
                        classes={{paper: classes.dialogPaper}}
                        open={this.state.loading}>
                        <DialogContent className={classes.dialogPaper}>

                            <center>
                                <img src={loader} alt=""/>
                            </center>
                        </DialogContent>
                    </Dialog>
                    <ThemeProvider theme={theme}>
                        <Grid container spacing={1}>
                            {this.renderNotification()}
                            {this.renderJsonFormFirst()}


                            {this.renderDefferalData()}
                            <br/>
                            <br/>
                            <center>

                                <Grid item xs={12}>


                                    {this.renderTagList()}
                                </Grid>


                            </center>
                            {this.renderFileUpload()}
                        </Grid>
                        <br/>

                        <button
                            style={{}}
                            className="btn btn-outline-danger"

                            onClick={this.renderImageCrop}>
                            Croped Image
                        </button>


                        <br/>
                        <br/>
                        {this.renderRemarksData()}
                        <br/>
                        <br/>
                        {this.renderRemarks()}
                        <br/>
                        {this.renderSubmitButton()}
                    </ThemeProvider>


                </div>
            </GridList>

        )

    }

}

export default withStyles(styles)(VerifyMakerInboxCase);
