import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Grow from "@material-ui/core/Grow";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {ThemeProvider} from "@material-ui/styles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import LiabilityUploadModal from "./LiabilityUploadModal";
import SingleImageShow from "./SingleImageShow";
import AccountNoGenerate from "./AccountNoGenerate";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CircularProgress from '@material-ui/core/CircularProgress';
import Notification from "../../NotificationMessage/Notification";
import GridList from "@material-ui/core/GridList";

let csRemarks = [
    {
        "varName": "remarks",
        "type": "textArea",
        "label": "Remarks",
        "grid": 12
    }];





const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};

function Transition(props) {
    return <Grow in={true} timeout="auto" {...props} />;
}

/*var JsonFormCasaIndividualAccountOpening = [

    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address",
        "grid": 6,
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
        "grid": 6,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "label": "SBS Code",
        "grid": 6,
    },
    {
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 6,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 6,
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Add Service",
        "grid": 12

    }
]


var service = [
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 6,

    },
    {

        "varName": "pageOfChequeBook",
        "type": "select",
        "label": "Page Of Cheque Book",
        "grid": 6,
        "enum": [
            "10",
            "20"
        ],
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 6
    },
    {
        "varName": "debitCardText",
        "type": "text",
        "label": "Name On Card",
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    }

]



var otherService = [


    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "additionalAccounts",
        "type": "title",
        "label": "Additional Accounts",
        "grid": 12

    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "dpsRequest",
        "type": "checkbox",
        "label": "DPS Request",
        "grid": 12
    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    }
];*/

var JsonFormCasaIndividualAccountOpening = [

    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address",
        "grid": 4,
    },
    {
        "varName": "schemeCode",
        "type": "text",
        "label": "Scheme Code",
        "grid": 4,
    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "grid": 4,
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ]
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
        "grid": 4,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "label": "SBS Code",
        "grid": 4,
    },
    /*{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },*/
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 4,

    },
    {
        "varName": "priority",
        "type": "select",
        "label": "Priority",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 4,
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Add Service",
        "grid": 12

    }
];


var service = [
    //cheque book
    {
        "varName": "chequeBookRequest",
        "type": "checkbox",
        "label": "Cheque Book Request",
        "grid": 12,

    },
    {

        "varName": "pageOfChequeBook",
        "type": "select",
        "label": "Page Of Cheque Book",
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,
        "grid": 4,
        "enum": [
            "25",
            "50",
            "100"
        ],

    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ],
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "deliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },

    {
        "varName": "chequeBookDesign",
        "type": "select",
        "label": "Cheque Book Design",
        "enum": [
            "Sapphire",
            "Citygem",
            "City Alo",
            "Other"
        ],
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "chequeBookRequest",
        "conditionalVarValue": true,

    },
//Debit Card
    {
        "varName": "debitCard",
        "type": "checkbox",
        "label": "Debit Card",
        "grid": 12
    },
    {
        "varName": "accountsType",
        "type": "text",
        "label": "Account Type",
        "enum": [
            "SAVINGS",
            "CURRENT",
            "FCY"
        ],

        "grid": 4,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "currency",
        "type": "select",
        "label": "Currency",
        "enum": [
            "BDT",
            "USD",
            "EUR",
            "GBP"
        ],
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "debitCardText",
        "type": "text",
        "label": "Name On Card",
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "cardType",
        "type": "select",
        "label": "Card Type",
        "grid": 4,
        "enum": [
            "VISA",
            "MASTER",
            "CITYMAXX",
        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "productType",
        "type": "text",
        "label": "Product Type",
        "grid": 4,
        "enum": [
            "#",
            "#",

        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "selectCcep",
        "type": "text",
        "label": "Select CCEP",
        "grid": 4,
        "enum": [
            "#",
            "#",

        ],
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },

    {
        "varName": "deliveryType",
        "type": "select",
        "label": "Delivery Type",
        "enum": [
            "Courier",
            "Branch"
        ],
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "debitCard",
        "conditionalVarValue": true,

    },
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 4

    },
    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "grid": 4,
        "conditional": true,
        "conditionalVarName": "smsAlertRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "a",
        "type": "title",
        "label": "",
        "grid": 4

    },


];


var otherService = [
    {
        "varName": "callCenterRegistration",
        "type": "checkbox",
        "label": "Call Center Registration",
        "grid": 12

    },

    {
        "varName": "cityTouchRequest",
        "type": "checkbox",
        "label": "City Touch",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email ",
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "cityTouchCustomerId",
        "type": "text",
        "label": "Customer Id ",
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },
    {
        "varName": "cityTouchCbNumber",
        "type": "text",
        "label": "CB Number",
        "grid": 6,
        "conditional": true,
        "conditionalVarName": "cityTouchRequest",
        "conditionalVarValue": true,

    },

    {
        "varName": "statementFacility",
        "type": "title",
        "label": "Statement Facility",
        "grid": 12

    },
    {
        "varName": "statementFacility",
        "type": "checkbox",
        "label": "E-Statement",
        "grid": 12

    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "conditional": true,
        "conditionalVarName": "statementFacility",
        "conditionalVarValue": true,
        "grid": 6

    },
    {
        "varName": "additionalAccounts",
        "type": "title",
        "label": "Additional Accounts",
        "grid": 12

    },
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    },
    {
        "varName": "dpsRequest",
        "type": "checkbox",
        "label": "DPS Request",
        "grid": 12
    },
    {
        "varName": "loanRequest",
        "type": "checkbox",
        "label": "Loan Request",
        "grid": 12
    }
];

var deferalOther =
    {
        "varName": "deferalOther",
        "type": "text",
        "label": "Please Specify",
        "grid": 6
    };
/*var additioanlAccounts=[
    {
        "varName":"fdr",
        "type":"checkbox",
        "label":"FDR",
        "grid":6
    },
    {
        "varName":"dps",
        "type":"checkbox",
        "label":"DPS",
        "grid":6
    },
    {
        "varName":"loan",
        "type":"checkbox",
        "label":"Loan",
        "grid":6
    }
]*/
var deferal =
    {
        "varName": "deferalType",
        "type": "select",
        "label": "Deferal Type",
        "enum": [
            "Applicant Photograph",
            "Nominee Photograph",
            "Passport",
            "Address proof",
            "Transaction profile",
            "other"
        ],
        "grid": 6
    };

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

class LiabilityExist extends Component {

    constructor(props) {
        super(props);
        this.state = {
            SelectedData: false,
            csDeferalPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            varValue: [],
            caseId: "",
            title: "",
            notificationMessage: "",
            app_uid: "-1",
            alert: false,
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "NO",
                accountType: "INSTAPACK",
                priority:"GENERAL"
            },
            fileUploadData: {},
            selectedDate: {},
            dropdownSearchData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage: "",
            imageModalBoolean: false,
            imgeListLinkSHow: false,
            accountDetailsModal: false,
            loaderNeeded: null,
            IndividualDedupModal: false,
            JointDedupModal: false,
            individualDataSaveId: '',
            jointDataSaveId: '',
            companyDataSaveId: '',
            numberOfCustomer:0,
            err: false,
            errorArray: {},
            errorMessages: {},


        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }




    addDeferalForm() {



        return this.state.values.map((el, i) =>

            <React.Fragment>

                <Grid item xs="6">
                    {
                        this.dynamicDeferral(el)
                    }

                </Grid>


                <Grid item xs="12">
                    {this.dynamicDeferralOther(el)}


                </Grid>
                <Grid item xs="6">
                    {
                        this.dynamicDate(el)
                    }
                    <button
                        style={{float:"right" } }
                        className="btn btn-outline-danger"
                        type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                    >
                        Remove
                    </button>
                </Grid>



                <Grid item xs="12"></Grid>

            </React.Fragment>
        )

    }

    getSearchvalue = (jsonObject) => {
        var clone = JSON.parse(JSON.stringify(jsonObject))
        for (var prop in clone)
            if (clone[prop] === '')
                delete clone[prop];
        return clone;
    }
    DedupDataSaveApi = (subServiceType) => {
        console.log(this.props.individualDedupData)
        console.log(this.props.companyDedupData)
        console.log(this.props.jointDedupData)
        if(subServiceType==="INDIVIDUAL" || subServiceType==="Individual A/C"){
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {"individualDedupData": this.props.individualDedupData,"dedupType":subServiceType}, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else  if(subServiceType==="Joint Account"){
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {"jointDedupData": this.props.jointDedupData,"dedupType":subServiceType}, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else  if(subServiceType === "Proprietorship A/C" || subServiceType === "NONINDIVIDUAL"){
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {"individualDedupData": this.props.individualDedupData,"dedupType":subServiceType}, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {"companyDedupData": this.props.companyDedupData,"dedupType":subServiceType}, {withCredentials: true})
                        .then((response)=>{
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error)=>{
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else  if(subServiceType === "Company Account"){
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {"jointDedupData": this.props.jointDedupData,"dedupType":subServiceType}, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {"companyDedupData": this.props.companyDedupData,"dedupType":subServiceType}, {withCredentials: true})
                        .then((response)=>{
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error)=>{
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else{
            alert("please type select")
        }
    }

    componentDidMount() {

        this.setState({
            loaderNeeded: false
        })
        this.state.inputData["csDeferal"] = "NO";


        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    console.log(response.data)
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            var status="";
                            response.data.map((deferal) => {
                                if(deferal.status==="ACTIVE"){
                                    status="Approved"
                                }
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    this.state.inputData["csDeferal"]="YES";
                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,
                        showValue: true,
                        appId: this.props.appId,
                        loaderNeeded: true
                    });

                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 652) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    {
                        this.DedupDataSaveApi(this.props.subServiceType)
                    }
                    console.log(response.data)
                    let inputData = this.getSearchvalue(this.props.searchValue);
                    inputData.csDeferal = "NO";
                    inputData.accountType = "INSTAPACK";
                    inputData.priority = "GENERAL";
                    inputData.smsAlertRequest = true;
                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        //inputData:JSON.parse(JSON.stringify(this.props.searchValue)),
                        inputData: inputData,
                        varValue: this.props.searchValue,
                        showValue: true,
                        getData: true,
                        loaderNeeded: true


                    });
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length > 0) {

            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Expire Date","Raise By","Raise Date","Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    }
    renderAddButtonShow = () => {

        return (
            <button
                className="btn btn-outline-danger"
                style={{
                    float: 'right',
                    verticalAlign: 'right',

                }}

                type='button' value='add more'
                onClick={this.addClick.bind(this)}


            >Add Defferal</button>
        )

    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };

    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    updateComponent = () => {
        this.forceUpdate();
    };

    returnSearchField = () => {
        if (this.state.showValue) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.commonJsonForm, this.updateComponent)
            )
        }
    };



    handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.state.inputData)



        if (this.state.inputData["csDeferal"]==="YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "AccountOpening";
        data.individualDedupData = this.state.individualDataSaveId;
        data.jointDedupData = this.state.jointDataSaveId;
        data.companyDedupData = this.state.companyDataSaveId;
        data.jointAccountCustomerNumber = this.props.jointAccountCustomerNumber;
        data.subServiceType = this.props.subServiceType;
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);

                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");
                        this.close();
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true

                        })
                        if (this.state.inputData.accountType === "NON-INSTAPACK" && this.state.deferalNeeded === false) {
                            this.setState({
                                accountDetailsModal: true
                            })
                        } else {

                            this.close()

                        }

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 652) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))

        this.state.inputData["csDeferal"]= "YES";
    }

    renderRemarks = () => {
        if (this.state.getData) {

            return (

                CommonJsonFormComponent.renderJsonForm(this.state, csRemarks, this.updateComponent)

            )
        }
        return;
    }
    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
            if(this.state.values.length>0){
                this.state.inputData["csDeferal"]= "YES"
            }
            else{
                this.state.inputData["csDeferal"]= "NO"
            }
        }


    }


    renderServiceTagging() {
        if (!this.state.deferalNeeded && this.state.getData)
            return (
                <React.Fragment>
                    <br/>
                    <br/>

                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, JsonFormCasaIndividualAccountOpening, this.updateComponent)
                    }

                    {
                        CommonJsonFormComponent
                            .renderJsonForm(this.state, service, this.updateComponent)
                    }


                    <Grid item xs={12}></Grid>
                    {
                        CommonJsonFormComponent
                            .renderJsonForm(this.state, otherService, this.updateComponent)
                    }

                    <br/>
                    <br/>
                </React.Fragment>


            );

    }

    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    sty
                    className="btn btn-outline-danger"
                    style={{
                        float: 'right',
                        verticalAlign: 'middle',
                    }}
                    onClick={this.uploadModal}

                >
                    Upload File
                </button>
            )
        }
    }
    viewImageModal = (event) => {
        event.preventDefault();

        this.setState({
            selectImage: event.target.value,
            imageModalBoolean: true
        })


    }
    closeModal = () => {
        this.setState({
            imageModalBoolean: false,
            IndividualDedupModal: false,
            JointDedupModal: false,

        })
    }
    closeUploadModal = (data) => {
        this.setState({

            imgeListLinkSHow: true,
            getImageLink: data,
            uploadModal:false
        })
    }
    accountDetailsModal = () => {
        this.setState({
            accountDetailsModal: false
        })
        this.props.closeModal();
    }
    renderImageLink = () => {

        if (this.state.imgeListLinkSHow && this.state.getImageLink!==undefined) {

            return (
                this.state.getImageLink.map((data) => {
                    return (
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }
    IndividualDedupModal = () => {
        this.setState({
            IndividualDedupModal: true
        })
    }
    JointDedupModal = () => {
        this.setState({
            JointDedupModal: true
        })
    }

    render() {

        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }

        if (this.state.loaderNeeded === false) {
            return (
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4>Exist
                                    Customer
                                    Account Opening<a><CloseIcon onClick={this.close} style={{
                                        position: 'absolute',
                                        right: 10,
                                        color: "#000000"
                                    }}/></a></h4>

                            </CardHeader>
                            <CardBody>
                                <CircularProgress style={{marginLeft: '50%'}}/>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>


            )
        }
        else if (this.state.inputData.accountType==="INSTAPACK" && this.props.subServiceType!=="Company Account") {
            return (


                <Card>
                    <CardHeader color="rose">
                        <h4>Exist
                            Customer
                            Account Opening<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>

                    </CardHeader>
                    <CardBody>
                        <div>

                            <ThemeProvider theme={theme}>

                                <Grid container spacing={3}>
                                    {this.renderNotification()}





                                    {this.returnSearchField()}


                                    {
                                        this.renderServiceTagging()
                                    }

                                    <br/>
                                    {this.renderAddButtonShow()}
                                    <Grid item xs='12'>

                                    </Grid>
                                    <br/>


                                    {
                                        this.addDeferalForm()
                                    }
                                    <Grid item xs='12'>

                                        {this.renderDefferalData()}

                                    </Grid>
                                    <Grid item xs='12'></Grid>
                                    {this.renderUploadButton()}
                                    {this.renderRemarks()}
                                    {this.renderImageLink()}
                                    <Grid item xs={12}></Grid>
                                    <Grid item xs={12}></Grid>
                                    <Grid item xs={12}></Grid>
                                    <Grid item xs={12}></Grid>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.uploadModal}>
                                        <DialogContent>
                                            <LiabilityUploadModal jointAccountCustomerNumber={this.props.jointAccountCustomerNumber} subServiceType={this.state.subServiceType} appId={this.state.appId}
                                                                  closeModal={this.closeUploadModal}/>
                                        </DialogContent>
                                    </Dialog>


                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="xl"
                                        open={this.state.imageModalBoolean}>
                                        <DialogContent>

                                            <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                                        </DialogContent>
                                    </Dialog>

                                    <Grid item xs="12"></Grid>
                                    <center>
                                        <button
                                            className="btn btn-outline-danger"
                                            style={{
                                                verticalAlign: 'middle',
                                            }}
                                            onClick={this.handleSubmit}

                                        >
                                            Submit
                                        </button>
                                    </center>
                                </Grid>

                            </ThemeProvider>






                        </div>



                    </CardBody>
                </Card>



            );
        }
        else if (this.state.inputData.accountType === "NON-INSTAPACK") {
            return (



                <Card>
                    <CardHeader color="rose">
                        <h4>Exist
                            Customer
                            Account Opening<a><CloseIcon onClick={this.close} style={{
                                position: 'absolute',
                                right: 10,
                                color: "#000000"
                            }}/></a></h4>

                    </CardHeader>
                    <CardBody>
                        <div>

                            <ThemeProvider theme={theme}>
                                <GridList  cellHeight={800} cols={1} >
                                    <Grid container spacing={3}>
                                        {this.renderNotification()}



                                        {this.returnSearchField()}


                                        {
                                            this.renderServiceTagging()
                                        }

                                        <br/>
                                        {this.renderAddButtonShow()}
                                        <Grid item xs='12'>

                                        </Grid>
                                        <br/>


                                        {
                                            this.addDeferalForm()
                                        }
                                        <Grid item xs='12'>

                                            {this.renderDefferalData()}

                                        </Grid>
                                        <Grid item xs='12'></Grid>
                                        {this.renderUploadButton()}
                                        {this.renderRemarks()}
                                        {this.renderImageLink()}
                                        <Grid item xs={12}></Grid>
                                        <Grid item xs={12}></Grid>
                                        <Grid item xs={12}></Grid>
                                        <Grid item xs={12}></Grid>
                                        <Dialog
                                            fullWidth="true"
                                            maxWidth="xl"
                                            open={this.state.uploadModal}>
                                            <DialogContent>
                                                <LiabilityUploadModal subServiceType={this.state.subServiceType} appId={this.state.appId}
                                                                      closeModal={this.closeUploadModal}/>
                                            </DialogContent>
                                        </Dialog>


                                        <Dialog
                                            fullWidth="true"
                                            maxWidth="xl"
                                            open={this.state.imageModalBoolean}>
                                            <DialogContent>

                                                <SingleImageShow data={this.state.selectImage} closeModal={this.closeModal}/>
                                            </DialogContent>
                                        </Dialog>

                                        <Grid item xs="12"></Grid>
                                        <center>
                                            <button
                                                className="btn btn-outline-danger"
                                                style={{
                                                    verticalAlign: 'middle',
                                                }}
                                                onClick={this.handleSubmit}

                                            >
                                                Submit
                                            </button>
                                        </center>
                                    </Grid>
                                </GridList>
                            </ThemeProvider>






                        </div>



                    </CardBody>
                </Card>


            );
        }

    }

}

export default withStyles(styles)(LiabilityExist);
