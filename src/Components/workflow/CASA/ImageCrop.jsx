import React, {Component} from 'react';
import ReactCrop, {makeAspectCrop} from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Card from "../../Card/Card";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../../Card/CardBody";
import Grid from "@material-ui/core/Grid";
class ImageCrop extends Component {
    state = {
        selectedImageURL: null,
        selectedImageURL1: null,
        //selectedImageURL: sample,
        crop: {
            x: 200,
            y: 700,
            width:100,
            height:100,
            // aspect: 16 / 9,
        },
        selectedFile: null,
        croppedImage: null,
        croppedImage1: null,
        modalWidth : 0,
        modalHeight : 0
    };

    componentDidMount() {
        var aof3;
         let fileUrl = backEndServerURL + "/case/files/" + this.props.appId;
         axios.get(fileUrl, {withCredentials: true})
             .then((response) => {
                 let string = "AOF3";

                 if (this.props.subServiceType === "Company Account" )
                    string = "SIGNATURE CARD";
                 else if (this.props.subServiceType==="New FDR Opnening" )
                     string = "AOF3";
                 else if (this.props.subServiceType==="Tag FDR Opnening" )
                     string = "AOF3";
                 else
                     string = "AOF3";
                 response.data.map((document) => {
                     if (document.indexOf(string) > -1) {
                         aof3 = document;
                         let fileReader = new FileReader();
                         axios.get(backEndServerURL+"/file/"+aof3,{withCredentials:true,responseType:'blob'})
                             .then((response)=>{
                                 console.log(response);
                                 fileReader.addEventListener('load',()=>
                                     this.setState({
                                         selectedImageURL:fileReader.result,
                                         selectedImageURL1:fileReader.result
                                     })
                                 );
                                 fileReader.readAsDataURL(response.data);
                             })



                     }


                 })


                 this.setState({
                     documentList: response.data,
                     getDocument: true
                 })
             })
             .catch((error) => {
                 console.log("error");
                 console.log(error);
             })

    }





    onCropComplete = (crop, pixels) => {
    }
    onCropComplete1 = (crop, pixels) => {
    }

    onCropChange = (crop) => {
        let modalWidth = document.getElementById("image-crop").offsetWidth;
        let modalHeight = document.getElementById("image-crop").offsetHeight;
        this.setState({crop, modalHeight, modalWidth });
        const croppedImg = this.getCroppedImg(this.refImageCrop, crop);
        this.setState({showImageCropper: false, croppedImage: croppedImg})
    }
    onCropChange1 = (crop1) => {
        let modalWidth = document.getElementById("image-crop").offsetWidth;
        let modalHeight = document.getElementById("image-crop").offsetHeight;
        this.setState({crop1, modalHeight, modalWidth });
        const croppedImg1 = this.getCroppedImg1(this.refImageCrop1, crop1);
        this.setState({showImageCropper: false, croppedImage1: croppedImg1})
    }



    getCroppedImg(image, pixelCrop) {
        /* the parameters: - the image element - the new width - the new height - the x point we start taking pixels - the y point we start taking pixels - the ratio */
        // Set up canvas for thumbnail
        // console.log(imgObj);
        // let img = new Image();
        // img.src = this.state.selectedImageURL;
        // let tempCanvas = document.createElement('canvas');
        // let tnCanvas = tempCanvas;
        // tnCanvas.width = newWidth;
        // tnCanvas.height = newHeight;
        // tnCanvas.getContext('2d').drawImage(img, startX, startY, newWidth, newHeight);
        // return tnCanvas;

        //let image = new Image();
        var image = new Image();
        image.src = this.state.selectedImageURL;

        /*const targetX = srcImage.width * pixelCrop.x / 100;
        const targetY = srcImage.height * pixelCrop.y / 100;
        const targetWidth = srcImage.width * pixelCrop.width / 100;
        const targetHeight = srcImage.height * pixelCrop.height / 100;

        const canvas = document.createElement('canvas');
        canvas.width = targetWidth;
        canvas.height = targetHeight;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            img,
            targetX,
            targetY,
            targetWidth,
            targetHeight,
            0,
            0,
            targetWidth,
            targetHeight
        );*/

        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / this.state.modalWidth;
        const scaleY = image.naturalHeight / this.state.modalHeight;
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        const ctx = canvas.getContext('2d');

        console.log(image.naturalWidth, image.naturalHeight);

        ctx.drawImage(
            image,
            pixelCrop.x * scaleX,
            pixelCrop.y * scaleY,
            pixelCrop.width * scaleX,
            pixelCrop.height * scaleY,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height
        );

        return canvas.toDataURL('image/jpeg');
    }
    getCroppedImg1(image, pixelCrop) {
        /* the parameters: - the image element - the new width - the new height - the x point we start taking pixels - the y point we start taking pixels - the ratio */
        // Set up canvas for thumbnail
        // console.log(imgObj);
        // let img = new Image();
        // img.src = this.state.selectedImageURL;
        // let tempCanvas = document.createElement('canvas');
        // let tnCanvas = tempCanvas;
        // tnCanvas.width = newWidth;
        // tnCanvas.height = newHeight;
        // tnCanvas.getContext('2d').drawImage(img, startX, startY, newWidth, newHeight);
        // return tnCanvas;

        //let image = new Image();
        var image = new Image();
        image.src = this.state.selectedImageURL1;

        /*const targetX = srcImage.width * pixelCrop.x / 100;
        const targetY = srcImage.height * pixelCrop.y / 100;
        const targetWidth = srcImage.width * pixelCrop.width / 100;
        const targetHeight = srcImage.height * pixelCrop.height / 100;

        const canvas = document.createElement('canvas');
        canvas.width = targetWidth;
        canvas.height = targetHeight;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            img,
            targetX,
            targetY,
            targetWidth,
            targetHeight,
            0,
            0,
            targetWidth,
            targetHeight
        );*/

        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / this.state.modalWidth;
        const scaleY = image.naturalHeight / this.state.modalHeight;
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        const ctx = canvas.getContext('2d');

        console.log(image.naturalWidth, image.naturalHeight);

        ctx.drawImage(
            image,
            pixelCrop.x * scaleX,
            pixelCrop.y * scaleY,
            pixelCrop.width * scaleX,
            pixelCrop.height * scaleY,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height
        );

        return canvas.toDataURL('image/jpeg');
    }


    handleSubmit=(event)=>{
        event.preventDefault();
        /*  var file = new File([this.state.croppedImage], "photo");
          console.log(file)*/
        console.log(this.state.croppedImage)

       let object={
           appId:this.props.appId,
           image:this.state.croppedImage,
           type:"signature"

       };


        let fileUploadPath = backEndServerURL + "/upload/signature/photo";
        axios({
            method: 'post',
            url: fileUploadPath,
            data: object,
            withCredentials: true

        })
            .then((response) => {

                this.props.closeModal();

                console.log(response);


            })
            .catch((error) => {
                console.log(error)
            })
    }
    close=()=>{
        this.props.closeModal()
    }
    render() {
        return (
        <div>

            <Card>
                <CardHeader color="rose">
                    <h4>Customer Photo and Signature Crop<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                </CardHeader>
                <CardBody>
                    <Grid container spacing={1}>
                        <Grid item xs={3}>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="App">
                                <div id = 'image-crop'>
                                    <center>
                                        <paper>
                                            <h2>Signature</h2>
                                        </paper>
                                    </center>
                                    <ReactCrop
                                        src={this.state.selectedImageURL}
                                        crop={this.state.crop}
                                        // onImageLoaded={this.onImageLoaded}
                                        onComplete={this.onCropComplete}
                                        onChange={this.onCropChange}
                                    />
                                </div>
                               <div >
                                   <table border="4">

                                   <img  src={this.state.croppedImage} alt=""/>
                                   </table>
                               </div>
                            </div>
                        </Grid>
                        <Grid item xs={3}>
                        </Grid>
                        {/*<Grid item xs={6}>
                            <div className="App">
                                <div id = 'image-crop'>
                                   <center>
                                       <paper>
                                           <h2>Photo</h2>
                                       </paper>
                                   </center>
                                    <ReactCrop
                                        src={this.state.selectedImageURL1}
                                        crop={this.state.crop1}
                                        // onImageLoaded={this.onImageLoaded}
                                        onComplete={this.onCropComplete1}
                                        onChange={this.onCropChange1}
                                    />
                                </div>
                                <img  src={this.state.croppedImage1} alt=""/>
                            </div>
                        </Grid>*/}

                    </Grid>
                    <center>
                        <Grid item xs={12}>
                            <button
                                className="btn btn-outline-danger"
                                style={{
                                    verticalAlign: 'right',

                                }}

                                type='button' value='add more'

                                onClick={this.handleSubmit}
                            >Submit
                            </button>

                        </Grid>
                    </center>
                </CardBody>
            </Card>
        </div>

    )


    }
}

export default ImageCrop
