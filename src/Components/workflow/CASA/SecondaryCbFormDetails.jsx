
import React, {Component} from "react";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";
import CardBody from "../../Card/CardBody";
import Grid from "@material-ui/core/Grid/index";
import withStyles from "@material-ui/core/styles/withStyles";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';

import {ThemeProvider} from "@material-ui/styles/index";
import theme from "../../JsonForm/CustomeTheme";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {Dialog} from "@material-ui/core/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import AccountNoGenerate from "./AccountNoGenerate";
import GridList from "@material-ui/core/GridList";
import VerifyMakerPhoto from "../VerifyMakerPhoto";
import SecondaryCBImage from "./SecondaryCBImage";


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});

var fileUpload =  [
    {
        "varName":"cbNumber",
        "type":"text",
        "label":"Customer Id",
        "grid":12,"readOnly":true,
        "length":9,


    },

    {
        "varName":"title",
        "type":"text",
        "label":"Title",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"customerName",
        "type":"text",
        "label":"Customer Name",
        "grid":12,"readOnly":true,
        "length":80,


    },

    {
        "varName":"shortName",
        "type":"text",
        "label":"Short Name",
        "grid":12,"readOnly":true,
        "length":10,


    },

    {
        "varName":"status",
        "type":"text",
        "label":"Status",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"statusAsOnDate",
        "type":"date",
        "label":"Status as on Date",
        "grid":12
    },

    {
        "varName":"acManager",
        "type":"text",
        "label":"AC Manager",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"occupationCode",
        "type":"text",
        "label":"Occuoation Code",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"constitution",
        "type":"text",
        "label":"Constitution",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"gender",
        "type":"text",
        "label":"Gender",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"staffFlag",
        "type":"text",
        "label":"Staff Flag",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"staffNumber",
        "type":"text",
        "label":"Staff Number",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"minor",
        "type":"text",
        "label":"Minor",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"nonResident",
        "type":"text",
        "label":"Non Resident",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"trade",
        "type":"text",
        "label":"Trade",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"nationalIdCard",
        "type":"text",
        "label":"National ID Card",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"dateOfBirth",
        "type":"date",
        "label":"Date of Birth",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"introducerCustomerId",
        "type":"text",
        "label":"Introducer Customer Id",
        "grid":12,"readOnly":true,
        "length":9,

        required:true,
    },

    {
        "varName":"introducerName",
        "type":"text",
        "label":"Introducer Name",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"introducerStaff",
        "type":"text",
        "label":"Introducer Staff",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"maritialStatus",
        "type":"text",
        "label":"Maritial Status",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"father",
        "type":"text",
        "label":"Father",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"mother",
        "type":"text",
        "label":"Mother",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"spouse",
        "type":"text",
        "label":"Spouse",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"communicationAddress1",
        "type":"text",
        "label":"Communication Address1",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"communicationAddress2",
        "type":"text",
        "label":"Communication Address2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"city1",
        "type":"text",
        "label":"City",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"state1",
        "type":"text",
        "label":"State",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"country1",
        "type":"text",
        "label":"Country",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"phoneNo11",
        "type":"text",
        "label":"Phone No1",
        "grid":12,"readOnly":true,
        "length":11,

        required:true,
    },

    {
        "varName":"phoneNo21",
        "type":"text",
        "label":"Phone No2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"telexNo1",
        "type":"text",
        "label":"Telex No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"email1",
        "type":"text",
        "label":"Email",
        "grid":12,"readOnly":true,

        email:true,

    },

    {
        "varName":"permanentAddress1",
        "type":"text",
        "label":"Permanent Address1",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"permanentAddress2",
        "type":"text",
        "label":"Permanent Address2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"city2",
        "type":"text",
        "label":"City",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"state2",
        "type":"text",
        "label":"State",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"country2",
        "type":"text",
        "label":"Country",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"phoneNo12",
        "type":"text",
        "label":"Phone No1",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"phoneNo222",
        "type":"text",
        "label":"Phone No2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"telexNo2",
        "type":"text",
        "label":"Telex No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"email2",
        "type":"text",
        "label":"Email",
        "grid":12,"readOnly":true,

        email:true,

    },

    {
        "varName":"employerAddress1",
        "type":"text",
        "label":"Employer Address1",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"employerAddress2",
        "type":"text",
        "label":"Employer Address2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"city3",
        "type":"text",
        "label":"City",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"state3",
        "type":"text",
        "label":"State",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"postalCode3",
        "type":"text",
        "label":"Postal Code",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"country",
        "type":"text",
        "label":"Country",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"phoneNo13",
        "type":"text",
        "label":"Phone No1",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"phoneNo23",
        "type":"text",
        "label":"Phone No2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"telexNo",
        "type":"text",
        "label":"Telex No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"email3",
        "type":"text",
        "label":"Email",
        "grid":12,"readOnly":true,

        email:true,

    },

    {
        "varName":"faxNo",
        "type":"text",
        "label":"Fax No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"combineStatement",
        "type":"text",
        "label":"Combine Statement",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"tds",
        "type":"text",
        "label":"TDS",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"pangirNo",
        "type":"text",
        "label":"PANGIR No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"passportNo",
        "type":"text",
        "label":"Passport No",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"issueDate",
        "type":"date",
        "label":"Issue Date",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"passportDetails",
        "type":"text",
        "label":"Passport Details",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"expiryDate",
        "type":"date",
        "label":"Expiry Date",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"purgedAllowed",
        "type":"text",
        "label":"Purged Allowed",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"freeText2",
        "type":"text",
        "label":"Free Text2",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"freeText5",
        "type":"text",
        "label":"Free Text 5",
        "grid":12,"readOnly":true,
        "length":10,


    },

    {
        "varName":"freeText8",
        "type":"text",
        "label":"Free Text 8",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"freeText9",
        "type":"text",
        "label":"Free Text 9",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"freeText13",
        "type":"text",
        "label":"Free Text13",
        "grid":12,"readOnly":true,



    },

    {
        "varName":"freeText14",
        "type":"text",
        "label":"Free Text14",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"freeText15",
        "type":"text",
        "label":"Free Text15",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"freeCode1",
        "type":"text",
        "label":"Free Code1",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"freeCode3",
        "type":"text",
        "label":"Free Code3",
        "grid":12,"readOnly":true,


        required:true,
    },

    {
        "varName":"freeCode7",
        "type":"text",
        "label":"Free Code 7",
        "grid":12,"readOnly":true,


        required:true,
    },
];
var selectFileName = {
    "varName": "fileName",
    "type": "dropdown",
    "required":true,
    "label": "This photo name",

    "grid": 6
}

class SecondaryCbFormDetails extends Component {
    state = {
        fileUploadData: {},
        getSplitFile: [],
        multipleScanningphotoShow: null,
        inputData: {},
        dropdownSearchData: {},
        selectedDate: {},
        documentList: [],
        getDocument: false,
        err: false,
        varValue: {},
        getData:false,
        errorArray: {},
        errorMessages: {},
        showValue:false

    }

    constructor(props) {
        super(props);


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    close = () => {
        this.props.closeModal();
    }
    closeModal=(account)=>{

        this.props.taging(this.props.tagingModalCbnumber);
        this.props.closeModal(account,"");
        this.setState({
            accountNo:false
        })
    }
    handleSubmit = (event) => {

        event.preventDefault();
        let url=backEndServerURL+"/secondaryCB";
        let object = {};
        object.appId = this.props.appId;
        object.person = this.props.tagingModalCbnumber;
        object.appData = this.state.inputData;
        axios.post(url,object,{withCredentials:true})
            .then((response)=>{
                console.log(response.data)
            })
            .catch((error)=>{
                console.log(error);
            })
        if(this.props.generateAccountNo==="1"){
            this.setState({
                accountNo:true
            })
        }
        else{
            this.props.taging(this.props.tagingModalCbnumber);
            this.props.closeModal("");
            this.setState({
                accountNo:false
            })
        }


    };

    renderImage=()=>{
        if(this.state.getDocument){
            return(
                <SecondaryCBImage customerNumber={this.props.customerNumber} closeModal={this.props.closeModal}
                             documentList={this.state.documentList}
                />
            )
        }
    }
    componentDidMount() {

        let fileUrl = backEndServerURL + "/case/files/" + this.props.appId;
        axios.get(fileUrl, {withCredentials: true})
            .then((response) => {
                let fileUrl = backEndServerURL + "/secondaryCB/" + this.props.appId+"/"+this.props.customerNumber;
                axios.get(fileUrl, {withCredentials: true})
                    .then((response) => {
                     console.log(response.data);
                        this.setState({
                            varValue: response.data,
                            inputData:response.data,
                            getData:true,
                            showValue:true

                        })
                    })
                    .catch((error) => {
                        console.log(error);
                    })

                    this.setState({

                        documentList: response.data,
                        getDocument: true
                    })


            })
            .catch((error) => {
                console.log(error);
            })
    }

renderForm=()=>{
        if(this.state.getData){
            console.log(this.state.varValue)
            return(
             CommonJsonFormComponent.renderJsonForm(this.state,fileUpload, this.updateComponent)
            )
        }
}
    render() {
        const {classes} = this.props;

        return (

            <div>

                <Card>
                    <CardHeader color="rose">
                        <h4> Input<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                    </CardHeader>
                    <CardBody>
                        <Grid container spacing={1}>

                            <Grid  item xs={9}>

                                {this.renderImage()}

                            </Grid>


                            <Grid item xs={3}>


                                <React.Fragment>
                                    <br/>
                                    <Dialog
                                        fullWidth="true"
                                        maxWidth="md"
                                        className={classes.modal}
                                        classes={{paper: classes.dialogPaper}}
                                        open={this.state.accountNo}>
                                        <DialogContent className={classes.dialogPaper}>
                                            <AccountNoGenerate taging={this.props.taging} tagingModalCbnumber={this.props.tagingModalCbnumber} closeModal={this.closeModal} />

                                        </DialogContent>
                                    </Dialog>


                                    <ThemeProvider theme={theme}>
                                        <GridList  cellHeight={800} cols={1} >
                                            <div>
                                                <Grid container spacing={1}>

                                                    {this.renderForm()}
                                                </Grid>
                                            </div>



                                        </GridList>
                                    </ThemeProvider>
                                    <div>


                                    </div>

                                </React.Fragment>


                            </Grid>


                        </Grid>
                    </CardBody>
                </Card>
            </div>

        )
    }

}

export default withStyles(styles)(SecondaryCbFormDetails);