import {
    BMjsonFormIndividualAccountOpeningSearch,
} from "../WorkflowJsonForm2";
import React, {Component} from "react";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";
import CardBody from "../../Card/CardBody";
import Grid from "@material-ui/core/Grid/index";
import withStyles from "@material-ui/core/styles/withStyles";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';

import {ThemeProvider} from "@material-ui/styles/index";
import theme from "../../JsonForm/CustomeTheme";
import FileTypeComponent from "../../JsonForm/FileTypeComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CircularProgress from "@material-ui/core/CircularProgress/index";
import {DropdownContext} from "reactstrap/es/DropdownContext";
import DropdownComponent from "../../JsonForm/DropdownComponent";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import {Dialog} from "@material-ui/core/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import AccountNoGenerate from "./AccountNoGenerate";


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});

var fileUpload =  [
    {
        "varName":"customerId",
        "type":"text",
        "label":"Customer Id",
        "grid":6,
        "length":9,


    },

    {
        "varName":"title",
        "type":"text",
        "label":"Title",
        "grid":6,


        required:true,
    },

    {
        "varName":"customerName",
        "type":"text",
        "label":"Customer Name",
        "grid":6,
        "length":80,


    },

    {
        "varName":"shortName",
        "type":"text",
        "label":"Short Name",
        "grid":6,
        "length":10,


    },

    {
        "varName":"status",
        "type":"text",
        "label":"Status",
        "grid":6,


        required:true,
    },

    {
        "varName":"statusAsOnDate",
        "type":"date",
        "label":"Status as on Date",
        "grid":6
    },

    {
        "varName":"acManager",
        "type":"text",
        "label":"AC Manager",
        "grid":6,



    },

    {
        "varName":"occupationCode",
        "type":"text",
        "label":"Occuoation Code",
        "grid":6,


        required:true,
    },

    {
        "varName":"constitution",
        "type":"text",
        "label":"Constitution",
        "grid":6,


        required:true,
    },

    {
        "varName":"gender",
        "type":"text",
        "label":"Gender",
        "grid":6,


        required:true,
    },

    {
        "varName":"staffFlag",
        "type":"text",
        "label":"Staff Flag",
        "grid":6,


        required:true,
    },

    {
        "varName":"staffNumber",
        "type":"text",
        "label":"Staff Number",
        "grid":6,


        required:true,
    },

    {
        "varName":"minor",
        "type":"text",
        "label":"Minor",
        "grid":6,



    },

    {
        "varName":"nonResident",
        "type":"text",
        "label":"Non Resident",
        "grid":6,


        required:true,
    },

    {
        "varName":"trade",
        "type":"text",
        "label":"Trade",
        "grid":6,


        required:true,
    },

    {
        "varName":"nationalIdCard",
        "type":"text",
        "label":"National ID Card",
        "grid":6,


        required:true,
    },

    {
        "varName":"dateOfBirth",
        "type":"date",
        "label":"Date of Birth",
        "grid":6,



    },

    {
        "varName":"introducerCustomerId",
        "type":"text",
        "label":"Introducer Customer Id",
        "grid":6,
        "length":9,

        required:true,
    },

    {
        "varName":"introducerName",
        "type":"text",
        "label":"Introducer Name",
        "grid":6,



    },

    {
        "varName":"introducerStaff",
        "type":"text",
        "label":"Introducer Staff",
        "grid":6,



    },

    {
        "varName":"maritialStatus",
        "type":"text",
        "label":"Maritial Status",
        "grid":6,


        required:true,
    },

    {
        "varName":"father",
        "type":"text",
        "label":"Father",
        "grid":6,


        required:true,
    },

    {
        "varName":"mother",
        "type":"text",
        "label":"Mother",
        "grid":6,


        required:true,
    },

    {
        "varName":"spouse",
        "type":"text",
        "label":"Spouse",
        "grid":6,


        required:true,
    },

    {
        "varName":"communicationAddress1",
        "type":"text",
        "label":"Communication Address1",
        "grid":6,



    },

    {
        "varName":"communicationAddress2",
        "type":"text",
        "label":"Communication Address2",
        "grid":6,



    },

    {
        "varName":"city1",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state1",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country1",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo11",
        "type":"text",
        "label":"Phone No1",
        "grid":6,
        "length":11,

        required:true,
    },

    {
        "varName":"phoneNo21",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo1",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email1",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"permanentAddress1",
        "type":"text",
        "label":"Permanent Address1",
        "grid":6,



    },

    {
        "varName":"permanentAddress2",
        "type":"text",
        "label":"Permanent Address2",
        "grid":6,



    },

    {
        "varName":"city2",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state2",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country2",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo12",
        "type":"text",
        "label":"Phone No1",
        "grid":6,



    },

    {
        "varName":"phoneNo222",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo2",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email2",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"employerAddress1",
        "type":"text",
        "label":"Employer Address1",
        "grid":6,



    },

    {
        "varName":"employerAddress2",
        "type":"text",
        "label":"Employer Address2",
        "grid":6,



    },

    {
        "varName":"city3",
        "type":"text",
        "label":"City",
        "grid":6,


        required:true,
    },

    {
        "varName":"state3",
        "type":"text",
        "label":"State",
        "grid":6,


        required:true,
    },

    {
        "varName":"postalCode3",
        "type":"text",
        "label":"Postal Code",
        "grid":6,



    },

    {
        "varName":"country",
        "type":"text",
        "label":"Country",
        "grid":6,


        required:true,
    },

    {
        "varName":"phoneNo13",
        "type":"text",
        "label":"Phone No1",
        "grid":6,



    },

    {
        "varName":"phoneNo23",
        "type":"text",
        "label":"Phone No2",
        "grid":6,



    },

    {
        "varName":"telexNo",
        "type":"text",
        "label":"Telex No",
        "grid":6,



    },

    {
        "varName":"email3",
        "type":"text",
        "label":"Email",
        "grid":6,

        email:true,

    },

    {
        "varName":"faxNo",
        "type":"text",
        "label":"Fax No",
        "grid":6,



    },

    {
        "varName":"combineStatement",
        "type":"text",
        "label":"Combine Statement",
        "grid":6,



    },

    {
        "varName":"tds",
        "type":"text",
        "label":"TDS",
        "grid":6,



    },

    {
        "varName":"pangirNo",
        "type":"text",
        "label":"PANGIR No",
        "grid":6,



    },

    {
        "varName":"passportNo",
        "type":"text",
        "label":"Passport No",
        "grid":6,



    },

    {
        "varName":"issueDate",
        "type":"date",
        "label":"Issue Date",
        "grid":6,



    },

    {
        "varName":"passportDetails",
        "type":"text",
        "label":"Passport Details",
        "grid":6,


        required:true,
    },

    {
        "varName":"expiryDate",
        "type":"date",
        "label":"Expiry Date",
        "grid":6,



    },

    {
        "varName":"purgedAllowed",
        "type":"text",
        "label":"Purged Allowed",
        "grid":6,



    },

    {
        "varName":"freeText2",
        "type":"text",
        "label":"Free Text2",
        "grid":6,



    },

    {
        "varName":"freeText5",
        "type":"text",
        "label":"Free Text 5",
        "grid":6,
        "length":10,


    },

    {
        "varName":"freeText8",
        "type":"text",
        "label":"Free Text 8",
        "grid":6,



    },

    {
        "varName":"freeText9",
        "type":"text",
        "label":"Free Text 9",
        "grid":6,



    },

    {
        "varName":"freeText13",
        "type":"text",
        "label":"Free Text13",
        "grid":6,



    },

    {
        "varName":"freeText14",
        "type":"text",
        "label":"Free Text14",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeText15",
        "type":"text",
        "label":"Free Text15",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode1",
        "type":"text",
        "label":"Free Code1",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode3",
        "type":"text",
        "label":"Free Code3",
        "grid":6,


        required:true,
    },

    {
        "varName":"freeCode7",
        "type":"text",
        "label":"Free Code 7",
        "grid":6,


        required:true,
    },
];
var selectFileName = {
    "varName": "fileName",
    "type": "dropdown",
    "required":true,
    "label": "This photo name",

    "grid": 6
}

class CheckerTagCBDetails extends Component {
    state = {
        fileUploadData: {},
        getSplitFile: [],
        multipleScanningphotoShow: null,
        inputData: {},
        dropdownSearchData: {},
        selectedDate: {},

    }

    constructor(props) {
        super(props);


    }


    updateComponent = () => {
        this.forceUpdate();
    };


    close = () => {
        this.props.closeModal();
    }





    render() {
        const {classes} = this.props;

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4>CB Details<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>
                        </CardHeader>
                        <CardBody>
                            <br/>
                            <Dialog
                                fullWidth="true"
                                maxWidth="md"
                                className={classes.modal}
                                classes={{paper: classes.dialogPaper}}
                                open={this.state.accountNo}>
                                <DialogContent className={classes.dialogPaper}>
                                    <AccountNoGenerate taging={this.props.taging} tagingModalCbnumber={this.props.tagingModalCbnumber} closeModal={this.closeModal} />

                                </DialogContent>
                            </Dialog>
                            <ThemeProvider theme={theme}>
                                <div>
                                    <Grid container spacing={3}>
                                        {CommonJsonFormComponent.renderJsonForm(this.state,fileUpload, this.updateComponent)}

                                    </Grid>
                                </div>



                            </ThemeProvider>


                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(CheckerTagCBDetails);