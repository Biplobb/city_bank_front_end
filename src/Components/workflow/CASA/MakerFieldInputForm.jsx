import {
    MAKERJsonFormMaintenanceWithPrintFileAddress,
    MAKERJsonFormMaintenanceWithPrintFileContact,
    MAKERJsonFormMaintenanceWithPrintFileEmail,
    MAKERJsonFormMaintenanceWithPrintFileNominee,
    MAKERJsonFormMaintenanceWithPrintFileTenor,
    MAKERJsonFormMaintenanceWithPrintFileScheme,
    MAKERJsonFormMaintenanceWithPrintFileMaturity,
    MAKERJsonFormMaintenanceWithPrintFileTitle,
    MAKERJsonFormMaintenanceWithPrintFileLink,
    MAKERJsonFormMaintenanceWithPrintFileEtin,
    MAKERJsonFormMaintenanceWithPrintFileDormant,
    MAKERJsonFormMaintenanceWithPrintFileInputFiled,
    MAKERJsonFormMaintenanceWithFile,

    MAKERJsonFormIndividualAccountOpening,
    MAKERUpdateAllInformation,
    MAKERsigniturePhotoUpload,

    MAKERJsonFormForDebitCard
    /*  BOMNewJsonFormJointAccountOpening,
      BOMNewJsonFormProprietorshipAccountOpening,
      MAKERJsonFormIndividualAccountOpening*/
} from "../WorkflowJsonForm";
import React, {Component} from "react";
import VerifyMakerInboxCase from "./VerifyMakerInboxCase";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios/index";
import FormSample from "../../JsonForm/FormSample";
import CardBody from "../../Card/CardBody";
import GridItem from "../../Grid/GridItem";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "../../Grid/GridContainer";
import Card from "../../Card/Card";
import Paper from "@material-ui/core/Paper/index";
import Button from "@material-ui/core/Button/index";
import Grid from "@material-ui/core/Grid/index";
const filteringJsonForm = {
    "variables": [
        {
            "varName": "customer_name",
            "type": "text",
            "label": "Customer Name",
            "number": false,
        },

        {
            "varName": "cb_number",
            "type": "text",
            "label": "CB Number",
        },
        {
            "varName": "branch_id",
            "type": "text",
            "label": "Sol Id",
        },
        {
            "varName": "service_type",
            "type": "text",
            "label": "Service Type",
        },


    ],

};

const filteringJsonForm1 = {
    "variables": [
        {
            "varName": "customer_name",
            "type": "empty",
            "label": "Customer Name",
            "number": false,
        },
        {
            "varName": "customer_name",
            "type": "empty",
            "label": "Customer Name",
            "number": false,
        },
        {
            "varName": "cb_number",
            "type": "empty",
            "label": "CB Number",
        }


    ],

};
const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: theme.spacing(1),
    }
});
class MakerFieldInputForm extends Component {


    constructor(props){
        super(props);


    }



    renderFilterForm = () => {
        return (
            <FormSample close={this.closeModal} grid="12" buttonName="Filter" onSubmit={this.getSubmitedForm}
                        jsonForm={filteringJsonForm}/>
        )
    }
    renderFilterForm1 = () => {
        return (
            <FormSample close={this.closeModal} grid="12"   onSubmit={this.getSubmitedForm}
                        jsonForm={filteringJsonForm1}/>
        )
    }


    render() {
        const {classes} = this.props;

        return (

            <GridContainer>

                <GridItem xs={12} sm={12} md={12}>
                    <Card>

                        <CardBody>
                            <br/>
                            <br/>

                            <div className={classes.root}>
                                <Grid container spacing={5}>
                                    <Grid container item xs={12} spacing={5}>

                                        <Grid item xs={4}>

                                                {this.renderFilterForm()}



                                        </Grid>

                                        <Grid item xs={8}>

                                                {this.renderFilterForm1()}



                                        </Grid>




                                    </Grid>
                                </Grid>
                            </div>


                        </CardBody>
                    </Card>
                </GridItem>


            </GridContainer>
        )
    }

}

export default withStyles(styles) (MakerFieldInputForm);