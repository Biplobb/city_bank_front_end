import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Grow from "@material-ui/core/Grow";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {ThemeProvider} from "@material-ui/styles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import LiabilityUploadModal from "../CASA/LiabilityUploadModal";
import SingleImageShow from "../CASA/SingleImageShow";
import AccountNoGenerate from "../AccountNoGenerate";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CircularProgress from '@material-ui/core/CircularProgress';
import Notification from "../../NotificationMessage/Notification";
import MyValidation from "../../../MyValidation";
import CSImageLinkEdit from "../CASA/CSImageLinkEdit";
import Link from "@material-ui/core/Link";



let csRemarks = [
    {
        "varName": "csRemarks",
        "type": "textArea",
        "label": "CS Remarks",
        "grid": 12
    }];

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "16px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "600",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};

function Transition(props) {
    return <Grow in={true} timeout="auto" {...props} />;
}

var deferalOther =
    {
        "varName": "deferalOther",
        "type": "text",
        "label": "Please Specify",

    };

var deferal =
    {
        "varName": "deferalType",
        "type": "select",
        "label": "Deferal Type",
        "enum": [
            "Applicant Photograph",
            "Nominee Photograph",
            "Passport",
            "Address proof",
            "Transaction profile",
            "other"
        ],

    };

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",

};

class AccountEdit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            SelectedData: false,
            csDeferalPage: "",
            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            varValue: [],
            caseId: "",
            title: "",
            notificationMessage: "",
            app_uid: "-1",
            alert: false,
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "NO",
                accountType: "INSTAPACK",
                priority: "GENERAL"
            },
            fileUploadData: {},
            selectedDate: {},
            dropdownSearchData: {},
            AddDeferal: false,
            debitCard: "",
            showValue: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            getMappingAllImage: false,
            accountDetailsModal: false,
            loaderNeeded: null,
            IndividualDedupModal: false,
            JointDedupModal: false,
            individualDataSaveId: '',
            jointDataSaveId: '',
            companyDataSaveId: '',
            numberOfCustomer: 0,
            err: false,
            errorArray: {},
            errorMessages: {},
            getRemarks: []


        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event, value) => {

        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));

            this.setState({values: values, deferalNeeded: true});

        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }


    addDeferalForm() {


        return this.state.values.map((el, i) =>
            <React.Fragment>
                <Grid item xs={3.5}>
                    {
                        this.dynamicDeferral(el)
                    }
                </Grid>
                <Grid item xs={3.5}>
                    {
                        this.dynamicDate(el)
                    }
                </Grid>
                <Grid item xs={3.5}>
                    {this.dynamicDeferralOther(el)}
                </Grid>
                <Grid item xs={1.5}>
                    <button
                        style={{float: "right"}}
                        className="btn btn-outline-danger"
                        type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                    >
                        Remove
                    </button>
                </Grid>
            </React.Fragment>
        )

    }

    getSearchvalue = (jsonObject) => {
        var clone = JSON.parse(JSON.stringify(jsonObject))
        for (var prop in clone)
            if (clone[prop] === '' || clone[prop] === 'null' || clone[prop] === 'undefined')
                delete clone[prop];
        return clone;
    }
    DedupDataSaveApi = (subServiceType) => {
        console.log(this.props.individualDedupData)
        console.log(this.props.companyDedupData)
        console.log(this.props.jointDedupData)
        if (subServiceType === "INDIVIDUAL" || subServiceType === "Individual A/C") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "individualDedupData": this.props.individualDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Joint Account") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }else if (subServiceType === "New FDR Opening") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }else if (subServiceType === "Tag FDR Opening") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Proprietorship A/C" || subServiceType === "NONINDIVIDUAL") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "individualDedupData": this.props.individualDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {
                        "companyDedupData": this.props.companyDedupData,
                        "dedupType": subServiceType
                    }, {withCredentials: true})
                        .then((response) => {
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        individualDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else if (subServiceType === "Company Account") {
            let Dedupurl = backEndServerURL + "/dedup/save";
            axios.post(Dedupurl, {
                "jointDedupData": this.props.jointDedupData,
                "dedupType": subServiceType
            }, {withCredentials: true})
                .then((response) => {
                    axios.post(Dedupurl, {
                        "companyDedupData": this.props.companyDedupData,
                        "dedupType": subServiceType
                    }, {withCredentials: true})
                        .then((response) => {
                            this.setState({
                                companyDataSaveId: response.data
                            })
                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    console.log(" dedup save data");
                    console.log(response.data);
                    this.setState({
                        jointDataSaveId: response.data
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        } else {
            alert("please type select")
        }
    }
    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }

    componentDidMount() {
        this.setState({
            loaderNeeded: false
        })
      var remarksArray = [];
      var inputData={};
        let url = backEndServerURL + "/fdr/info/CB1179323/" + this.props.id;
        axios.get(url, {withCredentials: true}).then((response) => {
            console.log(response.data);
            let varValue = {};
            varValue["communicationAddress"] = (response.data.address === null ||response.data.address === 'null' ) ? '' : response.data.address;
            varValue["occupationCode"] = (response.data.occupation === null ||response.data.occupation === 'null' ) ? '' : response.data.occupation;
            varValue["phoneNumber"] = (response.data.phone === null ||response.data.phone === 'null' ) ? '' : response.data.phone;
            varValue["customerName"] = (response.data.name === null ||response.data.name === 'null' ) ? '' : response.data.name ;
            varValue["etinNo"] = (response.data.tinNumber === null ||response.data.tinNumber === 'null' ) ? '' : response.data.tinNumber ;
            varValue["subSectorCode"] = (response.data.subSector === null ||response.data.subSector === 'null' ) ? '' : response.data.subSector;
            varValue["sectorCode"] = (response.data.sector === null ||response.data.sector === 'null' ) ? '' : response.data.sector;
            varValue["availableBalance"] = (response.data.availableBalance === null ||response.data.availableBalance === 'null' ) ? '' : response.data.availableBalance;
            varValue["csDeferal"] = "NO";
            varValue["accountSource"] = "NO";
            varValue["accountType"] = "NO";
            varValue["priority"] = "NO";
            varValue["doubleTennor"] = "Monthly";
            varValue["monthTennor"] ="Monthly";
            varValue["doubleSiMaturity"] = "No";
            varValue["monthSiMaturity"] = "Yes";
            varValue["monthMaturityYes"] = "Renew Principal Only and Credit Interest to the Account No";
            varValue["tennor"] = "Day";
            varValue["customerId"] ="CB5984143"
            varValue["cbNumber"] ="CB5984143"
            varValue["smsAlertRequest"] = true;
            this.setState({
                varValue: varValue,
                inputData :this.getSearchvalue(varValue),
                getAccountData: true,
                showValue : true
            })

            this.state.inputData["csDeferal"] = "NO";
            if (this.props.appId !== undefined) {
                let url = backEndServerURL + '/variables/' + this.props.appId;
                axios.get(url, {withCredentials: true}).then((response) => {
                        console.log(response.data)
                        let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                        axios.get(deferalListUrl, {withCredentials: true}).then((response) => {
                                let tableArray = [];
                                var status = "";
                                response.data.map((deferal) => {
                                    if (deferal.status === "ACTIVE") {
                                        status = "Approved"
                                    }
                                    tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, status));

                                });
                                this.setState({
                                    getDeferalList: tableArray
                                })
                                let getCommentsUrl = backEndServerURL + "/appRemarkGet/"+this.props.appId;
                                axios.get(getCommentsUrl, {withCredentials: true}).then((response) => {
                                    console.log(response.data);
                                    response.data.map((data) => {
                                        remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                                    })
                                    this.setState({
                                        getRemarks: remarksArray
                                    })
                                })
                                .catch((error) => {
                                    console.log(error)
                                })
                           })

                            .catch((error) => {
                                console.log(error);
                            })

                        this.state.inputData["csDeferal"] = "YES";
                        let varValue = response.data;
                        this.setState({
                            getData: true,
                            varValue: varValue,
                            appData: response.data,
                            //inputData: response.data,
                            showValue: true,
                            appId: this.props.appId,
                            loaderNeeded: true
                        });

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 652) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            } else {
                let url = backEndServerURL + "/startCase/cs_data_capture";
                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        {
                            this.DedupDataSaveApi(this.props.subServiceType)
                        }
                        console.log(response.data)
                        this.setState({
                            appId: response.data.id,
                            appData: response.data.inputData,
                            getNewCase: true,
                            //inputData:JSON.parse(JSON.stringify(this.props.searchValue)),
                            //inputData: inputData,
                            varValue: this.props.searchValue,
                            showValue: true,
                            getData: true,
                            loaderNeeded: true
                        });
                        console.log('a');
                        console.log(this.state.inputData);
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            }
        })
        .catch((error) => {
            console.log(error);
            if(error.response.status===452){
                Functions.removeCookie();
                this.setState({
                    redirectLogin:true
                })

            }
        })


    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {
        if (this.state.getDeferalList.length > 0) {
            return (
                <div>
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Expire Date", "Raise By", "Raise Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />

                    <br/>


                </div>

            )
        }

    };
    renderRemarksData = () => {
        if (this.state.getRemarks.length > 0) {
            return (
                <div>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Comments", "Comments By"]}
                        tableData={this.state.getRemarks}
                        tableAllign={['left', 'left']}
                    />
                    <br/>
                </div>
            )
        }

    }
    renderAddButtonShow = () => {

        return (
            <button
                className="btn btn-outline-danger"

                style={{
                    width:150,
                    float: 'right',
                    verticalAlign: 'right',

                }}

                type='button' value='add more'
                onClick={this.addClick.bind(this)}


            >Add Defferal</button>
        )

    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };

    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    updateComponent = () => {
        this.forceUpdate();
    };
    handleChangeComments = () => {
        alert("Ok")
    }
    returnJsonForm = () => {
        if (this.state.showValue) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.commonJsonForm, this.updateComponent)
            )


        }
    };
    returnServiceForm = () => {
        if (this.state.showValue) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, this.props.serviceJsonForm, this.updateComponent)
            )


        }
    };


    /* returnAccountTypeField = () => {
         if (this.state.showValue) {
             return (
                 <Grid item xs={3}>
                     <FormControl>

                         <InputLabel>Account Type</InputLabel>
                         <Select style={{}}
                             //defaultValue={this.returnDefaultValue(field)}

                                 value={this.state.inputData.accountType}

                                 name="accountType"
                                 onChange={(event) => {
                                     this.state.inputData.accountType = event.target.value;
                                     this.updateComponent()
                                 }}>


                             <MenuItem key="INSTAPACK" value="INSTAPACK">Insta Pack</MenuItem>)
                             <MenuItem key="NON-INSTAPACK" value="NON-INSTAPACK">Non-Insta Pack</MenuItem>)


                         </Select>

                     </FormControl>

                 </Grid>
             )
         }
     };*/

    handleSubmit = (event) => {
        event.preventDefault();

        console.log('a')
        console.log(this.state.inputData)
       // return false;
        //let error = MyValidation.defaultValidation(this.props.commonJsonForm, this.state)
        //this.forceUpdate();
        // console.log(this.state.inputData);
        // return false;
        console.log("Not working");
       // this.state.inputData;

        if (this.state.inputData["csDeferal"] === "YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.csRemarks + "/" + this.state.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.csRemarks =undefined;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "FDR Opening";
        //data.individualDedupData = this.state.individualDataSaveId;
        data.jointDedupData = this.state.jointDataSaveId;
        data.subServiceType = "Tag FDR Opening";
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;
        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;
        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                if (this.state.inputData.accountType === "NON-INSTAPACK" && this.state.inputData["cs_deferal"]==="NO" ) {
                    this.setState({
                        accountDetailsModal: true
                    })
                }
                if (this.state.inputData.accountType !== "NON-INSTAPACK" ||(this.state.inputData.accountType === "NON-INSTAPACK" && this.state.inputData["cs_deferal"]=="YES") ) {
                    var url = backEndServerURL + "/case/route/" + this.state.appId;

                    axios.get(url, {withCredentials: true})
                        .then((response) => {
                            console.log(response.data);
                            console.log("Successfully Routed!");

                            {
                                this.close();
                            }
                            this.setState({
                                title: "Successfull!",
                                notificationMessage: "Successfully Routed!",
                                alert: true

                            })
                            //




                        })
                        .catch((error) => {
                            console.log(error);

                        });
                }
            })
            .catch((error) => {
                console.log(error)
            });


    };
    handleSubmitDraft = (event) => {
        event.preventDefault();
        //let error = MyValidation.defaultValidation(this.props.commonJsonForm, this.state)
        //this.forceUpdate();
        console.log(this.state.inputData)
        // if(error===true){
        //     return 0;
        // }

        if (this.state.inputData["csDeferal"] === "YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);

                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var variableSetUrl = backEndServerURL + "/save/" + this.state.appId;

        let data = this.state.inputData;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "FDR Opening";
        data.individualDedupData = this.state.individualDataSaveId;
        data.jointDedupData = this.state.jointDataSaveId;
        data.companyDedupData = this.state.companyDataSaveId;
        data.jointAccountCustomerNumber = this.props.jointAccountCustomerNumber;
        data.subServiceType = this.props.subServiceType;
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.priority === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                if (this.state.inputData.accountType === "NON-INSTAPACK") {
                    this.setState({
                        accountDetailsModal: true
                    })
                }

                this.setState({
                    title: "Successfull!",
                    notificationMessage: "Successfully Draft!",
                    alert: true

                })
                this.close()
                window.location.reload();


            })
            .catch((error) => {
                console.log(error)
            });

    }
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }


    };


    stopNotification = () => {
        this.setState({
            alert: false
        })
    }

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);
        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],
        }))
        this.state.inputData["csDeferal"] = "YES";
    }

    renderRemarks = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, csRemarks, this.updateComponent)
            )
        }
        return;
    }

    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
            if (this.state.values.length > 0) {
                this.state.inputData["csDeferal"] = "YES"
            } else {
                this.state.inputData["csDeferal"] = "NO"
            }
        }
    }

    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

    renderUploadButton = () => {
        if (!this.state.deferalNeeded) {
            return (
                <button
                    style={{
                        width:150,
                    }}
                    className="btn btn-outline-danger"
                    onClick={this.uploadModal}
                >
                    Upload File
                </button>
            )
        }
    }

    closeModal = () => {
        this.setState({
            getMappingAllImage: false,
            IndividualDedupModal: false,
            JointDedupModal: false,

        })
    }
    closeUploadModal = (data) => {
        this.setState({


            uploadModal: false
        })
    }
    accountDetailsModal = () => {
        this.setState({
            accountDetailsModal: false
        })
        var url = backEndServerURL + "/case/route/" + this.state.appId;

        axios.get(url, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
                console.log("Successfully Routed!");
                this.setState({
                    title: "Successfull!",
                    notificationMessage: "Successfully Routed!",
                    alert: true

                })
                {
                    this.close()
                }

                window.location.reload();


            })
            .catch((error) => {
                console.log(error);

            });

    }

    IndividualDedupModal = () => {
        this.setState({
            IndividualDedupModal: true
        })
    }
    JointDedupModal = () => {
        this.setState({
            JointDedupModal: true
        })
    }
    mappingAllImage = (event) => {
        event.preventDefault();
        this.setState({
            getMappingAllImage: true
        })
    }

    render() {

        const {classes} = this.props;
        return (
            <Card>
                <CardHeader color="rose">
                    <h4>
                        FDR
                        Account Opening<a><CloseIcon onClick={this.close} style={{
                        position: 'absolute',
                        right: 10,
                        color: "#000000"
                    }}/></a></h4>
                </CardHeader>
                <CardBody>
                    <div>
                        <ThemeProvider theme={theme}>
                            <Grid container spacing={1}>
                                {this.renderNotification()}
                                {this.returnJsonForm()}
                                {this.returnServiceForm()}
                                <Grid item xs='12'>
                                </Grid>
                                {this.renderAddButtonShow()}
                                <Grid item xs='12'>
                                </Grid>
                                {
                                    this.addDeferalForm()
                                }
                                <Grid item xs='12'>
                                </Grid>
                                <Grid item xs='12'>
                                    {this.renderDefferalData()}
                                    {this.renderRemarksData()}
                                </Grid>
                                <Grid item xs='12'>
                                </Grid>
                                <Grid item xs={2}>
                                    {this.renderUploadButton()}
                                </Grid>
                                <Grid item xs={2}>
                                    <Link
                                        style={{
                                            width:150,
                                        }}
                                        component="button"
                                        variant="body2"
                                        onClick={this.mappingAllImage}
                                    >
                                        Assigned Image
                                    </Link>
                                </Grid>
                                {this.renderRemarks()}
                                <Grid item xs='12'>
                                </Grid>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="xl"
                                    open={this.state.getMappingAllImage}>
                                    <DialogContent>
                                        <CSImageLinkEdit appId={this.state.appId} closeModal={this.closeModal}/>
                                    </DialogContent>
                                </Dialog>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="xl"
                                    open={this.state.uploadModal}>
                                    <DialogContent>
                                        <LiabilityUploadModal subServiceType="INDIVIDUAL"
                                                              appId={this.state.appId}
                                                              closeModal={this.closeUploadModal}/>
                                    </DialogContent>
                                </Dialog>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="md"
                                    open={this.state.accountDetailsModal}>
                                    <DialogContent>

                                        <AccountNoGenerate closeModal={this.accountDetailsModal}/>
                                    </DialogContent>
                                </Dialog>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="xl"
                                    open={this.state.imageModalBoolean}>
                                    <DialogContent>
                                        <SingleImageShow data={this.state.selectImage}
                                                         closeModal={this.closeModal}/>
                                    </DialogContent>
                                </Dialog>
                                <Grid item xs='12'>
                                </Grid>
                                <center>
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{

                                        }}
                                        onClick={this.handleSubmit}

                                    >
                                        Submit
                                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button
                                        className="btn btn-outline-info"

                                        onClick={this.handleSubmitDraft}
                                    >
                                        Draft
                                    </button>&nbsp;&nbsp;&nbsp;
                                </center>
                            </Grid>
                        </ThemeProvider>
                    </div>
                </CardBody>
            </Card>
        );
    }

}

export default withStyles(styles)(AccountEdit);
