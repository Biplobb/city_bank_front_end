import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Grow from "@material-ui/core/Grow";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {ThemeProvider} from "@material-ui/styles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import OpeningUploadModal from "./OpeningUploadModal";
import SingleImageShow from "../CASA/SingleImageShow";
import FormSample from '../../JsonForm/CommonJsonFormComponent';

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
};
const jsonForm = [
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid" : 6

    }, {
        "varName": "availableBalance",
        "type": "text",
        "label": "Available Balance",
        "required": true,
        "grid" : 6
    },{
        "varName": "etinNo",
        "type": "text",
        "label": "E-Tin",
        "required": true,
        "grid" : 6

    },{
        "varName": "communicationAddress",
        "type": "text",
        "label": "Communication Address",
        "required": true,
        "grid" : 6
    },{
        "varName": "phoneNumber",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid" : 6
    },{
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "required": true,

    },{
        "varName": "sectorCode",
        "type": "text",
        "label": "Sector Code",
        "required": true,
        "grid" : 6
    },{
        "varName": "subSectorCode",
        "type": "text",
        "label": "Sub Sector Code",
        "required": true,
        "grid" : 6
    },{
        "varName": "casaLineStatus",
        "type": "select",
        "label": "CASA Line Status",
        "required": true,
        "enum": [
            "ACTIVE",
            "INACTIVE"
        ],
        "grid" : 6
    },{
        "varName": "accountDormancy",
        "type": "text",
        "label": "Account Dormancy",
        "required": true,
        "grid" : 6
    },{
        "varName": "proxyTransection",
        "type": "text",
        "label": "Proxy Transection",
        "required": true,
        "grid" : 6
    },{
        "varName": "freezeInfo",
        "type": "text",
        "label": "Freeze Information",
        "required": true,
        "grid" : 6
    },{
        "varName": "proxyTransection",
        "type": "text",
        "label": "Proxy Transection",
        "required": true,
        "grid" : 6
    },
];
var LoanCheckbox = [
    {
        "varName": "loan",
        "type": "checkbox",
        "label": "Loan",
        "grid": 6
    },

]


var loanTextField = {
    "varName": "loanText",
    "type": "text",
    "label": "Loan",
    "grid": 6
};
var creditCardBox = [
    {
        "varName": "creditCard",
        "type": "checkbox",
        "label": "Credit Card",
        "grid": 6
    },

]


var creditCardTextField = {
    "varName": "creditCardText",
    "type": "text",
    "label": "Credit Card",
    "grid": 6
};

var letterOfCreditFieldCheckbox = [{
    "varName": "letterOfCredit",
    "type": "checkbox",
    "label": "Letter Of Credit",
    "grid": 6,

}];
var deferalOther =
    {
        "varName": "deferalOther",
        "type": "text",
        "label": "Please Specify",
        "grid": 6
    };

var deferal = [
    {
        "varName": "",
        "type": "title",
        "label": "",
        "grid": 12
    },{
        "varName": "deferalType",
        "type": "select",
        "label": "Deferal Type",
        "enum": [
            "Applicant Photograph",
            "Nominee Photograph",
            "Passport",
            "Address proof",
            "Transaction profile",
            "other"
        ],
        "grid": 6
    }]

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};
var letterOfCreditTextField = {

    "varName": "letterOfCreditText",
    "type": "text",
    "label": "Letter Of Credit",
    "grid": 6

}

class AccountEditBom extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appId: '',
            varValue: [],
            appData: {},
            inputData : {},
            getNewCase: false,
            showValue: false,
            getAccountData: false,
            title:"",
            notificationMessage:"",
            alert:false,
            redirectLogin:false,
            SelectedData: false,
            csDeferalPage: "",
            AddDeferal: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage:"",
            imageModalBoolean:false,
            imgeListLinkSHow:false
        }
    }
    getSubmitedForm = (object) => {
        alert('in');
    }

    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

    closeUploadModal=(data)=>{
        this.setState({
            uploadModal: false,
            imgeListLinkSHow:true,
            getImageLink:data
        })
    }

    renderImageLink = () => {

        if (this.state.imgeListLinkSHow) {
            return(
                this.state.getImageLink.map((data)=>{
                    return(
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }

    componentDidMount() {
        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;


            axios.get(url, {withCredentials:true})
                .then((response) => {
                    let deferalListUrl=backEndServerURL + "/case/deferral/"+this.props.appId;
                    axios.get(deferalListUrl,{withCredentials:true})
                        .then((response)=>{
                            let imageUrl=backEndServerURL+"/case/files/"+this.props.appId;
                            axios.get(imageUrl,{withCredentials:true})
                                .then((response)=>{
                                    console.log(response.data);
                                    this.setState({
                                        getImageLink:response.data,
                                        getImageBoolean:true
                                    })
                                })
                                .catch((error)=>{
                                    console.log(error);
                                })
                            console.log(response.data);
                            let tableArray=[];
                            response.data.map((deferal) => {
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate,deferal.appliedBy,deferal.applicationDate,deferal.status));

                            });
                            this.setState({
                                getDeferalList:tableArray
                            })

                        })
                        .catch((error)=>{
                            console.log(error);
                        })

                    this.setState({
                        getData: true,
                        showValue: true,
                        inputData:response.data,
                        varValue: response.data,
                        appData: response.data
                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }
    returnLoanField = () => {
        if (this.state.inputData.loan === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, loanTextField)

    }
    returnCreditCardField = () => {
        if (this.state.inputData.creditCard === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, creditCardTextField)

    }
    returnLetterOfCreditField = () => {

        if (this.state.inputData.letterOfCredit === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, letterOfCreditTextField)
    };
    renderUploadButton=()=>{
        if(!this.state.deferalNeeded){
            return(
                <button className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'middle',
                        }}
                        onClick={this.uploadModal}
                >
                    Upload File
                </button>
            )
        }
    }

    renderUploadButton=()=>{
        if(!this.state.deferalNeeded){
            return(
                <button className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'middle',
                        }}
                        onClick={this.uploadModal}
                >
                    Upload File
                </button>
            )
        }
    }
    renderDefferalData = () => {
        if (this.state.getDeferalList.length > 0) {
            return (
                <div>
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />
                    <br/>
                </div>

            )
        }

    }

    updateComponent = () =>{
        this.forceUpdate();
    }
    closeModal=()=>{
        this.setState({
            imageModalBoolean:false
        })
    }

    handleSubmit = (bom_approval) => {
        this.state.inputData.bom_approval = bom_approval;
        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;
                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        this.setState({
                            title:"Successfull!",
                            notificationMessage:"Successfully Routed!",
                            alert:true
                        })
                        this.props.closeModal()
                    })
                    .catch((error) => {
                        console.log(error);
                        /* if(error.response.status===452){
                             Functions.removeCookie();
                             this.setState({
                                 redirectLogin:true
                             })
                         }*/
                    });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    close=()=>{
        this.props.closeModal();
    }
    renderServiceTagging() {
        return (
            <React.Fragment>
                <br/>
                <br/>
                {
                    CommonJsonFormComponent.renderJsonForm(this.state, LoanCheckbox, this.updateComponent)
                }
                {
                    this.returnLoanField()
                }
                <Grid item xs={12}></Grid>
                {
                    CommonJsonFormComponent.renderJsonForm(this.state, creditCardBox, this.updateComponent)
                }
                {
                    this.returnCreditCardField()
                }
                <Grid item xs={12}></Grid>
                {
                    CommonJsonFormComponent.renderJsonForm(this.state, letterOfCreditFieldCheckbox, this.updateComponent)
                }
                {
                    this.returnLetterOfCreditField()
                }

                <br/>
                <br/>
            </React.Fragment>

        );
    }
    renderEditForm = (() => {
        console.log(this.state.varValue);
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, jsonForm, this.updateComponent)
            )
        }
    })

    handleChange = (event, value) => {
        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));
            this.setState({values: values, deferalNeeded: true});
        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }


    submitApprove = (event) =>{
        event.preventDefault();
        let bom_approval= 'APPROVED';
        this.handleSubmit(bom_approval);
    }
    submitReturn = (event) =>{
        event.preventDefault();
        let bom_approval= 'RETURN';
        this.handleSubmit(bom_approval);
    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };

    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };

    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    renderSelectMenu = () => {

        if (this.props.appId !== undefined) {
            return (
                <Grid item xs='12'>
                    <TextField
                        value="YES"
                        label="Need Deferal?"
                        InputProps={{
                            readOnly: true
                        }}
                    />
                </Grid>
            )
        } else {
            return (
                <Grid item xs='12'>
                    <FormLabel component="legend">Need Deferal?</FormLabel>
                    <RadioGroup aria-label="csDeferal" name="csDeferal" value={this.state.inputData["csDeferal"]}
                                onChange={this.handleChange}>
                        <FormControlLabel value="YES" control={<Radio/>} label="YES"/>
                        <FormControlLabel value="NO" control={<Radio/>} label="NO"/>
                    </RadioGroup>
                </Grid>
            )
        }
    }





    render() {
        const {classes} = this.props;
        // {
        //
        //     Functions.redirectToLogin(this.state)
        //
        // }

        return (

            <GridContainer>
                {/*{this.renderNotification()}*/}
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>{this.props.name}</h4>
                        </CardHeader>
                        <CardBody>
                            <div>
                                <Grid container spacing={3}>
                                    <ThemeProvider theme={theme}>

                                        {this.renderEditForm()}
                                        <br/>
                                        <Grid item xs='12'>
                                            {this.renderAddButtonShow()}
                                        </Grid>
                                        <br/>
                                        {this.addDeferalForm()}
                                        <br/>

                                        <Grid item xs='12'></Grid>
                                        {this.renderServiceTagging()}
                                        <br/>
                                        <Grid item xs='12'></Grid>
                                        {this.renderSelectMenu()}
                                        <br/>
                                        <Grid item xs='12'>
                                            {this.renderDefferalData()}
                                        </Grid>
                                    </ThemeProvider>
                                </Grid>

                                <ThemeProvider theme={theme}>
                                    <Grid container spacing={1}>
                                        {this.renderImageLink()}
                                    </Grid>
                                </ThemeProvider>
                                <br/>
                                {this.renderUploadButton()}
                                <br/>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="xl"
                                    open={this.state.uploadModal}>
                                    <DialogContent>
                                        <OpeningUploadModal appId={this.state.appId} closeModal={this.closeUploadModal}/>
                                    </DialogContent>
                                </Dialog>
                                <Dialog
                                    fullWidth="true"
                                    maxWidth="xl"
                                    open={this.state.imageModalBoolean}>
                                    <DialogContent>

                                        <SingleImageShow data={this.state.selectImage}  closeModal={this.closeModal}/>
                                    </DialogContent>
                                </Dialog>
                                <br/>
                                <br/>
                                {this.renderSubmitButton()}
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }
}

export default withStyles(styles)(AccountEditBom);