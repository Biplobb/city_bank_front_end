import React,{Component} from 'react';
import axios from 'axios';
import {Dialog} from "@material-ui/core";
import {Grow} from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import DialogContent from "@material-ui/core/DialogContent";
import FormSample from '../../JsonForm/FormSample';
import {TextField} from "@material-ui/core";
import TextFieldComponent from '../../JsonForm/TextFieldComponent';
import SelectComponent from "../../JsonForm/SelectComponent";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import Table from "../../Table/Table";
import Functions from '../../../Common/Functions';
import {backEndServerURL} from "../../../Common/Constant";

import GridContainer from "../../Grid/GridContainer.jsx";
import GridItem from "../../Grid/GridItem.jsx";
import AccountEdit from './AccountEdit';
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import Button from "@material-ui/core/Button";
import 'antd/dist/antd.css';
import BranchAddEditDelete from "../../MasterModule/BranchAddEditDelete";
import CloseIcon from '@material-ui/icons/Close';
import FinacleService from "../FinacleService";
import DedupLiabilityService from "../DedupLiabilityService";
import OpeningCS from "./OpeningCS";
import {CSJsonFormForCasaIndividualFdr} from "../WorkflowJsonFormArin";


const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    dialogPaper: {
        // overflow: "visible"
    }
};
function f(state, fn) {
    console.log(state.appData);
    state.err = true;
    fn();
}
function Transition(props) {

    return <Grow in={true} timeout="auto" {...props} />;
}
class AccountList extends Component{
    state = {
        appData : "",
        err : false,
        inputData : {}
    };
    constructor(props) {
        super(props);
        this.state = {
            addRole: false,
            roleView: false,
            addUser: false,
            userView: false,
            getsearchValue: [],
            newAcoountOpeningModal: false,
            tableData: [[" ", " "]],
            roleId: '',
            assignMenuToRole: '',
            viewMenu: '',
            notificationFlag: false,
            alert:false,
            redirectLogin:false,
        };
    }

    handleSubmitJointAccountForm = () => {

        if (this.state.handleChangeJoint > 1) {
            let objectForJoinAccount = {"variables": []};
            var sl;
            for (var i = 0; i < this.state.handleChangeJoint; i++) {
                sl = i + 1;
                objectForJoinAccount.variables.push(
                    {
                        "varName": "Customer " + i,
                        "type": "title",
                        "label": "Customer: " + sl

                    },
                    {
                        "varName": "cbNumber" + i,
                        "type": "text",
                        "label": "CB Number",

                    },
                    {
                        "varName": "nid" + i,
                        "type": "text",
                        "label": "NID +",


                    },
                    {
                        "varName": "passport" + i,
                        "type": "text",
                        "label": "Passport +",


                    },
                    {
                        "varName": "customerName" + i,
                        "type": "text",
                        "label": "Customer Name",
                        "required":true

                    },
                    {
                        "varName": "dob" + i,
                        "type": "date",
                        "dates":true,
                        "required":true


                    },
                    {
                        "varName": "email" + i,
                        "type": "text",
                        "label": "Email",

                        "email": true,


                    },

                    {
                        "varName": "phone" + i,
                        "type": "text",
                        "label": "Phone Number",
                        "required":true


                    },

                    {
                        "varName": "tin" + i,
                        "type": "text",
                        "label": "eTin",


                    },


                    {
                        "varName": "registrationNo" + i,
                        "type": "text",
                        "label": "Birth Certificate/Driving License +",


                    },
                    {
                        "varName": "nationality" + i,
                        "type": "text",
                        "label": "Nationality",
                        "required":true

                    },
                )

            }
            this.setState({
                objectForJoinAccount: objectForJoinAccount,
                content: "JointForAccountOpening"

            })
            this.renderSearchForm();
            return;
        }
    }
    editAccount =(account)=> {
        this.setState({
            editAccount: true,
            accountNo: account
        })
    }
    accountEditModal = () => {

        return (
            <AccountEdit
                       data={this.state.tableData}
                       getAccountType={this.props.getAccountType}
                       id={this.state.accountNo}
                       searchValue={this.props.searchValue}
                       closeModal={this.closeModal}
                       serviceType='FDR Opening'
                       subServiceType='Tag FDR Opening'
                       commonJsonForm={ this.props.commonJsonForm}
                       serviceJsonForm={ this.props.serviceJsonForm}
                       secondButtonName="close"
                       name="Edit Account"/>
        )
    }

    getSubmitedForm = (object) => {
        console.log(object);

    }

    handleSubmit = (event) => {
        //console.log(this.state.appData);
        alert(this.state.inputData.appData)
    };
    closeModal = () =>{
        this.setState({
            editAccount: false,
            newAcoountOpeningModal: false,
        });
        this.props.closeModal();
    }

    close=()=>{
        this.props.closeModal();
    }

    createTableData = (cb, productCode, productName,accountNo) => {
        return ([cb, productCode, productName,

            <button
                className="btn btn-outline-danger"
                style={{
                    verticalAlign: 'middle',
                }}
                onClick={() => this.editAccount(accountNo)}>
                Tag</button>
        ]);
    }
    renderNewAccountOpeingForm = () => {
        let dateString;
        let postData = {};
        dateString = "";
        postData = {
            "dob": dateString
        };
        this.setState({
            newAcoountOpeningModal: true,
            getsearchValue: postData,
        })
    };
    componentDidMount() {

        //let url = backEndServerURL + "/fdr/accounts/" + this.props.customerId;
        let url = backEndServerURL + "/fdr/accounts/CB1179323";
        const tableData = [];
        axios.get(url, {withCredentials: true})
            .then((response) => {
                this.setState({roles: response.data});
                response.data.map((accounts) => {
                    console.log(response.data);
                    tableData.push(this.createTableData(accounts.cb, accounts.productCode, accounts.productName,accounts.accountNo));
                });
                this.setState({
                    tableData: tableData
                });

            })
            .catch((error) => {
                console.log(error);
                if(error.response.status===452){
                    Functions.removeCookie();
                    this.setState({
                        redirectLogin:true
                    })

                }
            });
    }

    updateComponent = () =>{
        this.forceUpdate();
    };

    handleChange = (event) =>{
        this.setState({appData : event.target.value});
    }

    render() {
        const {classes} = this.props;
        return (
            <section>
                <Dialog
                    fullWidth="true"
                    maxWidth="xl"
                    fullScreen={true}
                    open={this.state.editAccount}
                    TransitionComponent={Transition}
                    autoScrollBodyContent={true}
                >
                    <DialogContent className={classes.dialogPaper}>
                        {this.accountEditModal()}
                    </DialogContent>

                </Dialog>

                <Dialog
                    fullWidth="true"
                    maxWidth="xl"
                    className={classes.modal}
                    classes={{paper: classes.dialogPaper}}
                    open={this.state.newAcoountOpeningModal}>
                    <DialogContent className={classes.dialogPaper}>
                        <OpeningCS
                            closeModal={this.closeModal}
                            getAccountType={this.state.getAccountType}
                            accountType={this.state.tabMenuSelect}
                            serviceType="Fdr Opening"
                            subServiceType="New Fdr Opening"
                            searchValue={this.state.getsearchValue}
                        />
                    </DialogContent>
                </Dialog>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 ><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>Account List </h4>
                            </CardHeader>

                            <CardBody>
                                <br/>
                                <div>
                                    <button
                                        className="btn btn-outline-danger"
                                        style={{
                                            verticalAlign: 'right',
                                            position: "absolute",
                                            right: 10,

                                        }}
                                        onClick={() => this.renderNewAccountOpeingForm()}>
                                        New Account Creation
                                    </button>
                                </div>
                                <br/>
                                <Table
                                    tableHovor="yes"
                                    tableHeaderColor="primary"
                                    tableHead={["CB", "Product Name", "Product Code",""]}

                                    tableData={this.state.tableData}
                                    tableAllign={['left','left','left','left']}
                                /> <br/><br/><br/>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            </section>
        )
    }
}
export default  withStyles(styles) (AccountList);