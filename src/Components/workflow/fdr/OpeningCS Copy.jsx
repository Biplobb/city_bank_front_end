import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../../Grid/GridItem.jsx";
import GridContainer from "../../Grid/GridContainer.jsx";
import Card from "../../Card/Card.jsx";
import CardHeader from "../../Card/CardHeader.jsx";
import CardBody from "../../Card/CardBody.jsx";
import "../../../Static/css/RelationShipView.css";
import Grid from "@material-ui/core/Grid";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Grow from "@material-ui/core/Grow";
import Functions from '../../../Common/Functions';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {ThemeProvider} from "@material-ui/styles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import DateComponent from "../../JsonForm/DateComponent";
import theme from "../../JsonForm/CustomeTheme2";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import CloseIcon from '@material-ui/icons/Close';
import Table from "../../Table/Table";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import {Dialog} from "@material-ui/core";
import OpeningUploadModal from ".././OpeningUploadModal";
import SingleImageShow from ".././SingleImageShow";

let SearchForm = [
    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 6,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 6,
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 6,
    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "required": true,
        "grid": 6,
    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 6,
    },
    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 6,
    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 6,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 6,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 6,


    },]


let JsonFormCasaIndividualDeferal = {

    "type": {
        "varName": "type",
        "label": "Deferal Type"
    },
    "dueDate": {
        "varName": "dueDate",
        "label": "Expire Date"
    },

}

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginBottom: "3px",
            textDecoration: "none",
            "& small": {
                color: "#d81c26",
                fontSize: "65%",
                fontWeight: "400",
                lineHeight: "1"
            }
        },
        modal: {
            top: `${10}%`,
            maxWidth: `${80}%`,
            maxHeight: `${100}%`,
            margin: 'auto'

        },
        dialogPaper: {
            overflow: "visible"
        },

    }
};

function Transition(props) {
    return <Grow in={true} timeout="auto" {...props} />;
}

var JsonFormCasaIndividualAccountOpening = [

    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address",
        "grid": 6,
    },
    {
        "varName": "rmCode",
        "type": "text",
        "label": "RM Code",
        "grid": 6,
    },
    {
        "varName": "sbsCode",
        "type": "text",
        "label": "SBS Code",
        "grid": 6,
    },
    {
        "varName": "occupationCode",
        "type": "text",
        "label": "Occupation Code",
        "grid": 6,

    },
    {
        "varName": "ccepCompanyCode",
        "type": "text",
        "label": "CCEP Company Code",
        "grid": 6,

    },{
        "varName": "sowzeeAccount",
        "type": "text",
        "label": "Sowzee Account",
        "grid": 6,

    },
    {
        "varName": "accountType",
        "type": "select",
        "label": "Account Type",
        "enum": [
            "Type  1",
            "Type 2",
        ],
        "grid": 6,
    },

    {
        "varName": "urgency",
        "type": "select",
        "label": "URGENCY",
        "enum": [
            "GENERAL",
            "HIGH",
        ],
        "grid": 6,
    },
    {
        "varName": "title",
        "type": "title",
        "label": "Add Service",
        "grid": 12

    }
]


var LoanCheckbox = [
    {
        "varName": "loan",
        "type": "checkbox",
        "label": "Loan",
        "grid": 6
    },

]


var loanTextField = {
    "varName": "loanText",
    "type": "text",
    "label": "Loan",
    "grid": 6
};
var creditCardBox = [
    {
        "varName": "creditCard",
        "type": "checkbox",
        "label": "Credit Card",
        "grid": 6
    },

]


var creditCardTextField = {
    "varName": "creditCardText",
    "type": "text",
    "label": "Credit Card",
    "grid": 6
};

var letterOfCreditFieldCheckbox = [{
    "varName": "letterOfCredit",
    "type": "checkbox",
    "label": "Letter Of Credit",
    "grid": 6,

}];
var letterOfCreditTextField = {

    "varName": "letterOfCreditText",
    "type": "text",
    "label": "Letter Of Credit",
    "grid": 6

}
var sMSAlertFieldSelect = [
    {
        "varName": "smsAlertRequest",
        "type": "checkbox",
        "label": "SMS Alert Request",
        "grid": 12

    },
];
var fdrRequestFieldCheckbox = [
    {
        "varName": "fdrRequest",
        "type": "checkbox",
        "label": "FDR Request",
        "grid": 12
    }
];
var dpsRequestFieldCheckbox = [
    {
        "varName": "dpsRequest",
        "type": "checkbox",
        "label": "DPS Request",
        "grid": 12
    }
];
var csDeferal = {
    "varName": "cs_data_capture",
    "type": "select",
    "label": "Need Deferal?",
    "enum": [
        "YES",
        "NO"
    ],
    "grid": 6
};
var deferalOther =
    {
        "varName": "deferalOther",
        "type": "text",
        "label": "Please Specify",
        "grid": 6
    };

var deferal = [
    {
    "varName": "",
    "type": "title",
    "label": "",
    "grid": 12
    },{
    "varName": "deferalType",
    "type": "select",
    "label": "Deferal Type",
    "enum": [
        "Applicant Photograph",
        "Nominee Photograph",
        "Passport",
        "Address proof",
        "Transaction profile",
        "other"
    ],
    "grid": 6
}]

var date = {
    "varName": "expireDate",
    "type": "date",
    "label": "Expire Date",
    "grid": 6
};

class OpeningCS extends Component {

    constructor(props) {
        super(props);
        this.state = {

            values: [],
            appId: '',
            csDataCapture: '',
            message: "",
            appData: {},
            getData: false,
            getNewCase: false,
            varValue: [],
            caseId: "",
            title: "",
            notificationMessage: "",
            app_uid: "-1",
            alert: false,
            redirectLogin: false,
            type: [],
            dueDate: '',
            inputData: {
                csDeferal: "NO",
                accountType: "INSTAPACK"
            },
            fileUploadData: {},
            selectedDate: {},
            dropdownSearchData: {},

            loan: "",
            showValue: false,

            SelectedData: false,
            csDeferalPage: "",
            AddDeferal: false,
            getDeferalList: [],
            deferalNeeded: false,
            uploadModal: false,
            selectImage:"",
            imageModalBoolean:false,
            imgeListLinkSHow:false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event, value) => {
        this.state.inputData["csDeferal"] = value;
        this.updateComponent();
        if (value === "YES") {

            let values = [];
            values.push(Math.floor(Math.random() * 100000000000));
            this.setState({values: values, deferalNeeded: true});
        } else {
            this.setState({
                values: [],
                deferalNeeded: false
            })
        }
    }


    addDeferalForm() {



        return this.state.values.map((el, i) =>

            <React.Fragment>

                <Grid item xs="6">
                    {
                        this.dynamicDeferral(el)
                    }
                </Grid>
                <Grid item xs="12">
                    {this.dynamicDeferralOther(el)}
                </Grid>
                <Grid item xs="6">
                    {
                        this.dynamicDate(el)
                    }
                    <button

                        className="btn btn-outline-danger"
                        type='button' value='remove' onClick={this.removeClick.bind(this, el)}
                    >
                        Remove
                    </button>
                </Grid>



                <Grid item xs="12"></Grid>

            </React.Fragment>
        )

    }
    componentDidMount() {
        this.state.inputData["csDeferal"] = "NO"
        let varValue = [];
        if (this.props.appId !== undefined) {

            let url = backEndServerURL + '/variables/' + this.props.appId;

            axios.get(url,
                {withCredentials: true})
                .then((response) => {
                    let deferalListUrl = backEndServerURL + "/case/deferral/" + this.props.appId;
                    axios.get(deferalListUrl, {withCredentials: true})
                        .then((response) => {

                            console.log(response.data);
                            let tableArray = [];
                            response.data.map((deferal) => {
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate, deferal.appliedBy, deferal.applicationDate, deferal.status));

                            });
                            this.setState({
                                getDeferalList: tableArray
                            })

                        })
                        .catch((error) => {
                            console.log(error);
                        })
                    console.log(response.data);
                    let varValue = response.data;
                    this.setState({
                        getData: true,
                        varValue: varValue,
                        appData: response.data,
                        inputData: response.data,
                        showValue: true,
                        appId: this.props.appId
                    });

                })
                .catch((error) => {
                    console.log(error);
                    if (error.response.status === 452) {
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin: true
                        })

                    }
                });
        } else {
            let url = backEndServerURL + "/startCase/cs_data_capture";
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log("dfghjk")
                    console.log(response.data)
                    this.setState({
                        appId: response.data.id,
                        appData: response.data.inputData,
                        getNewCase: true,
                        varValue: this.props.searchValue,
                        showValue: true,


                    })
                })
                .catch((error) => {
                    console.log(error);
            })
        }
    }

    createTableData = (id, type, dueDate, appliedBy, applicationDate, status) => {

        return ([
            type, dueDate, appliedBy, applicationDate, status
        ])

    };
    renderDefferalData = () => {
        if (this.state.getDeferalList.length > 0) {
            return (
                <div>
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Due Date", "Created By", "Created Date", "Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />
                    <br/>
                </div>

            )
        }

    }
    renderAddButtonShow = () => {

            return (
                <button
                    className="btn btn-outline-danger"
                    style={{
                       float: 'right' ,
                        verticalAlign: 'right',
                    }}
                    type='button' value='add more'
                    onClick={this.addClick.bind(this)}
                >Add More Deferal</button>
            )

    }

    dynamicDeferral = (i) => {
        let deferalType = "deferalType" + i;
        let expireDate = "expireDate" + i;
        let defferalOther = "defferalOther" + i;
        let arrayData = [];
        /*arrayData.push({deferalType,expireDate,defferalOther});
        this.setState({
            getAllDefferal:arrayData
        })*/
        let field = JSON.parse(JSON.stringify(deferal));
        field.varName = "deferalType" + i;
        return SelectComponent.select(this.state, this.updateComponent, field);
    };

    dynamicDeferralOther = (i) => {
        if (this.state.inputData["deferalType" + i] === "other") {
            let field = JSON.parse(JSON.stringify(deferalOther));
            field.varName = "deferalOther" + i;
            return TextFieldComponent.text(this.state, this.updateComponent, field);
        }
    };
    dynamicDate = (i) => {
        let field = JSON.parse(JSON.stringify(date));
        field.varName = "expireDate" + i;
        return DateComponent.date(this.state, this.updateComponent, field);
    };

    updateComponent = () => {
        this.forceUpdate();
    };
    returnLoanField = () => {
        if (this.state.inputData.loan === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, loanTextField)

    }
    returnCreditCardField = () => {
        if (this.state.inputData.creditCard === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, creditCardTextField)

    }
    returnLetterOfCreditField = () => {

        if (this.state.inputData.letterOfCredit === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, letterOfCreditTextField)
    };
    returnSearchField = () => {
        if (this.state.showValue) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, SearchForm, this.updateComponent)
            )
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.state.inputData)
        if (this.state.inputData["csDeferal"]==="YES") {
            var defType = [];
            var expDate = [];

            let appId = this.state.appId;
            for (let i = 0; i < this.state.values.length; i++) {
                let value = this.state.values[i];
                let defferalType = this.state.inputData["deferalType" + value];
                if (defferalType === "other") {
                    defferalType = this.state.inputData["deferalOther" + value];
                }
                defType.push(defferalType);
                let expireDate = this.state.inputData["expireDate" + value];
                expDate.push(expireDate);
                console.log(expDate)
            }

            let deferalRaisedUrl = backEndServerURL + "/deferral/create/bulk";
            axios.post(deferalRaisedUrl, {appId: appId, type: defType, dueDate: expDate}, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        var variableSetUrl = backEndServerURL + "/variables/" + this.state.appId;

        let data = this.state.inputData;
        data.cs_deferal = this.state.inputData["csDeferal"];
        data.serviceType = "FdrOpening";
        data.subServiceType = "NewFdrOpening";
        // data.dueDate=this.state.dueDate;
        // data.type=this.state.type;

        if (this.state.inputData.urgency === "HIGH")
            data.urgency = 1;
        else
            data.urgency = 0;

        axios.post(variableSetUrl, data, {withCredentials: true})
            .then((response) => {

                console.log(response.data);

                var url = backEndServerURL + "/case/route/" + this.state.appId;

                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data)
                        console.log("Successfully Routed!");
                        this.close()
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",

                        })

                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();

                            this.setState({
                                redirectLogin: true
                            })

                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });

    }

    addClick() {
        let randomNumber = Math.floor(Math.random() * 100000000000);

        this.setState(prevState => ({
            values: [...prevState.values, randomNumber],

        }))

        this.state.inputData["csDeferal"]= "YES";
    }


    removeClick(i, event) {
        event.preventDefault();
        let pos = this.state.values.indexOf(i);
        if (pos > -1) {
            this.state.values.splice(pos, 1);
            this.updateComponent();
            if(this.state.values.length>0){
                this.state.inputData["csDeferal"]= "YES"
            }
            else{
                this.state.inputData["csDeferal"]= "NO"
            }
        }


    }



    renderServiceTagging() {
        if (!this.state.deferalNeeded && this.state.getData)
            return (
                <React.Fragment>
                    <br/>
                    <br/>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, JsonFormCasaIndividualAccountOpening, this.updateComponent)
                    }
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, LoanCheckbox, this.updateComponent)
                    }
                    {
                        this.returnLoanField()
                    }
                    <Grid item xs={12}></Grid>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, creditCardBox, this.updateComponent)
                    }
                    {
                        this.returnCreditCardField()
                    }
                    <Grid item xs={12}></Grid>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, letterOfCreditFieldCheckbox, this.updateComponent)
                    }
                    {
                        this.returnLetterOfCreditField()
                    }

                    <br/>
                    <br/>
                </React.Fragment>


            );

    }

    close = () => {
        this.props.closeModal();
    }
    uploadModal = () => {
        this.setState({
            uploadModal: true
        })
    }

renderUploadButton=()=>{
       if(!this.state.deferalNeeded){
           return(
               <button className="btn btn-outline-danger"
                  style={{
                       verticalAlign: 'middle',
                   }}
                   onClick={this.uploadModal}
                  >
                  Upload File
               </button>
           )
       }
}
    viewImageModal=(event)=>{
        event.preventDefault();

        this.setState({
            selectImage:event.target.value,
            imageModalBoolean:true
        })


    }
    closeModal=()=>{
        this.setState({
            imageModalBoolean:false
        })
    }
    closeUploadModal=(data)=>{
        this.setState({
            uploadModal: false,
            imgeListLinkSHow:true,
            getImageLink:data
        })
    }

    renderImageLink = () => {
        this.setState({
            imgeListLinkSHow:true,
        })
        if (this.state.imgeListLinkSHow) {
            return(
                this.state.getImageLink.map((data)=>{
                    return(
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })

            )


        }

    }

    render() {

        const {classes} = this.props;

        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4><a><CloseIcon onClick={this.close} style={{marginRight: "35%", color: "#000000"}}/></a>New Customer
                                Account Opening</h4>

                        </CardHeader>
                        <CardBody>
                            <div>
                                <Grid container spacing={3}>
                                    <ThemeProvider theme={theme}>

                                        {this.returnSearchField()}


                                        {
                                            this.renderServiceTagging()
                                        }
                                        <Grid item xs='12'></Grid>

                                        <Grid item xs='12'>
                                            {this.renderDefferalData()}
                                        </Grid>
                                        <Grid item xs='12'>
                                            {
                                                this.renderAddButtonShow()
                                            }
                                        </Grid>
                                        <br/>
                                        {
                                            this.addDeferalForm()
                                        }
                                        <Grid item xs='12'></Grid>
                                    </ThemeProvider>
                                </Grid>
                            </div>
                            <ThemeProvider theme={theme}>
                                <Grid container spacing={1}>
                                    {this.renderImageLink()}
                                </Grid>
                            </ThemeProvider>
                            {this.renderUploadButton()}
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.uploadModal}>
                                <DialogContent>
                                    <OpeningUploadModal appId={this.state.appId} closeModal={this.closeUploadModal}/>
                                </DialogContent>
                            </Dialog>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.imageModalBoolean}>
                                <DialogContent>

                                    <SingleImageShow data={this.state.selectImage}  closeModal={this.closeModal}/>
                                </DialogContent>
                            </Dialog>
                            <center>
                                <button
                                    className="btn btn-outline-danger"
                                    style={{
                                        verticalAlign: 'middle',
                                    }}
                                    onClick={this.handleSubmit} >
                                    Submit
                                </button>
                            </center>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );

    }

}

export default withStyles(styles)(OpeningCS);
