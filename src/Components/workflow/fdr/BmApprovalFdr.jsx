
import React from "react";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Table from "../../Table/Table";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Notification from "../../NotificationMessage/Notification";
import Grid from "@material-ui/core/Grid";
import Functions from "../../../Common/Functions";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";
import CardHeader from "../../Card/CardHeader";
import CloseIcon from '@material-ui/icons/Close';
import CardBody from "../../Card/CardBody";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import CircularProgress from '@material-ui/core/CircularProgress';

import {ThemeProvider} from "@material-ui/styles";
import theme from "../../JsonForm/CustomeTheme";
import withStyles from "@material-ui/core/styles/withStyles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";

let SearchForm = [
    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 6,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 6,
    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 6,
    },
    {
        "varName": "dob",
        "type": "date",
        "label": "Date Of Birth",
        "required": true,
        "grid": 6,
    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 6,
    },
    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 6,
    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 6,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 6,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 6,
    },]

var LoanCheckbox = [
    {
        "varName": "loan",
        "type": "checkbox",
        "label": "Loan",
        "grid": 6
    },

]


var loanTextField = {
    "varName": "loanText",
    "type": "text",
    "label": "Loan",
    "grid": 6,
    "readOnly":true,
};
var creditCardBox = [
    {
        "varName": "creditCard",
        "type": "checkbox",
        "label": "Credit Card",
        "grid": 6
    },

]


var creditCardTextField = {
    "varName": "creditCardText",
    "type": "text",
    "label": "Credit Card",
    "grid": 6,
    "readOnly":true,
};

var letterOfCreditFieldCheckbox = [{
    "varName": "letterOfCredit",
    "type": "checkbox",
    "label": "Letter Of Credit",
    "grid": 6,
    "readOnly":true,

}];
var letterOfCreditTextField = {

    "varName": "letterOfCreditText",
    "type": "text",
    "label": "Letter Of Credit",
    "grid": 6,
    "readOnly":true,

}

var deferalForm=[
    {
        "varName":"type",
        "type":"text",
        "label":"Deferal Type",
        "readOnly":true,
        "grid":6

    },
    {
        "varName":"dueDate",
        "type":"text",
        "label":"Deferal Type",
        "readOnly":true

    }
]

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};


class BmApprovalFdr extends React.Component {
    state = {
        message: "",
        appData: {},
        getData: false,
        varValue: [],
        redirectLogin: false,
        title: "",
        notificationMessage: "",
        alert: false,
        inputData: {},
        selectedDate: {},
        SelectedDropdownSearchData: null,
        dropdownSearchData: {},
        values: [],
        showValue: false,
        customerName: [],
        deferalType: [],
        expireDate: [],
        other: [],
        getDeferalList: [],
        loaderNeeded:null,
        getImageBoolean:false,
        getImageLink:[],
        imageModalBoolean:false,
        imgeListLinkSHow:false,
        selectImage:""
    }

    handleChange = (event) => {

        event.preventDefault();

        this.state.inputData[event.target.name] = event.target.value;


    }

    createTableData = (id, type, dueDate,appliedBy,applicationDate,status) => {

        return([
            type,dueDate,appliedBy,applicationDate,status
        ])

    };
    componentDidMount() {

        this.setState({
            loaderNeeded:false
        })
        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;
            axios.get(url, {withCredentials: true})
                .then((response) => {

                    let deferalListUrl=backEndServerURL + "/case/deferral/"+this.props.appId;
                    axios.get(deferalListUrl,{withCredentials:true})
                        .then((response)=>{

                            console.log(response.data);
                            let tableArray=[];
                            var status="";
                            response.data.map((deferal) => {
                                if(deferal.status==="APPROVAL_WAITING"){
                                    status="Waiting For Approval"
                                }
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate,deferal.appliedBy,deferal.applicationDate,status));
                            });
                            this.setState({
                                getDeferalList:tableArray
                            })
                            this.setState({
                                getImageLink:response.data,
                                getImageBoolean:true
                            })

                        })
                        .catch((error)=>{
                            console.log(error);
                        })
                    console.log(response.data)
                    this.setState({
                        getData: true,
                        showValue: true,
                        varValue: response.data,
                        appData: response.data,
                        loaderNeeded:true
                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }

    }


    updateComponent = () => {
        this.forceUpdate();
    };
    viewImageModal=(event)=>{
        event.preventDefault();
        this.setState({
            selectImage:event.target.value,
            imageModalBoolean:true
        })


    }

    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success" stopNotification={this.stopNotification} title={this.state.title}
                              message={this.state.notificationMessage}/>
            )
        }
    };
    renderImageLink = () => {

        if (this.state.getImageBoolean) {
            return(
                this.state.getImageLink.map((data)=>{
                    return(
                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>
                    )
                })
            )
        }
    }
    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close = () => {
        this.props.closeModal();
    }
    handleSubmit = (event,data) => {
        event.preventDefault();
        var appId=this.props.appId;
        var approval=data;
        let deferalUrl = backEndServerURL + "/deferral/approval";
        axios.post(deferalUrl, {appId,approval}, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
            })
        this.state.inputData.bm_approval = data;

        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;
        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;
                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        this.setState({
                            title: "Successfull!",
                            notificationMessage: "Successfully Routed!",
                            alert: true
                        })
                        this.props.closeModal()
                    })
                    .catch((error) => {
                        console.log(error);
                        if (error.response.status === 452) {
                            Functions.removeCookie();
                            this.setState({
                                redirectLogin: true
                            })
                        }
                    });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    renderForm = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, SearchForm, this.updateComponent)
            )
        }
        return;
    }
    returnLoanField = () => {
        if (this.state.inputData.loan === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, loanTextField)

    }
    returnCreditCardField = () => {
        if (this.state.inputData.creditCard === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, creditCardTextField)

    }
    returnLetterOfCreditField = () => {

        if (this.state.inputData.letterOfCredit === true)
            return TextFieldComponent
                .text(this.state, this.updateComponent, letterOfCreditTextField)
    };
    renderServiceTagging() {
        if (this.state.getData) {
            return (
                <React.Fragment>
                    <br/>
                    <br/>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, LoanCheckbox, this.updateComponent)
                    }
                    {
                        this.returnLoanField()
                    }
                    <Grid item xs={12}></Grid>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, creditCardBox, this.updateComponent)
                    }
                    {
                        this.returnCreditCardField()
                    }
                    <Grid item xs={12}></Grid>
                    {
                        CommonJsonFormComponent.renderJsonForm(this.state, letterOfCreditFieldCheckbox, this.updateComponent)
                    }
                    {
                        this.returnLetterOfCreditField()
                    }

                    <br/>
                    <br/>
                </React.Fragment>


            );
        }
    }
    renderDefferalData = () => {
        if (this.state.getDeferalList.length>0) {
            return(
                <div  >
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Expire Date","Raise By","Raise Date","Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />
                    <br/>
                </div>
            )
        }
    }
    renderSubmitButton = () => {
        if (this.state.getData) {
            return (
                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',
                        }}
                        type='button' value='add more'
                        onClick={(event)=>this.handleSubmit(event,"APPROVE")}
                    >Approve
                    </button> &nbsp;&nbsp;&nbsp;
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',
                        }}
                        type='button' value='add more'
                        onClick={(event)=>this.handleSubmit(event,"REJECT")}
                    >Return
                    </button>
                </div>
            )
        }
    }
    render() {
        const {classes} = this.props;
        {
            Functions.redirectToLogin(this.state)
        }
        if (this.state.loaderNeeded === false) {
            return (
                <center>
                    <CircularProgress />
                </center>
            )
        } else {
            return (
                <div>
                    <Grid item xs='12'></Grid>
                    <Grid container spacing={1}>
                        <ThemeProvider theme={theme}>
                            {this.renderForm()}
                        </ThemeProvider>
                    </Grid>
                    <br/>
                    <Grid item xs='12'></Grid>
                    <ThemeProvider theme={theme}>
                        <Grid container spacing={3}>
                            {this.renderServiceTagging()}
                        </Grid>
                    </ThemeProvider>
                    <br/>
                    <br/>
                    <Grid item xs='12'></Grid>
                    <ThemeProvider theme={theme}>
                        <Grid item={12}>
                            {this.renderDefferalData()}
                        </Grid>
                        <br/>
                        <br/>
                        <div>
                            <center>
                                {this.renderSubmitButton()}
                            </center>
                        </div>
                    </ThemeProvider>
                </div>
            )
        }
    }
}
export default withStyles(styles)(BmApprovalFdr);
