import React from "react";
import {backEndServerURL} from "../../../Common/Constant";
import axios from "axios";
import Table from "../../Table/Table";
import CommonJsonFormComponent from "../../JsonForm/CommonJsonFormComponent";
import SelectComponent from "../../JsonForm/SelectComponent";
import Notification from "../../NotificationMessage/Notification";
import Grid from "@material-ui/core/Grid";
import Functions from "../../../Common/Functions";
import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";
import Card from "../../Card/Card";

import CardHeader from "../../Card/CardHeader";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import CardBody from "../../Card/CardBody";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import SingleImageShow from "../CASA/SingleImageShow";
import {ThemeProvider} from "@material-ui/styles";
import theme from "../../JsonForm/CustomeTheme";
import withStyles from "@material-ui/core/styles/withStyles";
import TextFieldComponent from "../../JsonForm/TextFieldComponent";
import GridList from "@material-ui/core/GridList";



const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${80}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    }

};

let accountOpeningForm = [
/*    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/

    {
        "varName": "customerId",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
        "readOnly":true
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
        "readOnly":true
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "readOnly":true
    },

/*    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
        "readOnly":true
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

/*    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "dob2",
        "type": "text",
        "label": "D.O.B",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
        "readOnly":true
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
        "readOnly":true
    },

    // {
    //     "varName": "issueDate",
    //     "type": "text",
    //     "label": "Issue Date",
    //     "grid": 12,
    // },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
        "readOnly":true
    },

    // {
    //     "varName": "expiryDate",
    //     "type": "text",
    //     "label": "Expiry Date",
    //     "grid": 12,
    // },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
        "readOnly":true
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
        "readOnly":true
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
        "readOnly":true
    },
/*    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/
    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo12",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "country12",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        "readOnly":true
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
        "readOnly":true
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "To Ensure Source Of Fund",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner Identified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Is The Customer Engaged",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    // {
    //     "label": "TP",
    //     "type": "title",
    //     "grid": 12,
    //     "readOnly":true
    // },
    // {
    //     "varName": "acNo",
    //     "type": "text",
    //     "label": "Ac No",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "monthlyProbableIncome",
    //     "type": "text",
    //     "label": "Monthly Probable Income",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "monthlyProbableTournover",
    //     "type": "text",
    //     "label": "Monthly Probable Tournover",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "sourceOfFund",
    //     "type": "text",
    //     "label": "Source Of Fund",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "cashDeposit",
    //     "type": "text",
    //     "label": "Cash Deposit",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "DepositByTransfer",
    //     "type": "text",
    //     "label": "  Deposit By Transfer",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "foreignInwardRemittance",
    //     "type": "text",
    //     "label": "Foreign Inward Remittance",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "depositIncomeFromExport",
    //     "type": "text",
    //     "label": "Deposit Income From Export",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "deposittransferFromBoAc",
    //     "type": "text",
    //     "label": "Deposittransfer From Bo Ac",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "others1",
    //     "type": "text",
    //     "label": "Others",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "totalProbableDeposit",
    //     "type": "text",
    //     "label": "Total Probable Deposit",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "cashWithdrawal",
    //     "type": "text",
    //     "label": "Cash Withdrawal",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "withdrawalThrough",
    //     "type": "text",
    //     "label": "Withdrawal Through Transferinstrument",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "foreignOutwardRemittance",
    //     "type": "text",
    //     "label": "Foreign Outward Remittance",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "paymentAgainstImport",
    //     "type": "text",
    //     "label": "Payment Against Import",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "deposittransferToBoAc",
    //     "type": "text",
    //     "label": "Deposittransfer To Bo Ac",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "others2",
    //     "type": "text",
    //     "label": "Others",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "totalProbableWithdrawal",
    //     "type": "text",
    //     "label": "Total Probable  Withdrawal",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    /*{
        "label": "Others",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "statusAsOnDate",
        "type": "text",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "enum":["Yes","No"],
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
        "readOnly":true
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,

        required: true,
        "readOnly":true
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
        "readOnly":true

    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
        "readOnly":true
    },*/
    // {
    //     "varName":"acNo",
    //     "type":"text",
    //     "label":"Ac No",
    //     "grid":12,
    // },
    // {
    //     "varName":"name1",
    //     "type":"text",
    //     "label":"Name 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge1",
    //     "type":"text",
    //     "label":"Nominee Age 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare1",
    //     "type":"text",
    //     "label":"Nom Share 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList1",
    //     "type":"text",
    //     "label":"Nom Relation List 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"address3",
    //     "type":"text",
    //     "label":"Address 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList1",
    //     "type":"text",
    //     "label":"City List 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList1",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"name2",
    //     "type":"text",
    //     "label":"Name 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge2",
    //     "type":"text",
    //     "label":"Nominee Age 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare2",
    //     "type":"text",
    //     "label":"Nom Share 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList2",
    //     "type":"text",
    //     "label":"Nom Relation List 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"address4",
    //     "type":"text",
    //     "label":"Address 4",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList2",
    //     "type":"text",
    //     "label":"City List 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList2",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"name3",
    //     "type":"text",
    //     "label":"Name 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge3",
    //     "type":"text",
    //     "label":"Nominee Age 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare3",
    //     "type":"text",
    //     "label":"Nom Share 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList3",
    //     "type":"text",
    //     "label":"Nom Relation List 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"address5",
    //     "type":"text",
    //     "label":"Address 5",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList3",
    //     "type":"text",
    //     "label":"City List 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList3",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"acId",
    //     "type":"text",
    //     "label":"Ac Id",
    //     "grid":12,
    // },
    // {
    //     "varName":"relationType",
    //     "type":"text",
    //     "label":"Relation Type",
    //     "grid":12,
    // },
    // {
    //     "varName":"relationCode",
    //     "type":"text",
    //     "label":"Relation Code",
    //     "grid":12,
    // },
    // {
    //     "varName":"designationCode",
    //     "type":"text",
    //     "label":"Designation Code",
    //     "grid":12,
    // },
    // {
    //     "varName":"custId",
    //     "type":"text",
    //     "label":"Cust Id",
    //     "grid":12,
    // }
    /*{
        "label": "KYC",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid":12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid":12,
        "readOnly":true
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        //"label": "Doc Collect To Ensure Source Of Fund",
        "label": "Doc Collect To Ensure SOF",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        // "label": "Has The Beneficial Owner Of The Ac Been Identified",
        "label": "Beneficial Owner Identified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        //"label": "What Does The Customer Do/in What Type Of Business Is The Customer Engaged",
        "label": "Customer's Occupation or Business",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid":12,
        required: true,
        "readOnly":true
    },
    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        //"label": "Withdrawal Through Transferinstrument",
        "label": "Withdrawal (Transfer/instrument)",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },*/

    //// FDR ////
    {
        "label": "FDR Opening Form",
        "type": "title",
        "grid": 12,
    },{
        "varName":"solId",
        "type":"text",
        "label":"Sol Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"customerId",
        "type":"text",
        "label":"Customer Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"schemeCode",
        "type":"text",
        "label":"Scheme Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"acOpenDate",
        "type":"",
        "label":"Ac Open Date",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"solId",
        "type":"text",
        "label":"Ac Pref Int Cr",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"interestCreditAcId",
        "type":"text",
        "label":"Interest Credit Ac Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"withHoldingTaxLevel",
        "type":"text",
        "label":"With Holding Tax Level",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"withholdingTaxTax",
        "type":"text",
        "label":"Withholding Tax",
        "grid":12,
        "readOnly":true
    },
    // {
    //     "varName":"depositinstallAmount",
    //     "type":"text",
    //     "label":"Depositinstall Amount",
    //     "grid":12,
    //     "readOnly":true
    // },
    // {
    //     "varName":"depositPeriod",
    //     "type":"text",
    //     "label":"Deposit Period",
    //     "grid":12,
    //     "readOnly":true
    // },
    // {
    //     "varName":"valueDate",
    //     "type":"date",
    //     "label":"Value Date",
    //     "grid":12,
    // },
    {
        "varName":"repaymentAcId",
        "type":"text",
        "label":"Repayment Ac Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"autoClosureClosure",
        "type":"text",
        "label":"Auto Closure",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"autoRenewalRenewal",
        "type":"text",
        "label":"Auto Renewal",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"nominee'sName",
        "type":"text",
        "label":"Nominee's Name",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"relationship",
        "type":"text",
        "label":"Relationship",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"regNo",
        "type":"text",
        "label":"Reg No",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"address1",
        "type":"text",
        "label":"Address 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"cityCode1",
        "type":"text",
        "label":"City Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"stateCode1",
        "type":"text",
        "label":"State Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"countryCode1",
        "type":"text",
        "label":"Country Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code 1",
        "grid":12,
        "readOnly":true
    },
    // {
    //     "varName":"dateOfBirth",
    //     "type":"date",
    //     "label":"Date Of Birth",
    //     "grid":12,
    // },
    {
        "varName":"guardianName",
        "type":"text",
        "label":"Guardian Name",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"guardianCode",
        "type":"text",
        "label":"Guardian Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"address2",
        "type":"text",
        "label":"Address 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"cityCode2",
        "type":"text",
        "label":"City Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"stateCode2",
        "type":"text",
        "label":"State Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"countryCode2",
        "type":"text",
        "label":"Country Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"sectorCode",
        "type":"text",
        "label":"Sector Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"subSectorCode",
        "type":"text",
        "label":"Sub Sector Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"occupationCode",
        "type":"text",
        "label":"Occupation Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode1",
        "type":"text",
        "label":"Free Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode2",
        "type":"text",
        "label":"Free Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode3",
        "type":"text",
        "label":"Free Code 3",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"tranCreationDuringOv",
        "type":"text",
        "label":"Tran Creation During Ov",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"fundingAc",
        "type":"text",
        "label":"Funding Ac",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"typeOfTransaction",
        "type":"text",
        "label":"Type Of Transaction",
        "grid":12,
        "readOnly":true
    }
];
let tagAccountOpeningForm = [
/*    {
        "label": "AOF 1",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/

    {
        "varName": "customerId",
        "type": "text",
        "label": "Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Title",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "grid": 12,
        "length": 80,
        required: true,
        "readOnly":true
    },

    {
        "varName": "shortName",
        "type": "text",
        "label": "Short Name",
        "grid": 12,
        "length": 10,
        required: true,
        "readOnly":true
    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "readOnly":true
    },

/*    {
        "label": "AOF 2",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/

    {
        "varName": "introducerCustomerId",
        "type": "text",
        "label": "Introducer Customer ID",
        "grid": 12,
        "length": 9,
        required: true,
        "readOnly":true
    },

    {
        "varName": "introducerName",
        "type": "text",
        "label": "Introducer Name",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "introducerStaff",
        "type": "text",
        "label": "Introducer Staff",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "applicantMinor",
        "type":"select",
        "label": "Applicant Minor",
        "grid": 12,
        "enum":["Yes","No"],
    },
    {
        "varName": "gurdian",
        "type": "text",
        "label": "Gurdian",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "gurdianName",
        "type": "text",
        "label": "Gurdian Name",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "address23",
        "type": "text",
        "label": "Address",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "city1",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "state1",
        "type": "text",
        "label": "State",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "postal",
        "type": "text",
        "label": "Postal",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

    {
        "varName": "country3",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "applicantMinor",
        "conditionalVarValue": "Yes",
        required: true,
        "readOnly":true
    },

/*    {
        "label": "IIF Page 1",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/
    {
        "varName": "gender",
        "type": "text",
        "label": "Gender",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "residentStatus",
        "type": "select",
        "label": "Resident Status",
        "enum":["Resident","Non-Resident"],
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "nationalIdCard",
        "type": "text",
        "label": "National Id Card",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "dob2",
        "type": "text",
        "label": "D.O.B",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "father",
        "type": "text",
        "label": "Father ",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "mother",
        "type": "text",
        "label": "Mother",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "maritialStatus",
        "type": "select",
        "label": "Maritial Status",
        "enum":["Yes","No"],
        "grid": 12,
        required: true,
        "readOnly":true
    },
    {
        "varName": "spouse",
        "type": "text",
        "label": "Spouse",
        "conditional": true,
        "conditionalVarName": "maritialStatus",
        "conditionalVarValue": "Yes",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "pangirNo",
        "type": "text",
        "label": "PAN GIR No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "passportNo",
        "type": "text",
        "label": "Passport No",
        "grid": 12,
        "readOnly":true
    },

    // {
    //     "varName": "issueDate",
    //     "type": "text",
    //     "label": "Issue Date",
    //     "grid": 12,
    // },

    {
        "varName": "passportDetails",
        "type": "text",
        "label": "Passport Details",
        "grid": 12,
        "readOnly":true
    },

    // {
    //     "varName": "expiryDate",
    //     "type": "text",
    //     "label": "Expiry Date",
    //     "grid": 12,
    // },

    {
        "varName": "freeText5",
        "type": "text",
        "label": "Free Text 5",
        "grid": 12,
        "length": 17,
        "readOnly":true
    },

    {
        "varName": "freeText13",
        "type": "text",
        "label": "Free Text 13",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "freeText14",
        "type": "text",
        "label": "Free Text 14",
        "grid": 12,
        required: true,
        "readOnly":true
    },


    {
        "varName": "freeText15",
        "type": "text",
        "label": "Free Text 15",
        "grid": 12,
        required: true,
        "readOnly":true
    },
/*    {
        "label": "IIF Page 2",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },*/
    {
        "varName": "comAddress",
        "type": "text",
        "label": "Communication Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "communicationAddress2",
        "type": "text",
        "label": "Communication Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city2",
        "label": "City",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state2",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode3",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "country4",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo12",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo21",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "permanentAddress1",
        "type": "text",
        "label": "Permanent Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "permanentAddress2",
        "type": "text",
        "label": "Permanent Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city3",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state3",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode4",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "country5",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "phoneNo22",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "email2",
        "type": "text",
        "label": "Email",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "employerAddress1",
        "type": "text",
        "label": "Employer Address 1",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "employerAddress2",
        "type": "text",
        "label": "Employer Address 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "city4",
        "type": "select",
        "enum": ["Dhaka","Chittagong","Barishal","Rajshahi","Khulna"],
        "label": "City",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "state4",
        "type": "text",
        "label": "State",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "postalCode5",
        "type": "text",
        "label": "Postal Code",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "country12",
        "type": "select",
        "enum": ["Bangaladesh","India","Srilanka","USA","Canada"],
        "label": "Country",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "phoneNo13",
        "type": "text",
        "label": "Phone No 1 ",
        "grid": 12,
        "length":13,
        "readOnly":true
    },

    {
        "varName": "phoneNo23",
        "type": "text",
        "label": "Phone No 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "telexNo1",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "email3",
        "type": "text",
        "label": "Email",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "faxNo",
        "type": "text",
        "label": "Fax No",
        "grid": 12,
        "readOnly":true
    },

    {
        "label": "KYC",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid": 12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        "label": "To Ensure Source Of Fund",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Beneficial Owner Identified",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Is The Customer Engaged",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    // {
    //     "label": "TP",
    //     "type": "title",
    //     "grid": 12,
    //     "readOnly":true
    // },
    // {
    //     "varName": "acNo",
    //     "type": "text",
    //     "label": "Ac No",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "monthlyProbableIncome",
    //     "type": "text",
    //     "label": "Monthly Probable Income",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "monthlyProbableTournover",
    //     "type": "text",
    //     "label": "Monthly Probable Tournover",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "sourceOfFund",
    //     "type": "text",
    //     "label": "Source Of Fund",
    //     "grid": 12,
    //     required: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "cashDeposit",
    //     "type": "text",
    //     "label": "Cash Deposit",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "DepositByTransfer",
    //     "type": "text",
    //     "label": "  Deposit By Transfer",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "foreignInwardRemittance",
    //     "type": "text",
    //     "label": "Foreign Inward Remittance",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "depositIncomeFromExport",
    //     "type": "text",
    //     "label": "Deposit Income From Export",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "deposittransferFromBoAc",
    //     "type": "text",
    //     "label": "Deposittransfer From Bo Ac",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "others1",
    //     "type": "text",
    //     "label": "Others",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "totalProbableDeposit",
    //     "type": "text",
    //     "label": "Total Probable Deposit",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "cashWithdrawal",
    //     "type": "text",
    //     "label": "Cash Withdrawal",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "withdrawalThrough",
    //     "type": "text",
    //     "label": "Withdrawal Through Transferinstrument",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "foreignOutwardRemittance",
    //     "type": "text",
    //     "label": "Foreign Outward Remittance",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "paymentAgainstImport",
    //     "type": "text",
    //     "label": "Payment Against Import",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "deposittransferToBoAc",
    //     "type": "text",
    //     "label": "Deposittransfer To Bo Ac",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "others2",
    //     "type": "text",
    //     "label": "Others",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    //
    // {
    //     "varName": "totalProbableWithdrawal",
    //     "type": "text",
    //     "label": "Total Probable  Withdrawal",
    //     "grid": 12,
    //     required: true,
    //     numeric: true,
    //     "readOnly":true
    // },
    /*{
        "label": "Others",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },
    {
        "varName": "status",
        "type": "text",
        "label": "Status",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "statusAsOnDate",
        "type": "text",
        "label": "Status As On Date",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acManager",
        "type": "text",
        "label": "Ac Manager",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "occuoationCode",
        "type": "text",
        "label": "Occuoation Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "constitution",
        "type": "text",
        "label": "Constitution",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "staffFlag",
        "type":"select",
        "enum":["Yes","No"],
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "staffNumber",
        "type": "text",
        "label": "Staff Number",
        "grid": 12,
        "conditional": true,
        "conditionalVarName": "staffFlag",
        "conditionalVarValue": "Yes",
        "readOnly":true
    },

    {
        "varName": "minor",
        "type": "text",
        "label": "Minor",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "trade",
        "type": "text",
        "label": "Trade",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "telexNo2",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "telexNo3",
        "type": "text",
        "label": "Telex No",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "combineStatement",
        "type": "text",
        "label": "Combine Statement",
        "grid": 12,

        required: true,
        "readOnly":true
    },

    {
        "varName": "tds",
        "type": "text",
        "label": "Tds",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "purgedAllowed",
        "type": "text",
        "label": "Purged Allowed",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText2",
        "type": "text",
        "label": "Free Text 2",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText8",
        "type": "text",
        "label": "Free Text 8",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeText9",
        "type": "text",
        "label": "Free Text 9",
        "grid": 12,
        "readOnly":true

    },

    {
        "varName": "freeCode1",
        "type": "text",
        "label": "Free Code 1",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeCode3",
        "type": "text",
        "label": "Free Code 3",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "freeCode71",
        "type": "text",
        "label": "Free Code 7",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "currencyCode",
        "type": "text",
        "label": "Currency Code",
        "grid": 12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "withHoldingTax",
        "type": "text",
        "label": "With Holding Tax % ",
        "grid": 12,
        required: true,
        "readOnly":true
    },*/
    // {
    //     "varName":"acNo",
    //     "type":"text",
    //     "label":"Ac No",
    //     "grid":12,
    // },
    // {
    //     "varName":"name1",
    //     "type":"text",
    //     "label":"Name 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge1",
    //     "type":"text",
    //     "label":"Nominee Age 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare1",
    //     "type":"text",
    //     "label":"Nom Share 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList1",
    //     "type":"text",
    //     "label":"Nom Relation List 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"address3",
    //     "type":"text",
    //     "label":"Address 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList1",
    //     "type":"text",
    //     "label":"City List 1",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList1",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"name2",
    //     "type":"text",
    //     "label":"Name 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge2",
    //     "type":"text",
    //     "label":"Nominee Age 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare2",
    //     "type":"text",
    //     "label":"Nom Share 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList2",
    //     "type":"text",
    //     "label":"Nom Relation List 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"address4",
    //     "type":"text",
    //     "label":"Address 4",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList2",
    //     "type":"text",
    //     "label":"City List 2",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList2",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"name3",
    //     "type":"text",
    //     "label":"Name 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomineeAge3",
    //     "type":"text",
    //     "label":"Nominee Age 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomShareShare3",
    //     "type":"text",
    //     "label":"Nom Share 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"nomRelationList3",
    //     "type":"text",
    //     "label":"Nom Relation List 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"address5",
    //     "type":"text",
    //     "label":"Address 5",
    //     "grid":12,
    // },
    // {
    //     "varName":"cityList3",
    //     "type":"text",
    //     "label":"City List 3",
    //     "grid":12,
    // },
    // {
    //     "varName":"stateList3",
    //     "type":"text",
    //     "label":"State List",
    //     "grid":12,
    // },
    // {
    //     "varName":"acId",
    //     "type":"text",
    //     "label":"Ac Id",
    //     "grid":12,
    // },
    // {
    //     "varName":"relationType",
    //     "type":"text",
    //     "label":"Relation Type",
    //     "grid":12,
    // },
    // {
    //     "varName":"relationCode",
    //     "type":"text",
    //     "label":"Relation Code",
    //     "grid":12,
    // },
    // {
    //     "varName":"designationCode",
    //     "type":"text",
    //     "label":"Designation Code",
    //     "grid":12,
    // },
    // {
    //     "varName":"custId",
    //     "type":"text",
    //     "label":"Cust Id",
    //     "grid":12,
    // }
    /*{
        "label": "KYC",
        "type": "title",
        "grid": 12,
        "readOnly":true
    },

    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid":12,
        "length":13,
        required: true,
        "readOnly":true
    },

    {
        "varName": "acTitle",
        "type": "text",
        "label": "Ac Title",
        "grid":12,
        "readOnly":true
    },

    {
        "varName": "customerOccupation",
        "type": "select",
        "enum":["Teacher","Doctor","House-Wife","Privet Job Holder"],
        "label": "Customer Occupation",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "docCollectToEnsure",
        "type":"text",
        //"label": "Doc Collect To Ensure Source Of Fund",
        "label": "Doc Collect To Ensure SOF",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "collectedDocHaveBeenVerified",
        "type": "select",
        "enum":["Yes","No"],
        "label": "Collected Doc Have Been Verified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "howTheAddress",
        "type": "select",
        "enum":["Yes","No"],
        "label": "How The Address Is Verified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "hasTheBeneficialOwner",
        "type": "select",
        "enum":["Yes","No"],
        // "label": "Has The Beneficial Owner Of The Ac Been Identified",
        "label": "Beneficial Owner Identified",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "whatDoesTheCustomer",
        "type": "select",
        "enum":["Yes","No"],
        //"label": "What Does The Customer Do/in What Type Of Business Is The Customer Engaged",
        "label": "Customer's Occupation or Business",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "customersMonthlyIncome",
        "type": "select",
        "enum":[20000,50000,100000],
        "label": "Customers Monthly Income",
        "grid":12,
        required: true,
        "readOnly":true
    },
    {
        "label": "TP",
        "type": "title",
        "grid": 12,
    },
    {
        "varName": "acNo",
        "type": "text",
        "label": "Ac No",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "monthlyProbableIncome",
        "type": "text",
        "label": "Monthly Probable Income",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "monthlyProbableTournover",
        "type": "text",
        "label": "Monthly Probable Tournover",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "sourceOfFund",
        "type": "text",
        "label": "Source Of Fund",
        "grid":12,
        required: true,
        "readOnly":true
    },

    {
        "varName": "cashDeposit",
        "type": "text",
        "label": "Cash Deposit",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "DepositByTransfer",
        "type": "text",
        "label": "  Deposit By Transfer",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "foreignInwardRemittance",
        "type": "text",
        "label": "Foreign Inward Remittance",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "depositIncomeFromExport",
        "type": "text",
        "label": "Deposit Income From Export",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "deposittransferFromBoAc",
        "type": "text",
        "label": "Deposittransfer From Bo Ac",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "others1",
        "type": "text",
        "label": "Others",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "totalProbableDeposit",
        "type": "text",
        "label": "Total Probable Deposit",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "cashWithdrawal",
        "type": "text",
        "label": "Cash Withdrawal",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "withdrawalThrough",
        "type": "text",
        //"label": "Withdrawal Through Transferinstrument",
        "label": "Withdrawal (Transfer/instrument)",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "foreignOutwardRemittance",
        "type": "text",
        "label": "Foreign Outward Remittance",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "paymentAgainstImport",
        "type": "text",
        "label": "Payment Against Import",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "deposittransferToBoAc",
        "type": "text",
        "label": "Deposittransfer To Bo Ac",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "others2",
        "type": "text",
        "label": "Others",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },

    {
        "varName": "totalProbableWithdrawal",
        "type": "text",
        "label": "Total Probable  Withdrawal",
        "grid":12,
        required: true,
        numeric: true,
        "readOnly":true
    },*/

    //// FDR ////
    {
        "label": "FDR Opening Form",
        "type": "title",
        "grid": 12,
    },{
        "varName":"solId",
        "type":"text",
        "label":"Sol Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"customerId",
        "type":"text",
        "label":"Customer Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"schemeCode",
        "type":"text",
        "label":"Scheme Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"acOpenDate",
        "type":"",
        "label":"Ac Open Date",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"solId",
        "type":"text",
        "label":"Ac Pref Int Cr",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"interestCreditAcId",
        "type":"text",
        "label":"Interest Credit Ac Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"withHoldingTaxLevel",
        "type":"text",
        "label":"With Holding Tax Level",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"withholdingTaxTax",
        "type":"text",
        "label":"Withholding Tax",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"depositinstallAmount",
        "type":"text",
        "label":"Depositinstall Amount",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"depositPeriod",
        "type":"text",
        "label":"Deposit Period",
        "grid":12,
        "readOnly":true
    },
    // {
    //     "varName":"valueDate",
    //     "type":"date",
    //     "label":"Value Date",
    //     "grid":12,
    // },
    {
        "varName":"repaymentAcId",
        "type":"text",
        "label":"Repayment Ac Id",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"autoClosureClosure",
        "type":"text",
        "label":"Auto Closure",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"autoRenewalRenewal",
        "type":"text",
        "label":"Auto Renewal",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"nominee'sName",
        "type":"text",
        "label":"Nominee's Name",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"relationship",
        "type":"text",
        "label":"Relationship",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"regNo",
        "type":"text",
        "label":"Reg No",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"address1",
        "type":"text",
        "label":"Address 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"cityCode1",
        "type":"text",
        "label":"City Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"stateCode1",
        "type":"text",
        "label":"State Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"countryCode1",
        "type":"text",
        "label":"Country Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"postalCode1",
        "type":"text",
        "label":"Postal Code 1",
        "grid":12,
        "readOnly":true
    },
    // {
    //     "varName":"dateOfBirth",
    //     "type":"date",
    //     "label":"Date Of Birth",
    //     "grid":12,
    // },
    {
        "varName":"guardianName",
        "type":"text",
        "label":"Guardian Name",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"guardianCode",
        "type":"text",
        "label":"Guardian Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"address2",
        "type":"text",
        "label":"Address 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"cityCode2",
        "type":"text",
        "label":"City Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"stateCode2",
        "type":"text",
        "label":"State Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"countryCode2",
        "type":"text",
        "label":"Country Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"postalCode2",
        "type":"text",
        "label":"Postal Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"sectorCode",
        "type":"text",
        "label":"Sector Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"subSectorCode",
        "type":"text",
        "label":"Sub Sector Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"occupationCode",
        "type":"text",
        "label":"Occupation Code",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode1",
        "type":"text",
        "label":"Free Code 1",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode2",
        "type":"text",
        "label":"Free Code 2",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"freeCodeCode3",
        "type":"text",
        "label":"Free Code 3",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"tranCreationDuringOv",
        "type":"text",
        "label":"Tran Creation During Ov",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"fundingAc",
        "type":"text",
        "label":"Funding Ac",
        "grid":12,
        "readOnly":true
    },
    {
        "varName":"typeOfTransaction",
        "type":"text",
        "label":"Type Of Transaction",
        "grid":12,
        "readOnly":true
    }
];


let chekerRemarks = [
    {
        "varName": "chekerRemarks",
        "type": "textArea",
        "label": "Checker Remarks",
        "grid": 12
    }]
;
class OpeningSDC extends React.Component {
    state = {
        message: "",
        appData: {},
        getData: false,
        varValue: [],
        redirectLogin:false,
        title:"",
        notificationMessage:"",
        alert:false,
        getDeferalList: [],
        inputData:{},
        getImageLink:[],
        getImageBoolean:false,
        imageModalBoolean:false,
        getCheckerList: [],
        selectImage:"",
        errorMessages : {},
        errorArray : {},
        getRemarks: []
    }
    componentDidMount() {
        var remarksArray = [];
        if (this.props.appId !== undefined) {
            let url = backEndServerURL + '/variables/' + this.props.appId;
            axios.get(url, {withCredentials:true}).then((response) => {
                    let deferalListUrl=backEndServerURL + "/case/deferral/"+this.props.appId;
                    axios.get(deferalListUrl,{withCredentials:true}).then((response)=>{
                            let imageUrl=backEndServerURL+"/case/files/"+this.props.appId;
                            axios.get(imageUrl,{withCredentials:true}).then((response)=>{
                                    console.log(response.data);
                                    this.setState({
                                        getImageLink:response.data,
                                        getImageBoolean:true
                                    })
                                })
                                .catch((error)=>{
                                    console.log(error);
                                })
                            console.log(response.data);
                            let tableArray=[];
                            response.data.map((deferal) => {
                                tableArray.push(this.createTableData(deferal.id, deferal.type, deferal.dueDate,deferal.appliedBy,deferal.applicationDate,deferal.status));
                            });
                            this.setState({
                                getDeferalList:tableArray
                            })
                            let getCommentsUrl = backEndServerURL + "/appRemarkGet/"+this.props.appId;
                            axios.get(getCommentsUrl, {withCredentials: true})
                                .then((response) => {
                                    console.log(response.data);
                                    response.data.map((data) => {
                                        if(data.remarks !== 'undefined'){
                                            remarksArray.push(this.createRemarksTable(data.remarks, data.createByUserName, data.applicationRemarksDate, data.createByUserRole))
                                        }
                                    })
                                    this.setState({
                                        getRemarks: remarksArray
                                    })
                                })
                                .catch((error) => {
                                    console.log(error)
                                })
                        })
                        .catch((error)=>{
                            console.log(error);
                        })
                    this.setState({
                        getData: true,
                        showValue: true,
                        inputData:response.data,
                        varValue: response.data,
                        appData: response.data
                    });
                })
                .catch((error) => {
                    console.log(error);
                    /* if(error.response.status===452){
                         Functions.removeCookie();

                         this.setState({
                             redirectLogin:true
                         })

                     }*/
                });
        }
    }
    createRemarksTable = (remarks, name, a, b) => {
        return (
            [remarks, name, a, b]
        )
    }
    createTableData = (id, type, dueDate,appliedBy,applicationDate,status) => {
        return([
            type,dueDate,appliedBy,applicationDate,status
        ])
    };
    updateComponent = () => {
        this.forceUpdate();
    };

    handleSubmit = (checker_approved) => {
        this.state.inputData.checker_approval = checker_approved;
        var variableSetUrl = backEndServerURL + "/variables/" + this.props.appId;

        var commentsUrl = backEndServerURL + "/appRemarkSave/" + this.state.inputData.chekerRemarks + "/" + this.props.appId;
        axios.post(commentsUrl, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
            })
        this.state.inputData.chekerRemarks=undefined;

        axios.post(variableSetUrl, this.state.inputData, {withCredentials: true})
            .then((response) => {
                var url = backEndServerURL + "/case/route/" + this.props.appId;
                axios.get(url, {withCredentials: true})
                    .then((response) => {
                        console.log(response.data);
                        this.setState({
                            title:"Successfull!",
                            notificationMessage:"Successfully Routed!",
                            alert:true
                        })
                        this.props.closeModal();
                        window.location.reload();
                    })
                    .catch((error) => {
                        console.log(error);
                        /* if(error.response.status===452){
                             Functions.removeCookie();
                             this.setState({
                                 redirectLogin:true
                             })
                         }*/
                    });
            })
            .catch((error) => {
                console.log(error)
            });
    }
    renderImageCrop=()=>{
        if(this.state.getData){
            console.log(backEndServerURL+"/signaturePhoto/"+this.props.appId+"/signiture")
            return(
                <img width='100%' src={ backEndServerURL+"/signaturePhoto/"+this.props.appId+"/signature"} alt=""/>
            )
        }
    }
    submitApprove = (event) =>{
        event.preventDefault();
        let checker_approved= 'APPROVED';
        this.handleSubmit(checker_approved);
    }
    submitReturn = (event) =>{
        event.preventDefault();
        let checker_approved = 'NOTAPPROVED';
            this.handleSubmit(checker_approved);
    }
    renderEditForm = (() => {
        if(this.props.subserviceType === 'New FDR Opening'){
            if (this.state.getData) {
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, accountOpeningForm, this.updateComponent)
                )
            }
        } else if(this.props.subserviceType === 'Tag FDR Opening'){
            if (this.state.getData) {
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, tagAccountOpeningForm, this.updateComponent)
                )
            }
        }

        return ;
    })
    rederService = (() => {
        if(this.props.subserviceType === 'Tag FDR Opening'){
            if (this.state.getData) {
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, this.props.serviceForm, this.updateComponent)
                )
            }
        }
        return ;
    })
    renderSericeForm = (() => {
        if(this.props.subserviceType === 'Tag FDR Opening'){
            if (this.state.getData) {
                return (
                    CommonJsonFormComponent.renderJsonForm(this.state, this.props.serviceForm, this.updateComponent)
                )
            }
        }
        return ;
    })
    // bomApproval = () => {
    //     if (this.state.getData) {
    //         return (
    //             SelectComponent.select(this.state, this.updateComponent, bomApproval)
    //         )
    //     }
    //     return;
    // }

    checkerList = () => {
        if (this.state.getData) {
            let checkerJsonForm = {
                "varName": "next_user",
                "type": "select",
                "label": "Send To",
                "enum": []
            };
            this.state.getCheckerList.map((checker) => {

                checkerJsonForm.enum.push(checker)
            })
            return (
                SelectComponent.select(this.state, this.updateComponent, checkerJsonForm)
            )

        }

    }

    handleChange = (event) => {
        event.target.name = event.target.value;

    };
    renderNotification = () => {
        if (this.state.alert) {
            return (
                <Notification type="success"  stopNotification={this.stopNotification} title={this.state.title} message={this.state.notificationMessage}/>
            )
        }
    };
    stopNotification = () => {
        this.setState({
            alert: false
        })
    }
    close=()=>{
        this.props.closeModal();
    }
    renderSubmitButton = () => {
        if (this.state.getData) {
            return (
                <div>
                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',
                        }}
                        type='button' value='add more'
                        onClick={this.submitApprove}
                    > Approve
                    </button>

                    &nbsp;
                    &nbsp;
                    &nbsp;

                    <button
                        className="btn btn-outline-danger"
                        style={{
                            verticalAlign: 'right',
                        }}
                        type='button' value='add more'
                        onClick={this.submitReturn}
                    > Return
                    </button>
                </div>

            )
        }
    }
    updateComponent = () => {
        this.forceUpdate();
    };
    renderDefferalData = () => {


        if (this.state.getDeferalList.length>0) {
            return(
                <div  >
                    <Table
                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Deferal Type", "Due Date","Created By","Application Date","Status"]}
                        tableData={this.state.getDeferalList}
                        tableAllign={['left', 'left']}
                    />
                    <br/>
                </div>

            )
        }

    }

    viewImageModal=(event)=>{
        event.preventDefault();
        this.setState({
            selectImage:event.target.value,
            imageModalBoolean:true
        })
    }
    closeModal=()=>{
        this.setState({
            imageModalBoolean:false
        })
    }
    renderImageLink = () => {

        if (this.state.getImageBoolean) {

            return(
                this.state.getImageLink.map((data)=>{
                    return(

                        <Grid item={6}>
                            <button type="submit" value={data} onClick={this.viewImageModal}>{data}</button>
                        </Grid>

                    )
                })


            )
        }

    }
    renderRemarks = () => {
        if (this.state.getData) {
            return (
                CommonJsonFormComponent.renderJsonForm(this.state, chekerRemarks, this.updateComponent)
            )
        }
        return;
    }
    renderRemarksData = () => {
        if (this.state.getRemarks.length > 0) {
            console.log('in');
            console.log(this.state.getRemarks);

            return (
                <div style={{"border-style": "groove", "border-width": "1px"}}>
                    <Table

                        tableHovor="yes"
                        tableHeaderColor="primary"
                        tableHead={["Remarks", "Raised By", "Date", "Role"]}
                        tableData={this.state.getRemarks}
                        tableAllign={['left', 'left','left', 'left']}
                    />
                    <br/>
                </div>
            )
        }
    }

    render() {
        const {classes} = this.props;
        {
            Functions.redirectToLogin(this.state)
        }

        return (

            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        {this.renderNotification()}
                        <CardBody>
                            <GridList cellHeight={800} cols={1}>
                            <Dialog
                                fullWidth="true"
                                maxWidth="xl"
                                open={this.state.imageModalBoolean}>
                                <DialogContent>
                                    <SingleImageShow data={this.state.selectImage}  closeModal={this.closeModal}/>
                                </DialogContent>
                            </Dialog>
                            <div>
                                <Grid item xs='12'></Grid>
                                <Grid container spacing={1}>
                                    <ThemeProvider theme={theme}>
                                        {this.rederService()}
                                        <br/>
                                        <br/>
                                        {this.renderEditForm()}
                                        <br/>
                                        <br/>
                                        {this.renderDefferalData()}
                                        <br/>
                                        <br/>
                                        {this.renderRemarksData()}
                                        <br/>
                                        <br/>
                                        {this.renderRemarks()}
                                        <br/>
                                        <br/>
                                    </ThemeProvider>
                                </Grid>
                                <br/>
                                {this.renderImageCrop()}
                                <br/>
                                <br/>
                                <br/>
                                {this.renderSubmitButton()}
                            </div>
                            </GridList>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }

}

export default withStyles(styles)(OpeningSDC);
