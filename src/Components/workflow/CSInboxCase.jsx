import React, {Component} from "react";
import Liability from "./CASA/Liability";
import CSRejectedPage from "./CASA/CSRejectedPage";
import LiabilityExist from "./CASA/LiabilityExist";
import AgentBanking from "./AGENT/AgentBanking";
import DST from "./CSU/DST";
import {
    CSjsonFormIndividualAccountOpeningSearchForwardTo,
    CSjsonFormJointAccountOpeningSearchForwardTo, CSjsonFormNonIndividualAccountOpeningSearchForwardTo
} from "./WorkflowJsonForm2";
import {
    CSJsonFormForCasaIndividual,
    CSJsonFormForCasaJoint,
    CSJsonFormForCasaProprietorship,
    CSJsonFormForCasaCompany,
} from "./WorkflowJsonForm4";
import {
    BMJsonFormForCasaIndividualTagFdr,
    BMJsonFormForCasaIndividualTagFdr2,
    CSJsonFormForCasaIndividualFdr,
    CSJsonFormForCasaIndividualTagFdr,
    CSJsonFormForCasaIndividualTagFdr2,
} from "./WorkflowJsonFormArin";
import CSOpening from "./fdr/CSOpening";
import ExistingCS from "./fdr/ExistingCS";
import OpeningBM from "./fdr/OpeningBM";
import AccountMaintenance from "./AccountMaintenance/AccountMaintenance";
let SearchForm = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid":3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "required": true,
        "grid": 3,


    },];

let SearchFormIndividual = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 3,

    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 3,


    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 3,

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 3,

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 3,


    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 3,

    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 3,

    },
    {
        "varName": "nationality",
        "type": "text",
        "label": "Nationality",
        "enum":[
            "Bangladesh",
            "Japan",
            "Other",
        ],
        "required": true,
        "grid": 3,


    },

];
let SearchFormNonIndividual =[

    {
        "varName": "companyName",
        "type": "text",
        "label": "Company Name",
        "required": false,
        "readOnly": false,
        "grid":3


    },

    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "required": false,

        "readOnly": false,
        "grid":3


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required": false,
        "grid":3

    },

    {
        "varName": "companyEtin",
        "type": "text",
        "label": "Company ETin",
        "required": false,
        "grid":3


    },
    {
        "varName": "tradeLicense",
        "type": "text",
        "label": "Trade License",
        "required": false,
        "readOnly": false,
        "grid":3


    },
    {
        "varName": "certificate",
        "type": "text",
        "label": "Certificate Of Incorporation",
        "required": false,
        "grid":3


    }




];
let SearchFormJoint = [

    {
        "varName": "cbNumber",
        "type": "text",
        "label": "Cb Number",
        "grid":3


    },
    {
        "varName": "accountSource",
        "type": "text",
        "label": "Account Source",
        "grid":3


    },
    {
        "varName": "accountTitle",
        "type": "text",
        "label": "Account TItle",
        "grid":3


    },
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "grid":3,

        "email": true,


    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Mobile Number",
        "required":true,
        "grid":3


    }
];
class CSInboxCase extends Component {
    agent=()=>{
        if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Agent Account Opening" && this.props.taskTitle==='cs_data_capture' ) {
            return (<AgentBanking closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                  subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                                  appId={this.props.appUid}/>)
        }
    };
    dst=()=>{
        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "INDIVIDUAL" || this.props.subServiceType === "Individual A/C")) {
            return (
                <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     commonJsonForm={CSjsonFormIndividualAccountOpeningSearchForwardTo}
                     serviceType={this.props.serviceType}
                     subServiceType={this.props.subServiceType}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && this.props.subServiceType === "Joint Account") {
            return (
                <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                     commonJsonForm={CSjsonFormJointAccountOpeningSearchForwardTo}
                     serviceType={this.props.serviceType}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     subServiceType={this.props.subServiceType}
                     searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Proprietorship A/C" || this.props.subServiceType === "NONINDIVIDUAL")) {
            return (
                <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     commonJsonForm={CSjsonFormNonIndividualAccountOpeningSearchForwardTo}
                     serviceType={this.props.serviceType}
                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     subServiceType={this.props.subServiceType}
                     searchValue={this.props.searchValue}
                />
            )
        } else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType === "Company Account")) {
            return (
                <DST closeModal={this.props.closeModal}
                     getAccountType={this.props.getAccountType}
                     accountType={this.props.accountType}
                     commonJsonForm={CSjsonFormNonIndividualAccountOpeningSearchForwardTo}
                     serviceType={this.props.serviceType}
                     jointAccountCustomerNumber={this.props.jointAccountCustomerNumber}
                     individualDedupData={this.props.individualDedupData}
                     jointDedupData={this.props.jointDedupData}
                     companyDedupData={this.props.companyDedupData}
                     subServiceType={this.props.subServiceType}
                     searchValue={this.props.searchValue}
                />
            )
        } else {
            alert("Please select Tab")
        }
    };

    casa=()=>{

        if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C") && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaIndividual}
                               appId={this.props.appUid}/>)
        }

        if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Joint Account" && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaJoint}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === "Account Opening" && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL") && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaProprietorship}
                               appId={this.props.appUid}/>)
        }

        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Company Account" && this.props.taskTitle==='cs_data_capture' ) {

            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={CSJsonFormForCasaCompany}
                               appId={this.props.appUid}/>)
        }

        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Others" && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }


        else if (this.props.serviceType === "Account Opening"  && this.props.taskTitle==='cs_case_rejected' ) {

            return (<CSRejectedPage closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                                    appId={this.props.appUid}/>)
        }
        else{
            alert("Internal Problem")
        }
    };
    renderExistAccountOpeningcase=()=>{

        if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="INDIVIDUAL" || this.props.subServiceType==="Individual A/C") && this.props.taskTitle==='cs_data_capture' ) {
            return (<LiabilityExist closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormIndividual}
                               appId={this.props.appUid}/>)
        }
        else  if (this.props.serviceType === 'ExistAccountOpening' &&  this.props.subServiceType==="Joint Account"  && this.props.taskTitle==='cs_data_capture' ) {
            return (<LiabilityExist closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormJoint}
                                    appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && (this.props.subServiceType==="Proprietorship A/C" || this.props.subServiceType==="NONINDIVIDUAL") && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Partnership A/C" && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Limited Company A/C" && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening' && this.props.subServiceType==="Others" && this.props.taskTitle==='cs_data_capture' ) {
            return (<Liability closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                               subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'ExistAccountOpening'  && this.props.taskTitle==='cs_case_rejected' ) {
            return (<CSRejectedPage closeModal={this.props.closeModal} serviceType={this.props.serviceType} department="CASA"  subServiceType={this.props.subServiceType}
                                    subserviceType={this.props.subserviceType} titleName={this.props.subserviceType}  commonJsonForm={SearchFormNonIndividual}
                                    appId={this.props.appUid}/>)
        }
        else{
            alert("External Problem")
        }


    };
    fdr=()=>{
        if (this.props.serviceType === 'FDR Opening' && this.props.subServiceType === 'New FDR Opening') {

            return (<CSOpening closeModal={this.props.closeModal}
                               serviceType={this.props.serviceType}
                               subserviceType={this.props.subServiceType}
                               commonJsonForm={CSJsonFormForCasaIndividualFdr}
                               titleName={this.props.serviceType}
                               appId={this.props.appUid}/>)
        }
        else if (this.props.serviceType === 'FDR Opening' && this.props.subServiceType === 'Tag FDR Opening') {
            return (<ExistingCS closeModal={this.props.closeModal}
                                serviceType={this.props.serviceType}
                                subserviceType={this.props.subServiceType}
                                commonJsonForm={CSJsonFormForCasaIndividualTagFdr}
                                serviceJsonForm={ CSJsonFormForCasaIndividualTagFdr2}
                                titleName={this.props.serviceType}
                                appId={this.props.appUid}/>)
        }
    }
    maintenance=()=>{

        if (this.props.serviceType === 'Maintenance' && this.props.taskTitle === 'cs_data_capture' && this.props.subServiceType==="AccountMaintenance") {
            return (<AccountMaintenance closeModal={this.props.closeModal} serviceType={this.props.serviceType}
                                        department="CASA"
                                        subServiceType={this.props.subServiceType}titleName="Account Maintenance"
                                        maintenanceType={this.props.maintenanceType}
                                        appId={this.props.appUid}/>)

        }
    }
    inboxCase = () => {


        if (this.props.serviceType === "Account Opening") {

            return (
                this.casa()
            )
        }
        else if (this.props.serviceType === "FDR Opening") {

            return (
                this.fdr()
            )
        }
        else if (this.props.serviceType === "Maintenance") {

            return (
                this.maintenance()
            )
        }

        else if (this.props.serviceType === "Account Opening" && this.props.subServiceType==="Agent Account Opening") {

            return (
                this.agent()
            )
        }

        else if (this.props.serviceType === "ExistAccountOpening") {
            return (
                this.renderExistAccountOpeningcase()
            )
        }

    }

close=()=>{

        this.props.closeModal();
}
    render() {

       

        return (

                 this.inboxCase()



        );

    }

}

export default CSInboxCase;