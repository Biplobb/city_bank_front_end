import React,{Component} from 'react';
import { notification } from 'antd';
class Notification extends Component{



    openNotification = () => {
        const config = {
            duration:8,
            message: this.props.title,
            placement:'bottomLeft ',
            description:this.props.message,
        };
        if(this.props.type==='error'){
            notification.error(config);
        }
        else if(this.props.type==='success'){
            notification.success(config);
        }
        else{
            notification.open(config);
        }


    };
    open=()=>{
        if(this.props.title ){
            this.openNotification();
            this.props.stopNotification();
        }
    }
    render(){

        return(
            <div>

                {
                    this.open()
                }
            </div>

        )
    }

}
export default Notification;
