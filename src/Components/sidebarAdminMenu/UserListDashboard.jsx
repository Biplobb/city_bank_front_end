import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import UserList from "./UserList";
const styles = theme => ({
    root: {
        display: 'flex',

    },

    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
        //content color
        backgroundColor: "#f0f0fe"

    },
    tableContainer: {
        height: 320,


    },

});

class UserListDashboard extends React.Component {
    state = {
        open: true,
    };
    render() {


        return (
            <UserList />

            );
    }
}

UserListDashboard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserListDashboard);
