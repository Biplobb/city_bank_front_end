import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import { backEndServerURL } from '../../Common/Constant';
import cookie from 'react-cookies';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { Redirect } from "react-router-dom";
import axios from 'axios';
import '../../Static/css/login.css';
import Grid from '@material-ui/core/Grid';
import Functions from '../../Common/Functions';


const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        background: '#f6f1ff'
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 400,
    },
});

class RoleAddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            authority: '',
            redirectLogin:false,
        }
    }

    handleSubmit = (event) => {


        event.preventDefault();

        let url = backEndServerURL + '/role/add/' + this.state.authority;



        let authHeader = "Bearer " + cookie.load("accessToken");

        axios.post(url, {}, { withCredentials : true })
            .then((response) => {
                console.log(response.data);
                this.setState({ redirect: true });
            })
            .catch((error) => { console.log(error)
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return (<Redirect to="/dashboard" />);
        }

    };

    handleChange =event=> {
        this.setState({
            authority : event.target.value
        })

    };

    render() {
        const { classes } = this.props;
        {

            Functions.redirectToLogin(this.state)

        }

        return (
            <GridContainer>
                {
                    this.renderRedirect()

                }
                <GridItem xs={6} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4 className={classes.cardTitleWhite}>Role Add User</h4>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.root}>
                                <form onSubmit={this.handleSubmit} className={classes.form}>
                                    <Grid container spacing={24}>
                                        <Grid item xs={12} sm={3}>
                                            <FormControl margin="normal" fullWidth>
                                                <InputLabel htmlFor="authority">UserName</InputLabel>
                                                <Input  onChange={this.handleChange } id="authority" name="authority" autoComplete="authority" autoFocus />
                                            </FormControl>
                                            <FormControl margin="normal" fullWidth>
                                                <InputLabel htmlFor="authority">Role Id</InputLabel>
                                                <Input value={this.props.id} onChange={this.handleChange } id="authority" name="roleId" autoComplete="authority"   />
                                            </FormControl>

                                            <br />
                                            <br />
                                            <br />
                                        </Grid>
                                    </Grid>
                                    <center>
                                        <Button
                                            type="primary" htmlType="submit" className={classes.Buttoncolorchange}
                                            variant="contained"
                                            color="secondary"
                                        >
                                            Create
                                        </Button>
                                    </center>
                                </form>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

        );
    }
}

export default withStyles(styles)(RoleAddUser);