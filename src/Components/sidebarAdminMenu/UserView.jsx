import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Table from "../Table/Table";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

class UserView extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        const { classes } = this.props;
        return (

            <GridContainer>

                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h4 className={classes.cardTitleWhite}>Single User View</h4>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.root}>
                                <Grid container spacing={24}>

                                    <Grid item xs={12} sm={6}>
                                        <Paper className={classes.paper}>
                                            <Table
                                                tableHeaderColor="primary"

                                                tableData={[
                                                    ["Name", "Biplob"],

                                                ]}
                                                tableAllign={['left', 'left']}
                                            />

                                        </Paper>


                                    </Grid>
                                    <Grid item xs={12} sm={6}>

                                        <Paper className={classes.paper}>
                                            <Table
                                                tableHeaderColor="primary"

                                                tableData={[
                                                    ["Role Id", "12"],
                                                ]}
                                                tableAllign={['left', 'left']}
                                            />
                                        </Paper>
                                    </Grid>

                                </Grid>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>

            </GridContainer>

        );
    }
}

export default withStyles(styles)(UserView);
