import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import Card from "../Card/Card.jsx";
import CardHeader from "../Card/CardHeader.jsx";
import CardBody from "../Card/CardBody.jsx";
import "../../Static/css/RelationShipView.css";
import {backEndServerURL} from '../../Common/Constant';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import axios from 'axios';
import '../../Static/css/login.css';
import Grid from '@material-ui/core/Grid';
import Notification from "../NotificationMessage/Notification";
import Functions from '../../Common/Functions';

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        background: '#f6f1ff'
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 400,
    },
});

class AddRole extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authority: '',
            alert: false,
            redirectLogin:false,
        }
    }

    notification = () => {
        if (this.state.alert) {

            return (<Notification stopNotification={this.stopNotification} title="Role Assigned!!!"
                                  message="successfully Assigned!!"/>)
        }

    }

    stopNotification = () => {
        this.setState({
            alert: false
        })
    }


    handleSubmit = (event) => {

        event.preventDefault();

        let url = backEndServerURL + '/role/add/' + this.state.authority;


        axios.post(url, {}, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                this.setState({
                    alert: true
                })

                this.props.closeModal();

            })
            .catch((error) => {
                console.log(error);
                if(error.response.status===452){
                    Functions.removeCookie();

                    this.setState({
                        redirectLogin:true
                    })

                }
            });
    }


    handleChange = event => {
        this.setState({
            authority: event.target.value
        })

    };

    render() {
        const {classes} = this.props;
        {

            Functions.redirectToLogin(this.state)

        }
        return (
            <div>

                {this.notification()}
                <GridContainer>

                    <GridItem xs={6} sm={12} md={12}>
                        <Card>
                            <CardHeader color="rose">
                                <h4 className={classes.cardTitleWhite}>Add Role</h4>
                            </CardHeader>
                            <CardBody>
                                <div className={classes.root}>
                                    <form onSubmit={this.handleSubmit} className={classes.form}>
                                        <Grid container spacing={24}>
                                            <center>
                                                <Grid item xs={12} sm={3}>


                                                    <FormControl margin="normal" fullWidth>
                                                        <InputLabel htmlFor="authority">Role Name</InputLabel>
                                                        <Input onChange={this.handleChange} id="authority"
                                                               name="authority" autoComplete="authority" autoFocus/>
                                                    </FormControl>
                                                    <FormControl margin="normal" fullWidth>
                                                        <InputLabel htmlFor="description">Description</InputLabel>
                                                        <Input onChange={this.handleChange} id="description"
                                                               name="description" autoComplete="description"/>
                                                    </FormControl>


                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                </Grid>
                                            </center>
                                        </Grid>
                                        <center>
                                            <Button
                                                type="primary" htmlType="submit" className={classes.Buttoncolorchange}
                                                variant="contained"
                                                color="secondary"
                                            >
                                                Create
                                            </Button>
                                        </center>
                                    </form>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        );
    }
}

export default withStyles(styles)(AddRole);