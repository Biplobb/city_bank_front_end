
import React,{Component} from 'react';
import {notification } from 'antd';


class Modal extends   Component{
      openNotification = () => {
        notification.open({
            message: 'Notification Title',
            description:
                'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
            onClick: () => {
                console.log('Notification Clicked!');
            },
        });
    };

    render(){
     return(
       <div>
           {this.openNotification()}
       </div>


     );
 }
}
export default Modal;