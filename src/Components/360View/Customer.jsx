import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Customer360View from "./Customer360View";
import DemographicsView from "./DemographicsView";
import {Paper} from "@material-ui/core";
import Box from '@material-ui/core/Box';
import GridItem from "../Grid/GridItem";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardBody from "../Card/CardBody";
import GridContainer from "../Grid/GridContainer";
import CloseIcon from '@material-ui/icons/Close';


function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={2}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        backgroundColor: theme.palette.background.paper,
    },
    tabRoot: {
        minWidth: '50%',
        fontSize: 20,
    },
});

class Customer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            IDENTIFICATION_NO: '',
            value: true,
            tabMenuSelect: 'Customer360View',
            redirectLogin:false,
        }
    }

    renderSearchForm = () => {


        if (this.state.tabMenuSelect === 'Customer360View') {
            return (
                <Customer360View IDENTIFICATION_NO={this.props.IDENTIFICATION_NO}/>
            )
        } else if (this.state.tabMenuSelect === 'DemographicsView') {
            return (
                <DemographicsView IDENTIFICATION_NO={this.props.IDENTIFICATION_NO}/>
            )

        } else {

        }

    };
    handleChange = (name, value) => {
        this.setState({
            tabMenuSelect: value
        })

    };

   /* handleChangeIndex = index => {
        this.setState({value: index});
    };*/
    close=()=>{
        this.props.closeModal()
    }
    render() {

        const {classes} = this.props;

        return (

            <section>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <div color="rose" className={classes.root}>
                                <paper>

                                    <CardHeader color="rose">

                                        <h4>Customer Info<a><CloseIcon onClick={this.close} style={{  position: 'absolute', right: 10, color: "#000000"}}/></a></h4>

                                    </CardHeader>
                                </paper>
                                <CardHeader color="red">
                                    <center>
                                        <h4 color="red" className={classes.cardTitleWhite}>
                                            <Paper square>
                                                <Tabs indicatorColor="secondary"
                                                      textColor="secondary"
                                                      aria-label="icon label tabs example"
                                                      value={this.state.tabMenuSelect}
                                                      onChange={this.handleChange}>
                                                    <Tab key="Customer360View" value="Customer360View"
                                                         label="Customer 360 View" classes={{ root: classes.tabRoot }}/>
                                                    <Tab key="DemographicsView" value="DemographicsView"
                                                         label="Demographics View" classes={{ root: classes.tabRoot }}/>

                                                </Tabs>
                                            </Paper>

                                        </h4>
                                    </center>
                                </CardHeader>
                            </div>
                            <Paper square>
                                <CardBody>

                                    <div>
                                        {this.renderSearchForm()}
                                        <br/>
                                    </div>
                                </CardBody>
                            </Paper>
                        </Card>
                    </GridItem>

                </GridContainer>
            </section>
        )

    }
}

export default withStyles(styles, {withTheme: true})(Customer);