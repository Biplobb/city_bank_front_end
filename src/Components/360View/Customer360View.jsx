import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Card from "../Card/Card.jsx";
import CardBody from "../Card/CardBody.jsx";
import Table from "../Table/Table.jsx";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {backEndServerURL} from "../../Common/Constant";
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from "axios";
import Functions from '../../Common/Functions';

const styles = theme => ({
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "#000",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#000"
        }
    },
    cardTitleWhite: {
        color: "#000",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    modal: {
        top: `${10}%`,
        maxWidth: `${100}%`,
        maxHeight: `${100}%`,
        margin: 'auto'

    },
    root: {
        display: 'flex',
        flexWrap: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },

    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',

    },

});

class Customer360View extends Component {
    constructor(props) {
        super(props)
        this.state = {
            varValue: [],
            varValueP: [],
            varValueQ: [],
            getData: false,
            productData: [],
            customerId: '',
            mainCustId: '',
            tableData: [[" ", " "]],
            IDENTIFICATION_NO: '',
            uniqueId: '',
            CustomerSegment: '',
            varValueG: [],
            varValueS: [],
            searchTableData: null,
            renderCoreDetailsData: false,
            renderValueOfHoldings: false,
            renderGroupDetailsData: false,
            renderGroupProductHoldings: false,
            renderProductPropensityXsellData: false,
            renderServicePropensityXsellData: false,
            groupId:'',

            redirectLogin:false,


        }
    }

    createTableData = (id, customerId, customerName, type) => {

        return ([customerId, customerName, type]);

    }
    getGroupingData = () => {
        let url = backEndServerURL + "/getAllGroupMember/" + this.props.IDENTIFICATION_NO;

        const getAllGroupMember = [];
        axios.get(url, {withCredentials: true})
            .then((response) => {
                console.log(response.data);
                let groupUid='';

                response.data.map((group) => {
                    groupUid=group.groupUid;
                    getAllGroupMember.push(this.createTableData(group.id, group.customerId, group.customerName, group.role));
                });
                this.setState({
                    groupId:groupUid,
                    tableData: getAllGroupMember,
                    renderGroupDetailsData: true
                })

            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    renderGroupDetailsData: true
                })
            });

    }

    getProductPropencityXsellData = () => {

        let url = backEndServerURL + "/servicePropensity/getAll/PRODUCT/" + this.props.IDENTIFICATION_NO;

        let varValueP = [];
        axios.get(url, {withCredentials: true})
            .then((response) => {

                response.data.map(p => {
                    varValueP.push([p]);
                });

                console.log(varValueP)

                this.setState({
                    varValueP: varValueP,
                    renderProductPropensityXsellData: true
                });

            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    renderProductPropensityXsellData: true
                });

            });
    }


    getServicePropensityXsellData = () => {

        let url = backEndServerURL + "/servicePropensity/getAll/SERVICE/" + this.props.IDENTIFICATION_NO;
        let varValueQ = [];
        axios.get(url, {withCredentials: true})
            .then((response) => {

                response.data.map(q => {
                    varValueQ.push([q]);
                });

                this.setState({
                    varValueQ: varValueQ,
                    renderServicePropensityXsellData: true,
                });
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    renderServicePropensityXsellData: true,
                });
            });
    }
    getGroupProductHoldingsData = () => {
        if (this.props.IDENTIFICATION_NO !== undefined) {
            let url = backEndServerURL + "/view/360/groupProductHoldings/" + this.props.IDENTIFICATION_NO;
            console.log(url);
            let varValueG = [];
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response);
                    response.data.map(g => {
                        varValueG.push([g]);
                    });

                    this.setState({
                        varValueG: varValueG,
                        renderGroupProductHoldings: true
                    });
                })
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        varValueG: varValueG,
                        renderGroupProductHoldings: true
                    });
                });
        } else {

        }
    }


    componentDidMount() {

        if (this.props.IDENTIFICATION_NO !== undefined) {


            let url = backEndServerURL + "/view/360/" + this.props.IDENTIFICATION_NO;
            console.log("Customer360  " + url);
            let varValue = [];
            let customerSegment = ""
            axios.get(url, {withCredentials: true})
                .then((response) => {
                    console.log(response.data);

                    if (response.data.coreValue[0] !== null) {
                        console.log(true);
                        console.log(response.data.coreValue[0].HOMEBRANCH);

                        varValue["CUSTOMERNAME"] = response.data.coreValue[0].CUSTOMERNAME;
                        varValue["IDENTIFICATION_NO"] = response.data.coreValue[0].IDENTIFICATION_NO;
                        varValue["CUSTOMERBAND"] = response.data.coreValue[0].CUSTOMERBAND;

                        if (response.data.coreValue[0].CUSTOMERBAND >0 && response.data.coreValue[0].CUSTOMERBAND <4) {
                            this.setState({
                                CustomerSegment: "HNI"
                            })
                        } else if (response.data.coreValue[0].CUSTOMERBAND >3 &&   response.data.coreValue[0].CUSTOMERBAND <=6) {
                            this.setState({
                                CustomerSegment: "Affluent"
                            })
                        } else if (response.data.coreValue[0].CUSTOMERBAND ===7 ) {
                            this.setState({
                                CustomerSegment: "Mass"
                            })
                        } else {
                            this.setState({
                                CustomerSegment: ""
                            })
                        }


                        varValue["HOMEBRANCH"] = response.data.coreValue[0].HOMEBRANCH;


                        varValue["OUTOCCUPATION"] = response.data.coreValue[0].OUTOCCUPATION;
                        varValue["CUSTOMERCONSTITUTION"] = response.data.coreValue[0].CUSTOMERCONSTITUTION;
                        varValue["CUSTSTAFF"] = response.data.coreValue[0].CUSTSTAFF;
                        if (response.data.coreValue[0].CUSTSTAFF === "N") {
                            varValue["CUSTSTAFF"] = "NO"
                        } else if (response.data.coreValue[0].CUSTSTAFF === "Y") {
                            varValue["CUSTSTAFF"] = "YES"
                        } else {
                            varValue["CUSTSTAFF"] = ""
                        }

                        varValue["CAAMB"] = response.data.coreValue[0].CAAMB;
                        varValue["SAAMB"] = response.data.coreValue[0].SAAMB;

                        varValue["DPSBALANCE"] = response.data.coreValue[0].DPSBALANCE;
                        varValue["TWOWHEELEROUTSTANDING"] = response.data.coreValue[0].TWOWHEELEROUTSTANDING;

                        varValue["RELATIONSHIPVALUE"] = response.data.coreValue[0].RELATIONSHIPVALUE;
                        varValue["DEBITCARD"] = response.data.coreValue[0].DEBITCARD;

                        if (response.data.coreValue[0].DEBITCARD === "N") {
                            varValue["DEBITCARD"] = "NO"
                        } else if (response.data.coreValue[0].DEBITCARD === "Y") {
                            varValue["DEBITCARD"] = "YES"
                        } else {
                            varValue["DEBITCARD"] = ""
                        }

                        varValue["DEBITCARDTYPE"] = response.data.coreValue[0].DEBITCARDTYPE;
                        varValue["CAABL"] = response.data.coreValue[0].CAABL;
                        varValue["SAABL"] = response.data.coreValue[0].SAABL;
                        varValue["SNDBALANCE"] = response.data.coreValue[0].SNDBALANCE;
                        varValue["SMELOANOUTSTANDING"] = response.data.coreValue[0].SMELOANOUTSTANDING;
                        varValue["HOMELOANOUTSTANDING"] = response.data.coreValue[0].HOMELOANOUTSTANDING;
                        varValue["AUTOLOANOUTSTANDING"] = response.data.coreValue[0].AUTOLOANOUTSTANDING;
                        varValue["SECUREDLOAN"] = response.data.coreValue[0].SECUREDLOAN;
                        varValue["CREDITCARD"] = response.data.coreValue[0].CREDITCARD;

                        if (response.data.coreValue[0].CREDITCARD === "N") {
                            varValue["CREDITCARD"] = "NO"
                        } else if (response.data.coreValue[0].CREDITCARD === "Y") {
                            varValue["CREDITCARD"] = "YES"
                        } else {
                            varValue["CREDITCARD"] = ""
                        }
                        varValue["CREDITCARDTYPE"] = response.data.coreValue[0].CREDITCARDTYPE;
                        varValue["MONITORINGRM"] = response.data.coreValue[0].MONITORINGRM;
                        varValue["DATEOFBIRTH"] = response.data.coreValue[0].DATEOFBIRTH;
                        let customerSInce=response.data.coreValue[0].CUSTOPNDATE.split('T')
                        varValue["CUSTOPNDATE"] =customerSInce[0];
                        varValue["FDBALANCE"] = response.data.coreValue[0].FDBALANCE;
                    } else {

                    }
                    this.setState({
                        varValue: varValue,
                        getData: true,
                        IDENTIFICATION_NO: varValue["IDENTIFICATION_NO"],
                        renderCoreDetailsData: true,
                        renderValueOfHoldings: true,

                    })

                })
                .catch((error) => {
                    console.log(error);
                    if(error.response.status===452){
                        Functions.removeCookie();

                        this.setState({
                            redirectLogin:true
                        })

                    }else{

                    }
                    this.setState({
                        renderCoreDetailsData: true,
                        renderValueOfHoldings: true,
                    })
                });

            this.getProductPropencityXsellData();
            this.getServicePropensityXsellData();
            this.getGroupingData();

            this.getGroupProductHoldingsData();

        } else {

        }
    }


    renderCoreDetailsData = () => {
        if (this.state.renderCoreDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Customer Name", this.state.varValue["CUSTOMERNAME"]],
                        ["Customer ID", this.state.varValue["IDENTIFICATION_NO"]],
                        ["Customer Segment", this.state.CustomerSegment],
                        ["Customer Band", this.state.varValue["CUSTOMERBAND"]],
                        ["Home Branch", this.state.varValue["HOMEBRANCH"]],
                        ["Line Of Business", this.state.varValue[""]],

                    ]}
                    tableAllign={['left', 'left']}
                />
            )
        } else if (this.state.renderCoreDetailsData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderCoreDetailsData2 = () => {
        var today = new Date();

        let dob = new Date(this.state.varValue["DATEOFBIRTH"]);
        let newDate =new Date(today-dob);
        if (this.state.renderCoreDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["Age In Years", newDate.getFullYear()-1970],
                        ["Occupation", this.state.varValue["OUTOCCUPATION"]],
                        ["Customer Constitution", this.state.varValue["CUSTOMERCONSTITUTION"]],
                        ["Staff", this.state.varValue["CUSTSTAFF"]],
                        ["Customer Since",this.state.varValue["CUSTOPNDATE"]],
                        ["Name of RM", this.state.varValue["MONITORINGRM"]],
                        ["Group ID", this.state.groupId],
                        ["Group ID details", this.state.varValue[""]]
                    ]}
                    tableAllign={['left', 'left']}
                />

            )
        } else if (this.state.renderCoreDetailsData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderValueOfHoldings = () => {

        if (this.state.renderCoreDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["CA AMB", this.state.varValue["CAAMB"]],
                        ["SA AMB", this.state.varValue["SAAMB"]],
                        ["TD Balance", this.state.varValue["FDBALANCE"]],
                        ["DPS Balance", this.state.varValue["DPSBALANCE"]],
                        ["2-Wheeler Outstanding", this.state.varValue["TWOWHEELEROUTSTANDING"]],
                        ["PL Outstanding", this.state.varValue[""]],
                        ["Relationship Value", this.state.varValue["RELATIONSHIPVALUE"]],
                        ["Debit Card", this.state.varValue["DEBITCARD"]],
                        ["Debit Card Type", this.state.varValue["DEBITCARDTYPE"]],

                    ]}
                    tableAllign={['left', 'left']}
                />

            )
        } else if (this.state.renderValueOfHoldings === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderValueOfHoldings2 = () => {

        if (this.state.renderCoreDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["View Data", "Value"
                    ]}
                    tableData={[
                        ["CA Balance", this.state.varValue["CAABL"]],
                        ["SA Balance", this.state.varValue["SAABL"]],
                        ["SND Balance", this.state.varValue["SNDBALANCE"]],
                        ["SME Loan Outstanding", this.state.varValue["SMELOANOUTSTANDING"]],
                        ["Home Loan Outstanding", this.state.varValue["HOMELOANOUTSTANDING"]],
                        ["Auto Loan Outstanding", this.state.varValue["AUTOLOANOUTSTANDING"]],
                        ["Secured Loan", this.state.varValue["SECUREDLOAN"]],
                        ["Credit Card", this.state.varValue["CREDITCARD"]],
                        ["Credit Card Type", this.state.varValue["CREDITCARDTYPE"]],
                        ["Credit Card Limit", this.state.varValue[""]]
                    ]}
                    tableAllign={['left', 'left']}
                />


            )
        } else if (this.state.renderValueOfHoldings === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderGroupDetailsData = () => {

        if (this.state.renderGroupDetailsData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["Customer ID", "Name", "Type"
                    ]}
                    tableData={
                        this.state.tableData
                    }
                    tableAllign={['left', 'left', 'left']}
                />

            )
        } else if (this.state.renderGroupDetailsData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderGroupProductHoldings = () => {

        if (this.state.renderGroupProductHoldings === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["Group Product Holdings"]}
                    tableData={
                        this.state.varValueG
                    }
                    tableAllign={['left']}
                />


            )
        } else if (this.state.renderGroupProductHoldings === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderGroupServiceHoldings = () => {


        return (
            <Table
                tableHeaderColor="primary"
                tableHead={["Group Service Holdings"]}
                tableData={
                    this.state.varValueS
                }
                tableAllign={['left']}
            />
        )
    }
    renderCustomerHistoryData = () => {
        return (
            <Table
                tableHeaderColor="primary"
                tableHead={["View Data", "Value"
                ]}
                tableData={[
                    ["Last Query date", this.state.varValue[""]],
                    ["Details of Query", this.state.varValue[""]],
                    ["Attended by", this.state.varValue[""]],
                    ["Customer Feedback", this.state.varValue[""]],
                ]}
                tableAllign={['left', 'left']}
            />
        )


    }
    renderProductPropensityXsellData = () => {

        if (this.state.renderProductPropensityXsellData === true) {
            return (
                <Table
                    tableHeaderColor="primary"
                    tableHead={["PRODUCT PROPENSITY XSELL"
                    ]}
                    tableData={
                        this.state.varValueP
                    }
                    tableAllign={['left']}
                />


            )
        } else if (this.state.renderProductPropensityXsellData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }

    }
    renderServicePropensityXsellData = () => {
        if (this.state.renderServicePropensityXsellData === true) {
            return (
                <center>
                    <Table
                        tableHeaderColor="primary"
                        tableHead={["SERVICE PROPENSITY XSELL"
                        ]}
                        tableData={
                            this.state.varValueQ
                        }
                        tableAllign={['left']}
                    />
                </center>


            )
        } else if (this.state.renderServicePropensityXsellData === false) {

            return (
                <CircularProgress style={{marginLeft: '50%'}}/>
            )
        } else {

        }


    }
    renderRemindersData = () => {
        return (

            <Table
                tableHeaderColor="primary"

                tableData={[
                    ["TD Expiry Date:Sept 2016/Personal Loan end Date:Dec 2017"]
                ]}
                tableAllign={['left']}
            />


        )
    }
    renderCustomerSegmentData = () => {
        return (
            <Table
                tableHeaderColor="primary"

                tableData={[
                    ["Details of Query", "Follow up date Customer"],
                    ["Attended by", "feedback"]
                ]}
                tableAllign={['left']}
            />
        )
    }

    render() {
        const {classes} = this.props;
        console.log(typeof this.state.varValue["CUSTOPNDATE"])

        {

            Functions.redirectToLogin(this.state)


        }
        return (
            <section>
                <center>
                    <GridContainer>

                        <GridItem xs={12} sm={12} md={12}>
                            <Card>

                                <CardBody>

                                    <div className={classes.root}>
                                        <Grid container spacing={1}>
                                            <Grid container item xs={12} spacing={5}>


                                                <Grid item xs={12}>
                                                    <h4>CORE DETAILS</h4>
                                                </Grid>
                                                <Grid item xs={6}>


                                                    <Paper className={classes.paper}>

                                                        {this.renderCoreDetailsData()}

                                                    </Paper>

                                                </Grid>
                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderCoreDetailsData2()}
                                                    </Paper>


                                                </Grid>
                                                <Grid item xs={12}>
                                                    <h4>VALUE OF HOLDINGS</h4>
                                                </Grid>
                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderValueOfHoldings()}

                                                    </Paper>


                                                </Grid>
                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>
                                                        {this.renderValueOfHoldings2()}

                                                    </Paper>

                                                </Grid>

                                                <Grid item xs={12}>
                                                    <h4>Group Details</h4>
                                                </Grid>
                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderGroupDetailsData()}

                                                    </Paper>

                                                </Grid>


                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>
                                                        {this.renderGroupProductHoldings()}

                                                    </Paper>

                                                </Grid>
                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>
                                                        {this.renderGroupServiceHoldings()}


                                                    </Paper>

                                                </Grid>

                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>
                                                        <center><h4>Customer History</h4></center>
                                                        {this.renderCustomerHistoryData()}

                                                    </Paper>

                                                </Grid>


                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>

                                                        {this.renderProductPropensityXsellData()}


                                                    </Paper>

                                                </Grid>
                                                <Grid item xs={6}>

                                                    <Paper className={classes.paper}>
                                                        {this.renderServicePropensityXsellData()}

                                                    </Paper>

                                                </Grid>


                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>
                                                        <center><h4>REMINDERS</h4></center>
                                                        {this.renderRemindersData()}


                                                    </Paper>


                                                </Grid>


                                                <Grid item xs={12}>

                                                    <Paper className={classes.paper}>

                                                        <center><h4>CUSTOMER REVIEW DETAILS</h4></center>

                                                        {this.renderCustomerSegmentData()}


                                                    </Paper>


                                                </Grid>


                                            </Grid>

                                        </Grid>

                                    </div>


                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </center>
            </section>
        )
    }


}

export default withStyles(styles)(Customer360View);