import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import DashBoardHeader from "../DashBoardHeader";



const styles = theme => ({
    root: {
        display: 'flex',

    },

    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
        //content color
        backgroundColor: "#f0f0fe"

    },
    tableContainer: {
        height: 320,


    },
    h5: {
        marginBottom: theme.spacing.unit * 2,

    },
});

class ProtectedLayout extends React.Component {
    state = {
        open: true,
    };


    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <DashBoardHeader/>

                <main className={classes.content}>
                    <div className={classes.appBarSpacer}/>

                    <Typography component="div" className={classes.chartContainer}>

                    </Typography>
                    <br/>
                    <div className={classes.tableContainer}>
                        {this.props.children}

                    </div>
                </main>

            </div>

        );
    }
}

ProtectedLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProtectedLayout);
