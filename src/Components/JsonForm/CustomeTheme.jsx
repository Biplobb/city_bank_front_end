import {createMuiTheme} from "@material-ui/core";

let theme = createMuiTheme({
    overrides: {
       /* MuiInputLabel: {
            root: {
                color: "LIGHTCORAL",
                "&$focused": {
                    color: "LIGHTCORAL",
                }
            }
        },
        MuiInput: {
            underline: {
                '&:before': {
                    borderBottom: '1px solid red',
                },
                '&:hover:not($disabled):before': {
                    borderBottom: '2px solid red',
                },
                '&:focused:after': {
                    borderBottom: '2px solid red',
                },
                '&:after': {
                    borderBottom: '2px solid red',
                }

            },
        },*/
        MuiInputLabel: {
            root: {
                color: "#4d0000",
                "&$focused": {
                    color: "#4d1329",
                }
            }
        },
        MuiInput: {
            underline: {
                '&:before': {
                    borderBottom: '1px solid black',
                },
                '&:hover:not($disabled):before': {
                    borderBottom: '2px solid black',
                },
                '&:focused:after': {
                    borderBottom: '2px solid black',
                },
                '&:after': {
                    borderBottom: '2px solid black',
                }

            },
        },
        MuiFormControl: {
            root: {
                minWidth: 280
            }
        },



    }


});

export default theme;