import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Select from "@material-ui/core/Select";

class DateComponent {

    static returnDefaultValue(state, field){
        if (state.showValue){
            let dateString = state.varValue[field.varName];

            if (dateString === -1)
                return state.selectedDate[field.varName];

            if (dateString === null || dateString === undefined || dateString.trim() === '') {
                state.selectedDate[field.varName] = null;
                state.varValue[field.varName] = -1;
                return null;
            }

            let dateMonthYear = dateString.split(",")[0].trim();
            let seperatedDateMonthYear = dateMonthYear.split("/");
            let returnData = seperatedDateMonthYear[2] + "-" + seperatedDateMonthYear[0] + "-" + seperatedDateMonthYear[1];
            state.selectedDate[field.varName] = returnData;
            state.varValue[field.varName] = -1;

            return  returnData;
            //return state.selectedDate[field.varName];
        }
        else {
            if(state.selectedDate[field.varName] === undefined)
                return null;
            else
                return state.selectedDate[field.varName];
        }


    }

    static date(state,update,field) {


        return(

            <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <KeyboardDatePicker
                    style={{
                        /*  marginTop: field.marginTop,
                          marginBottom: field.marginBottom*/
                    }}
                    value={this.returnDefaultValue(state, field)}
                    //inputVariant="outlined"
                    // helperText={this.state.errorMessages[field.varName]}
                    //error={this.state.errorArray[field.varName]}
                    required={field.required}
                    label={field.label}

                    onChange={(date) => {
                        state.selectedDate[field.varName] = date;
                        state.inputData[field.varName] = (date !== null) ? date.toLocaleString('en-US') : "";
                        update();
                    }}
                    format="dd-MM-yyyy"
                />

            </MuiPickersUtilsProvider>



        )


    }

}

export default DateComponent;