import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";

class ReadonlyFormComponent {
    static handleChange(event,state,update){
        state.inputData[event.target.name]=event.target.value;
        update();
    }
    static returnDefaultValue(field,state){
        if (state.varValue === undefined || state.varValue == null)
            return "";
        return (state.varValue[field.varName]);


    }

    static readOnly(state, update, field) {
        return(

            <TextField
                //helperText={state.errorMessages[varName]}
                //error={state.errorArray[varName]}
                //variant="outlined"
                value={this.returnDefaultValue(field,state)}
                required={field.required} key={field.varName}
                name={field.varName} label={field.label}
                onChange={(event)=>this.handleChange(event,state,update)}
                InputProps={{
                    readOnly: true
                }}
            />

        )


    }
}

export default ReadonlyFormComponent;