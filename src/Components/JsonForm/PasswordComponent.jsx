import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";

class PasswordComponent {

    static handleChange(event,state,update){

        state.inputData[event.target.name]=event.target.value;
        update();
    }
    static password(state,update,field) {


        return(

            <TextField

                //helperText={state.errorMessages[varName]}
                //error={state.errorArray[varName]}
                //variant="outlined"
                type='password'
                required={true} key={field.varName}
                name={field.varName} label={field.label}
                onChange={(event)=>this.handleChange(event,state,update)}
                InputProps={{
                    readOnly: false
                }}
            />

        )


    }

}

export default PasswordComponent;