
import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {FormHelperText} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

class SelectComponent {
    static handleChange(event,state,update){
        state.inputData[event.target.name]=event.target.value;
        update();
    }

    static select(state,update,field) {
      return(


              <FormControl >

                  <InputLabel >{field.label}</InputLabel>
                  <Select
                      helperText={state.errorMessages[field.varName]}
                      error={state.errorArray[field.varName]}

                           value={state.inputData[field.varName]}
                          //error={this.state.errorArray[varName]}
                      // htmlFor="outlined-age-simple"

                          inputProps={{
                              readOnly: field.readOnly
                          }}
                          required={field.required}
                          name={field.varName}
                          onChange={(event)=>this.handleChange(event,state,update)}>



                        {  field.enum.map((option) => {
                              return (<MenuItem key={option}
                                                value={option}>{option}</MenuItem>)
                          })
                      }
                  </Select>

              </FormControl>


      )
}

}

export default SelectComponent;