import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FormControl from "@material-ui/core/FormControl";


class RadioButtonComponent {

    static handleChange(event,state,update){

        state.inputData[event.target.name]=event.target.value;
        update();
    }
    static returnDefaultValue = (state, field) =>{
        if (field.readOnly)
            return state.varValue[field.varName];

        if (state.showValue){
            if (state.varValue[field.varName] === -1)
                return state.inputData[field.varName];
            state.inputData[field.varName] = state.varValue[field.varName];
            state.varValue[field.varName] = -1;
        }
        return state.inputData[field.varName];
    }
    static radio(state,update,field) {


        return(

                <FormControl>
                    <FormLabel>{field.label}</FormLabel>
                    <RadioGroup
                       /* style={{
                            marginTop: marginTop,
                            marginBottom: marginBottom
                        }}*/
                        //defaultValue={this.returnDefaultValue(field)}
                       // helperText={this.state.errorMessages[varName]}
                        //error={this.state.errorArray[varName]}
                        value={this.returnDefaultValue(state, field)}
                        required={field.required}
                        name={field.varName}
                        onChange={(event)=>this.handleChange(event,state,update)}
                    >
                        {
                            field.enum.map((radios) => {
                                return (<FormControlLabel
                                    control={<Radio/>}
                                    value={radios}
                                    label={radios}

                                />)
                            })
                        }


                    </RadioGroup>
                </FormControl>

        )


    }

}

export default RadioButtonComponent;