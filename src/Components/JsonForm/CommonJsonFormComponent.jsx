import React, {Component} from 'react';
import TextFieldComponent from "./TextFieldComponent";
import SelectComponent from "./SelectComponent";
import RadioButtonComponent from "./RadioButtonComponent";
import DateComponent from "./DateComponent";
import DropdownComponent from "./DropdownComponent";
import Grid from "@material-ui/core/Grid";
import PasswordComponent from "./PasswordComponent";
import CheckboxComponent from "./CheckboxComponent";
import FileTypeComponent from "./FileTypeComponent";
import TextAreaComponent from "./TextAreaComponent";
import comment from "../../Static/Comments.png";
import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
 
import TextField from "@material-ui/core/TextField";


let form = [{
    "varName": "passport",
    "type": "text",
    "label": "Passport",
    "grid": 3,


}];

class CommonJsonFormComponent {

    static handleChange = (event,handleChangeComments) => {
        event.preventDefault();
        console.log(event.target.name+"Comments");
        let varName=event.target.name+"Comments";

        handleChangeComments()
    }
    static update = () => {
        this.forceUpdate();
    }

    static renderField(state, field, update) {

        if (field.type === "title") {
            return (
                <Grid item xs={field.grid}>

                    <h6 style={{color: "#FF5733"}}>{field.label}</h6>


                </Grid>
            )
        } else if (field.type === "text") {

            return (
                <Grid item xs={field.grid}>
                    {
                        TextFieldComponent.text(state, update, field)
                     }

                  {/* <img name={field.varName}  src={comment} alt=""/>*/}



                </Grid>
            )

        }
        else if (field.type === "empty") {

            return (
                <Grid item xs={field.grid}>
                </Grid>
            )

        }
        else if (field.type === "textArea") {

            return (
                <Grid item xs={field.grid}>
                    {
                        TextAreaComponent.text(state, update, field)
                    }

                </Grid>
            )

        } else if (field.type === "file") {

            return (
                <Grid item xs={field.grid}>
                    {
                        FileTypeComponent.file(state, update, field)
                    }
                  

                </Grid>
            )

        } else if (field.type === "password") {
            if (field.readOnly === true) {
                return (
                    <Grid item xs={field.grid}>
                        {
                            PasswordComponent.readOnly(state, update, field)}

                    </Grid>
                )
            } else {
                return (
                    <Grid item xs={field.grid}>
                        {
                            PasswordComponent.password(state, update, field)}

                    </Grid>
                )
            }
        } else if (field.type === "dropdown") {
            if (field.readOnly === true) {
                return (
                    <Grid item xs={field.grid}>
                        {
                            DropdownComponent.readOnly(state, update, field)
                        }
                      
                    </Grid>
                )
            } else {
                return (
                    <Grid item xs={field.grid}>
                        {
                            DropdownComponent.dropdown(state, update, field)
                        }
                      
                    </Grid>
                )
            }
        } else if (field.type === "date") {
            if (field.readOnly === true) {
                return (
                    <Grid item xs={field.grid}>
                        {

                            TextFieldComponent.text(state, update, field)}
                    </Grid>
                )
            } else {
                return (
                    <Grid item xs={field.grid}>
                        {
                            DateComponent.date(state, update, field)
                        }

                    </Grid>
                )
            }
        } else if (field.type === "radio") {
            return (
                <Grid item xs={field.grid}>
                    {
                        RadioButtonComponent.radio(state, update, field)
                    }
                    <img src={comment} alt=""/>
                </Grid>
            )
        } else if (field.type === "select") {
            return (
                <Grid item xs={field.grid}>
                    {
                        SelectComponent.select(state, update, field)
                    }
                  
                </Grid>
            )
        } else if (field.type === "checkbox") {
            return (
                <Grid item xs={field.grid}>
                    {
                        CheckboxComponent.checkbox(state, update, field)
                    }

                </Grid>
            )
        }
    }

    static renderJsonForm(state, jsonForm, update) {


        return (
            <React.Fragment>
                {
                    jsonForm.map((field) => {
                        if (field.conditional === true) {
                            if (state.inputData[field.conditionalVarName] === field.conditionalVarValue) {

                                return this.renderField(state, field, update);
                            }
                        } else {
                            return this.renderField(state, field, update);
                        }


                    })
                }
            </React.Fragment>
        )


    }


}

export default CommonJsonFormComponent;