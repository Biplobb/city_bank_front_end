import TextField from "@material-ui/core/TextField";
import Autocomplete from '@material-ui/lab/Autocomplete';
import React from "react";

class AutoCompleteComponent {

    static handleChange(event,state,update, field){
        state.inputData[field.varName] = event.target.value;
        //update();
    }

    static returnDefaultValue(field,state){
       if(state.showValue){
           if (state.varValue === undefined || state.varValue == null)
               return "";

            console.log(state.varValue[field.varName]);
           return state.varValue[field.varName];
       }
    }

    

    static renderSelect(state,update,field) {
        let defaultProps = {
            options: state.autoComplete[field.varName],
            getOptionLabel: option => option.label,
          };

           return(
            <Autocomplete onSelect={(event) => this.handleChange(event, state, update, field)}
            {...defaultProps}
            defaultValue = {this.returnDefaultValue(field, state)}
            renderInput={(params) => <TextField {...params} label={field.label} margin="normal" fullWidth/>}
          />

           )


    }

}

export default AutoCompleteComponent;