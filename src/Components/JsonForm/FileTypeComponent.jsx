import TextField from "@material-ui/core/TextField";
import React from "react";

class FileTypeComponent {

    static handleChange(event,state,update,field){

        console.log(event.target.files[0])
         state.fileUploadData[field.varName]=event.target.files[0];
      console.log(field.varName)
         update();

    }


    static file(state,update,field) {
        return(
           <div>
               <label htmlFor="">{field.label}</label>
               <br/>
               <input
                   type="file"
                   name={field.varName} label={field.label}
                   onChange={(event)=>this.handleChange(event,state,update,field)}
               />
           </div>
        )


    }

}

export default FileTypeComponent;