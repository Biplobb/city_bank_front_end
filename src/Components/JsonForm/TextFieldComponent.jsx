import TextField from "@material-ui/core/TextField";
import React from "react";

class TextFieldComponent {

    static handleChange(event,state,update){
        state.inputData[event.target.name]=event.target.value;
        update();
    }
    static returnDefaultValue(field,state){
       if(state.showValue){
           if (state.varValue === undefined || state.varValue == null)
               return "";
           return state.varValue[field.varName];
       }
    }

    static text(state,update,field) {
           return(
               <TextField
                   helperText={state.errorMessages[field.varName]}
                   error={state.errorArray[field.varName]}
                   //variant="outlined"

                   defaultValue={this.returnDefaultValue(field,state)}             required={field.required} key={field.varName}
                   name={field.varName} label={field.label}
                   onChange={(event)=>this.handleChange(event,state,update)}
                   InputProps={{
                       readOnly: field.readOnly
                   }}
               />

           )


    }

}

export default TextFieldComponent;