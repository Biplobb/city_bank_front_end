import React,{Component} from 'react';
import axios from 'axios';
import CommonJsonFormComponent from './Components/JsonForm/CommonJsonFormComponent';
import AutoCompleteComplete from './Components/JsonForm/AutoCompleteComponent';
import {TextField} from "@material-ui/core";
import MyValidation from "./MyValidation";



let SearchForm = [

    {
        "varName": "nid",
        "type": "text",
        "label": "NID",
        "grid": 6,
        "errorMessage" : "Error",
        "required": true,
        "validation" : "nid"
    },
    {
        "varName": "passport",
        "type": "text",
        "label": "Passport",
        "grid": 6,
        "errorMessage" : "Error"

    },
    {
        "varName": "customerName",
        "type": "text",
        "label": "Customer Name",
        "required": true,
        "grid": 6,
        "errorMessage" : "Error"

    },
    /* {
         "varName": "dob",
         "type": "date",
         "label": "Date Of Birth",
         "required": true,
         "grid": 6,


     },*/
    {
        "varName": "email",
        "type": "text",
        "label": "Email",
        "email": true,
        "grid": 6,
        "errorMessage" : "Error"

    },

    {
        "varName": "phone",
        "type": "text",
        "label": "Phone Number",
        "required": true,
        "grid": 6,
        "errorMessage" : "Error"

    },

    {
        "varName": "tin",
        "type": "text",
        "label": "eTin",
        "grid": 6,
        "errorMessage" : "Error"
    },


    {
        "varName": "registrationNo",
        "type": "text",
        "label": "Birth Certificate/Driving License",
        "grid": 6,
        "conditional" : true,
        "conditionalVarName" : "nationality",
        "conditionalVarValue" : "BD",
        "errorMessage" : "Error"

    },
    {
        "varName": "nationality",
        "type": "select",
        "label": "Nationality",
        "required": true,
        "grid": 6,
        "enum" : [
            "BD",
            "USA"
        ],
        "errorMessage" : "Error"
    },]

class DataCaptureCS extends Component{
    constructor(){
        super();
        this.state = {
            appData : "",
            err : false,
            inputData : {},
            errorArray : {},
            errorMessages : {},
            autoComplete : {
                "reza" : [
                    { title: 'The Shawshank Redemption', year: 1994 },
      { title: 'The Godfather', year: 1972 },
      { title: 'The Godfather: Part II', year: 1974 }
                ]
            },
            showValue : true,
            varValue : {
                "reza" : { title: 'The Godfather', year: 1972 }
            }
        };
    }
    
    updateComponent = () =>{
        this.forceUpdate();
    };

    handleChange = (event) =>{
        this.setState({appData : event.target.value});
    }

    handleSubmit = (event) =>{
        event.preventDefault();

        console.log(this.state.inputData);

        let error = MyValidation.defaultValidation(SearchForm, this.state)

        this.forceUpdate();
        console.log(error)

        // if(error){
        //     this.forceUpdate();
        // }
        // else{
        //     alert("Submited")
        // }

    }

    render() {
    
        return (
            <div>
               {CommonJsonFormComponent.renderJsonForm(this.state, SearchForm, this.updateComponent)}
                {AutoCompleteComplete.renderSelect(this.state, this.updateComponent, {"label" : "reza", "varName": "reza"})}
                <button onClick = {this.handleSubmit} >Submit</button>
            </div>
        )
    }
}


export default DataCaptureCS;