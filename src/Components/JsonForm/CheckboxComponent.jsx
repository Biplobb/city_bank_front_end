import React from "react";
import CheckBox from "@material-ui/core/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";


class CheckboxComponent {
    static handleChange(event, state, update) {

        state.inputData[event.target.name] = event.target.checked;

        update();
    }

    static returnDefaultValue = (state, field) => {
        if (field.readOnly === true) {
            return state.varValue[field.varName];
        }


        // if (state.showValue){
        //     if (state.varValue[field.varName] === -1)
        //         return state.inputData[field.varName];
        //     state.inputData[field.varName] = state.varValue[field.varName];
        //     state.varValue[field.varName] = -1;
        // }
        return state.inputData[field.varName];
    }

    static checkbox(state, update, field) {
        return (
            <FormControlLabel

                control={

                    <CheckBox
                        helperText={state.errorMessages[field.varName]}
                        error={state.errorArray[field.varName]}
                        style={{
                            /*marginTop: field.marginTop,
                            marginBottom: field.marginBottom*/
                        }}
                        //helperText={this.state.errorMessages[field.varName]}
                        //error={this.state.errorArray[field.varName]}
                        checked={this.returnDefaultValue(state, field)}
                        disabled={field.readOnly}
                        required={field.required} key={field.varName}
                        //variant="outlined"
                        defaultValue={this.returnDefaultValue(state, field)}
                        name={field.varName}
                        label={field.label}
                        InputProps={{
                            readOnly: field.readOnly
                        }}
                        onChange={(event) => this.handleChange(event, state, update)}/>

                }

                label={field.label}

            />
        )
    }

}

export default CheckboxComponent;