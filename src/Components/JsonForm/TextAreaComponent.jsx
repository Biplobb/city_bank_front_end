import TextField from "@material-ui/core/TextField";
import React from "react";

class TextAreaComponent {

    static handleChange(event,state,update){
        state.inputData[event.target.name]=event.target.value;
        update();
    }
    static returnDefaultValue(field,state){
        if(state.showValue){
            if (state.varValue === undefined || state.varValue == null)
                return "";
            return state.varValue[field.varName];
        }
    }

    static text(state,update,field) {
        return(
            <TextField
                //helperText={state.errorMessages[varName]}
                //error={state.errorArray[varName]}
                 variant="outlined"
                multiline
                rows="2"
                 style={ {width:'90%'}}
                defaultValue={this.returnDefaultValue(field,state)}   required={field.required} key={field.varName}
                name={field.varName} label={field.label}
                onChange={(event)=>this.handleChange(event,state,update)}
                InputProps={{
                    readOnly: field.readOnly
                }}
            />

        )


    }

}

export default TextAreaComponent;