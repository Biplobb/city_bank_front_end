import cookie from "react-cookies";
import {Redirect} from "react-router-dom";
import React from "react";


class Functions{
    static removeCookie () {
        cookie.remove("spring_session");
        localStorage.removeItem("username");
        localStorage.removeItem("roles");
        localStorage.removeItem("menus");
    }

    static redirectToLogin(state) {
        if (state.redirectLogin === true){
            return (<Redirect to={{
                pathname: '/login',
            }}/>);
        }
    }

    static convertToDate(dateString){
        if (dateString === null || dateString === undefined) {
                return null;
            }

            let dateMonthYear = dateString.split(",")[0].trim();
            let seperatedDateMonthYear = dateMonthYear.split("/");
            let returnData = seperatedDateMonthYear[2] + "-" + seperatedDateMonthYear[1] + "-" + seperatedDateMonthYear[0];
            return  returnData;
    }

};

export default Functions;

