import React, {Component} from 'react';
import ProtectedLayout from "./Components/Layouts/ProtectedLayout";
import { Route, Switch} from "react-router-dom";


//360view
import Customer from "./Components/360View/Customer";
import DemographicsView from "./Components/360View/DemographicsView";
import Grouping from './Components/360View/Grouping';

//accountOpeningProcess
import NewAccountDashboard from "./Components/accountOpeningProcess/NewAccountDashboard";
import DashboardNewAccountSearchResult from "./Components/accountOpeningProcess/DashboardNewAccountSearchResult";

//AdminModule

import User from "./Components/AdminModule/User";


// File
import UploadFileToAutoRead from "./Components/File/UploadFileToAutoRead";

//grouping
import GroupAndMemberAdd from './Components/grouping/GroupAndMemberAdd';
import GroupingApproval from './Components/grouping/GroupingApproval';
import TestAddForm from './Components/grouping/TestAddForm';


//JsonForm


//Layouts


//MasterModule
import Branch from "./Components/MasterModule/Branch";
import BusinessDivision from "./Components/MasterModule/BusinessDivision";
import CustomerCategory from "./Components/MasterModule/CustomerCategory";
import City from "./Components/MasterModule/City";
import CityAddEditDelete from "./Components/MasterModule/CityAddEditDelete";
import CustomerConstitution from "./Components/MasterModule/CustomerConstitution";
import CustomerOffer from "./Components/MasterModule/CustomerOffer";
import Country from "./Components/MasterModule/Country";
import CustomerBand from "./Components/MasterModule/CustomerBand";
import CustomerSegment from "./Components/MasterModule/CustomerSegment";
import CustomerType from "./Components/MasterModule/CustomerType";
import Gender from "./Components/MasterModule/Gender";
import LineOfBusiness from "./Components/MasterModule/LineOfBusiness";
import Notification from "./Components/MasterModule/Notification";
import Occupation from "./Components/MasterModule/Occupation";
import Product from "./Components/MasterModule/Product";
import Salutation from "./Components/MasterModule/Salutation";
import Sector from "./Components/MasterModule/Sector";
import SourceMaster from "./Components/MasterModule/SourceMaster";
import State from "./Components/MasterModule/State";
import SubSector from "./Components/MasterModule/SubSector";


//ProcessFlow
import BranchProcessFlowConfigureAdd from "./Components/ProcessFlow/BranchProcessFlowConfigureAdd";
import SdAndBranchFlowConfigureAllData from "./Components/ProcessFlow/SdAndBranchFlowConfigureAllData";
import SdAndBranchProcessFlow from "./Components/ProcessFlow/SdAndBranchProcessFlow";
import SdFlowConfigureAddUpdateDelete from "./Components/ProcessFlow/SdFlowConfigureUpdateDelete";
import SdProcessFlowMasterAdd from "./Components/ProcessFlow/SdProcessFlowMasterAdd";
import SdProcessFlowMasterBranchSearch from "./Components/ProcessFlow/SdProcessFlowMasterBranchSearch";


//sidebarAdminMenu
import AddMenu from "./Components/sidebarAdminMenu/AddMenu";
import MenuListDashboard from "./Components/sidebarAdminMenu/MenuListDashboard";
import RoleListDashboard from "./Components/sidebarAdminMenu/RoleListDashboard";
import UserListDashboard from "./Components/sidebarAdminMenu/UserListDashboard";
import SubMenuAdd from "./Components/AdminModule/SubMenuAdd";


//Table
import RelashionShipView from "./Components/Table/RelashionShipView";
import CustomerList from "./Components/Table/CustomerList";


//workflow
import BMApproval from "./Components/workflow/CASA/BMApproval";
import DataCaptureCS from "./Components/workflow/DataCaptureCS";
import VerifyDocumentBOM from "./Components/workflow/CASA/VerifyDocumentBOM";
import MaintenanceWIthPrintFile from "./Components/workflow/MaintenanceWIthPrintFile";
import MaintenanceWIthFile from "./Components/workflow/MaintenanceWIthFile";
import Inbox from "./Components/workflow/Inbox";
import ClaimCase from "./Components/workflow/ClaimCase";
import DebitCard from "./Components/workflow/DebitCard";
import DebitCardList from "./Components/workflow/DebitCardList";
import DebitCardListDownload from "./Components/workflow/DebitCardListDownload";
import MakerFieldInputForm from "./Components/workflow/CASA/MakerFieldInputForm";
import UploadDeferal from "./Components/workflow/CASA/UploadDeferal";
import CSRejectedPage from "./Components/workflow/CASA/CSRejectedPage";
import ImageCrop from "./Components/workflow/CASA/ImageCrop";
import BMBulkAccountOpening from "./Components/workflow/BMBulkAccountOpening";
import Bondmanagement from "./Components/workflow/BOND/Bondmanagement";
import CBGlobalSearch from "./Components/CBGlobalSearch";
import CustomerDedupSearch from "./Components/CustomerDedupSearch";
import WelcomeLetterGenerate from "./Components/WelcomeLetterGenerate";
import CustomerServedList from "./Components/CustomerServedList";
import ManagerView from "./Components/ManagerView";
import ProcessmakerDashboard from "./Components/ProcessmakerDashboard";
import AgentBanking from "./Components/workflow/AGENT/StaticData";
import AdminReassignCase from "./Components/workflow/CASA/AdminReassignCase";
import TestReza from "./TestReza";

// import Notification from "./Components/NotificationMessage/Notification";
//test
import TestForm from "./Components/JsonForm/TestForm";



class ProtectedRoute extends Component {
    menuShow = (menuList,includeMenu,url,componentName) => {

        if (menuList.includes(includeMenu)) {
            return (
                <Route path={url} component={componentName}/>
            );
        }


    }

    render() {

        let menuList = JSON.parse(localStorage.getItem("menus"));


        if (menuList === null) {


            menuList = [];
        }
        return (
            <ProtectedLayout>

                <Switch>
                    {/*   360view  */}
                    <Route path="/Customer" component={Customer}/>
                    <Route path="/DemographicsView" component={DemographicsView}/>
                    <Route path="/Grouping" component={Grouping}/>


                   {/*  accountOpeningProcess*/}
                    <Route path="/newaccount" component={NewAccountDashboard}/>
                    <Route path="/newaccountsearchResult" component={DashboardNewAccountSearchResult}/>


                    {/*  AdminModule*/}

                    {
                        this.menuShow(menuList,"User","/userListDashboard","UserListDashboard")

                    }
                    <Route path="/user" component={User}/>
                    <Route path="/subMenuAdd" component={SubMenuAdd}/>


                  {/*  File*/}
                    <Route path="/fileRead" component={UploadFileToAutoRead}/>

                   {/*  grouping*/}
                    <Route path="/GroupAndMemberAdd" component={GroupAndMemberAdd}/>
                    <Route path="/GroupingApproval" component={GroupingApproval}/>
                    <Route path="/TestAddForm" component={TestAddForm}/>



                    {/*  JsonForm*/}
                    <Route path="/DebitCard" component={DebitCard}/>
                    <Route path="/DebitCardList" component={DebitCardList}/>
                    <Route path="/DebitCardListDownload" component={DebitCardListDownload}/>
                    <Route path="/MakerFieldInputForm" component={MakerFieldInputForm}/>
                    <Route path="/UploadDeferal" component={UploadDeferal}/>
                    <Route path="/CSRejectedPage" component={CSRejectedPage}/>
                    <Route path="/ImageCrop" component={ImageCrop}/>
                    <Route path="/BMBulkAccountOpening" component={BMBulkAccountOpening}/>

                    <Route path="/Bondmanagement" component={Bondmanagement}/>





                    {/*   Layouts*/}
                    <Route path="/inbox" component={Inbox}/>
                    <Route path="/ClaimCase" component={ClaimCase}/>



                    {/*  MasterModule*/}
                    <Route path="/branch" component={Branch}/>
                    <Route path="/businessDivision" component={BusinessDivision}/>
                    <Route path="/customerCategory" component={CustomerCategory}/>
                    <Route path="/city" component={City}/>
                    <Route path="/CityAddEditDelete" component={CityAddEditDelete}/>
                    <Route path="/customerConstitution" component={CustomerConstitution}/>
                    <Route path="/CustomerOffer" component={CustomerOffer}/>
                    <Route path="/country" component={Country}/>
                    <Route path="/customerBand" component={CustomerBand}/>
                    <Route path="/customerSegment" component={CustomerSegment}/>
                    <Route path="/customerType" component={CustomerType}/>
                    <Route path="/gender" component={Gender}/>
                    <Route path="/lineOfBusiness" component={LineOfBusiness}/>
                    <Route path="/notificationMaster" component={Notification}/>
                    <Route path="/occupation" component={Occupation}/>
                    <Route path="/Product" component={Product}/>
                    <Route path="/salutation" component={Salutation}/>
                    <Route path="/sector" component={Sector}/>
                    <Route path="/sourceMaster" component={SourceMaster}/>
                    <Route path="/state" component={State}/>
                    <Route path="/subSector" component={SubSector}/>

                    {/*  ProcessFlow*/}
                    <Route path="/BranchProcessFlowConfigureAdd" component={BranchProcessFlowConfigureAdd}/>
                    <Route path="/SdAndBranchFlowConfigureAllData" component={SdAndBranchFlowConfigureAllData}/>
                    <Route path="/SdAndBranchProcessFlow" component={SdAndBranchProcessFlow}/>
                    <Route path="/SdFlowConfigureAddUpdateDelete" component={SdFlowConfigureAddUpdateDelete}/>
                    <Route path="/SdProcessFlowMasterAdd" component={SdProcessFlowMasterAdd}/>
                    <Route path="/SdProcessFlowMasterBranchSearch" component={SdProcessFlowMasterBranchSearch}/>


                    {/*  sidebarAdminMenu*/}
                    <Route path="/addMenu" component={AddMenu}/>
                    <Route path='/menuListDashboard' component={MenuListDashboard}/>
                    <Route path='/roleListDashboard' component={RoleListDashboard}/>
                    <Route path="/customerForm" component={UserListDashboard}/>
                    <Route path="/userListDashboard" component={UserListDashboard}/>

                    {/*  Table*/}
                    <Route path="/relationship" component={RelashionShipView}/>
                    <Route path='/customerList' component={CustomerList}/>


                    {/*   workflow*/}
                    <Route path="/bmApproval" component={BMApproval}/>
                    <Route path="/dataCaptureCS" component={DataCaptureCS}/>
                    <Route path="/verifyDocumentBOM" component={VerifyDocumentBOM}/>
                    <Route path="/MaintenanceWIthPrintFile" component={MaintenanceWIthPrintFile}/>
                    <Route path="/MaintenanceWIthFiles" component={MaintenanceWIthFile}/>
                    <Route path="/AgentBanking" component={AgentBanking}/>
                    <Route path="/AdminReassignCase" component={AdminReassignCase}/>


                    <Route path="/cbGlobalSearch" component={CBGlobalSearch}/>
                    <Route path="/CustomerDedupSearch" component={CustomerDedupSearch}/>
                    <Route path="/WelcomeLetterGenerate" component={WelcomeLetterGenerate}/>
                    <Route path='/customerServed' component={CustomerServedList}/>
                    <Route path="/statistics" component={CustomerServedList}/>
                    <Route path='/managerView' component={ManagerView}/>
                    <Route path="/ProcessmakerDashboard" component={ProcessmakerDashboard}/>
                    <Route path="/welcomeLetter" component={WelcomeLetterGenerate}/>
                    <Route path="/testReza" component={TestReza}/>
                    <Route path="/TestForm" component={TestForm}/>




                    {/*<Route path="/Notification" component={Notification}/>*/}


                </Switch>


            </ProtectedLayout>

        )
    }
}

export default ProtectedRoute;